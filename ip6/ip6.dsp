# Microsoft Developer Studio Project File - Name="ip6" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** 編集しないでください **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ip6 - Win32 Debug
!MESSAGE これは有効なﾒｲｸﾌｧｲﾙではありません。 このﾌﾟﾛｼﾞｪｸﾄをﾋﾞﾙﾄﾞするためには NMAKE を使用してください。
!MESSAGE [ﾒｲｸﾌｧｲﾙのｴｸｽﾎﾟｰﾄ] ｺﾏﾝﾄﾞを使用して実行してください
!MESSAGE 
!MESSAGE NMAKE /f "ip6.mak".
!MESSAGE 
!MESSAGE NMAKE の実行時に構成を指定できます
!MESSAGE ｺﾏﾝﾄﾞ ﾗｲﾝ上でﾏｸﾛの設定を定義します。例:
!MESSAGE 
!MESSAGE NMAKE /f "ip6.mak" CFG="ip6 - Win32 Debug"
!MESSAGE 
!MESSAGE 選択可能なﾋﾞﾙﾄﾞ ﾓｰﾄﾞ:
!MESSAGE 
!MESSAGE "ip6 - Win32 Release" ("Win32 (x86) Application" 用)
!MESSAGE "ip6 - Win32 Debug" ("Win32 (x86) Application" 用)
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ip6 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I ".\src\misc\win32\vc6" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "LSB_FIRST" /D "SOUND" /D "USE_FMLITE" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "NDEBUG"
# ADD RSC /l 0x411 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "ip6 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I ".\src\misc\win32\vc6" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "LSB_FIRST" /D "SOUND" /D "USE_FMLITE" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "_DEBUG"
# ADD RSC /l 0x411 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ip6 - Win32 Release"
# Name "ip6 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "misc"

# PROP Default_Filter ""
# Begin Group "fmlite"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\misc\fmlite\opngenc.cpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\fmlite\opngeng.cpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\fmlite\psggenc.cpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\fmlite\psggeng.cpp
# End Source File
# End Group
# Begin Group "win32"

# PROP Default_Filter ""
# Begin Group "vc6"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\misc\win32\vc6\stdint.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\misc\win32\guard.hpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\win32\vkmap.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\misc\arithmetic.hpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\guard.hpp
# End Source File
# Begin Source File

SOURCE=.\src\misc\ringbuffer.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\buffer.c
# End Source File
# Begin Source File

SOURCE=.\src\d88.c
# End Source File
# Begin Source File

SOURCE=.\src\Debug.c
# End Source File
# Begin Source File

SOURCE=.\src\disk.c
# End Source File
# Begin Source File

SOURCE=.\src\error.c
# End Source File
# Begin Source File

SOURCE=.\src\fdc.c
# End Source File
# Begin Source File

SOURCE=.\src\fm.cpp
# End Source File
# Begin Source File

SOURCE=.\src\fm.hpp
# End Source File
# Begin Source File

SOURCE=.\src\iP6.c
# End Source File
# Begin Source File

SOURCE=.\src\message.c
# End Source File
# Begin Source File

SOURCE=.\src\Option.c
# End Source File
# Begin Source File

SOURCE=.\src\ossSound.c
# End Source File
# Begin Source File

SOURCE=.\src\P6.c
# End Source File
# Begin Source File

SOURCE=.\src\Refresh.c
# End Source File
# Begin Source File

SOURCE=.\src\Unix.c
# End Source File
# Begin Source File

SOURCE=.\src\Win32.c
# End Source File
# Begin Source File

SOURCE=.\src\Win32fscr.c
# End Source File
# Begin Source File

SOURCE=.\src\Win32stick.c
# End Source File
# Begin Source File

SOURCE=.\src\WinKanji.c
# End Source File
# Begin Source File

SOURCE=.\src\WinSound.c
# End Source File
# Begin Source File

SOURCE=.\src\Xconf.c
# End Source File
# Begin Source File

SOURCE=.\src\Z80.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\src\buffer.h
# End Source File
# Begin Source File

SOURCE=.\src\Build.h
# End Source File
# Begin Source File

SOURCE=.\src\cgrom.h
# End Source File
# Begin Source File

SOURCE=.\src\Codes.h
# End Source File
# Begin Source File

SOURCE=.\src\CodesCB.h
# End Source File
# Begin Source File

SOURCE=.\src\CodesED.h
# End Source File
# Begin Source File

SOURCE=.\src\CodesXCB.h
# End Source File
# Begin Source File

SOURCE=.\src\CodesXX.h
# End Source File
# Begin Source File

SOURCE=.\src\cycles.h
# End Source File
# Begin Source File

SOURCE=.\src\d88.h
# End Source File
# Begin Source File

SOURCE=.\src\Debug.h
# End Source File
# Begin Source File

SOURCE=.\src\disk.h
# End Source File
# Begin Source File

SOURCE=.\src\error.h
# End Source File
# Begin Source File

SOURCE=.\src\extkanji.h
# End Source File
# Begin Source File

SOURCE=.\src\fdc.h
# End Source File
# Begin Source File

SOURCE=.\src\fm.h
# End Source File
# Begin Source File

SOURCE=.\src\Help.h
# End Source File
# Begin Source File

SOURCE=.\src\Keydef.h
# End Source File
# Begin Source File

SOURCE=.\src\message.h
# End Source File
# Begin Source File

SOURCE=.\src\Option.h
# End Source File
# Begin Source File

SOURCE=.\src\os.h
# End Source File
# Begin Source File

SOURCE=.\src\P6.h
# End Source File
# Begin Source File

SOURCE=.\src\Refresh.h
# End Source File
# Begin Source File

SOURCE=.\src\Sound.h
# End Source File
# Begin Source File

SOURCE=.\src\Tables.h
# End Source File
# Begin Source File

SOURCE=.\src\Unix.h
# End Source File
# Begin Source File

SOURCE=.\src\Win32.h
# End Source File
# Begin Source File

SOURCE=.\src\Win32fscr.h
# End Source File
# Begin Source File

SOURCE=.\src\Win32stick.h
# End Source File
# Begin Source File

SOURCE=.\src\WinKeydef.h
# End Source File
# Begin Source File

SOURCE=.\src\WinMenu.h
# End Source File
# Begin Source File

SOURCE=.\src\Xconf.h
# End Source File
# Begin Source File

SOURCE=.\src\Xconfdef.h
# End Source File
# Begin Source File

SOURCE=.\src\Z80.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\src\iP6.rc
# End Source File
# End Group
# End Target
# End Project
