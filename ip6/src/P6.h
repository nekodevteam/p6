/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           P6.h                          **/
/**                                                         **/
/** by ISHIOKA Hiroshi 1998,1999                            **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
#define _POSIX_
#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#if defined(_WIN32)
#include <windows.h>
#else	/* defined(_WIN32) */
#include "misc/unix/win32.h"
#endif	/* defined(_WIN32) */

#include "Z80.h"            /* Z80 emulation declarations    */

/* #define UNIX  */         /* Compile iP6 for for Unix/X    */
/* #define MSDOS */         /* Compile iP6 for MSDOS/VGA     */
/* #define MITSHM */        /* Use MIT SHM extensions for X  */

#ifndef UNIX
#undef MITSHM
#endif

#if !defined(PATH_MAX) && defined(_WIN32)
#include <windows.h>
#define PATH_MAX	MAX_PATH
#endif	/* !defined(PATH_MAX) && defined(_WIN32) */

#define NORAM     0xFF      /* Byte to be returned from      */
                            /* non-existing pages and ports  */

#define CAS_NONE		0
#define CAS_SAVEBYTE	1
#define CAS_LOADING		2
#define CAS_LOADBYTE	3

#define MAX_AUTOPERIOD	8

/* -------------------------------- */
/* 99.06.02. */


#define FILE_LOAD_TAPE 0
#define FILE_SAVE_TAPE 1
#define FILE_DISK 2
#define FILE_PRNT 3

#define STICK0_SPACE 0x80
#define STICK0_LEFT  0x20
#define STICK0_RIGHT 0x10
#define STICK0_DOWN  0x08
#define STICK0_UP    0x04
#define STICK0_STOP  0x02
#define STICK0_SHIFT 0x01



void Keyboard(void);
void OpenFile1(unsigned int num);

/****************************************************************/
/*** existROM(): exist ROM file                               ***/
/*** must call after calling StartP6()                        ***/
/****************************************************************/
int existROM(int version);
int existROMsub(int version, LPCTSTR dir);


/****************************************************************/
/*** Allocate memory, load ROM images, initialize mapper, VDP ***/
/*** CPU and start the emulation. This function returns 0 in  ***/
/*** the case of failure.                                     ***/
/****************************************************************/
int StartP6(void);

/****************************************************************/
/*** Free memory allocated by StartP6().                     ***/
/****************************************************************/
void TrashP6(void);

/****************************************************************/
/*** Allocate resources needed by the machine-dependent code. ***/
/************************************** TO BE WRITTEN BY USER ***/
int InitMachine(void);

/****************************************************************/
/*** Deallocate all resources taken by InitMachine().         ***/
/************************************** TO BE WRITTEN BY USER ***/
void TrashMachine(void);

/** InitVariable ********************************************/
/* Initialize  Variable                                      */
/*     before calling InitMachine32                          */
/*************************************************************/
void InitVariable(void);

void UpdateScreen(void);
void enableCmtInt(void);
void SetTitle(fpos_t pos);

void sw_nowait_mode(int mode);
void sw_srline(int mode);
int ResetPC(int reboot);

LPTSTR my_strncpy(LPTSTR dest, LPCTSTR src, int max);

// -----------------------------------------------------------------------
/******** Variables used to control emulator behavior ********/
extern byte Verbose;                  /* Debug msgs ON/OFF   */
extern int P6Version;            /* 0=60,1=62,2=64,3=66,4=68 */
extern int newP6Version;	/* new P6Version */
/*************************************************************/


/* -------------------------------- */
extern int portBC;			// VRTC Interrupt address
extern int portC1;
extern int portC8;			/* crt contoller */
extern int portCA;		/* Graphics scroll X low*/
extern int portCB;		/* Graphics scroll X high*/
extern int portCC;		/* Graphics scroll Y */
extern int portCE;			/* Graphics Y ZAHYOU */
extern int portCF;			/* */
extern int portDC;		/* FDC status */

extern int portD1;
extern int portD2;			/* disk unit controll input */
extern int portF6;					//I/O [F6]  TIMER COUNTUP 
extern int portF7;			//I/O [F7]  TIMER INT ADDRESS 

extern int portFA;		/* Interrupt Controll */
extern int portFB;		/* Interrupt address Controll */

extern int sr_mode;                 	/* TRUE: sr_mode  FALSE: not sr_mode */


extern int  palet[16];			/* SR Palet */
extern int textpalet1[16];
extern int textpalet2[16];

extern int bitmap;			/* TRUE: bitmap   FALSE: text */
extern int cols;			/* text cols  for WIDTH */
extern int rows;			/* text rows  for WIDTH */
extern int lines;			/* graphics LINES */

extern int disk_type;		// TRUE: PD765A   FALSE: mini disk
extern int disk_num;		// DRIVE NUMBERS  0:disk non  1-2:disk on line
extern int new_disk_num;	//                     (next boot time)
extern int UseDiskLamp;		// 1: use disk lamp  0: no use


extern byte *CGROM6;
extern byte *CGROM;

extern int  extkanjirom;	// EXT KANJIROM 1:enable  0:disable
extern int  new_extkanjirom;
extern int  extram;			// EXT RAM      1:enable  0:disable
extern int  new_extram;

extern byte *EXTROM;
extern byte *EXTROM1;
extern byte *EXTROM2;
extern TCHAR Ext1Name[PATH_MAX];    /* Extension ROM 1 file  (4000h-5fffh)*/
extern TCHAR Ext2Name[PATH_MAX];    /* Extension ROM 2 file  (6000h-7fffh)*/
extern TCHAR ExtRomName[PATH_MAX];  /* Extension ROM name    (4000h-7fffh)*/
extern byte *VRAM;
extern byte *TEXTVRAM;
extern byte *RAM;
extern byte CRTMode;

extern byte PSGReg;
extern byte PSGTMP[ 0xc0];		// add 2002/10/15
extern byte JoyState[2];
extern byte CSS;

extern byte UPeriod;   /* Number of interrupts/screen update */
extern byte p6key;
extern byte stick0;
extern byte timerInt;
extern byte keyGFlag;
extern byte kanaMode;
extern byte katakana;
extern byte kbFlagGraph;
extern byte kbFlagCtrl;
extern byte TimerSW;
extern byte TimerSW_F3;
extern byte next90;
extern TCHAR PrnName[PATH_MAX];    /* Printer redir. file */

extern TCHAR DskName[2][PATH_MAX];    /* Disk image file     */
extern TCHAR CasName[2][PATH_MAX];    /* Tape image file     */


extern TCHAR CasPath[2][PATH_MAX];    /* Tape image search path */
extern TCHAR DskPath[2][PATH_MAX];    /* Disk image search path */

extern TCHAR RomPath[PATH_MAX];    /* Rom  image search path */
extern TCHAR ImgPath[PATH_MAX];    /* snapshot image path */
extern TCHAR MemPath[PATH_MAX];    /* memory   image path */

extern FILE *DskStream[2];
extern FILE *CasStream[2];

extern byte CasMode;

extern int  PatchLevel;
extern int  scr4col;
extern int FastTape;	// use Fast Tape     1: high  0:normal

extern int   CPUclock;					// CPU clock
extern int   CPUclock_bak;				// CPU clock backup
extern int   drawwait;					// draw wait
extern int   drawwait_bak;				// draw wait backup

extern int busreq;						// busreq  1:ON  0:OFF
extern int srline;						// sr line   enable when busreq=1


extern int   UseSaveTapeMenu;		// save tape menu      1: on  0: off
extern int   UseStatusBar;              // status bar          1: on  0: off
extern int   UseCPUThread;       // 1: CPU $B$rJL%9%l%C%I$GF0$+$9!#(B  0: $BC10l%9%l%C%I$GF0$+$9!#(B

extern int   Console;			// use console mode  1: use

extern LPCTSTR Title;				// title name

