/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           message.c                     **/
/** by Windy 2003                                           **/
/*************************************************************/
/*
What? 
    gettext もどきです。

    mgettext("hoge hoge")で呼び出すと、対応するメッセージが有ればそれを返却します。
    無ければ、引数をそのまま返却します。

TO DO:
    LOCALE によって、返すメッセージを変更する。
    LOCALE によって、文字コードを変換してから返す。
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "P6.h"
#include "message.h"


static LPCTSTR transtable[][2]={
{
    TEXT("Please REBOOT 'iP6 Plus' to enable the change of options."),
    TEXT("(*)マークの設定は、iP6 Plusを再起動後に有効になります。"),
},
{
    TEXT("Warning!  Using 1D format disk when This machine type ,must convert file format."),
    TEXT("警告！　この機種で、1Dタイプのディスクを使うときは、ファイル変換しないと正常に使用できません。"),
},
{
    TEXT("Error!    Can't use 1DD format disk in this machine type. disk ejected."),
    TEXT("この機種で 1DDタイプのディスクは使えないので、イジェクトします。"),
},
{
	TEXT("RESET CPU Ok?"),
	TEXT("リセットしてもいいですか？"),
},
{
	TEXT("Exit Ok?"),
	TEXT("終了してもいいですか？"),
},
{
	TEXT("RESET failed!  check ROM files!"),
	TEXT("リセットに失敗しました。　ROMファイルをチェックしてください。"),
},
{
	TEXT("Reboot to enable new setting?"),
	TEXT("設定を有効にするために、今すぐ再起動しますか？"),
},
{NULL,NULL}
};


LPCTSTR mgettext(LPCTSTR key)
{
    int  i;
    int  destIdx;
    LPCTSTR p;
    
    destIdx=1;
    p= key;
    for(i=0; transtable[i][0]!=NULL ; i++)
        {
         if( lstrcmp( key, transtable[i][0]) ==0)
            {
             p=  transtable[i][destIdx];
             break;
            }
        }
    return( p);
}


