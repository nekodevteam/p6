/* Unix.h */
/* Common definitons and declarations for Unix.c and Refresh.c */

// --------------- 宣言する場合は、#define GLOBAL する -------------
#ifdef  GLOBAL
#define EXTRN
#else
#define EXTRN extern
#endif

#include "Refresh.h"

/* functions and variables in Unix.c used by Refresh.c */
//EXTRN  int Mapped;

void PutImage(void);
void ClearScr(void);
int resizewindow(int bitmap_scale, int win_scale);
int textout(int x,int y,char *str);
int messagebox(char *str, char *title);
int setMenuTitle(int type);
int JoystickGetState(int joy_no);
void ClearWindow(void);
void SLEEP(int s);
void putStatusBar(void);
void outputdebugstring(char *buff);
void PutDiskAccessLamp(int sw);
int unixIdle();

int resize(int w,int h);		// resize window 2002/4/29
