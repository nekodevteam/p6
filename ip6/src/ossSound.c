/*
** Sound.c
**
** This file countains PSG/SCC emulation stuff for fMSX via the /dev/dsp
** device present in Linux and FreeBSD (and probably in other PC Unices
** using USS(/Lite) (formerly known as VoxWare) sound driver). Also very
** experimental support for Sun's /dev/audio is present.
**
** (C) Ville Hallik (ville@physic.ut.ee) 1996
**
** You can do almost anything you want with this file, provided that notice
** about original author still appears there and in case you make any
** modifications, PLEASE label it as modified version.
**
** Known problems:
** 1. The noise and envelope frequencies may be wrong - I dont have
**    real MSX handy to compare them.
** 2. The seventh bit of port 0xAA is not supported (it's almost impossible
**    to do with the method I use here), fortunately it's not used in most
**    cases.
** 3. Modifications of PSG registers are checked (theoretically ;) once
**    per every SOUND_BUFSIZE/SOUND_RATE seconds. It's about 11ms with the
**    values of 256bytes and 22050Hz, which should be satisfactory for most
**    cases.
** 4. Support for Sun's /dev/audio is crap, but it works somehow ;)
** 5. I don't know how realistic is the emulation of SCC chip.
**
** last modified on Sat Apr 22 1996
*/
#include "P6.h"

#ifdef UNIX

int UseSound=1;	 // added       compile for disabling -DSOUND

#ifdef SOUND

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include "Sound.h"
#include "fm.h"

#ifdef SUN_AUDIO

/*
** Size of audio buffer
*/
   //#define SOUND_BUFSIZE 256


/* Do not change this! It's not implemented. */
  //#define SOUND_RATE 8000

#else /* SUN_AUDIO */

#ifdef LINUX
#include <linux/soundcard.h>
#else
#include <machine/soundcard.h>		// modifed by windy 2002/4/13
#endif

#endif // SUN_AUDIO

static volatile char s_running;
static pthread_t s_thread;
static int sounddev=-1;
const unsigned int sound_rate=SOUND_RATE;

static void* SoundMainLoop(void* pParam);

static void CloseSoundDev(void)
{
	if (sounddev != -1)
	{
#ifndef SUN_AUDIO
		ioctl(sounddev, SNDCTL_DSP_RESET);
#endif /* not SUN_AUDIO */
		close(sounddev);
		sounddev = -1;
	}
}

void TrashSound(void)
{
	if (s_running)
	{
		s_running = 0;
		pthread_join(s_thread, NULL);
	}

	CloseSoundDev();
}

static int OpenSoundDev(void)
{
  int I,J,K;
  
	if (sounddev != -1) return 0;

#ifdef SUN_AUDIO

  if(Verbose) printf("  Opening /dev/audio...");
  if((sounddev=open("/dev/audio",O_WRONLY | O_NONBLOCK))==-1)
  { if(Verbose) puts("FAILED");return(0); }

  /*
  ** Sun's specific initialization should be here...
  ** We assume, that it's set to 8000kHz u-law mono right now.
  */

#else

  /*** At first, we need to open /dev/dsp: ***/
  if(Verbose) printf("  Opening /dev/dsp...");
  if((sounddev=open("/dev/dsp",O_WRONLY | O_NONBLOCK))==-1)
  { if(Verbose) puts("FAILED");return(0); }

  if(Verbose) printf("OK\n  Setting mode: 16bit...");

  J=AFMT_S16_LE;I=(ioctl(sounddev,SNDCTL_DSP_SETFMT,&J)<0);
  if(!I)
  {
    J=CHANNEL;
    if(Verbose) printf("%dchannel(s)...", J);
    I|=(ioctl(sounddev,SNDCTL_DSP_CHANNELS,&J)<0) && (J==CHANNEL);
  }
  if(I) { if(Verbose) puts("FAILED");return(0); }
  /*** Set sampling rate ***/
  if(!I)
  {
    if(Verbose) printf("OK\n  Setting sampling rate: %dHz...",sound_rate);
    I|=(ioctl(sounddev,SNDCTL_DSP_SPEED,&sound_rate)<0);
    if(Verbose) printf("(got %dHz)...",sound_rate);
  }
  if(I) { if(Verbose) puts("FAILED");return(0); }
  /*** Here we set the number of buffers to use **/
  if(!I)
  {

#if 1
    if(Verbose) printf("OK\n  Adjusting buffers: %d buffers %d bytes each...",
     SOUND_NUM_OF_BUFS, 1<<SOUND_BUFSIZE_BITS);
    J=K=SOUND_BUFSIZE_BITS|(SOUND_NUM_OF_BUFS<<16);
    I|=(ioctl(sounddev,SNDCTL_DSP_SETFRAGMENT,&J)<0);

    if((J & 0xffff)<16) J=(J& 0xffff0000) | (1<<(J&0xffff));
    K=(1<<SOUND_BUFSIZE_BITS)| (SOUND_NUM_OF_BUFS<<16);
    printf("J=%X K=%X \n",J,K);
    if( J != K ) {
      if(Verbose) printf("(got %d buffers %d bytes each)...",
       J>>16,1<<(J&0xFFFF));
//     if((J>>16) < SOUND_NUM_OF_BUFS) {I=-1; printf("line:%d",__LINE__);}
//     if((J&0xffff) !=(1<<SOUND_BUFSIZE_BITS)) {I=-1; printf("line %d",__LINE__); }
//     if((J&0xffff) !=(1<<8)) {I=-1; printf("line %d",__LINE__); }

      I=-1;
    }
#endif
  }
  if(I) { if(Verbose) puts("FAILED");return(0); }

#endif // SUN_AUDIO

  if(Verbose) puts("OK");
  return(1);
}

int InitSound(void )
{
  if(!UseSound) return(0);

  if(Verbose) puts("Starting sound server:");

  sounddev=-1;

  if( !OpenSoundDev() ) return(0);

  if(Verbose) { printf("Forking..."); fflush( stdout ); }

  s_running = 1;
  if (pthread_create(&s_thread, NULL, SoundMainLoop, NULL) != 0)
  {
    if(Verbose) puts("FAILED");
    s_running = 0;
    return 0;
  }

  if(Verbose) puts("OK");

  return(1);
}

void StopSound(void)       { ym2203_mute(1); }
void ResumeSound(void)     { ym2203_mute(0); }

static void* SoundMainLoop(void *pParam)
{
  signed short soundbuf[SOUND_BUFSIZE];
  unsigned int writebytes ,p;

  ym2203_init();		// ym2203 init

  while(s_running)
   {
    if( sounddev == -1 ) {
      sleep(1);
      continue;
    }
	ym2203_makewave( soundbuf ,sizeof(soundbuf));	// ym2203 makewave
	writebytes = sizeof(soundbuf) / 2 * CHANNEL;

#ifdef SUN_AUDIO

    /*
    ** Flush output first, don't care about return status.
    ** After this write next buffer of audio data. This method
    ** produces a horrible click on each buffer :( Any ideas,
    ** how to fix this?
    */

    ioctl( sounddev, AUDIO_DRAIN );
    write( sounddev, soundbuf, writebytes );

#else

    /*
    ** We'll block here until next DMA buffer becomes free.
    ** It happens once per (1<<SOUND_BUFSIZE_BITS)/sound_rate seconds.
    */
    p=0;
	while( p < writebytes ) {
        const int written=write( sounddev, ((char*)soundbuf) + p, writebytes - p);
        if( written >0 )
            {
             p += written;
          //   printf("write() written=%d\n",written); 
            }
    	else if( written <0 && errno!=0 && errno!= EAGAIN && errno!= EINTR)
            {
          //   printf("write() failed %d \n",errno);
             break;
            }
        else if ((written<0) && ((errno==0)||(errno==EAGAIN)) )
           {
		 // printf("sound: sleep & retrying... \n");
                 usleep(1);
             }

      }

#endif // SUN_AUDIO

  }
  return NULL;
}

#endif // SOUND
#endif
