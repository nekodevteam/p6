/*
	 Win32.h
   Programmed by Windy
*/
#ifndef _WIN32_H
#define _WIN32_H

#ifdef WIN32
#include <tchar.h>
#include <windows.h>
#endif


//#ifdef  GLOBAL
//#undef  EXTRN
//#define EXTRN
//#else
//#undef  EXTRN
//#define EXTRN extern
//#endif

#include "Win32stick.h"
#include "Win32fscr.h"

#ifndef _countof
#define _countof(a)			(sizeof((a)) / sizeof((a)[0]))
#endif	// !_countof

#if defined(_MSC_VER) && (_MSC_VER < 1300) && !defined(_WIN32_WCE)
typedef unsigned long DWORD_PTR;		/*!< DWORD_PTR */
#endif	// defined(_MSC_VER) && (_MSC_VER < 1300) && !defined(_WIN32_WCE)

#if defined(_WIN32_WCE)
#define rewind(f)		fseek(f, 0, FILE_BEGIN)
#endif	// defined(_WIN32_WCE)

void SLEEP(int s);
int unixIdle(void);

void PutImage(void);
void ClearScr(void);
int  messagebox(LPCTSTR str, LPCTSTR title);
void outputdebugstring(LPCTSTR buff);
void putlasterror(void);

int setMenuTitle(int type);
int resizewindow( int bitmap_scale,int win_scale);

// ****************************************************************************
//          dibsection
// ****************************************************************************
typedef struct dibstruct {
	int					Width ,Height;		// size
	BYTE				*lpBMP;				// pixel buffer
	BITMAPINFO			*lpBmpInfo;			// Dibsection info
	HBITMAP				hBitmap1;			// bitmap handle
	HDC					hdcBmp;				// bitmap hdc
	} HIBMP;

HIBMP createIBMP(HWND hwnd, int width , int height, int bitpix);
void releaseIBMP(HIBMP hb);

//extern HWND hwndMain;
//extern HINSTANCE hInst;
//extern HICON     hIcon;

LRESULT CALLBACK WindowFunc( HWND ,UINT ,WPARAM, LPARAM);


void ClearWindow(void);				//          ClearWindow: $B%&%$%s%I%&A4BN$r%/%j%"(B
void ClearStatusBar(void);          //          ClearStatusBar: $B%9%F!<%?%9%P!<$r%/%j%"(B
int textout(int x,int y,LPCTSTR str); //          textout: $B%(%_%e%l!<%?$N2hLL$KJ8;z$r=PNO(B


void PutDiskAccessLamp(int sw);

void putStatusBar(void);

int  debug_printf( LPCTSTR fmt ,...);
extern int debug_level;

//#undef EXTRN
#endif
