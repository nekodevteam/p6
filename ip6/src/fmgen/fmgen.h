// ---------------------------------------------------------------------------
//	FM Sound Generator
//	Copyright (C) cisc 1998, 2001.
// ---------------------------------------------------------------------------
//	$Id: fmgen.h,v 1.26 2001/03/30 01:05:52 cisc Exp $

#ifndef FM_GEN_H
#define FM_GEN_H

#include "types.h"

#if (_MSC_VER >= 1200) && (!defined(_WIN64)) && (!defined(_WIN32_WCE))
	#define FM_USE_X86_CODE
#endif	// (_MSC_VER >= 1200) && (!defined(_WIN64)) && (!defined(_WIN32_WCE))

// ---------------------------------------------------------------------------
//	$B=PNO%5%s%W%k$N7?(B
//
#define FM_SAMPLETYPE	int32				// int16 or int32

// ---------------------------------------------------------------------------
//	$BDj?t$=$N#1(B
//	$B@EE*%F!<%V%k$N%5%$%:(B

#define FM_LFOBITS		8					// $BJQ99IT2D(B
#define FM_TLBITS		7

// ---------------------------------------------------------------------------

#define FM_TLENTS		(1 << FM_TLBITS)
#define FM_LFOENTS		(1 << FM_LFOBITS)
#define FM_TLPOS		(FM_TLENTS/4)

// ---------------------------------------------------------------------------

namespace FM
{	
	//	Types ----------------------------------------------------------------
	typedef FM_SAMPLETYPE	Sample;
	typedef int32 			ISample;

	enum OpType { typeN=0, typeM=1 };
	
	//	Tables ($B%0%m!<%P%k$J$b$N$d(B asm $B$+$i;2>H$5$l$k$b$NEy(B) -----------------
	void MakeTable();
	void MakeTimeTable(uint ratio);
	extern uint32 tltable[];
	extern int32  cltable[];
	extern uint32 dltable[];
	extern int    pmtable[2][8][FM_LFOENTS];
	extern uint   amtable[2][4][FM_LFOENTS];
	extern uint   aml, pml;
	extern int    pmv;		// LFO $BJQ2=%l%Y%k(B

	void StoreSample(ISample& dest, int data);

	//	Operator -------------------------------------------------------------
	class Operator
	{
	public:
		Operator();
		static void MakeTable();
		static void	MakeTimeTable(uint ratio);
		
		ISample	Calc(ISample in);
		ISample	CalcL(ISample in);
		ISample CalcFB(uint fb);
		ISample CalcFBL(uint fb);
		ISample CalcN(uint noise);
		void	Prepare();
		void	KeyOn();
		void	KeyOff();
		void	Reset();
		void	ResetFB();
		int		IsOn();

		void	SetDT(uint dt);
		void	SetDT2(uint dt2);
		void	SetMULTI(uint multi);
		void	SetTL(uint tl, bool csm);
		void	SetKS(uint ks);
		void	SetAR(uint ar);
		void	SetDR(uint dr);
		void	SetSR(uint sr);
		void	SetRR(uint rr);
		void	SetSL(uint sl);
		void	SetSSGEC(uint ssgec);
		void	SetFNum(uint fnum);
		void	SetDPBN(uint dp, uint bn);
		void	SetMode(bool modulator);
		void	SetAMON(bool on);
		void	SetMS(uint ms);
		void	Mute(bool);
		
		static void SetAML(uint l);
		static void SetPML(uint l);
		
	private:
		typedef uint32 Counter;
		
		ISample	out, out2;

	//	Phase Generator ------------------------------------------------------
		uint32	PGCalc();
		uint32	PGCalcL();

		uint	dp;			// $B&$(BP
		uint	detune;		// Detune
		uint	detune2;	// DT2
		uint	multiple;	// Multiple
		uint32	pgcount;	// Phase $B8=:_CM(B
		uint32	pgdcount;	// Phase $B:9J,CM(B
		int32	pgdcountl;	// Phase $B:9J,CM(B >> x

	//	Envelope Generator ---------------------------------------------------
		enum	EGPhase { next, attack, decay, sustain, release, off };
		
		void	EGCalc();
		void	ShiftPhase(EGPhase nextphase);
		void	ShiftPhase2();
		void	SetEGRate(uint);
		void	EGUpdate();
		
		OpType	type;		// OP $B$N<oN`(B (M, N...)
		uint	bn;			// Block/Note
		int		eglevel;	// EG $B$N=PNOCM(B
		int		eglvnext;	// $B<!$N(B phase $B$K0\$kCM(B
		int32	egstep;		// EG $B$N<!$NJQ0\$^$G$N;~4V(B
		int32	egstepd;	// egstep $B$N;~4V:9J,(B
		int		egtransa;	// EG $BJQ2=$N3d9g(B (for attack)
		int		egtransd;	// EG $BJQ2=$N3d9g(B (for decay)
		int		egout;		// EG+TL $B$r9g$o$;$?=PNOCM(B
		int		tlout;		// TL $BJ,$N=PNOCM(B
		int		pmd;		// PM depth
		int		amd;		// AM depth

		uint	ksr;		// key scale rate
		EGPhase	phase;
		uint*	ams;
		uint8	ms;

		bool	keyon;		// current key state
		
		uint8	tl;			// Total Level	 (0-127)
		uint8	tll;		// Total Level Latch (for CSM mode)
		uint8	ar;			// Attack Rate   (0-63)
		uint8	dr;			// Decay Rate    (0-63)
		uint8	sr;			// Sustain Rate  (0-63)
		uint8	sl;			// Sustain Level (0-127)
		uint8	rr;			// Release Rate  (0-63)
		uint8	ks;			// Keyscale      (0-3)
		uint8	ssgtype;	// SSG-Type Envelop Control

		bool	amon;		// enable Amplitude Modulation
		bool	paramchanged;	// $B%Q%i%a!<%?$,99?7$5$l$?(B
		bool	mute;
		
	//	Tables ---------------------------------------------------------------
		enum TableIndex { dldecay = 0, dlattack = 0x400, };
		
		static Counter ratetable[64];
		static uint32 multable[4][16];

	//	friends --------------------------------------------------------------
		friend class Channel4;
#ifdef FM_USE_X86_CODE
		friend void __stdcall FM_NextPhase(Operator* op);
#endif	// FM_USE_X86_CODE
	};
	
	//	4-op Channel ---------------------------------------------------------
	class Channel4
	{
	public:
		Channel4();
		void SetType(OpType type);
		
		ISample Calc();
		ISample CalcL();
		ISample CalcN(uint noise);
		ISample CalcLN(uint noise);
		void SetFNum(uint fnum);
		void SetFB(uint fb);
		void SetKCKF(uint kc, uint kf);
		void SetAlgorithm(uint algo);
		int Prepare();
		void KeyControl(uint key);
		void Reset();
		void SetMS(uint ms);
		void Mute(bool);
		void Refresh();
		
	private:
		static const uint8 fbtable[8];
		uint	fb;
		int		buf[4];
		int*	in[3];			// $B3F(B OP $B$NF~NO%]%$%s%?(B
		int*	out[3];			// $B3F(B OP $B$N=PNO%]%$%s%?(B
		int*	pms;

	public:
		Operator op[4];
	};
}

#endif // FM_GEN_H
