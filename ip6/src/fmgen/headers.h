#ifndef WIN_HEADERS_H
#define WIN_HEADERS_H

#ifdef _WIN32
#ifndef STRICT
#define STRICT
#endif	// STRICT
#define WIN32_LEAN_AND_MEAN

#include <tchar.h>
#include <windows.h>
#else	// _WIN32
#include "../misc/unix/win32.h"
#endif	// _WIN32

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#endif	// WIN_HEADERS_H
