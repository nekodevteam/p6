// ---------------------------------------------------------------------------
//	FM Sound Generator
//	Copyright (C) cisc 1998, 2000.
// ---------------------------------------------------------------------------
//	$Id: fmgeninl.h,v 1.17 2001/03/30 01:05:52 cisc Exp $

#ifndef FM_GEN_INL_H
#define FM_GEN_INL_H

// ---------------------------------------------------------------------------
//	$BDj?t$=$N#2(B
//	
#define FM_PI			3.14159265358979323846

#define FM_SINEPRESIS	2			// EG$B$H%5%$%sGH$N@:EY$N:9(B  0($BDc(B)-2($B9b(B)

#define FM_CLENTS		(0xc00 << FM_SINEPRESIS)	// 0x400(C++) 0x800(asm) $B0J>e(B 0xc00 $BL$K~(B

#define FM_OPSINBITS	10
#define FM_OPSINENTS	(1 << FM_OPSINBITS)

#define FM_EGCBITS		18			// eg $B$N(B count $B$N%7%U%HCM(B
#define FM_LFOCBITS		14
#define FM_PGBITS		9

#define FM_ISHIFT		3

#define FM_RATIOBITS	12			// 8-12 $B$/$i$$$^$G!)(B

namespace FM
{

// ---------------------------------------------------------------------------
//	Operator
//

//	AM $B$N%l%Y%k$r@_Dj(B
inline void Operator::SetAML(uint l)
{
	aml = l & (FM_LFOENTS - 1);
}

//	PM $B$N%l%Y%k$r@_Dj(B
inline void Operator::SetPML(uint l)
{
	pml = l & (FM_LFOENTS - 1);
}

//	$B%U%#!<%I%P%C%/%P%C%U%!$r%/%j%"(B
inline void Operator::ResetFB()
{
	out = out2 = 0;
}

//	$B%-!<%*%s(B
inline void Operator::KeyOn()
{
	if (!keyon)
	{
		keyon = true;
		if (phase == off || phase == release)
		{
			ShiftPhase(attack);
			EGUpdate();
			out = out2 = 0;
			pgcount = 0;
		}
	}
}

//	$B%-!<%*%U(B
inline void	Operator::KeyOff()
{
	if (keyon)
	{
		keyon = false;
		ShiftPhase(release);
	}
}

//	$B%*%Z%l!<%?$O2TF/Cf$+!)(B
inline int Operator::IsOn()
{
	return phase - off;
}

//	Detune (0-7)
inline void Operator::SetDT(uint dt)
{
	detune = dt * 0x20, paramchanged = true;
}

//	DT2 (0-3)
inline void Operator::SetDT2(uint dt2)
{
	detune2 = dt2 & 3, paramchanged = true;
}

//	Multiple (0-15)
inline void Operator::SetMULTI(uint mul)	
{ 
	multiple = mul, paramchanged = true;
}

//	Total Level (0-127) (0.75dB step)
inline void Operator::SetTL(uint _tl, bool csm)
{
	if (!csm)
		tl = _tl, paramchanged = true;
	tll = _tl;
}

//	Attack Rate (0-63)
inline void Operator::SetAR(uint _ar)
{
	ar = _ar; paramchanged = true;
}

//	Decay Rate (0-63)
inline void Operator::SetDR(uint _dr)
{ 
	dr = _dr; paramchanged = true;
}

//	Sustain Rate (0-63)
inline void Operator::SetSR(uint _sr)		
{ 
	sr = _sr; paramchanged = true;
}

//	Sustain Level (0-127)
inline void Operator::SetSL(uint _sl)		
{ 
	sl = _sl; paramchanged = true;
}

//	Release Rate (0-63)
inline void Operator::SetRR(uint _rr)		
{ 
	rr = _rr; paramchanged = true;
}

//	Keyscale (0-3)
inline void Operator::SetKS(uint _ks)		
{ 
	ks = _ks; paramchanged = true; 
}

//	SSG-type Envelop (0-15)
inline void Operator::SetSSGEC(uint ssgec)	
{ 
	ssgtype = ssgec; 
}

inline void Operator::SetAMON(bool on)		
{ 
	amon = on;  
	paramchanged = true;
}

inline void Operator::Mute(bool m)
{
	mute = m;
	paramchanged = true;
}

inline void Operator::SetMS(uint _ms)
{
	ms = _ms;
	paramchanged = true;
}

// ---------------------------------------------------------------------------
//	4-op Channel

//	$B%*%Z%l!<%?$N<oN`(B (LFO) $B$r@_Dj(B
inline void Channel4::SetType(OpType type)
{
	for (int i=0; i<4; i++)
		op[i].type = type;
}

//	$B%;%k%U!&%U%#!<%I%P%C%/%l!<%H$N@_Dj(B (0-7)
inline void Channel4::SetFB(uint feedback)
{
	fb = fbtable[feedback];
}

//	OPNA $B7O(B LFO $B$N@_Dj(B
inline void Channel4::SetMS(uint ms)
{
	op[0].SetMS(ms);
	op[1].SetMS(ms);
	op[2].SetMS(ms);
	op[3].SetMS(ms);
}

//	$B%A%c%s%M%k!&%^%9%/(B
inline void Channel4::Mute(bool m)
{
	for (int i=0; i<4; i++)
		op[i].Mute(m);
}

//	$BFbIt%Q%i%a!<%?$r:F7W;;(B
inline void Channel4::Refresh()
{
	for (int i=0; i<4; i++)
		op[i].paramchanged = true;
}

// ---------------------------------------------------------------------------
//
//
inline void StoreSample(Sample& dest, ISample data)
{
	if (sizeof(Sample) == 2)
		dest = (Sample) Limit(dest + data, 0x7fff, -0x8000);
	else
		dest += data;
}

}

#endif // FM_GEN_INL_H
