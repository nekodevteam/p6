//	$Id: file.h,v 1.6 1999/11/26 10:14:09 cisc Exp $

#pragma once

#include "types.h"

/**
 * @brief $B%U%!%$%k(B I/O $B%/%i%9(B
 */
class FileIO
{
public:
	/**
	 * $B%U%i%0(B
	 */
	enum Flags
	{
		open		= 0x000001,
		readonly	= 0x000002
	};

	/**
	 * $B%7!<%/(B $B%a%=%C%I(B
	 */
	enum SeekMethod
	{
		begin	= 0,
		current	= 1,
		end		= 2,
	};

	/**
	 * $B%3%s%9%H%i%/%?(B
	 */
	FileIO()
		: m_fp(NULL)
		, m_flags(0)
	{
	}

	/**
	 * $B%3%s%9%H%i%/%?(B
	 * @param[in] filename $B%U%!%$%kL>(B
	 * @param[in] flags $B%U%i%0(B
	 */
	FileIO(LPCTSTR filename, uint flags = 0)
		: m_fp(NULL)
		, m_flags(0)
	{
		Open(filename, flags);
	}

	/**
	 * $B%G%9%H%i%/%?(B
	 */
	~FileIO()
	{
		Close();
	}

	/**
	 * $B%U%!%$%k$r3+$/(B
	 * @param[in] filename $B%U%!%$%kL>(B
	 * @retval true $B@.8y(B
	 * @retval false $B<:GT(B
	 */
	bool Open(LPCTSTR filename, uint = 0)
	{
		Close();

		m_flags = readonly;

		m_fp = _tfopen(filename, TEXT("rb"));
		if (m_fp != NULL)
		{
			m_flags |= open;
		}

		return (m_fp != NULL);
	}

	/**
	 * $B%U%!%$%k$rJD$8$k(B
	 */
	void Close()
	{
		if (m_fp != NULL)
		{
			fclose(m_fp);
			m_fp = NULL;
			m_flags = 0;
		}
	}

	/**
	 * $B%U%!%$%k$+$i$NFI$_=P$7(B
	 * @param[in] dest $B%P%C%U%!(B
	 * @param[in] len $B%5%$%:(B
	 * @return $B%5%$%:(B
	 */
	int32 Read(void* dest, int32 len)
	{
		if (m_fp != NULL)
		{
			return static_cast<int32>(fread(dest, 1, len, m_fp));
		}
		else
		{
			return -1;
		}
	}

	/**
	 * $B%U%!%$%k$r%7!<%/(B
	 * @param[in] fpos $B0LCV(B
	 * @param[in] method $B%a%=%C%I(B
	 * @retval true $B@.8y(B
	 * @retval false $B<:GT(B
	 */
	bool Seek(int32 fpos, SeekMethod method)
	{
		if (m_fp != NULL)
		{
			int whence;
			switch (method)
			{
				case begin:
					whence = SEEK_SET;
					break;

				case current:
					whence = SEEK_CUR;
					break;

				case end:
					whence = SEEK_END;
					break;

				default:
					return false;
			}
			return (fseek(m_fp, fpos, whence) != -1);
		}
		else
		{
			return false;
		}
	}

private:
	FILE* m_fp;			/*!< $B%U%!%$%k(B */
	uint32 m_flags;		/*!< $B%U%i%0(B */

	/* $B%3%T!<6X;_(B */
	FileIO(const FileIO&);
	const FileIO& operator=(const FileIO&);
};
