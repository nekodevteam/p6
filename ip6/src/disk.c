/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           disk.c                        **/
/**                                                         **/
/**                     Inteligent  disk unit               **/
/** by windy                                                **/
/*************************************************************/
/*
$B%$%s%F%j$8$'$s$H%?%$%W$G$9!#(B

$B%(%_%e%l!<%?=hM}$KIU$$$F(B

   $B%3%^%s%I!?%G!<%?$rFI$_9~$s$G$$$-!"5,Dj$ND9$5$KC#$7$?$i!"3F%3%^%s%I$N<B9T$r$7$^$9!#(B
   $B%G!<%?$N<u?.MW5a$,$"$C$?$i!"4{$KFI$s$G$"$k%G!<%?$r(B $BEO$7$^$9!#(B

$B3F%3%^%s%I$N@bL@(B

  write sector $B$O!"%3%^%s%I$N8e$K!"=q$-9~$_$?$$%G!<%?$rJB$Y$^$9!#(B
  read  sector $B$O!"%3%^%s%I$rH/9T$7$F$+$i!"(BSEND DATA $B$G(B $B%G!<%?$rFI$_$^$9!#(B
  result status$B$O!"%3%^%s%I$N<B9T7k2L$rJV$7$^$9!#(B
  result drive status $B$O!"%I%i%$%V$N>uBV$rJV$7$^$9!#(B
  XMIT         $B$O!"9bB.%b!<%IEk:\$NM-L5$r(B $B%A%'%C%/$9$k$N$K;HMQ$7$F$$$k$h$&$G$9!#(B
  SetSurfaceMode$B$O!"%$%s%F%j$8$'$s$H%?%$%W$G!"(B1DD$B$+!"(B1D$B$+$N%A%'%C%/$r$7$F$$$k$_$?$$$G$9(B

$B9bB.%b!<%I$K$D$$$F(B

  $BDL>o%b!<%I$O!"0l2s$N%O%s%I%7%'%$%/$K!"#1%P%$%H$7$+<uEO$7$G$-$J$$$N$KBP$7$F!"(B
  $B9bB.%b!<%I$O!"0l2s$N%O%s%I$7$'$$$/$G!"#2%P%$%H$b<uEO$7$G$-$^$9!#(B
    $B%(%_%e%l!<%?E*$K$O!"9bB.%b!<%I$G$bDL>o%b!<%I$G$b!"$[$H$s$IF1$8<BAu$G$9!#(B
*/
#ifdef __APPLE__
#include <machine/types.h>
#endif

#include <string.h>
//#include <dirent.h>	// dirent.h $B$,$J$$4D6-$b$"$k$i$7$$$N$G30$7$^$9!#(B(^^;
#include "P6.h"
#include "disk.h"
#include "d88.h"
#include "error.h"
#include "os.h"


#define PC_SEND_DONE       0x8
#define PC_RECV_READY      0xA
#define PC_RECV_DONE       0xC
#define PC_SEND_REQUEST    0xE

#define FD_SEND_DONE         1
#define FD_RECV_READY        2
#define FD_RECV_DONE         4

#define SET_8255                  0x91

//extern int disk_num;

static int cmd_length[]={1 ,0 ,5 ,1 ,8 ,2 ,1 ,1 ,5 ,1 ,1 ,1 ,1,1,1,1,  1 ,0,1};

int fd_send;			// 1: PC -> FD Senging..
int fd_recv;            // 1: PC <- FD Recieving..
int fdindex_in=0;       // index of fdbuff input
int fdindex_out=0;      // index of fdbuff output
byte fdbuff_in[256*16];	// fd intenal buffer input
byte fdbuff_out[256*16];// fd intenal buffer output
						// 256*16 : length of 1 Track


// ************************************
//      controll (PC -> FD)
// ************************************
void outD3( byte Value)
{
// portD3=Value;
 if( Value ==SET_8255)	                                // 8255 mode set
              {
               fd_send=fd_recv= fdindex_in= fdindex_out=0;

               // printf("disk: set 8255 mode \n");
              }
                                                                                // PC karano SEND_REQUEST
 if( Value ==PC_SEND_REQUEST+1)  // F
               {
	 fd_send = 1;
	 portD2 |= FD_RECV_READY;	                  // FD RECV ready on   2
	 fdindex_in=0;
	 //printf(" PC_SEND_REQUEST on\n");
	}
 if( Value ==PC_SEND_REQUEST)     // E
               {
	 fd_send = 1;
	 //printf(" PC_SEND_REQUEST off\n");
	}					// PC gawa SEND DONE
 if( Value ==PC_SEND_DONE+1)       // 9
            {
               fd_send= 1;
                portD2 |= FD_RECV_DONE;		// FD RECV done on   4
	 //printf(" PC_SEND_DONE on\n");
	}
 if( Value ==PC_SEND_DONE)           // 8
            {
               fd_send= 0;
                portD2 &= ~FD_RECV_DONE;	// FD RECV done off  4
	if( ((fdbuff_in[0]==1)||(fdbuff_in[0]==0x11)) && fdindex_in ==2 ) 	// Write data
	              {
	                cmd_length[ fdbuff_in[0]]= 5+256*fdbuff_in[1];	// set length
	              }
	if( fdindex_in == cmd_length[ fdbuff_in[0]])
                	exec_command();		// execute command
	// printf(" PC_SEND_DONE off\n");
	}


                                                                         // PC gawa Recive READY
 if( Value ==PC_RECV_READY+1)     // B
      {
       fd_recv = 1;
       portD2 |= FD_SEND_DONE;		// FD Send done on  1
       //printf(" PC_RECV_READY on\n");
      }
 if( Value ==PC_RECV_READY)        // A	// PC gawa Recieve Ready off
      {
       fd_recv = 1;
       //printf(" PC_RECV_READY off\n");
      }
 if( Value ==PC_RECV_DONE+1)    // D
      {
       fd_recv = 1;
       portD2 &= ~FD_SEND_DONE;		// FD Send done off  1
       //printf(" PC_RECV_DONE on\n");
      }
 if( Value ==PC_RECV_DONE)        // C
      {
       fd_recv = 0;
       //printf(" PC_RECV_DONE off\n");
      }
 }

// ************************************
//      controll (PC <- FD)
// ************************************
byte inpD2(void)
{
  int ret;
  if(DskStream)
        ret=portD2;
  else
        ret=NORAM;
  return(ret);
}

// ************************************
//       data (PC <- FD)
// ************************************
byte inpD0(void)
{
 byte Value = 0x2e;
 if( fdindex_out > 256*16) { return NORAM;} // out of range ...max Transfer is 16 Sector
 if(fd_recv==1)
    {
     Value=fdbuff_out[ fdindex_out++];
 //   printf("r=%02X",Value);
    }
 return (Value);
}

// ************************************
//      cmd/data (PC -> FD)
// ************************************
void outD1(byte Value)
{
 if( fdindex_in > 256*16) { printf("cmd/data: out of range %d\n",fdindex_in);return;}
 if(fd_send==1 || fdbuff_in[0]==0x11)	// when write sector (fast) command ,too
    {
     fdbuff_in[ fdindex_in++]= Value;
//   dump( fdbuff_in, fdindex_in); // debug
    }
 else
   {
    portD1=Value;	// support for PC-6001Mk2 DISK -  $B=q$-9~$^$l$?CM$r$=$N$^$^JV$9!#(B
   }
}


// ************************************
//      controll output
// ************************************
void disk_out( byte_fast Port, byte Value)
{
 switch( Port)
    {
     case 0xD1: outD1( Value); break;
     case 0xD3: outD3( Value); break;
    }
}

// ************************************
//      controll input
// ************************************
byte disk_inp( byte_fast Port)
{
 byte Value = 0;
 switch( Port)
    {
     case 0xD0: Value = inpD0( ); break;
     case 0xD2: Value = inpD2(); break;
     case 0xD1: Value = portD1; break;	// suport for pc-6001MK2 disk
    }
 return( Value);
}
// ************************************
//      exec command
// ************************************
void exec_command(void)
 {
  int nn,dd,tt,ss;
  static int Result;	// $BD>A0$N%3%^%s%I$N@.H](B
// byte *p=NULL;

 PutDiskAccessLamp(1);

 PRINTDEBUG(TEXT("disk: "));
 switch ( fdbuff_in[0])
    {
     case 0:    PRINTDEBUG(TEXT("initialize        "));
                fdbuff_out[0]=0x10;break;

     case 0x11: PRINTDEBUG(TEXT("(fast mode)"));	// fast mode Write Sector
     case 1:    PRINTDEBUG(TEXT("Write Sector  "));
                nn=fdbuff_in[1]; dd=fdbuff_in[2]; tt=fdbuff_in[3]; ss=fdbuff_in[4];
                if( !getDiskProtect())
                   {
                	write_d88sector(nn,dd,tt,ss-1,fdbuff_in+5);
					Result=0x40;
                   }
                else
                   {
					Result=0x01;		// not writeable
				   }
                break;

     case 2:    PRINTDEBUG(TEXT("Read  Sector "));
//                fdbuff_out[0]=0x40;
     			Result=0x40;
                break;

     case 0x12: PRINTDEBUG(TEXT("(fast mode)"));	// fast mode  Send Data
     case 3:    PRINTDEBUG(TEXT("Send  Data   "));
                nn=fdbuff_in[1]; dd=fdbuff_in[2]; tt=fdbuff_in[3]; ss=fdbuff_in[4];
                memcpy( fdbuff_out, read_d88sector( nn,dd ,tt ,ss-1), 256*nn);
                Result=0x40;
                break;

     case 6:    PRINTDEBUG(TEXT("Result Status "));
//                fdbuff_out[0]=0x40;
				fdbuff_out[0]= Result;
                break;

     case 7:    PRINTDEBUG(TEXT("Drive  Status "));
                if( disk_num)		// drive numbers   0: 00h  1: 10h  2: 30h
                      fdbuff_out[0]=0x10;
                else
                      fdbuff_out[0]=0x00;
                break;
     case 0xB:  PRINTDEBUG(TEXT("XMit   "));
                fdbuff_out[0]=(is1DD())?  0 : 0xff ;break;
	// if 1DD disk then set 00h  ( $B9bB.%b!<%I(B   fast mode)
	// if 1D  disk then set FFh  ( $BHs9bB.%b!<%I(B normal mode)
	// $BK\Ev$O!"9bB.%b!<%I$H(B $B%G%#%9%/%?%$%W$O4X78$J$$$N$G$9$,!"6/0z$K<BAu$7$^$7$?!#(B
     case 0x17:PRINTDEBUG(TEXT("SetSurfaceMode")); break;
     default:  printf("Not implimented %d",fdbuff_in[0]);break;
     }
  fdindex_out=0;

 SLEEP(10);
 PutDiskAccessLamp(0);
  //  dump( fdbuff_in , cmd_length[ fdbuff_in[0]]);
 }




// **********************************************
//     dump    for debug
// **********************************************
void dump(char *buff,int length)
 {
  int i;
  printf("%d\t ",length);
  for(i=0; i< length; i++)
      {
       printf("%02x ",*(buff+i) & 0xff);
      }
  printf("\n");
 }

