/*
    iP6 Plus 用のSR ROM を、iP6 Win や iP6 for Unix/X 用のROMに変換
    name is cnvroms.c
    Written by Windy
    Date is 2004/1/2
*/
/*
    現状では、saver3で取り込んだROMは iP6 Plus でしか使えませんが、
    それを、他のエミュレータでも使えるように変換するプログラムです。
    でも、他のエミュレータのSR対応も進めば、不要になっていくと思います。
*/
#include <stdio.h>
#include <string.h>

#define VERSION "1.0"

#define byte unsigned char

static  struct {
      char *out;	// output
      char *in;	    // input
      int start;	// start address of input
      int length;	// length of output
      }          roms[]={{"BASICROM" , "SYSTEMROM1" ,0x0000,0x8000},
                         {"KANJIROM" , "SYSTEMROM2" ,0x8000,0x8000},
					  	 {"VOICEROM" , "SYSTEMROM2" ,0x4000,0x4000},
					  	 {"SYSROM2"  , "SYSTEMROM2" ,0x2000,0x2000},
					  	 {"CGROM60"  , "CGROM68"    ,0x0000,0x2000},
					  	 {"CGROM66"  , "CGROM68"    ,0x2000,0x2000},};


static char *ext[]={".60",".62",".64",".66",".68"}; // extension
int convert_roms(int ver);

int main(void)
{
    static char buff[256];
    
    printf("\n");
    printf("--- SR Rom Converter -----------------------------Version %s \n" ,VERSION);
    printf("\n");
    printf("This is converter from iP6 Plus' SR ROM file to iP6Win or iP6 for Unix ROM file\n");
    printf("Convert ok? (y/n) ");
    fgets(buff,255,stdin);
    if( buff[0]=='Y' || buff[0]=='y') 
        {
        convert_roms(2);       // for PC-6001mk2SR
        convert_roms(4);       // for PC-6601SR
        }
    printf("Goodby....\n");
    return(0);
}


int convert_roms(int ver)
{
    int status;
    int ret;
    FILE *fp_in=NULL;
    FILE *fp_out=NULL;
    FILE *fp_test;
    static char filename_in[512];
    static char filename_out[512];
    int i,j;
    
    for(i=0; i<6; i++)
        {
         ret=1;
         sprintf(filename_in ,"%s%s",roms[i].in , ext[ver]);
         sprintf(filename_out,"%s%s",roms[i].out, ext[ver]);

         printf("Loading %14s...", filename_in);
         fp_in=fopen( filename_in,"rb");
         if( fp_in== NULL) {printf("NOT FOUND\n"); ret=0; continue;}

         printf("OK\t Saving  %14s...", filename_out);
         fp_test=fopen( filename_out,"rb");
         if( fp_test!= NULL) { fclose(fp_test);printf("EXIST!\nAlready rom file is exist, do nothing.\n");ret=0; break;}
         
         fp_out=fopen( filename_out,"wb");
         if( fp_out== NULL) {printf("FAILED\n"); ret=0; break;}
         
         status = fseek( fp_in, roms[i].start , SEEK_SET);
         if( status!=0) {printf("Error!  seek FAILED\n"); ret=0; break;}
         
         for(j=0; j< roms[i].length; j++)
            {
             int ch;
             ch = fgetc(fp_in);
             if( ch==EOF) {ret=0; printf("FAILED \nReached end of file from input\n");break;}
             ch = fputc(ch,fp_out);
             if( ch==EOF) {ret=0; printf("FAILED \nReached end of file from output\n");break;}
            }

         if( ret ==1)  {printf("OK\n");}
        }
    if( fp_out!=NULL) fclose(fp_out);
    if( fp_in!=NULL)  fclose(fp_in);
    return(ret);
}


