/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           d88.c                         **/
/**                                                         **/
/**                    access to d88 format                 **/
/** by Windy                                                **/
/*************************************************************/
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <assert.h>
#include "P6.h"
#include "Build.h"
#include "Option.h" // for ConfigWrite
#include "d88.h"
#include "message.h"

#include "os.h"


#define MAXTRACK  164
#define MAXSECTOR  16
#define MAXDATA   256


// ****************************************************************************
//           d88 $B%U%!%$%k!!%X%C%@!<(B
// ****************************************************************************
struct _head {
  char diskname[17];
  char reserve[9];
  char writeprotect;
  char disktype;
  unsigned long disksize;
} d88head;

unsigned long d88track[ MAXTRACK];

struct _sector {
  char  c;
  char  h;
  char  r;
  char  n;
  short sectors;
  char  mitudo;
  char  deleted;
  char  status;
  char  reserve[5];
  short size;
 } d88sector;

// ****************************************************************************
//           Endian Swaper
// ****************************************************************************
#ifndef LSB_FIRST
#define SWAPDW(val) ((((val) & 0x000000ff) << 24) + \
				     (((val) & 0x0000ff00) <<  8) + \
				     (((val) & 0x00ff0000) >>  8) + \
				     (((val) & 0xff000000) >> 24))

#define SWAPW(val)  ((((val) & 0x00ff) << 8) + \
				     (((val) & 0xff00) >> 8))
#else
#define SWAPDW(val) (val)
#define SWAPW(val)  (val)
#endif

// ****************************************************************************
//           internal variable
// ****************************************************************************
static int typetrack;	// 1: 1DDitt      2: Ditt 
static int typebad;		// 1: bad format  0: normal format
static int issys=-1;	// 1: system disk 0: other disk
static int isprotect;   // 1: write protect 0: writable 

// ****************************************************************************
//           internal function
// ****************************************************************************
void chk_tracktable(void);


#if 0
// ****************************************************************************
//           dump2
// ****************************************************************************
void dump2(char *buff,int length)
{
 int i,j;
 printf("------------------------------------------------------\n");
 for( i=0; i< length; i+=16)
   {
    printf("%04X :",i & 0xffff);
    for( j=0; j<16; j++)
      {
       char c= *(buff+i+j);
       printf("%02X ",c & 0xff);
      }
    for( j=0; j<16; j++)
      {
       char c= *(buff+i+j);
       printf("%c",c <0x20 ? 0x2e: c);
      }
    printf("\n");
//    if( i ==0x80|| i==0xf8) { getch();}
   }
}
#endif

// ****************************************************************************
//           read_d88head:
// ****************************************************************************

int read_d88head(void)
{
 static const TCHAR msg1D[]=TEXT("Warning!  Using 1D format disk when This machine type ,must convert file format.");
 static const TCHAR msg1DD[]=TEXT("Error!    Can't use 1DD format disk in this machine type. disk ejected.");


 if(Verbose) printf("Reading  header of D88 file..... ");
 if(DskStream[0]) {
      rewind( DskStream[0]);
      if( fread( &d88head, sizeof(d88head),1,DskStream[0]) !=1) { printf("read error d88head"); return(-1);}
      if( fread( &d88track, sizeof(d88track),1,DskStream[0])!=1){ printf("read error d88track");return(-1);}

#ifndef LSB_FIRST			// for MSB_FIRST  converter  2003/4/1
      {
      int i;
	  d88head.disksize  = SWAPDW( d88head.disksize );
	  for(i=0; i< MAXTRACK ; i++)
	  	{
		 d88track[i]= SWAPDW( d88track[i] );
		}
      }
#endif

	// ----------- if PC-6001mk2 and PC-6601 and 1DD then error -------------
      if(( P6Version==1 || P6Version ==3)&& is1DD())
                 { 
                  fclose( DskStream[0]); DskStream[0]=NULL; *DskName[0]=0;
                  _tprintf( msg1DD);
#ifdef WIN32
				  messagebox( _N(msg1DD), TEXT(PROGRAM_NAME) TEXT(" floppy disk information"));
				  ConfigWrite();
#endif
                  /* messagebox $B$d!"(BConfigWrite $B$O$3$3$G;H$&$Y$-$+!)(B */

                 }
	/* SR$B$G$N!"(B1D format $B$O!"(B1D->1DD $B%3%s%P!<%H$KI,MW$J$N$G!"7Y9p$@$1$K$7$F$*$/(B */
      	// ----------- if PC-6601SR and  1D then error --------------------------
      if(  P6Version==4 && !is1DD() && !isSYS()      )
                 {
                  _tprintf( msg1D );
#ifdef WIN32
				  messagebox( _N(msg1D), TEXT(PROGRAM_NAME) TEXT(" floppy disk information"));
				  ConfigWrite();
#endif
                 }
	  chk_tracktable();
       }

 if(Verbose && DskStream[0]) printf("Ok\n");
 return(0);
}

// ****************************************************************************
//           chk_tracktable
// ****************************************************************************
void chk_tracktable(void)
{
 int i;

 // ----------- check 1DDitt or Ditt? -----------------
 for(i=0; i< 10; i+=2)
 	{
	 if( d88track[i+0]!=0 && d88track[i+1]==0)
		   {
			typetrack=2;
		   }
	 else
		   {
	 		typetrack=1;
		   }
	}
 if( typetrack==2)
			printf("Ok\nThis disk seems to made by Ditt \n");
 else
			printf("Ok\nThis disk seems to made by 1DDitt \n");

 // ----------- bad track table? -----------------

 typebad=0;
 if( d88track[0]==0x2b0 && d88track[1]==0 && 
     d88track[2]==0x3c0 && d88track[3]==0 && d88track[4]==0x4d0)
 	{
    printf("Reparing disk track table ....");
	typebad=1; typetrack=2;
	 for(i=0; i< 35*2 ; i+=2)	// for 1D disk
 		{
		 d88track[i+0]= 0x2b0+ (i/2)*(256+16)*16;	// set correct track table
		 d88track[i+1]= 0;
		}
//    printf("Done.\n");
	}

#ifdef DEBUG2
	 for(i=0; i< 40*2 ; i++)
 		{
		 printf("%04X ",d88track[i]);
		}
  printf("typetrack =%d typebad=%d  \n",typetrack,typebad);
#endif
}


// ****************************************************************************
//           seek_d88
// ****************************************************************************
int seek_d88(int drive, int track, int sector)
{
	 int found=0;
	 int i;
	 unsigned long adr;

	/* -------------- seek   track --------------- */
	 adr= d88track[ track* typetrack];

	 if(!adr) return 0;
 	 if( fseek(DskStream[0], adr, SEEK_SET)!=0)
	   	  {
	   	   printf("seek_d88(): Seek error track=%d adr=%4lX  \n",track ,adr); 
	   	   return(0);
	   	  }

	/* -------------- search sector --------------- */
	 i=0;
	 do  {
		 if( fread( &d88sector ,sizeof(d88sector),1, DskStream[0])!=1)
        	  {
        	   printf("seek_d88(): Read error c=%d adr=%4lX \n",track,adr);
        	   return(0);
        	  }

#ifndef LSB_FIRST		// for MSB_FIRST		2003/4/1
	  d88sector.sectors = SWAPW ( d88sector.sectors );
	  d88sector.size    = SWAPW ( d88sector.size );
#endif

		//printf("c=%d h=%d r=%d n=%d\n",d88sector.c,d88sector.h,d88sector.r,d88sector.n);

		 if( track == d88sector.c && sector ==d88sector.r + typebad)
			{
			 fseek(DskStream[0], 0,SEEK_CUR);	// for fwrite
			 found =1;
		  // printf("seek_d88():  sector found! c=%d,h=%d,r=%d,n=%d\n",	d88sector.c ,d88sector.h,d88sector.r, d88sector.n);
			 break;
			}
	 	 if( fseek(DskStream[0], d88sector.size, SEEK_CUR)!=0)
	   	     {
	   	      printf("seek_d88(): fseek error track=%d  adr=%4lX\n",track,adr); 
	   	      return(0);
	   	     }
	   	  i++;
         }
	 while(i<d88sector.sectors);
//	 if(!found) printf("seek_d88(): sector not found track (C)=%d sector (R)=%d  max=%d\n",track,sector , d88sector.sectors);
	 return( found);
}

// ****************************************************************************
//           read_d88sector:
// ****************************************************************************
// input
//   len: input length
byte* read_d88sector(int len, int drive ,int track ,int sector)
{
		
    int i=0;
    int sectors;
    byte *p;
    static byte buff[ 20*256];

	int  size=0;

    memset(buff,0,sizeof(buff));
    if( DskStream[0])
       {
        p=buff;
        if( !seek_d88(drive,track,sector+i+1))
           	   {
           	    printf("read_d88sector(): seek error track=%d,sector=%d\n",track,sector); 
           	    return(buff);
           	    }
        
        sectors= len/ (d88sector.size / 0x100);	// $BFI$_9~$_%;%/%?!<?t(B $B5a$a$k(B
		// printf("len=%d, size=%d sectors=%d\n",len ,d88sector.size, sectors);
		for(i=0; i<sectors; i++)
		 {
		// printf("** seek_d88 c=%d h=%d r=%d\n",track,0,sector+i+1);
           if( !seek_d88(drive,track,sector+i+1))
           	   {
           	    printf("read_d88sector(): seek error track=%d,sector=%d\n",track,sector); 
           	    break;
           	    }
		// printf("read data\n");
           if( fread( p , d88sector.size ,1,DskStream[0])!=1)
               { 
               printf("read_d88sector(): Read error c=%d \n",track);
               break;
               }
           p   += d88sector.size;
		   size+= d88sector.size;
		   if( size > 19*256) break;	// against buffer-overflow! 2003/1/5
          }
       }


   return( buff);
//       dump( buff ,sizeof(sectordata[c][r]));
}


// ****************************************************************************
//           write_d88sector:
// ****************************************************************************
// write Data * kai
int write_d88sector(int len, int drive ,int track ,int sector, byte *buff)
{
    int i=0;
    int sectors;
    byte *p;

    if( DskStream[0]) {
        p=buff;
            
        if( !seek_d88(drive,track,sector+i+1))
           	   {
           	    printf("write_d88sector(): seek error track=%d,sector=%d\n",track,sector); 
           	    return(0);
           	    }
        
        sectors= len/ (d88sector.size / 0x100);	// $B=q$-9~$_%;%/%?!<?t(B $B5a$a$k(B
	//  printf("len=%d, size=%d sectors=%d\n",len ,d88sector.size, sectors);
		for(i=0; i<sectors; i++)
		 {
		// printf("** seek_d88 c=%d h=%d r=%d\n",track,0,sector+i+1);
           if( !seek_d88(drive,track,sector+i+1))
           	   {
           	    printf("[d88.c][write_d88sector]: seek error track=%d,sector=%d\n",track,sector); 
           	    break;
           	    }
		// printf("write data\n");
           if( fwrite( p , d88sector.size ,1,DskStream[0])!=1)
               { 
               printf("[d88.c][write_d88sector]: Write error c=%d \n",track);
               break;
               }
           p+= d88sector.size;
         }
            
            
#if 0
            PRINTDEBUG3(" (WRITE: track=%2d ,sector=%2d ) \n",track,sector+1);
           for(i=0; i< len ; i++)		// kaisuu kurikaesu
             {
              if( !seek_d88(drive,track,sector+i+1))
              	   { printf("write_d88sector(): seek failed \n"); return(-1);}
              if( fwrite( p , d88sector.size,1,DskStream[0])!=1)
                   { printf("write_d88sector(): Write error c=%d \n",track); return(-1);}
              p+= d88sector.size;
           }
#endif
        }
    return(0);
}


// ****************************************************************************
//           is1DD: $B%G%#%9%/$,!"(B1DD$B$+$I$&$+(B
// ****************************************************************************
int is1DD(void)
{
 int ret;

 ret=0;
 switch ( d88head.disktype )
	{
	case 0x00: ret=0; break; // 2D -->  1D  (by Ditt)
    case 0x10: ret=1; break; // 2DD--> 1DD  (by Ditt)
	case 0x30: ret=0; break; // 1D          (by 1DDitt)
    case 0x40: ret=1; break; // 1DD         (by 1DDitt)
    default:   ret=-1; printf("d88.c : Invalid disk type =%d\n",d88head.disktype); break;
	}
 return( ret);    // 1: 1DD    0: 1D
}

// ****************************************************************************
//           isSYS: $B%G%#%9%/$N@hF,(B (Track 0  Sector 1)$B$K!"(BSYS $B$,=q$+$l$F$$$k$+!)(B
// ****************************************************************************
int isSYS(void)
{
 // ------------- system disk? -----------------------
 if(issys==-1)
	 issys= !strncmp( (char *)read_d88sector(1,0,0,0),"SYS",3);

 return(issys);
}

// ****************************************************************************
//           setDiskprotect
//    1: write protected   0: normal		// 2003/1/21
// ****************************************************************************
void setDiskProtect(int Value)
{
 isprotect= Value;
}

// ****************************************************************************
//           getDiskprotect
//     1: write protected   0: normal
// ****************************************************************************
int getDiskProtect(void)
{
 return( isprotect);
}



// ****************************************************************************
//           create new d88 file
// In: type:  0  1D
//            1  1DD
// ****************************************************************************
int createNewD88(char *path , int type)
{
    FILE *fp;
    int  maxtrack;
    int  maxsector=  16;
    int  sectorsize=256;
    int  filldata;
    int  reservetrack;
	int  disktype;
    int  c,h,r,n;
    int  i;

    switch( type)
        {
         case 0: maxtrack=35; reservetrack=18; disktype=0x00; break;   // 1D
         case 1: maxtrack=80; reservetrack=37; disktype=0x10; break;   // 1DD
         default: return(0);
        }

	if(Verbose) printf("Creating a new file (%s) ...",path);
    fp= fopen( path ,"w+b");
    if( fp==NULL) {if(Verbose) printf("FAILED"); return(0);}
	if(Verbose) printf("OK\nFormatting %s ...",path);

    memset( &d88head  , 0,sizeof(d88head ));
    d88head.disktype= disktype;
    d88head.disksize= SWAPDW( 0x2b0+ (maxtrack)*(256+16)*16 );

    memset( &d88track , 0,sizeof(d88track));
    for(i=0; i< maxtrack*2 ; i+=2)
 		{
		 d88track[i+0]= SWAPDW( 0x2b0+ (i/2)*(256+16)*16 );	// set correct track table
		 d88track[i+1]= 0;
		}

    fwrite( &d88head  , sizeof(d88head) ,1,fp);
    fwrite( &d88track , sizeof(d88track),1,fp);
    
    h=0; n=1;
    memset( &d88sector, 0,sizeof(d88sector));
    for( c=0; c< maxtrack; c++)
        for( r=1; r<=16; r++)
            {
             d88sector.c=c; d88sector.h=h; d88sector.r=r; d88sector.n=n;
             d88sector.sectors= SWAPW(maxsector );
             d88sector.size   = SWAPW(sectorsize);
             fwrite( &d88sector , sizeof(d88sector),1,fp);

             
             for(i=0; i< sectorsize; i++)
                {
                 filldata= 0xff;
                 if( c== reservetrack)  // Reserve track?  (DIRECTORY / ID/ FAT)
                   {
                    if( r==13) {        // ID $B$N:n@.(B
                         filldata=0x00;
                        }
                    else if(r>=14) {    // FAT $B$N:n@.(B
                         if((i==reservetrack*2) || (i==reservetrack*2+1)) filldata=0xFE;
                        }
                   }
                 fputc( filldata ,fp);
                }
            }

    fclose(fp);
	if(Verbose) printf("OK\n");
	return(1);
}

