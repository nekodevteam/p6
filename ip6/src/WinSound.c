/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                         WinSound.c                      **/
/**                                                         **/
/** modified by windy 2002-2003                             **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
/*
int InitSound(void );       $B%5%&%s%I%I%i%$%P$r=i4|2=$7$^$9!#(B

void TrashSound(void);  	$B%5%&%s%I%I%i%$%P$N8e=hM}$r$7$^$9!#(B

void StopSound(void)        $B%5%&%s%I%9%l%C%I$r0l;~Dd;_$7$^$9!#(B

void ResumeSound(void)      $B0l;~Dd;_$7$?(B $B%5%&%s%I%9%l%C%I$r:F3+$7$^$9!#(B

static DWORD WINAPI SoundMainLoop( LPVOID param)

	$B%5%&%s%I%G%P%$%9$,!!<!$N%5%&%s%I%G!<%?$r=PNO$9$k%?%$%_%s%0$r!!4F;k$9$k%9%l%C%I$G$9!#(B
	$BI,MW$G$"$l$P!"<!$N%G!<%?$r(B fmgen $B$G@8@.$7$F!"(B
	$B%5%&%s%I%G%P%$%9$K=PNO$7$^$9!#(B

static void play_sub(short *src,int len)

	$B%5%&%s%I%G%P%$%9$K=PNO$7$^$9!#%P%C%U%!%j%s%0$b$7$F$$$^$9!#(B

----------------------------------------------------------------------
Special Thanks:

$B%A%g%3$\$s$5$s$K!"%9%l%C%I$rDI2C$7$F$b$i$$$^$7$?!#(B(^^; 
CPU$B@jM-N($NDc2<$K4sM?$7$F$$$^$9!#(B
$B$"$j$,$H$&$4$6$$$^$7$?!#(B(2003/11)
*/
#if !defined(_WIN32_WCE)
#define USECREATE
#endif	// !defined(_WIN32_WCE)

#ifdef WIN32

#include "Sound.h"

// **************************************************
//		Variables
// **************************************************
int UseSound=1;

#ifdef SOUND

#include "P6.h"
#include "Z80.h"
#include <stdlib.h>
#include <stdio.h>

#include <assert.h>
#include <windows.h>
#include "fm.h"
#include "Win32.h"

// --------------- WIN32 Variables -------------
HWAVEOUT     hwo;					// sound device handle
WAVEFORMATEX wfx;					// format setting
WAVEHDR      whdr[SOUND_NUM_OF_BUFS];	// data block

static short pcmBuffer[ _countof(whdr)][ SOUND_BUFSIZE];  // pcm buffer

#ifdef USECREATE
static int SoundThreadRun;
static DWORD dwThreadId1;	        	// id of thread
static HANDLE hThread1;					// handle of thread
#endif	// USECREATE

// **************************************************
//		functions
// **************************************************
static void play_sub(HWAVEOUT hwo, WAVEHDR *wh);

#ifdef USECREATE
static DWORD WINAPI SoundMainLoop( LPVOID param);
#else	// USECREATE
static void CALLBACK WaveOutCallback(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2);
#endif	// USECREATE

// **************************************************
//		Trash sound
// **************************************************
void TrashSound(void)
{
	printf("Closing sound system...");
#ifdef USECREATE
	if(hThread1 != NULL) {
		SoundThreadRun=0;			/* thread aborting...*/
		Sleep(1000);
	    CloseHandle( hThread1 );
	    hThread1 = NULL;
	}
#endif	// USECREATE

	if (hwo)
	{
		unsigned int i;
	    for(i=0; i< _countof(whdr); i++)
		    waveOutUnprepareHeader(hwo,&whdr[i],sizeof(WAVEHDR));
	    waveOutClose( hwo);
	    hwo = NULL;
	}
	printf("Done\n");
}



// **************************************************
//		Init Sound
// **************************************************
int InitSound(void )
{
  unsigned int i;
  MMRESULT mmRes;

  if(!UseSound) return(0);
  if(Verbose) printf("Starting sound server:");

  ym2203_init();

 // -------------- start Thread -------------------------------------
  if(Verbose) { printf("OK\n  create Sound Thread...\n"); }

#ifdef USECREATE
  SoundThreadRun=1;	/* True: Thread run   False: Thread abort */
  hThread1= CreateThread(NULL,0,
      (LPTHREAD_START_ROUTINE) SoundMainLoop ,
      0 /*(LPVOID)&hwndMain*/,
      0, 
      &dwThreadId1);
  if( !hThread1 )
  	{
	  putlasterror();
	  return(0);
	}
#endif // USECREATE

 // -------------- open device --------------------------
   if(Verbose) printf("OK\nOpening Sound device...");
     wfx.wFormatTag    = WAVE_FORMAT_PCM;
     wfx.nChannels     = CHANNEL;
     wfx.nSamplesPerSec= SOUND_RATE;		// sampling rate freq.
     wfx.wBitsPerSample= SAMPLING_BITS;		// sampling bit
     wfx.nBlockAlign   = wfx.nChannels * wfx.wBitsPerSample / 8;
     wfx.nAvgBytesPerSec=wfx.nSamplesPerSec * wfx.nBlockAlign;
     wfx.cbSize = 0;

#ifdef USECREATE
	 mmRes = waveOutOpen( &hwo, WAVE_MAPPER, &wfx, dwThreadId1, 0, CALLBACK_THREAD );
#else	// USECREATE
	mmRes = waveOutOpen( &hwo, WAVE_MAPPER, &wfx, (DWORD_PTR)WaveOutCallback, 0, CALLBACK_FUNCTION );
#endif	// USECREATE
	 if( mmRes != MMSYSERR_NOERROR) {
			messagebox(TEXT("wave device open error \n"),TEXT(""));
	        return(0);
			}

  if(Verbose) puts("OK");

 // -------------- pomping sound buffers  --------------------------
  ZeroMemory(&whdr, sizeof(whdr));
  for(i=0 ; i< _countof(whdr); i++)
	{
	whdr[i].lpData = (char*)pcmBuffer[ i];
	whdr[i].dwBufferLength = sizeof( pcmBuffer[ i]);
	memset(whdr[i].lpData, 0,sizeof( pcmBuffer[ i]));

	 mmRes = waveOutPrepareHeader(hwo, &whdr[i],sizeof(WAVEHDR));
	 if( mmRes != MMSYSERR_NOERROR) {
			messagebox(TEXT("waveOutPrepareHeader(): failed!!\n"),TEXT(""));
	 	        return(0);
			}
	// ******************** play **********************************
	 mmRes = waveOutWrite(        hwo, &whdr[i],sizeof(WAVEHDR));
	 if( mmRes != MMSYSERR_NOERROR) {
			messagebox(TEXT("waveOutWrite(): failed!!"),TEXT(""));
		        return(0);
			}
	}


  return(1);
}

void StopSound(void) { ym2203_mute(1); }
void ResumeSound(void) { ym2203_mute(0); }

#ifdef USECREATE
// **************************************************
//	Sound Thread
// **************************************************
static DWORD WINAPI SoundMainLoop( LPVOID param)
{
  MSG Msg;

  while( SoundThreadRun)
    {
	  if(GetMessage(&Msg, 0,0,0)) 
		 {
		  switch( Msg.message)
			{
			case MM_WOM_DONE:  //printf("thread) done\n");
                    play_sub((HWAVEOUT)Msg.wParam, (WAVEHDR *)Msg.lParam);
//					testPutData((char*)soundbuf, sizeof(soundbuf));
					break;

			default: break;
		   }
		}
     }   	// while(1)
 return(0);
}
#else	// USECREATE
static void CALLBACK WaveOutCallback(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2)
{
	if ((uMsg == WOM_DONE) && (dwParam1))
	{
		play_sub(hwo, (WAVEHDR *)dwParam1);
	}
}
#endif	// USECREATE

// **************************************************
//	   play sub
// ******y********************************************
static void play_sub(HWAVEOUT hwo, WAVEHDR *wh)
{
	MMRESULT mmRes;

     mmRes= waveOutUnprepareHeader( hwo, wh, sizeof(WAVEHDR));
	 if( mmRes != MMSYSERR_NOERROR) {
			printf("waveOutUnPrepareHeader(): failed!!\n");
			}

	ym2203_makewave((short *)(wh->lpData), sizeof(pcmBuffer[0]) * (2 / CHANNEL));
	wh->dwBufferLength = sizeof(pcmBuffer[0]);

	 mmRes = waveOutPrepareHeader(hwo, wh,sizeof(WAVEHDR));
	 if( mmRes != MMSYSERR_NOERROR) {
			messagebox(TEXT("wave device PrepareHeader: failed!!"),TEXT(""));
	 	    return;
			}
	// ******************** play **********************************
	 mmRes = waveOutWrite(        hwo, wh,sizeof(WAVEHDR));
	 if( mmRes != MMSYSERR_NOERROR) {
			messagebox(TEXT("wave device Write failed!!"),TEXT(""));
		    return;
			}
}

#endif // SOUND
#endif // WIN32
