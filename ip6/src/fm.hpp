/**
 * @file	fm.hpp
 * @brief	FM 音源クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <limits.h>
#include "Sound.h"
#include "misc/arithmetic.hpp"
#include "misc/guard.hpp"
#include "misc/ringbuffer.hpp"

#ifdef USE_FMLITE
#include "misc/fmlite/opnlite.hpp"
typedef OpnLite OpnClass;
#else	// USE_FMLITE
#include "fmgen/headers.h"
#include "fmgen/opna.h"
typedef FM::OPN OpnClass;
#endif	// USE_FMLITE

/**
 * @brief FM 音源クラス
 */
class Fm
{
private:
	/**
	 * @brief インターバル
	 */
	struct Interval
	{
		unsigned int nStep;				/*!< ステップ */
		unsigned int nFraction;			/*!< あまり */

		/**
		 * コンストラクタ
		 * @param[in] nNumber Hz 分子
		 * @param[in] nDenom Hz 分母
		 */
		Interval(unsigned int nNumer, unsigned int nDenom)
			: nStep((nNumer << 12) / nDenom)
			, nFraction(0)
		{
		}

		/**
		 * 設定
		 * @param[in] nNumber Hz 分子
		 * @param[in] nDenom Hz 分母
		 */
		void Set(unsigned int nNumer, unsigned int nDenom)
		{
			nStep = (nNumer << 12) / nDenom;
		}

		/**
		 * ステップ
		 * @param[in] n カウント
		 * @return 増加値
		 */
		unsigned int Process(unsigned int n)
		{
			this->nFraction += n * this->nStep;
			const unsigned int nRet = this->nFraction >> 12;
			this->nFraction &= (1 << 12) - 1;
			return nRet;
		}
	};

	bool m_bEnabled;					/*!< 有効 */
	bool m_bMuted;						/*!< ミュート */
	unsigned int m_nCurrentLine;		/*!< ライン数 */
	unsigned int m_nLastStatusLine;		/*!< ステータス ライン数 */
	unsigned int m_nLastStreamLine;		/*!< ストリーム ライン数 */
	Interval m_intervalStatus;			/*!< ステータス */
	Interval m_intervalStream;			/*!< インターバル */
	Guard m_cs;							/*!< ガード */
	OpnClass m_opn;						/*!< OPN クラス */
	RingBuffer m_buffer;				/*!< リング バッファ */

	/**
	 * タイマを進める
	 */
	void ProcessTimer()
	{
		const unsigned int nPastLines = m_nCurrentLine - m_nLastStatusLine;
		if (nPastLines)
		{
			m_nLastStatusLine = m_nCurrentLine;
			m_opn.Count(m_intervalStatus.Process(nPastLines));
		}
	}

	/**
	 * ミックス
	 * @param[out] pData データ
	 * @param[in] cbData サイズ
	 */
	void Mix(short* pData, unsigned int cbData)
	{
		const unsigned int nTotalSamples = cbData / (sizeof(*pData) * CHANNEL);
		unsigned int nIndex = 0;
		while (nIndex < nTotalSamples)
		{
			const unsigned int nSlice = 256;

			const unsigned int nRemain = nTotalSamples - nIndex;
			const unsigned int nSamples = minimum(nRemain, nSlice);
			nIndex += nSamples;

			signed int buffer[nSlice * 2];
			memset(buffer, 0, nSamples * sizeof(buffer[0]) * 2);
			m_opn.Mix(buffer, nSamples);

			for (unsigned int i = 0; i < nSamples; i++)
			{
#if (CHANNEL >= 1)
				pData[0] = static_cast<short>(clamp(buffer[(i * 2) + 0], SHRT_MIN, SHRT_MAX));
#endif	// (CHANNEL >= 1)
#if (CHANNEL >= 2)
				pData[1] = static_cast<short>(clamp(buffer[(i * 2) + 1], SHRT_MIN, SHRT_MAX));
#endif	// (CHANNEL >= 2)
				pData += CHANNEL;
			}
		}
	}

	/**
	 * ストリームを作成する
	 */
	void ProcessStream()
	{
		const unsigned int nPastLines = m_nCurrentLine - m_nLastStreamLine;
		if (nPastLines)
		{
			m_nLastStreamLine = m_nCurrentLine;
			unsigned int nRemainSamples = m_intervalStream.Process(nPastLines);

			if (!m_bMuted)
			{
				while (nRemainSamples)
				{
					unsigned int nFreeBytes = 0;
					short* pBuffer = static_cast<short*>(m_buffer.Lock(nFreeBytes));
					if (pBuffer == NULL)
					{
						break;
					}
					const unsigned int nFreeSamples = nFreeBytes / (sizeof(*pBuffer) * CHANNEL);
					const unsigned int nSamples = minimum(nRemainSamples, nFreeSamples);
					Mix(pBuffer, nSamples * sizeof(*pBuffer) * CHANNEL);
					m_buffer.Unlock(nSamples * (sizeof(*pBuffer) * CHANNEL));
					nRemainSamples -= nSamples;
				}
			}
		}
	}

public:
	/**
	 * コンストラクタ
	 */
	Fm()
		: m_bEnabled(false)
		, m_bMuted(false)
		, m_nCurrentLine(0)
		, m_nLastStatusLine(0)
		, m_nLastStreamLine(0)
		, m_intervalStatus(1000000, 60 * 262)
		, m_intervalStream(SOUND_RATE, 60 * 262)
	{
	}

	/**
	 * デストラクタ
	 */
	~Fm()
	{
	}

	/**
	 * 初期化
	 * @retval true 成功
	 * @retval false 失敗
	 */
	inline bool Initialize()
	{
		const bool r = m_opn.Init(3993600, SOUND_RATE, true);
		if (r)
		{
			m_opn.Reset();
			m_opn.SetVolumeFM(12);
			m_opn.SetVolumePSG(12);
			m_buffer.Allocate(SOUND_NUM_OF_BUFS * SOUND_BUFSIZE * sizeof(short));
		}
		m_bEnabled = r;
		return r;
	}

	/**
	 * ミュートする
	 * @param[in] bMute ミュート
	 * @return 以前のミュート状態
	 */
	inline bool Mute(bool bMute)
	{
		const bool bRet = m_bMuted;
		m_bMuted = bMute;
		return bRet;
	}

	/**
	 * ライン数をインクリメント
	 */
	inline void IncLine()
	{
		m_nCurrentLine++;
	}

	/**
	 * 一画面のライン数
	 * @param[in] nLines ライン数
	 */
	inline void SetLines(unsigned int nLines)
	{
		m_intervalStatus.Set(1000000, 60 * nLines);
		m_intervalStream.Set(SOUND_RATE, 60 * nLines);
	}

	/**
	 * リード ステータス
	 * @return ステータス
	 */
	inline uint_fast8_t ReadStatus()
	{
		if (m_bEnabled)
		{
			ProcessTimer();
			return m_opn.ReadStatus();
		}
		else
		{
			return 0;
		}
	}

	/**
	 * リード レジスタ
	 * @param[in] r レジスタ
	 * @return データ
	 */
	inline int ReadRegister(uint_fast8_t r)
	{
		if (m_bEnabled)
		{
			return m_opn.GetReg(r);
		}
		else
		{
			return 0;
		}
	}

	/**
	 * レジスタ書き込み
	 * @param[in] r レジスタ
	 * @param[in] v データ
	 */
	inline void WriteRegister(uint_fast8_t r, uint_fast8_t v)
	{
		if (m_bEnabled)
		{
			m_cs.lock();
			ProcessStream();
			m_cs.unlock();
			m_opn.SetReg(r, v);
		}
	}

	/**
	 * ストリームを取得する
	 * @param[in] pData データ
	 * @param[in] cbData データ サイズ
	 */
	inline void Get(void* pData, unsigned int cbData)
	{
		/* リング バッファから取得 */
		m_cs.lock();
		const unsigned int ret = m_buffer.Read(pData, cbData);
		if (ret < cbData)
		{
			/* 残りは作成 */
			m_nLastStreamLine = m_nCurrentLine;
			if (!m_bMuted)
			{
				Mix(reinterpret_cast<short*>(static_cast<char*>(pData) + ret), cbData - ret);
			}
			else
			{
				memset(static_cast<char*>(pData) + ret, 0, cbData - ret);
			}
		}
		m_cs.unlock();
	}
};
