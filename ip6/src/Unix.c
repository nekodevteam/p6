/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Unix.c                        **/
/**                                                         **/
/** Modified by Windy 2002-2003                             **/
/** This code is written by ISHIOKA Hiroshi 1998-2000       **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
#ifdef UNIX
/** Standard Unix/X #includes ********************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#undef   GLOBAL
#include "P6.h"
#define  GLOBAL
#include "Sound.h"
#include "Unix.h"
#include "Xconf.h"
#include "Build.h"
#include "error.h"

/** MIT Shared Memory Extension for X ************************/
#ifdef MITSHM
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
XShmSegmentInfo SHMInfo;
int UseSHM=1;
/** The following is used for MITSHM-auto-detection: **/
int ShmOpcode;       /* major opcode for MIT-SHM */
int (*OldHandler)(Display *, XErrorEvent *);
#endif

const char *Title=PROGRAM_NAME " Unix/X  " BUILD_NO; // add 2002/4/3 by Windy
static char TitleBuf[256];

/** Variables to pass options to the X-drivers ***************/
int SaveCPU=1;
int do_install, do_sync;

/** X-related definitions ************************************/
#define KeyMask KeyPressMask|KeyReleaseMask
#define OtherMask FocusChangeMask|ExposureMask|StructureNotifyMask
#define MyEventMask KeyMask|OtherMask

/** Various X-related variables ******************************/
char *Dispname=NULL;
Display *Dsp;
Window Wnd;
Atom DELETE_WINDOW_atom, WM_PROTOCOLS_atom;
Colormap CMap;
int CMapIsMine=0;
GC DefaultGC;

XImage *ximage;

/* variables related to colors */
XVisualInfo VisInfo;
long VisInfo_mask=VisualNoMask;
byte Xtab[4];
static XID white,black;

#if 0
//ColTyp BPal[16],BPal11[4],BPal12[8],BPal13[8],BPal14[4],BPal15[8],BPal53[32],BPal62[32],BPal61[16];
//byte *XBuf;
// int bitpix;
// Bool lsbfirst;
#endif

void InitColor(int);
/*void TrashColor(void);*/
static void reget_shm();
static void OnKey(XKeyEvent* ev);
static void OnFocusOut(void);

/* size of window */
Bool wide,high;
// int Width,Height; /* this is ximage size. window size is +20 */

/** Various variables and short functions ********************/

byte *Keys;
//int scr4col = 0;

void OnBreak(int Arg) { CPURunning=0; }

static unsigned int s_lastFrameTick;

/**
 * $B&LIC$rF@$k(B
 * @return $B&LIC(B(32bit)
 */
static unsigned int GetMicrosecond()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec * 1000 * 1000) + tv.tv_usec;
}

/**
 * $B%9%j!<%W$9$k(B
 * @param[in] until $B;XDj;~4V$^$G%9%j!<%W$9$k(B
 * @return $BBT$C$?;~4V(B(us)
 */
static unsigned int SleepUntil(unsigned int until)
{
	unsigned int ret = 0;
	do
	{
		int us = s_lastFrameTick - GetMicrosecond();
		if (us <= 0)
		{
			break;
		}
		if (us > 1000) us = 1000;
		ret += us;
		usleep(us);
	} while (1 /*CONSTCOND*/);
	return ret;
}

/**
 * 1$B%U%l!<%`BT$D(B
 * @return $BBT$C$?;~4V(B(ms)
 * @retval -1 $B=hM}Mn$A(B
 */
int unixIdle(void)
{
	const unsigned int now = GetMicrosecond();
	const unsigned int past = now - s_lastFrameTick;
	if (past >= (1000 * 1000 * MAX_AUTOPERIOD / 60))
	{
		s_lastFrameTick = now;
		return -1;
	}
	else
	{
		s_lastFrameTick += (1000000 / 60);
		return SleepUntil(s_lastFrameTick) / 1000;
	}
}

void PutImage_roll(void)
{
#ifdef MITSHM
	if(UseSHM) 
	    XShmPutImage
	      (Dsp,Wnd,DefaultGC,ximage,0,0,10,10,Width,Height,False);
  	else
#endif
	XPutImage(Dsp,Wnd,DefaultGC,ximage,0,0,10,10,Width,Height);
	XFlush(Dsp);
}

/** PUTIMAGE: macro copying image buffer into a window ********/
/** Hardware scroll not Implimented   $B%O!<%I%&%(%"%9%/%m!<%k<BAu$9$k$3$H(B*/
/** 204 line        not Implimented   204$B%i%$%s<BAu$9$k$3$H(B */
static void PutImageInner(void)
{
#ifdef MITSHM
	if(UseSHM) 
	    XShmPutImage
	      (Dsp,Wnd,DefaultGC,ximage,0,0,10,10,Width,Height,False);
	  else
#endif
	XPutImage(Dsp,Wnd,DefaultGC,ximage,0,0,10,10,Width,Height);
  	XFlush(Dsp);
}

void PutImage(void)
{
	while (XEventsQueued(Dsp, QueuedAlready) > 0)
	{
		XEvent ev;
		XNextEvent(Dsp, &ev);
		switch (ev.type)
		{
			case KeyPress:
			case KeyRelease:
				OnKey((XKeyEvent *)&ev);
				break;

			case FocusOut:
				OnFocusOut();
				break;

			case ClientMessage:
				if ((ev.xclient.message_type == WM_PROTOCOLS_atom) && (ev.xclient.data.l[0] == DELETE_WINDOW_atom))
				{
					CPURunning = 0;
				}
				break;

			default:
				break;
		}
	}

	PutImageInner();
}

/*
void RefreshScreen(void)
{
  if (!Mapped) return;
  PutImageInner();
}
*/

/** Keyboard bindings ****************************************/
#include "Keydef.h"

/** TrashMachine *********************************************/
/** Deallocate all resources taken by InitMachine().        **/
/*************************************************************/
void TrashMachine(void)
{
	if(Verbose) printf("Shutting down...\n");

	/*TrashColor();*/
	if(Dsp&&Wnd)
		{
#ifdef MITSHM
	    if(UseSHM)
		    {
		      XShmDetach(Dsp,&SHMInfo);
		      if(SHMInfo.shmaddr) shmdt(SHMInfo.shmaddr);
		      if(SHMInfo.shmid>=0) shmctl(SHMInfo.shmid,IPC_RMID,0);
			}
	    else
#endif // MITSHM
	    if(ximage) XDestroyImage(ximage);
		}
	if(CMapIsMine) XFreeColormap(Dsp, CMap);
	if(Dsp) XCloseDisplay(Dsp);

#ifdef SOUND
	StopSound();TrashSound();
#endif // SOUND
}


static struct vc {
    int clasnum;
    char *name;
} visualClasses[6]=
  {
      { StaticGray, "StaticGray"},
      { GrayScale, "GrayScale"},
      { StaticColor, "StaticColor"},
      { PseudoColor, "PseudoColor"},
      { TrueColor, "TrueColor"},
      { DirectColor, "DirectColor"}
  };

void SetDepth(int x)
{
    VisInfo.depth=x;
    VisInfo_mask |= VisualDepthMask;
}

void SetScreen(int x)
{
    VisInfo.screen=x;
    VisInfo_mask |= VisualScreenMask;
}

void SetVisual(char *s)
{
    int i;
    char *vc, *sp;

    for (i=0; i<6; i++) {
        for (  vc=visualClasses[i].name, sp=s ; (*vc!=0) ; vc++, sp++ )
	    if (tolower(*vc)!=tolower(*sp))
		goto next;
#ifdef __cplusplus
	VisInfo.c_class=visualClasses[i].clasnum;
#else	// __cplusplus
	VisInfo.class=visualClasses[i].clasnum;
#endif	// __cplusplus
	VisInfo_mask |= VisualClassMask;
	return;
      next: ;
    }
    printf("%s is not a known visual class.\n",s);
}

void SetVisualId(long x)
{
    VisInfo.visualid=(VisualID) x;
    VisInfo_mask |= VisualIDMask;
}

/* Function to choose a PixMapFormat and set some variables associated
   with them.  Note that the depth comes from a visual and is
   therefore supported by the screen */
int ChooseFormat(void)
{
	int i;
	int nForm; /* # pixmapformats, depth */
	XPixmapFormatValues *flist, choice; /* list of format describers
					  and choice */
	ColTyp T;
	char *order;

	flist=XListPixmapFormats(Dsp,&nForm);
	if (!flist) return (0); 

	choice.bits_per_pixel=choice.depth=0;
	for (i=0;i<nForm;i++)
	    {
	    if (flist[i].depth==VisInfo.depth)
		choice=flist[i];
    	}

	bitpix=choice.bits_per_pixel; 

	XFree((void *)flist);
	if (!bitpix) {
    	if (Verbose &1) printf("FAILED\n"); 
    	return (0);
  		}

	order="";
	if (bitpix>8)
		{
    	order=(ImageByteOrder(Dsp)==LSBFirst) ? "LSB first" : "MSB first";
    	/* how to swap the bytes ? */
    	T.ct_xid = (ImageByteOrder(Dsp)==LSBFirst)? 0x03020100L : 0x00010203L ;
    	if (ImageByteOrder(Dsp)==MSBFirst)
		for (i=bitpix ; i<32 ; i+=8)
		    T.ct_xid=(T.ct_xid>>8)+((T.ct_xid&0xFF)<<24);
    	/* if T were represented by the bytes 0 1 2 3 
    	   then no swapping would be needed.. */
		for (i=0;i<4;i++) 
    		Xtab[T.ct_byte[i]]=i;
		/*  Xtab[j]==i means that byte i in the representation of ColTyp used
	    by the machine on which fmsx is running, corresponds to byte j in
    	the representation of ColTyp in the XImage 
		*/
  		}
	if (bitpix<8)
	    order=(lsbfirst=(BitmapBitOrder(Dsp)==LSBFirst)) 
		? "lsb first" : "msb first"; 
	  if (Verbose & 1) 
	      printf("depth=%d, %d bit%s per pixel (%s)\n", VisInfo.depth,bitpix,
		     (bitpix==1)?"":"s",order);
	  choosefuncs(lsbfirst,bitpix);
	  /*setwidth(wide);*/
      setwidth(scale-1);

	return 1;
}

int ChooseVisual(void)
{
    int n_items,i;
    XVisualInfo *VisInfo_l;

    if (Verbose&1) printf("OK\n  Choosing Visual...");

    if (VisInfo_mask!=VisualNoMask) {
	VisInfo_l=XGetVisualInfo(Dsp, VisInfo_mask, &VisInfo, &n_items);
	if (n_items!=0) {
	    if ( (Verbose&1) && (n_items>1)) 
		printf("%d matching visuals...",n_items);
	    VisInfo=VisInfo_l[0]; /* this may not be the best match */
	    XFree(VisInfo_l);
	    goto found;
	} else 
	    if (Verbose&1)
		printf("no matching visual...");
    }

    if (Verbose&1)
	printf("using default...");

    VisInfo.screen= DefaultScreen(Dsp);
    VisInfo.visualid=DefaultVisual(Dsp, VisInfo.screen)->visualid;
    VisInfo_l=XGetVisualInfo(Dsp, VisualIDMask|VisualScreenMask, 
			     &VisInfo, &n_items);
    if (n_items!=0) {
	VisInfo=VisInfo_l[0]; /* this is probably the only match */
	XFree(VisInfo_l);
	goto found;
    }

    if (Verbose&1)
	printf("FAILED\n");
    return 0;

    found:
    for (i=6 ; i ; )
#ifdef __cplusplus
	if (visualClasses[--i].clasnum==VisInfo.c_class)
#else	// __cplusplus
	if (visualClasses[--i].clasnum==VisInfo.class)
#endif	// __cplusplus
	    break;

    if (Verbose&1) {
	printf("\n    visualid=0x%lx: %s, ", VisInfo.visualid, 
	       (i>=0) ? visualClasses[i].name : "unknown");
    }

    return 1;
}

#ifdef MITSHM
/* Error handler, used only to trap BadAccess errors on XShmAttach() */
int Handler(Display * d, XErrorEvent * Ev)
{ 
	if ( (Ev->error_code==10) /* BadAccess */ && 
    	 (Ev->request_code==(unsigned char) ShmOpcode) &&
    	 (Ev->minor_code==1)  /* X_ShmAttach */ )
    	UseSHM=0; /* X-terminal apparently remote */
  	else return OldHandler(d,Ev);
  	return 0;
}

/* Function that tries to XShmAttach an XImage, returns 0 in case of
faillure */
int TrySHM(int depth, int screen_num)
{
	long size;

	if(Verbose & 1)
	    printf("  Using shared memory:\n    Creating image...");
	ximage=XShmCreateImage(Dsp,VisInfo.visual,depth,
                           ZPixmap,NULL,&SHMInfo,Width,Height);
	if(!ximage) return (0); 

	size=(long)ximage->bytes_per_line*ximage->height;
	if(Verbose & 1) printf("OK\n    Getting SHM info for %ld %s...",
			 (size&0x03ff) ? size : size>>10,
			 (size&0x03ff) ? "bytes" : "kB" );

	SHMInfo.shmid=shmget(IPC_PRIVATE, size, IPC_CREAT|0777);
	if(SHMInfo.shmid<0) return (0);

	if(Verbose & 1) printf("OK\n    Allocating SHM...");
	XBuf=(byte *)(ximage->data=SHMInfo.shmaddr=(char *)shmat(SHMInfo.shmid,0,0));
	if(!XBuf) return (0);

	SHMInfo.readOnly=False;
	if(Verbose & 1) printf("OK\n    Attaching SHM...");
	OldHandler=XSetErrorHandler(Handler);
	if (!XShmAttach(Dsp,&SHMInfo)) return(0);
	XSync(Dsp,False);
	XSetErrorHandler(OldHandler);
	return(UseSHM);
}
#endif

/*  Inform window manager about the window size, 
    attributes and other hints
**/
void SetWMHints(void) 
{
    XTextProperty win_name,icon_name;
    char * value[1];
    XSizeHints *size;
    XWMHints *hint;
    XClassHint *klas;
    char *argv[]= {NULL};
    int argc=0;
    
    size=XAllocSizeHints();
    hint=XAllocWMHints();
    klas=XAllocClassHint();
    
    size->flags=PSize|PMinSize|PMaxSize;
    size->min_width=size->max_width=size->base_width=Width+20;
    size->min_height=size->max_height=size->base_height=Height+20;
    
    hint->input=True;
    hint->flags=InputHint;
    
    /*value[0]=Title;*/
    value[0]=TitleBuf;
    XStringListToTextProperty(value, 1, &win_name);
    value[0]="iP6";
    XStringListToTextProperty(value, 1, &icon_name);
    
    klas->res_name="iP6";
    klas->res_class="IP6";
    
    XSetWMProperties(Dsp, Wnd, &win_name, &icon_name, argv, argc,
		     size, hint, klas);
    XFree(size); 
    XFree(hint);
    XFree(klas);

    DELETE_WINDOW_atom=XInternAtom(Dsp, "WM_DELETE_WINDOW", False);
    WM_PROTOCOLS_atom =XInternAtom(Dsp, "WM_PROTOCOLS", False);

    XChangeProperty(Dsp, Wnd, WM_PROTOCOLS_atom , XA_ATOM, 32,
		    PropModeReplace, (unsigned char*) &DELETE_WINDOW_atom,1);

}

/** InitMachine **********************************************/
/** Allocate resources needed by Unix/X-dependent code.     **/
/*************************************************************/
int InitMachine(void)
{
	int K,L;
	int depth, screen_num;
	Window root;

	signal(SIGINT,OnBreak);
	signal(SIGHUP,OnBreak);
	signal(SIGQUIT,OnBreak);
	signal(SIGTERM,OnBreak);

#ifdef SOUND
	if(UseSound) InitSound();
#endif // SOUND
	Width=M6WIDTH*scale;
	Height=M6HEIGHT*scale;
	if(Verbose)
	    printf("Initializing Unix/X drivers:\n  Opening display...");
	Dsp=XOpenDisplay(Dispname);

	if(!Dsp) { if(Verbose) printf("FAILED\n");return(0); }
	if (do_sync) XSynchronize(Dsp, True);

	if (!ChooseVisual()) 
      	return 0;
	screen_num=VisInfo.screen;  depth=VisInfo.depth;
	root=RootWindow(Dsp, screen_num);

  	if (!ChooseFormat()) 
      return 0;

	white=XWhitePixel(Dsp,screen_num);
	black=XBlackPixel(Dsp,screen_num);
	DefaultGC=DefaultGC(Dsp,screen_num);
	if ( do_install 
         ||(VisInfo.visualid != DefaultVisual(Dsp,screen_num)->visualid)) {
      		CMap=XCreateColormap(Dsp, root, VisInfo.visual, AllocNone);
      		CMapIsMine=1;
		 } 
	else CMap=DefaultColormap(Dsp,screen_num);

	if (Verbose & 1) 
	      printf("  Using %s colormap (0x%lx)\n", 
		  CMapIsMine?"private":"default", CMap);

		{
    	XSetWindowAttributes wat;
    
    	if(Verbose & 1) printf("  Opening window...");
    
    	wat.colormap=CMap;
    	wat.event_mask=MyEventMask;
    	wat.background_pixel=0;
/*      wat.background_pixmap=None; */
    	wat.border_pixel=0;
/*      wat.border_pixmap=None; */

    	Wnd=XCreateWindow(Dsp,root,0,0,Width+20,Height+20,0,depth,InputOutput,
		      VisInfo.visual, 
		      CWColormap|CWEventMask|CWBackPixel|CWBorderPixel, &wat);
  		}
	strcpy(TitleBuf, Title); /* added */
	SetWMHints();
	if(!Wnd) { if(Verbose & 1) printf("FAILED\n");return(0); }
	if(Verbose & 1) printf("OK\n");

	InitColor(screen_num); 

	XMapRaised(Dsp,Wnd);
	XClearWindow(Dsp,Wnd);

#ifdef MITSHM
	if(UseSHM) /* check whether Dsp supports MITSHM */
	    UseSHM=XQueryExtension(Dsp,"MIT-SHM",&ShmOpcode,&K,&L);
	if(UseSHM)
		{ 
        if(!(UseSHM=TrySHM(depth, screen_num))) /* Dsp might still be remote */ 
			{
      		if(SHMInfo.shmaddr) shmdt(SHMInfo.shmaddr);
      		if(SHMInfo.shmid>=0) shmctl(SHMInfo.shmid,IPC_RMID,0);
      		if(ximage) XDestroyImage(ximage);
      		if(Verbose & 1) printf("FAILED\n");
    		}
  		} 
	if (!UseSHM)
#endif
		{
    	long size;
    
    	size=(long)sizeof(byte)*Height*Width*bitpix/8;
    	if(Verbose & 1) printf("  Allocating %ld %s RAM for image...",
			   (size&0x03ff) ? size : size>>10,
			   (size&0x03ff) ? "bytes" : "kB");
    	XBuf=(byte *)malloc(size);
    	if(!XBuf) { if(Verbose & 1) printf("FAILED\n");return(0); }


    	if(Verbose & 1) printf("OK\n  Creating image...");
    	ximage=XCreateImage(Dsp,VisInfo.visual,depth,
                        ZPixmap,0,(char *)XBuf,Width,Height,8,0);
    	if(!ximage) { if(Verbose & 1) printf("FAILED\n");return(0); }
  		}
	if(Verbose & 1) printf("OK\n");

  	build_conf();

  	s_lastFrameTick = GetMicrosecond();

	return(1);
}


/** Keyboard *************************************************/
/** Check for keyboard events, parse them, and modify P6    **/
/** keyboard matrix.                                        **/
/*************************************************************/
void Keyboard(void)
{
}

static void OnKey(XKeyEvent* ev)
{
	int J = XLookupKeysym(ev,0);

    	/* for stick,strig */
	    {
      	byte tmp;
      	switch(J) {
        	case XK_space  : tmp = STICK0_SPACE; break;
        	case XK_Left   : tmp = STICK0_LEFT; break;
        	case XK_Right  : tmp = STICK0_RIGHT; break;
        	case XK_Down   : tmp = STICK0_DOWN; break;
        	case XK_Up     : tmp = STICK0_UP; break;
        	case XK_Pause  : tmp = STICK0_STOP; break;
        	case XK_Shift_L: 
        	case XK_Shift_R: tmp = STICK0_SHIFT; break;
        	default: tmp = 0;
			}
		if(ev->type==KeyPress) 
        	stick0 |= tmp;
		else
            stick0 &= ~tmp;
    	}
    	/* end of for stick,strig */

		if(ev->type==KeyPress)
    		{
      		switch(J)
      			{
	  			case XK_F9:			/* reset sample code */
	  				if( stick0 & STICK0_SHIFT)
						{
				 		stick0=0;
				 		ResetZ80();
						}
					break;  
				case XK_F10:
#ifdef SOUND
					StopSound();          /* Silence the sound        */
#endif
					run_conf();
					ClearScr();
#ifdef SOUND
					ResumeSound();        /* Switch the sound back on */
#endif
					break;
#ifdef DEBUG
      			case XK_F11: Trace=!Trace;break;
#endif
      			case XK_F12: CPURunning=0;break;

      			case XK_Control_L: kbFlagCtrl=1;break;
      			case XK_Alt_L: kbFlagGraph=1;break;

      			case XK_Insert: J=XK_F13;break; /* Ins -> F13 */
      			}

		if((P6Version==0)&&(J==0xFFC6)) J=0; /* MODE key when 60 */
   		J&=0xFF;
		if (kbFlagGraph)
			Keys = Keys7[J];
      	else if (kanaMode)
			if (katakana)
	  			if (stick0 & STICK0_SHIFT) 
                	Keys = Keys6[J];
	  			else
                	Keys = Keys5[J];
			else
	  			if (stick0 & STICK0_SHIFT) 
            		Keys = Keys4[J];
				else 
            		Keys = Keys3[J];
      		else
				if (stick0 & STICK0_SHIFT) 
                	Keys = Keys2[J];
				else 
                	Keys = Keys1[J];
		keyGFlag = Keys[0]; p6key = Keys[1];

      /* control key + alphabet key */
		if ((kbFlagCtrl == 1) && (J >= XK_a) && (J <= XK_z))
			{keyGFlag = 0; p6key = J - XK_a + 1;}
      /*if (p6key != 0x00) IFlag = 1;*/
		if (Keys[1] != 0x00) 
        	KeyIntFlag = INTFLAG_REQ;
	    } else {
     	 	if (J==XK_Alt_L) kbFlagGraph=0;
			if (J==XK_Control_L) kbFlagCtrl=0;
    	}
}

static void OnFocusOut()
{
	if(SaveCPU)
	{
		XEvent ev;
#ifdef SOUND
		StopSound();          /* Silence the sound        */
#endif
		while(!XCheckWindowEvent(Dsp,Wnd,FocusChangeMask,&ev)&&CPURunning)
		{
			if(XCheckWindowEvent(Dsp,Wnd,ExposureMask,&ev)) PutImageInner();
			usleep(1000);
		}
#ifdef SOUND
	    ResumeSound();        /* Switch the sound back on */
#endif
	}
}


/* Below are the routines related to allocating and deallocating
   colors */

/* set up coltable, alind8? etc. */
void InitColor(int screen_num)
{
	ColTyp col;
	XColor Color;
	register byte i,j;
	register word R,G,B,H;
#if 0
	int param[16][3] = /* {R,G,B} */
    { {0,0,0},{4,3,0},{0,4,3},{3,4,0},{3,0,4},{4,0,3},{0,3,4},{3,3,3},
      {0,0,0},{4,0,0},{0,4,0},{4,4,0},{0,0,4},{4,0,4},{0,4,4},{4,4,4} };
    /*
    { {0,1,0},{4,3,0},{0,4,3},{3,4,0},{3,0,4},{4,0,3},{0,3,4},{3,3,3},
      {0,1,0},{4,0,0},{0,4,0},{4,4,0},{0,0,4},{4,0,4},{0,4,4},{1,4,1} };
      */
	unsigned short trans[] = { 0x0000, 0x3fff, 0x7fff, 0xafff, 0xffff };
#endif
	int Pal11[ 4] = { 15, 8,10, 8 };
	int Pal12[ 8] = { 10,11,12, 9,15,14,13, 1 };
	int Pal13[ 8] = { 10,11,12, 9,15,14,13, 1 };
	int Pal14[ 4] = {  8,10, 8,15 };
	int Pal15[ 8] = {  8,13,11,10, 8,13,10,15 };
	int Pal53[32] = {  0, 4, 1, 5, 2, 6, 3, 7, 8,12, 9,13,10,14,11,15,
		    10,11,12, 9,15,14,13, 1,10,11,12, 9,15,14,13, 1 };

	for(H=0;H<2;H++)
	  for(B=0;B<2;B++)
    	for(G=0;G<2;G++)
    	  for(R=0;R<2;R++)
        	{
          	Color.flags=DoRed|DoGreen|DoBlue;
          	i=R|(G<<1)|(B<<2)|(H<<3);
          	Color.red=trans[param[i][0]];
          	Color.green=trans[param[i][1]];
          	Color.blue=trans[param[i][2]];
          	col.ct_xid=XAllocColor(Dsp,CMap,&Color)? Color.pixel:black;
	  		if (bitpix <= 8) 
	    		BPal[i].ct_byte[0]=col.ct_xid;
	  		else 
	    		for(j=0; j<4; j++)
	      			BPal[i].ct_byte[j]=col.ct_byte[Xtab[j]];
        	}
	/* setting color list for each screen mode */
	for(i=0;i<4;i++) BPal11[i] = BPal[Pal11[i]];
	for(i=0;i<8;i++) BPal12[i] = BPal[Pal12[i]];
	for(i=0;i<8;i++) BPal13[i] = BPal[Pal13[i]];
	for(i=0;i<4;i++) BPal14[i] = BPal[Pal14[i]];
	for(i=0;i<8;i++) BPal15[i] = BPal[Pal15[i]];
	for(i=0;i<32;i++) BPal53[i] = BPal[Pal53[i]];

	// ******************* fast palet      add 2002/9/27 *********
	for(i=0;i<32;i++) BPal62[i] = BPal53[i];  // for RefreshScr62/63
	for(i=0;i<16;i++) BPal61[i] = BPal[i];    // for RefreshScr61

}



/** ClearScr: clear a window *********************************/
void ClearScr(void)
{
	memset(XBuf,(byte)black,Width*Height*bitpix/8);
}

void SetTitle(fpos_t pos)
{
	static long oldpos = 0;
	const long curpos = *(long *)&pos;
	if (oldpos != curpos)
	{
		oldpos = curpos;
		sprintf(TitleBuf, "%s  [%ldcnt.]", Title, curpos);
		SetWMHints();
	}
}

// **********************************************************
//        resize a window
//     $BAk$NBg$-$5$r(B $BF0E*$KJQ99$9$k!#(B
//
// In:  bitmap_scale: 1 or 2
//      win_scale:    1 or 2
// Written by Windy
// Problem: MITSHM $B$r$D$+$C$F$$$k$H!"$H$F$bCY$/$J$k!#(B
// **********************************************************
/* win_scale :not Implimented $B$G$9!#(B $B<BAu$7$h$&!*(B(^^;;*/
int resizewindow(int bitmap_scale, int win_scale)
{
	int ret;
	ret=0;
	scale= bitmap_scale;
	if( scale==1)		// Interlace on/off
	 	 IntLac = 0;
	else if(scale==2)
 		 IntLac = 1;
 
	Width = M6WIDTH* scale;		// change size
	Height= M6HEIGHT* scale;

#ifdef MITSHM
    if( UseSHM)
      {
       reget_shm();				// reget shared memory (very slowly)
       SetWMHints();
       XResizeWindow( Dsp,Wnd,Width,Height);		// resize window
       ret= 1;
      }
	if(!UseSHM)
#endif
     {
       int size;
        SetWMHints();

        if( XBuf!=NULL) free( XBuf);
        size=(long)sizeof(byte)*Height*Width*bitpix/8;
        XBuf=(byte *)malloc( size);
        if( XBuf !=NULL) {
        	ximage=XCreateImage(Dsp,VisInfo.visual,VisInfo.depth,
                        ZPixmap,0,(char *)XBuf,Width,Height,8,0);		// re-Making image
        	XResizeWindow( Dsp,Wnd,Width+20,Height+20);		// resize window
        	setwidth( scale-1);
        	debug_printf("Resized: width=%d\n",Width);
        	ret=1;
           }
       }
return( ret);
}


// **********************************************************
//    MITSHM $B6&M-%a%b%j$r(B $B0lC6GK4~$7$F!"<hF@$7$J$*$7$F$_$k(B   (test code)
// **********************************************************
static void reget_shm()
{
  int K,L;
  int depth, screen_num;
#ifdef MITSHM
    if(UseSHM)
    {
      XShmDetach(Dsp,&SHMInfo);
      if(SHMInfo.shmaddr) shmdt(SHMInfo.shmaddr);
      if(SHMInfo.shmid>=0) shmctl(SHMInfo.shmid,IPC_RMID,0);
    }

      if(UseSHM) /* check whether Dsp supports MITSHM */
           UseSHM=XQueryExtension(Dsp,"MIT-SHM",&ShmOpcode,&K,&L);
      if(UseSHM)
         { if(!(UseSHM=TrySHM(VisInfo.depth, screen_num))) /* Dsp might still be remote */
          {
           if(SHMInfo.shmaddr) shmdt(SHMInfo.shmaddr);
           if(SHMInfo.shmid>=0) shmctl(SHMInfo.shmid,IPC_RMID,0);
           if(ximage) XDestroyImage(ximage);
           if(Verbose & 1) printf("FAILED\n");
         }
       }
#endif
}



// ******************************** textout ******************************
int textout(int x,int y,char *str)
{
	return(0);		// not implimented
}

// ******************************** messagebox ******************************
int messagebox(char *str, char *title)
{
	return(0);		// not implimented
}

/* ************************** set menu title ****************************** */
/* set filename to open menu */
int setMenuTitle(int type)
{
	return(0);		// not implimented
}

/* *********************** make extkanjirom ****************************** */
/* In: mem  EXTKANJIROM
  Out: ret  1:success  0: failed  */
int make_extkanjirom(char *mem)
{
	return(0);		// not implimented
}

/* *********************** JoystickGetState ****************************** */
int JoystickGetState(int joy_no)
{
	return(0);		// not implimented
}

// ****************************************************************************
//          ClearWindow: $B%&%$%s%I%&A4BN$r%/%j%"(B
// ****************************************************************************
void ClearWindow(void)
{
}

// ****************************************************************************
//          ClearStatusBar: $B%9%F!<%?%9%P!<$r%/%j%"(B
// ****************************************************************************
void ClearStatusBar(void)
{
}

// ****************************************************************************
//          sleepus: $BL2$k(B
// ****************************************************************************
void SLEEP(int s)
{
	const unsigned int us = s * 1000;
	s_lastFrameTick += us;
	SleepUntil(us + GetMicrosecond());
}

// ****************************************************************************
//          putStatusBar: $B%9%F!<%?%9%P!<I=<((B
// ****************************************************************************
void putStatusBar(void)
{
}

// ****************************************************************************
//          outputdebugstring: $B%G%P%C%,$K%a%C%;!<%8$rAw$k(B
// ****************************************************************************
void outputdebugstring(char *buff)
{
}

// ****************************************************************************
//          PutDiskAccessLamp: $B%G%#%9%/$N%"%/%;%9%i%s%W!!E@Et(B/$B>CEt(B
// ****************************************************************************
void PutDiskAccessLamp(int sw)
{
}

#endif // UNIX
