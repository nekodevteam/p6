/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           fdc.c                         **/
/**                                                         **/
/**               Internal Floppy disk Controller           **/
/** by Windy                                                **/
/*************************************************************/
/*
FDC (Floppy Disk Controller) -  uPD765A

$B%(%_%e%l!<%?=hM}$KIU$$$F(B

   $B3F%3%^%s%I$O5,Dj$ND9$5$^$G=q$-9~$^$l$?;~E@$G!"<B9T$7$^$9!#(B
   CPU$B$+$i$N%G!<%?$N<uEO$7$O!"(B1024$B%P%$%H$N(B FD BUFFER$B$r2p$7$F9T$o$l$^$9!#(B

$B$"$H!"<B5!$G$O!"(BFDC$B$H!"(BFDD BUFFER$B4V$N%G!<%?E>Aw$O(B DMA$B$G9T$C$F$$$k$h$&$G$9!#(B
$B$7$+$7!"%(%_%e%l!<%?$G$O!"<jH4$-$r$7$F$$$^$9!#(B
$B%G%#%9%/$+$iFI$_<h$C$?%G!<%?$O!"$9$0$K(B FDD BUFFER$B$K=q$-9~$s$G$$$^$9!#(B
$B$@$+$i!"(BDMA$B$,E>AwCf$rI=$9%U%i%0$b!"$"$,$k$3$H$OM-$j$^$;$s!&!&!#(B($B4@(B)

---------------------------------------------------------------------------
$B4XO"(B I/O PORT $B0lMw(B

B1H:

    b3 b2     B1   B0
    0  TYPE  DIR   DMA
	    |     |     ---- DMA SW 0: ON              1:OFF
	    |     ----DMA DIRECTION 0: CPU->FD BUFFER  1:FD BUFFER ->CPU
	    --------- FDD TYPE      0: $BFbB!(B            1:$B30IU$1(B

B2H: bit0=0$B$G(B DMA$BE>AwCf$G$9!#(B
   ($B",$?$@$7!"$3$N%(%_%e%l!<%?%3!<%I$G$O!">o$K#1$K$7$F$*$+$J$$$HF0$+$J$$!#!K(B


B3H: FDC-IF$B@)8f$G!"(BB2H$B$rFI$`A0$K(Bbit0=0$B$G=PNO$9$k$H%9%F%$%?%9$N<}F@$,2DG=$K$J$k(B


D0H: FDD BUFFER $B$N(B0000H - 00FFH
D1H: FDD BUFFER $B$N(B0100H - 01FFH
D2H: FDD BUFFER $B$N(B0200H - 02FFH
D3H: FDD BUFFER $B$N(B0300H - 03FFH

CPU$B$,!"(B4$B8DFI$_<h$k>l9g$O!"(BD3H,D2h,D1h,D0h $B$N=gHV$KFI$_<h$k!#(B
$B0J2<!"(B
3$B8DFI$_<h$k>l9g$O!"(B    D2h,D1h,D0h$B$HFI$_<h$k!#(B
2$B8DFI$_<h$k>l9g$O!"(B        D1h,D0h$B$HFI$_<h$k!#(B
1$B8DFI$_<h$k>l9g$O!"(B            D0h$B$HFI$_<h$k!#(B

$B$?$@$7!"(BFDD Buffer$B$X$N%"%/%;%9$O!"(BSTACK$B$N$h$&$K!"8eF~$l@h$@$7$G9T$o$l$k!#(B
FDC$B$+$i!"(BFDD Buffer$B$XE>Aw$9$k$H$-$O!"(BSTACK$B$r(Bpush$B$9$k$h$&$K!"3JG<$7$F$$$-!"(B
FDD Buffer$B$+$i!!(BCPU$B$XE>Aw$9$k$H$-$O!"(BSTACK$B$r(Bpop $B$9$k$h$&$K!"=P$7$F$$$/!#(B


D4H: FDD$B$N(BReady$B?.9f(B?	bit1$B$,%I%i%$%V(B2 bit0$B$,%I%i%$%V(B1	 0$B$N$H$-(Bready$B$_$?$$(B

D6H: FDD$B$N(BMortorOn$B?.9f(B?	bit1$B$,%I%i%$%V(B2 bit0$B$,%I%i%$%V(B1	 0$B$N$H$-(Bmotor on$B$_$?$$(B

D8H: $B=q$-9~$_Jd=~2sO)$N(Bon/off ? 	bit0$B$,(B0$B$N$H$-(Bon$B$K$J$k$_$?$$(B

DAH: FD BUFFER $B$N%G!<%?E>AwNL;XDj!)(B  100h$BC10L$G;XDj$9$k!#(B

DEH: $BFf(B


----------------------------------------------------------------------------
*/
#include <stdio.h>
#include <memory.h>
#include "Z80.h"
#include "P6.h"
#include "d88.h"
#include "error.h"

#include "os.h"


//************ FDC Status *******************
#define FDC_BUSY              0x10
#define FDC_READY             0x00
#define FDC_NON_DMA           0x20
#define FDC_FD2PC             0x40
#define FDC_PC2FD             0x00
#define FDC_DATA_READY        0x80

//************* Result Status 0 *************
#define ST0_NOT_READY         0x08
#define ST0_SEEK_END          0x20
#define ST0_FAILED_CMD        0x40
#define ST0_INVALID_CMD       0x80

//************* Result Status 1 *************
#define ST1_NOT_WRITABLE      0x2

//************* Result Status 2 *************

//************* Result Status 3 *************
#define ST3_TRACK0             0x10
#define ST3_READY              0x20
#define ST3_WRITE_PROTECT      0x40
#define ST3_FAULT              0x80

struct _fdc {
    int cylinder;	// current cylinder (seek command)
    int length;		// transfer length
} fdc;


char fdc_buff[4][256+1];     // FD data Buffer
int    idx[4];               // FD data Buffer Index

char fdc_buff_in[10+1];		// FD cmd Buffer
int    idx_in;
char fdc_buff_out[10+1];	// FD cmd Buffer
int    idx_out;

static int   cmd_length[]={0,0,0,3,2,9,9,2,1,0,0,0,0,6,0,3};


//************* internal function *************
void fdc_command(void);
void fdc_read_data(char *fdc_buff_in);
void fdc_write_data(char *fdc_buff_in);


extern void PutDiskAccessLamp(int sw);


// ************************************
//      Initialize
// ************************************
void fdc_init(void)
{
 int i;
 portDC= FDC_DATA_READY | FDC_READY | FDC_PC2FD;
 for(i=0; i<4;i++)
      {
       idx[ i]=0;
       memset( fdc_buff[i], 0 ,256);
      }
 memset( fdc_buff_in, 0,10);
 idx_in=0;
}


// ************************************
//      push data to FD Buffer      OUT (0xD0...0xD3)
// ************************************
void fdc_push_buffer( int port ,byte Value)
{
 port=port & 0xf;
 if(idx[ port]>255) {PRINTDEBUG(TEXT("[fdc.c][fdc_push_buffer] out of range: push_buffer port=%X PC=%X\n"),port,getPC()); return;}
 fdc_buff[ port ] [ idx[ port]]= Value;
 idx[ port]++;
}

// ************************************
//      pop data from FD Buffer      IN (0xD0...0xD3)
// ************************************
byte fdc_pop_buffer( int port)
{
 byte Value;
 Value= NORAM;
 port=port & 0xf;
  if(idx[ port]<1) {PRINTDEBUG(TEXT("[fdc.c][fdc_pop_buffer] out of range: pop_buffer  port=%X PC=%X\n"),port,getPC()); return(Value);}
  //if(idx[ port]==1) {printf("pop_buffer  recieve done port=%X PC=%X\n",port,getPC());}
 idx[ port]--;

  Value = fdc_buff[ port] [ idx[ port]];
  return ( Value);
}


// ************************************
//      push status to FD Buffer Out
// ************************************
void fdc_push_status( int Value)
{
 fdc_buff_out[ idx_out++]= Value;
}

// ************************************
//      pop status from FD Buffer Out
// ************************************
byte fdc_pop_status(void)
{
 return(fdc_buff_out[ --idx_out]);
}

// ************************************
//	FDC transfer sectors  fdd buffer$B$NE>AwNL(B
// ************************************
void fdc_outDA(byte Value)
{
 fdc.length = ~(Value-0x10);
 //printf("fdc.length= %x  \n",fdc.length );
}

// ************************************
//	FDC status
// ************************************
byte fdc_inpDC(void)
{
 return( portDC);
}

// ************************************
//	FDC write
// ************************************
void fdc_outDD(byte Value)
{
 fdc_buff_in[ idx_in++]= Value;
 //printf( "cmd_len=%d %d\n",cmd_length[ fdc_buff_in[ 0] & 0xf],idx_in);
 if( cmd_length[ fdc_buff_in[ 0] & 0xf] ==idx_in)	// $B%3%^%s%ID9$N%3%^%s%I$,Mh$?$i(B $B%3%^%s%I<B9T(B
     {
      fdc_command();
     }
}

// ************************************
//	FDC  read
// ************************************
byte fdc_inpDD(void)
{
// printf("r=%X\n", fdc_buff_out[ idx_out]);
//  printf("idx_out=%d ", idx_out);
// ********************* PC$BB&$,!"A4$F<u?.$7$?$i!"(BPC2FD $B$K$9$k!#(B*********
  if( idx_out==1)  { portDC= FDC_DATA_READY | FDC_PC2FD; PRINTDEBUG(TEXT("[fdc.c][fdc_inpDD] Receive done..\n"));}
  if( idx_out<=0)  { printf("FDC read : out of range %d",idx_out);}

//  return( fdc_buff_out[ idx_out--]);
  return(fdc_pop_status());
}

// ************************************
//	FDC  Command Execute
// ************************************
void fdc_command(void)
{
 int cmdno;
 static int pre_cmdno=-1;
 
 cmdno= fdc_buff_in[0] & 0xf;
 PRINTDEBUG(TEXT("[fdc.c][fdc_command] "));
 switch( cmdno )
      {
       case 0x03: PRINTDEBUG(TEXT("Specify  "));
                  break;
       case 0x05: PRINTDEBUG(TEXT("Write Data "));
                  fdc_write_data(fdc_buff_in);
                  break;
       case 0x06: PRINTDEBUG(TEXT("Read Data  "));
                  fdc_read_data( fdc_buff_in);
                  break;
       case 0x08: PRINTDEBUG(TEXT("Sense Interrupt Status "));
              if( pre_cmdno != cmdno)
                 {
       			  if( disk_num /*DskStream*/ )     // disk ok?
	                fdc_push_status( fdc.cylinder); // disk ok
	              else
	                fdc_push_status( 255);	// disk non
	              
	              if( pre_cmdno != cmdno)
	                  fdc_push_status( ST0_SEEK_END );	// normal
	             }
              else
	              {
	                  fdc_push_status( ST0_SEEK_END |ST0_INVALID_CMD );
	              }
                  portDC=FDC_DATA_READY | FDC_FD2PC;  // for RESULT
                  break;

       case 0x0D: PRINTDEBUG(TEXT("Write ID    "));
                  break;	 // Format is Not Implimented

       case 0x07: PRINTDEBUG(TEXT("Recalibrate ")); 
                  fdc_buff_in[2]=0x00;		// Seek to TRACK0
       case 0x0F: PRINTDEBUG(TEXT("Seek "));
                  fdc.cylinder= fdc_buff_in[2];
                  break;
       default:   printf("Not Impliment cmd=%X",fdc_buff_in[0]);
                  break;
      }
  pre_cmdno = cmdno;
  PRINTDEBUG(TEXT("\n"));
 
//  dump3( fdc_buff_in ,cmd_length[ fdc_buff_in[0]&0xf] );
  idx_in=0;

}


// ************************************
//	read data
// ************************************
void fdc_read_data(char *fdc_buff_in)
{
 byte *p;
 int d,c,r;
 int i;

 if( DskStream[0])
    {
     PutDiskAccessLamp(1);

     SLEEP( 50); 		// $B%&%(%$%H=hM}(B   2003/10/5
     

	 d= fdc_buff_in[1];
	 c= fdc.cylinder;
	 c= (!is1DD() && isSYS() && P6Version==4) ? c / 2: c;   // if 1D and sys-disk and PC-6601SR then c= c/2
	 r= fdc_buff_in[4]-1;

	 p= read_d88sector( fdc.length, d,c,r);

//	 for(i= fdc.length-1; i>=0; i--)
	 for(i= 0; i< fdc.length; i++)

	    {
#if 0					// test test code ------------------------
		 if( c>=3)
		   {
		 	for(j=255; j>=0; j--)
		 		{
		 		 fdc_buff[i][j]=*p;
		 		 p++;
		 		}
	    	 idx[i]=256;
		    }
		 else	
#endif
		 	{
		     memcpy( fdc_buff[ i], p,256);
	    	 idx[i]=256;
		     p+=256;
			}

//	     for(j=0; j<256; j++) { fdc_push_buffer(i,*p); p++;}
//	      memcpy( fdc_buff[ i], read_d88sector(1 ,d ,c ,r+i), 256);
//	      idx[i]=256;
	    }
    }

 // ---------- Result Status -------------
 fdc_push_status( 0);  // n
 fdc_push_status( 0);  // r
 fdc_push_status( 0);  // h
 fdc_push_status( fdc.cylinder );  // c
 fdc_push_status( 0);  // st2
 fdc_push_status( 0);  // st1
 if( DskStream[0])
	 fdc_push_status( 0);  // st0
 else
	 fdc_push_status( ST0_FAILED_CMD);  // st0	media not ready
	

 PutDiskAccessLamp(0);
 portDC=FDC_DATA_READY| FDC_FD2PC;	// for RESULT
}

// ************************************
//	write data
// ************************************
void  fdc_write_data(char *fdc_buff_in)
{
 int d,c,r;
 int i,j;
 byte buff[256*4];
 byte *p;

 if( DskStream[0] && !getDiskProtect())	// check write protect
    {
     PutDiskAccessLamp(1);
     SLEEP( 50); 		// $B%&%(%$%H=hM}(B    2003/10/5

	 p= buff;
     d= fdc_buff_in[1];
     c= /*fdc_buff_in[2]; */ fdc.cylinder;
     r= fdc_buff_in[4]-1;
//     for(i= fdc.length-1; i>=0; i--) 
	 for(i= 0; i< fdc.length; i++)
       {
         for(j=0; j<256;j++) { *p=fdc_pop_buffer(i); p++; }	// write data
       }
     write_d88sector( fdc.length, d ,c ,r,  buff);


    }

 // ---------- Result Status -------------
 fdc_push_status( 0);  		// n
 fdc_push_status( 0);  		// r
 fdc_push_status( 0);  		// h
 fdc_push_status( fdc.cylinder );  // c
 fdc_push_status( 0);  		// st2

 if( !getDiskProtect())
	 fdc_push_status( 0);  	// st1
 else
     fdc_push_status( ST1_NOT_WRITABLE ); // st1   not writeable 2003/1/21

 if( DskStream[0])
	 fdc_push_status( 0);  // st0
 else
	 fdc_push_status( ST0_FAILED_CMD);  // st0	media not ready

 PutDiskAccessLamp(0);
 portDC=FDC_DATA_READY| FDC_FD2PC;	// for RESULT
}



// **********************************************
//     dump    for debug
// **********************************************
void dump3(char *buff,int length)
 {
  int i;
  printf("\t ");
  for(i=0; i< length; i++)
      {
       printf("%02x ",*(buff+i) & 0xff);
      }
  printf("\n");
 }


#if 0
     for(i= fdc.length-1; i>=0; i--) 
       {
         for(j=0; j<256;j++) { buff[ j ]=fdc_pop_buffer(i); }	// write data
         write_d88sector(1, d ,c ,r+i,  buff);
       }
#endif
