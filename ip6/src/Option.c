/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Option.c                      **/
/** modified by Windy 2002                                  **/
/** This code is based on ISHIOKA Hiroshi 1998-2000         **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
/* TO DO :  Win32 $B$+$iC&5Q$9$k!#(B*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#ifdef _WIN32
#include <direct.h>
#else	/* _WIN32 */
#include <unistd.h>
#endif	/* _WIN32 */

#include "P6.h"
#include "Sound.h"
#include "Refresh.h"
#include "Help.h"
#include "Option.h"

#include "os.h"

extern int SaveCPU;

#ifdef MITSHM
extern int UseSHM;
#endif

extern int isFullScr;


// ****************************************************************************
//          Internal variable
// ****************************************************************************
static FILE *configStream;
static TCHAR ConfigPath[PATH_MAX];	// config file path


// ****************************************************************************
//          CONFIG FILE OPTIONS
// ****************************************************************************
static const LPCTSTR ConfigOptions[]=
{ 
TEXT("[CONFIG]"),TEXT("P6Version="),TEXT("UseSound="),TEXT("disk_num="),TEXT("CPUclock="),TEXT("scale="),TEXT("intLac="),
TEXT("CasPath="),TEXT("DskPath="),TEXT("CasName="),TEXT("DskName="),TEXT("scr4col="),TEXT("drawWait="),TEXT("FastTape="),
TEXT("UPeriod="),TEXT("extkanjirom="),TEXT("extram="),TEXT("srline="),
TEXT("ImgPath="),TEXT("MemPath="),
TEXT("Ext1Name="),TEXT("Ext2Name="),TEXT("dummy="),
TEXT("CasPath(save)="),TEXT("CasName(save)="),
TEXT("UseSaveTapeMenu="),TEXT("UseStatusBar="),
TEXT("UseDiskLamp="),
NULL
};


// ****************************************************************************
//          ConfigInit
// ****************************************************************************
void ConfigInit(void)
{
//#ifdef WIN32
 ConfigPath[0]=0;
#ifdef _WIN32
 if( _tgetcwd(ConfigPath, PATH_MAX)==NULL)
#else	// _WIN32
 if( getcwd(ConfigPath, PATH_MAX)==NULL)
#endif	// _WIN32
	{
	 messagebox(TEXT("getcwd failed"),TEXT(""));
     printf("getcwd failed\n");
	}
 lstrcat(ConfigPath,TEXT("\\ip6kai.ini"));
//#endif
}




// ****************************************************************************
//          ConfigWrite
// ****************************************************************************
int ConfigWrite(void)
{
//#ifdef WIN32
// char tmpCasName[PATH_MAX],tmpDskName[ PATH_MAX];

 configStream= (FILE*)_tfopen( ConfigPath,TEXT("w"));
 if(configStream==NULL) { messagebox(TEXT("Cannot write config file "),TEXT(""));return(0);}
    
 if(!WaitFlag) 	sw_nowait_mode(0);// NOWAIT???E?c?A?e?U?Anormal speed?E?s?$B!&(B?B2003/8/24   Thanks  Bernie

 _ftprintf(configStream,TEXT("[CONFIG]\n"));
 _ftprintf(configStream,TEXT("P6Version=%d\n"),newP6Version);
#ifdef SOUND
 _ftprintf(configStream,TEXT("UseSound=%d\n") ,UseSound);
#else
 _ftprintf(configStream,TEXT("UseSound=%d\n") ,0);
#endif
 _ftprintf(configStream,TEXT("disk_num=%d\n") ,new_disk_num);

 _ftprintf(configStream,TEXT("CPUclock=%d\n") ,CPUclock);
 _ftprintf(configStream,TEXT("scale=%d\n")    ,scale);
 _ftprintf(configStream,TEXT("intLac=%d\n")   ,IntLac);
 _ftprintf(configStream,TEXT("CasPath=%s\n")  ,CasPath[0]);
 _ftprintf(configStream,TEXT("DskPath=%s\n")  ,DskPath[0]);
 _ftprintf(configStream,TEXT("CasName=%s\n")  ,CasName[0]);
 _ftprintf(configStream,TEXT("DskName=%s\n")  ,DskName[0]);
 _ftprintf(configStream,TEXT("scr4col=%d\n")  ,scr4col);
 _ftprintf(configStream,TEXT("drawWait=%d\n") ,drawwait );
 _ftprintf(configStream,TEXT("FastTape=%d\n") ,FastTape );
 _ftprintf(configStream,TEXT("UPeriod=%d\n")  ,UPeriod );
 _ftprintf(configStream,TEXT("extkanjirom=%d\n")  ,new_extkanjirom );
 _ftprintf(configStream,TEXT("extram=%d\n")  ,new_extram );
 _ftprintf(configStream,TEXT("srline=%d\n")  ,srline );
 _ftprintf(configStream,TEXT("ImgPath=%s\n")  ,ImgPath );
 _ftprintf(configStream,TEXT("MemPath=%s\n")  ,MemPath );
 _ftprintf(configStream,TEXT("Ext1Name=%s\n") ,Ext1Name );
 _ftprintf(configStream,TEXT("Ext2Name=%s\n") ,Ext2Name );

 _ftprintf(configStream,TEXT("CasPath(save)=%s\n")  ,CasPath[1]);
 _ftprintf(configStream,TEXT("CasName(save)=%s\n")  ,CasName[1]);
 _ftprintf(configStream,TEXT("UseSaveTapeMenu=%d\n")  ,UseSaveTapeMenu);
 _ftprintf(configStream,TEXT("UseStatusBar=%d\n")  ,UseStatusBar);

 _ftprintf(configStream,TEXT("UseDiskLamp=%d\n") ,UseDiskLamp);

 fclose( configStream);

	// NOWAIT???E?c?ANOWAIT ?o?O?E?s?$B!&(B?B2003/8/24   Thanks Bernie
 if(!WaitFlag) sw_nowait_mode(1);
//#endif
 return(1);
}



// ****************************************************************************
//          ConfigRead
// ****************************************************************************
int ConfigRead(void)
{
//#ifdef WIN32
 int  i,J,N;
 size_t K;
 int  max;
 static TCHAR tmp[30][256];
 
 printf(" Reading Config file ...");
 configStream= (FILE*)_tfopen( ConfigPath,TEXT("r"));
 if(configStream==NULL) { printf("FAILED\n"); return(0);}
 
 for(i=0; i<30; i++)
	{
	if(_fgetts(tmp[i],255,configStream)==NULL)
		break;
	 K = lstrlen( tmp[i])-1;
	 if( tmp[i][K]=='\n')
		 tmp[i][K] ='\0';	// chop
	}
 max= i;
 fclose( configStream);

 for(N=0; N< max ; N++)
	{
         for(J=0;  ConfigOptions[J] ; J++)
	   {
	    K= lstrlen( ConfigOptions[J]);
	    if(!memcmp(tmp[N] ,ConfigOptions[J],K * sizeof(TCHAR)))
			break;
	   }

	 switch( J)
	   {
	     case 0:break;
	     case 1:P6Version= _ttoi( &tmp[N][K] ); 
			    P6Version = (P6Version <=4)? P6Version: 0; 
			    newP6Version= P6Version;  break;
	     case 2:
#ifdef SOUND
		 		UseSound = _ttoi( &tmp[N][K] );
#endif
				break;
	     case 3:disk_num = _ttoi( &tmp[N][K] ); 
		    	new_disk_num= disk_num; 
		    	break;
	     case 4:CPUclock = _ttoi( &tmp[N][K] ); 
	     		if( CPUclock==0) CPUclock=4; 
	     		break;
	     case 5:scale    = _ttoi( &tmp[N][K] ); 
	     	    if(scale<=0|| scale >=3) scale=2;
	     	    new_scale= scale; 
	     	    break;
	     case 6:IntLac   = _ttoi( &tmp[N][K] ); break;
	     case 7: my_strncpy(CasPath[0], &tmp[N][K],PATH_MAX); break;
	     case 8: my_strncpy(DskPath[0], &tmp[N][K],PATH_MAX); break;
	     case 9: my_strncpy(CasName[0], &tmp[N][K],PATH_MAX); break;
	     case 10:my_strncpy(DskName[0], &tmp[N][K],PATH_MAX); break;
	     case 11:scr4col = _ttoi( &tmp[N][K] ); break;
	     case 12:drawwait= _ttoi( &tmp[N][K] ); 
	     		 if(drawwait<0) drawwait=0;
	     		 if(drawwait>262) drawwait=262;
	     		 break;
	     case 13:FastTape= _ttoi( &tmp[N][K] ); break;
	     case 14:UPeriod = (byte)_ttoi( &tmp[N][K] ); 
	     		 if(UPeriod>8 ) UPeriod=8; 
	     		 break;
	     case 15:extkanjirom = _ttoi( &tmp[N][K] );
		 		 new_extkanjirom = extkanjirom;
	     		 break;
	     case 16:extram      = _ttoi( &tmp[N][K] );
		 		 new_extram  = extram;
	     		 break;
	     case 17:srline      = _ttoi( &tmp[N][K] );
		 		 if( srline >80) srline=80;
				 if( srline <0)  srline=80;
	     		 break;
	     case 18: my_strncpy(ImgPath, &tmp[N][K],PATH_MAX); break;
	     case 19: my_strncpy(MemPath, &tmp[N][K],PATH_MAX); break;
	     case 20: my_strncpy(Ext1Name, &tmp[N][K],PATH_MAX); break;
	     case 21: my_strncpy(Ext2Name, &tmp[N][K],PATH_MAX); break;
		 case 22: break;
	     case 23: my_strncpy(CasPath[1], &tmp[N][K],PATH_MAX); break;	/* tape (save)*/
	     case 24: my_strncpy(CasName[1], &tmp[N][K],PATH_MAX); break;
	     case 25: UseSaveTapeMenu    = _ttoi( &tmp[N][K] ); break;
	     case 26: UseStatusBar       = _ttoi( &tmp[N][K] ); break;
	     case 27: UseDiskLamp        = _ttoi( &tmp[N][K] ); break;
	   }
    }
 printf("Done.\n");
//#endif
 return(1);
}



// ****************************************************************************
//          COMMAND LINE OPTIONS
// ****************************************************************************
static const LPCTSTR Options[]=
{ 
  TEXT("verbose"),TEXT("help"),TEXT("shm"),TEXT("noshm"),TEXT("trap"),TEXT("sound"),TEXT("nosound"),TEXT("patch"),TEXT("scale"),TEXT("notimer"),
  TEXT("fasttape"),TEXT("extkanjirom"),TEXT("extram"),TEXT("srline"),TEXT("dum"),TEXT("dum"),TEXT("dum"),
  TEXT("tape"),TEXT("disk"),TEXT("clock"),TEXT("60"),TEXT("62"),TEXT("64"),TEXT("66"),TEXT("68"),TEXT("console"),
  TEXT("scr4col"),TEXT("drawWait"),TEXT("nointlac"),TEXT("rom1"),TEXT("rom2"),TEXT("extrom"),
  TEXT("savetape"),
  TEXT("thread"),
  NULL
};


// ****************************************************************************
//          chkOption: check command line options
// ****************************************************************************
/** bug fixed: first option starts from argv[1] 2003/5/11 **/
int chkOption( int argc, LPTSTR argv[])
{
  int J;
  int N;




  for(N=1; N<argc; N++)
     {
      for(J=0; Options[J];J++)
	      if( !lstrcmp(argv[N]+1,Options[J]))
     		      break;
      switch(J)
        {
        case 0:  N++; /* verbose */
                 if(N<argc) Verbose=(byte)_ttoi(argv[N]);
                 else _tprintf(TEXT("%s: No verbose level supplied\n"),argv[0]);
                 TrapBadOps=Verbose&0x10;
                 break;
        case 1:  /* help */
                 _tprintf(TEXT("%s by ISHIOKA Hiroshi    (C) 1998,1999\n"),Title);
                 for(J=0;HelpText[J];J++) puts(HelpText[J]);
                 exit(0); // return(0);
#ifdef MITSHM
        case 2:  UseSHM=1;break; /* shm */
        case 3:  UseSHM=0;break; /* noshm */
#endif
#ifdef DEBUG
        case 4:  N++; /* trap */
                 if(N>=argc)
                   _tprintf(TEXT("%s: No trap address supplied\n"),argv[0]);
                 else
                   if(!lstrcmp(argv[N],TEXT("now"))) Trace=1;
                   else (void)_stscanf(argv[N],TEXT("%hX"),&Trap);
                 break;
#endif
#ifdef SOUND
        case 5:  UseSound=1;break; /* sound */
        case 6:  UseSound=0;break; /* nosound */
#endif
        case 7:  N++; /* patch */
                 if(N<argc) PatchLevel=_ttoi(argv[N]);
                 else _tprintf(TEXT("%s: No patch level supplied\n"),argv[0]);
				 if(PatchLevel<1) PatchLevel=0;
				 if(PatchLevel>1) PatchLevel=1;
                 break;
        case 8:  N++; /* scale */
	         	if(N<argc) scale=_ttoi(argv[N]);
				 if(scale<1) scale=1;
				 if(scale>2) scale=2;
                 break;
        case 9:  TimerSWFlag=0;	/* notimer */
        		 break;
        case 10: FastTape=1;
		break;
        case 11: extkanjirom=1;
		break;
        case 12: extram  =1;
		break;
        case 13: N++;	/* srline */
	        	 if(N<argc) srline= _ttoi(argv[N]);
	         	 if( srline>80) srline=80;
                 break;
        case 17: N++;  /* tape */
                 if(N<argc)  my_strncpy(CasName[0], argv[N],PATH_MAX);
                 break;
        case 18: N++;  /* disk */
                 if(N<argc)  {
							 my_strncpy(DskName[0], argv[N],PATH_MAX);
							 disk_num=1;		// patch for Unix
							}
                 break;
#ifndef UNIX
        case 19: N++;	/* clock */
	        	 if(N<argc) CPUclock= _ttoi(argv[N]);
	         	 if( CPUclock==0) CPUclock=4;
                 break;
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:  P6Version =J-20;		/* p6version */
        		 break;
        case 25: Console   =1;		/* console */
        		 break;
        case 26: scr4col   =1;		/* scr4col */
        		 break;
        case 27: N++;				/* drawWait */
	         	if(N<argc) drawwait= _ttoi(argv[N]);
	         	if( drawwait<0) drawwait=0;
	         	if( drawwait>262) drawwait=262;
	         	break;
        case 28: IntLac    =0;		/* nointlac */
				break;
        case 29: N++;  				/* Ext1Name */
                 if(N<argc)  my_strncpy(Ext1Name, argv[N],PATH_MAX);
                 break;
        case 30: N++;  				/* Ext2Name */
                 if(N<argc)  my_strncpy(Ext2Name, argv[N],PATH_MAX);
                 break;
        case 32: N++;  				/* savetape */
                 if(N<argc)  my_strncpy(CasName[1], argv[N],PATH_MAX);
                 break;
        case 33: N++;  				/* thread */
                 UseCPUThread =1;
                 break;
        default: _tprintf(TEXT("Wrong option '%s'\n"),argv[N]);
#endif	// !UNIX
        }
     }
 return(1);
}
