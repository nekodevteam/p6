/*!
 * @file	guard.h
 * @brief	クリティカル セクション クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#if defined(_WIN32)

#include "win32/guard.hpp"

#else	// defined(_WIN32)

#include <mutex>
#define	Guard	std::mutex			/*!< Mutex クラス */

#endif	// defined(_WIN32)
