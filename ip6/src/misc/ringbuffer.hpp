/**
 * @file	ringbuffer.hpp
 * @brief	リング バッファ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "arithmetic.hpp"
#include <memory.h>

/**
 * @brief リング バッファ クラス
 */
class RingBuffer
{
protected:
	unsigned char* m_pBuffer;		/*!< バッファ */
	unsigned int m_nSize;			/*!< サイズ */
	unsigned int m_nIndex;			/*!< 書き込み ポインタ */
	unsigned int m_nAvail;			/*!< 存在サイズ */

public:
	/**
	 * コンストラクタ
	 */
	RingBuffer()
		: m_pBuffer(NULL)
		, m_nSize(0)
		, m_nIndex(0)
		, m_nAvail(0)
	{
	}

	/**
	 * デストラクタ
	 */
	~RingBuffer()
	{
		Allocate(0);
	}

	/**
	 * アロケート
	 * @param[in] nSize サイズ
	 */
	void Allocate(unsigned int nSize)
	{
		delete[] m_pBuffer;
		m_pBuffer = NULL;
		if (nSize)
		{
			m_pBuffer = new unsigned char [nSize];
			memset(m_pBuffer, 0, nSize);
		}
		m_nSize = nSize;
		m_nIndex = 0;
		m_nAvail = 0;
	}

	/**
	 * サイズ
	 * @return サイズ
	 */
	inline unsigned int Size() const
	{
		return m_nSize;
	}

	/**
	 * 残りバッファ サイズ
	 * @return 残りバッファ サイズ
	 */
	inline unsigned int Remain() const
	{
		return m_nSize - m_nAvail;
	}

	/**
	 * リード
	 * @param[in] pData データ
	 * @param[in] cbData データ長
	 * @return 読み込んだサイズ
	 */
	unsigned int Read(void* pData, unsigned int cbData)
	{
		const unsigned int nReadSize = minimum(m_nAvail, cbData);
		if (nReadSize > 0)
		{
			const unsigned int nFirst = m_nSize - m_nIndex;
			if (nReadSize < nFirst)
			{
				memcpy(pData, m_pBuffer + m_nIndex, nReadSize);
				m_nIndex += nReadSize;
			}
			else
			{
				memcpy(pData, m_pBuffer + m_nIndex, nFirst);
				const unsigned int nSecond = nReadSize - nFirst;
				m_nIndex = nSecond;
				if (nSecond)
				{
					memcpy(static_cast<unsigned char*>(pData) + nFirst, m_pBuffer, nSecond);
				}
			}
			m_nAvail -= nReadSize;
		}
		return nReadSize;
	}

	/**
	 * ライト
	 * @param[in] pData データ
	 * @param[in] cbData データ長
	 * @return 書き込んだサイズ
	 */
	unsigned int Write(const void* pData, unsigned int cbData)
	{
		const unsigned int nRemain = Remain();
		const unsigned int nWriteSize = minimum(nRemain, cbData);
		if (nWriteSize > 0)
		{
			unsigned int nIndex = m_nIndex + m_nAvail;
			if (nIndex >= m_nSize)
			{
				nIndex -= m_nSize;
			}

			const unsigned int nFirst = m_nSize - nIndex;
			if (nWriteSize < nFirst)
			{
				memcpy(m_pBuffer + nIndex, pData, nWriteSize);
			}
			else
			{
				memcpy(m_pBuffer + nIndex, pData, nFirst);
				const unsigned int nSecond = nWriteSize - nFirst;
				if (nSecond)
				{
					memcpy(m_pBuffer, static_cast<const unsigned char*>(pData) + nFirst, nSecond);
				}
			}
			m_nAvail += nWriteSize;
		}
		return nWriteSize;
	}

	/**
	 * ロック
	 * @param[out] nSize サイズ
	 * @return ポインタ
	 */
	void* Lock(unsigned int& nSize)
	{
		const unsigned int nRemain = Remain();
		if (nRemain == 0)
		{
			nSize = 0;
			return NULL;
		}

		unsigned int nIndex = m_nIndex + m_nAvail;
		if (nIndex >= m_nSize)
		{
			nIndex -= m_nSize;
		}
		nSize = minimum(m_nSize - nIndex, nRemain);
		return m_pBuffer + nIndex;
	}

	/**
	 * アンロック
	 * @param[in] nSize サイズ
	 */
	void Unlock(unsigned int nSize)
	{
		const unsigned int nRemain = Remain();
		m_nAvail += minimum(nRemain, nSize);
	}
};
