/**
 * @file	vkmap.h
 * @brief	Defines the virtual key table.
 */

#pragma once

/**
 * @brief マップ
 */
struct VKMap
{
	unsigned int code;		/*!< コード */
	LPCTSTR lpName;			/*!< 名前 */
};

#define DECLARE_VKMAP(key)	{key, TEXT(#key)}		/*!< キー宣言 */

/**
 * キーマップ
 */
static const struct VKMap s_vkmapitem[] =
{
	DECLARE_VKMAP(VK_LBUTTON),
	DECLARE_VKMAP(VK_RBUTTON),
	DECLARE_VKMAP(VK_CANCEL),
	DECLARE_VKMAP(VK_MBUTTON),
	{0x0005, TEXT("VK_XBUTTON1")},
	{0x0006, TEXT("VK_XBUTTON2")},
	DECLARE_VKMAP(VK_BACK),
	DECLARE_VKMAP(VK_TAB),
	DECLARE_VKMAP(VK_CLEAR),
	DECLARE_VKMAP(VK_RETURN),
	DECLARE_VKMAP(VK_SHIFT),
	DECLARE_VKMAP(VK_CONTROL),
	DECLARE_VKMAP(VK_MENU),
	DECLARE_VKMAP(VK_PAUSE),
	DECLARE_VKMAP(VK_CAPITAL),
	DECLARE_VKMAP(VK_KANA),
	DECLARE_VKMAP(VK_JUNJA),
	DECLARE_VKMAP(VK_FINAL),
	DECLARE_VKMAP(VK_KANJI),
	DECLARE_VKMAP(VK_ESCAPE),
	DECLARE_VKMAP(VK_CONVERT),
	{0x001d, TEXT("VK_NONCONVERT")},
	{0x001e, TEXT("VK_ACCEPT")},
	{0x001f, TEXT("VK_MODECHANGE")},
	DECLARE_VKMAP(VK_SPACE),
	DECLARE_VKMAP(VK_PRIOR),
	DECLARE_VKMAP(VK_NEXT),
	DECLARE_VKMAP(VK_END),
	DECLARE_VKMAP(VK_HOME),
	DECLARE_VKMAP(VK_LEFT),
	DECLARE_VKMAP(VK_UP),
	DECLARE_VKMAP(VK_RIGHT),
	DECLARE_VKMAP(VK_DOWN),
	DECLARE_VKMAP(VK_SELECT),
	DECLARE_VKMAP(VK_PRINT),
	DECLARE_VKMAP(VK_EXECUTE),
	DECLARE_VKMAP(VK_SNAPSHOT),
	DECLARE_VKMAP(VK_INSERT),
	DECLARE_VKMAP(VK_DELETE),
	DECLARE_VKMAP(VK_HELP),
	{'0', TEXT("VK_0")},
	{'1', TEXT("VK_1")},
	{'2', TEXT("VK_2")},
	{'3', TEXT("VK_3")},
	{'4', TEXT("VK_4")},
	{'5', TEXT("VK_5")},
	{'6', TEXT("VK_6")},
	{'7', TEXT("VK_7")},
	{'8', TEXT("VK_8")},
	{'9', TEXT("VK_9")},
	{'A', TEXT("VK_A")},
	{'B', TEXT("VK_B")},
	{'C', TEXT("VK_C")},
	{'D', TEXT("VK_D")},
	{'E', TEXT("VK_E")},
	{'F', TEXT("VK_F")},
	{'G', TEXT("VK_G")},
	{'H', TEXT("VK_H")},
	{'I', TEXT("VK_I")},
	{'J', TEXT("VK_J")},
	{'K', TEXT("VK_K")},
	{'L', TEXT("VK_L")},
	{'M', TEXT("VK_M")},
	{'N', TEXT("VK_N")},
	{'O', TEXT("VK_O")},
	{'P', TEXT("VK_P")},
	{'Q', TEXT("VK_Q")},
	{'R', TEXT("VK_R")},
	{'S', TEXT("VK_S")},
	{'T', TEXT("VK_T")},
	{'U', TEXT("VK_U")},
	{'V', TEXT("VK_V")},
	{'W', TEXT("VK_W")},
	{'X', TEXT("VK_X")},
	{'Y', TEXT("VK_Y")},
	{'Z', TEXT("VK_Z")},
	DECLARE_VKMAP(VK_LWIN),
	DECLARE_VKMAP(VK_RWIN),
	DECLARE_VKMAP(VK_APPS),
	{0x005F, TEXT("VK_SLEEP")}, 
	DECLARE_VKMAP(VK_NUMPAD0),
	DECLARE_VKMAP(VK_NUMPAD1),
	DECLARE_VKMAP(VK_NUMPAD2),
	DECLARE_VKMAP(VK_NUMPAD3),
	DECLARE_VKMAP(VK_NUMPAD4),
	DECLARE_VKMAP(VK_NUMPAD5),
	DECLARE_VKMAP(VK_NUMPAD6),
	DECLARE_VKMAP(VK_NUMPAD7),
	DECLARE_VKMAP(VK_NUMPAD8),
	DECLARE_VKMAP(VK_NUMPAD9),
	DECLARE_VKMAP(VK_MULTIPLY),
	DECLARE_VKMAP(VK_ADD),
	DECLARE_VKMAP(VK_SEPARATOR),
	DECLARE_VKMAP(VK_SUBTRACT),
	DECLARE_VKMAP(VK_DECIMAL),
	DECLARE_VKMAP(VK_DIVIDE),
	DECLARE_VKMAP(VK_F1),
	DECLARE_VKMAP(VK_F2),
	DECLARE_VKMAP(VK_F3),
	DECLARE_VKMAP(VK_F4),
	DECLARE_VKMAP(VK_F5),
	DECLARE_VKMAP(VK_F6),
	DECLARE_VKMAP(VK_F7),
	DECLARE_VKMAP(VK_F8),
	DECLARE_VKMAP(VK_F9),
	DECLARE_VKMAP(VK_F10),
	DECLARE_VKMAP(VK_F11),
	DECLARE_VKMAP(VK_F12),
	DECLARE_VKMAP(VK_F13),
	DECLARE_VKMAP(VK_F14),
	DECLARE_VKMAP(VK_F15),
	DECLARE_VKMAP(VK_F16),
	DECLARE_VKMAP(VK_F17),
	DECLARE_VKMAP(VK_F18),
	DECLARE_VKMAP(VK_F19),
	DECLARE_VKMAP(VK_F20),
	DECLARE_VKMAP(VK_F21),
	DECLARE_VKMAP(VK_F22),
	DECLARE_VKMAP(VK_F23),
	DECLARE_VKMAP(VK_F24),
	DECLARE_VKMAP(VK_NUMLOCK),
	DECLARE_VKMAP(VK_SCROLL),
//	DECLARE_VKMAP(VK_OEM_NEC_EQUAL),
//	DECLARE_VKMAP(VK_OEM_FJ_JISHO),
//	DECLARE_VKMAP(VK_OEM_FJ_MASSHOU),
//	DECLARE_VKMAP(VK_OEM_FJ_TOUROKU),
//	DECLARE_VKMAP(VK_OEM_FJ_LOYA),
//	DECLARE_VKMAP(VK_OEM_FJ_ROYA),
	DECLARE_VKMAP(VK_LSHIFT),
	DECLARE_VKMAP(VK_RSHIFT),
	DECLARE_VKMAP(VK_LCONTROL),
	DECLARE_VKMAP(VK_RCONTROL),
	DECLARE_VKMAP(VK_LMENU),
	DECLARE_VKMAP(VK_RMENU),
	{0x00a6, TEXT("VK_BROWSER_BACK")},
	{0x00a7, TEXT("VK_BROWSER_FORWARD")},
	{0x00a8, TEXT("VK_BROWSER_REFRESH")},
	{0x00a9, TEXT("VK_BROWSER_STOP")},
	{0x00aa, TEXT("VK_BROWSER_SEARCH")},
	{0x00ab, TEXT("VK_BROWSER_FAVORITES")},
	{0x00ac, TEXT("VK_BROWSER_HOME")},
	{0x00ad, TEXT("VK_VOLUME_MUTE")},
	{0x00ae, TEXT("VK_VOLUME_DOWN")},
	{0x00af, TEXT("VK_VOLUME_UP")},
	{0x00b0, TEXT("VK_MEDIA_NEXT_TRACK")},
	{0x00b1, TEXT("VK_MEDIA_PREV_TRACK")},
	{0x00b2, TEXT("VK_MEDIA_STOP")},
	{0x00b3, TEXT("VK_MEDIA_PLAY_PAUSE")},
	{0x00b4, TEXT("VK_LAUNCH_MAIL")},
	{0x00b5, TEXT("VK_LAUNCH_MEDIA_SELECT")},
	{0x00b6, TEXT("VK_LAUNCH_APP1")},
	{0x00b7, TEXT("VK_LAUNCH_APP2")},
	{0x00ba, TEXT("VK_OEM_1")},
	{0x00bb, TEXT("VK_OEM_PLUS")},
	{0x00bc, TEXT("VK_OEM_COMMA")},
	{0x00bd, TEXT("VK_OEM_MINUS")},
	{0x00be, TEXT("VK_OEM_PERIOD")},
	{0x00bf, TEXT("VK_OEM_2")},
	{0x00c0, TEXT("VK_OEM_3")},
	{0x00db, TEXT("VK_OEM_4")},
	{0x00dc, TEXT("VK_OEM_5")},
	{0x00dd, TEXT("VK_OEM_6")},
	{0x00de, TEXT("VK_OEM_7")},
	{0x00df, TEXT("VK_OEM_8")},
	{0x00e1, TEXT("VK_OEM_AX")},
	{0x00e2, TEXT("VK_OEM_102")},
	{0x00e3, TEXT("VK_ICO_HELP")},
	{0x00e4, TEXT("VK_ICO_00")},
	{0x00e5, TEXT("VK_PROCESSKEY")},
	{0x00e6, TEXT("VK_ICO_CLEAR")},
	{0x00e7, TEXT("VK_PACKET")},
	{0x00e9, TEXT("VK_OEM_RESET")},
	{0x00ea, TEXT("VK_OEM_JUMP")},
	{0x00eb, TEXT("VK_OEM_PA1")},
	{0x00ec, TEXT("VK_OEM_PA2")},
	{0x00ed, TEXT("VK_OEM_PA3")},
	{0x00ee, TEXT("VK_OEM_WSCTRL")},
	{0x00ef, TEXT("VK_OEM_CUSEL")},
	{0x00f0, TEXT("VK_OEM_ATTN")},
	{0x00f1, TEXT("VK_OEM_FINISH")},
	{0x00f2, TEXT("VK_OEM_COPY")},
	{0x00f3, TEXT("VK_OEM_AUTO")},
	{0x00f4, TEXT("VK_OEM_ENLW")},
	{0x00f5, TEXT("VK_OEM_BACKTAB")},
	DECLARE_VKMAP(VK_ATTN),
	DECLARE_VKMAP(VK_CRSEL),
	DECLARE_VKMAP(VK_EXSEL),
	DECLARE_VKMAP(VK_EREOF),
	DECLARE_VKMAP(VK_PLAY),
	DECLARE_VKMAP(VK_ZOOM),
	DECLARE_VKMAP(VK_NONAME),
	DECLARE_VKMAP(VK_PA1),
	DECLARE_VKMAP(VK_OEM_CLEAR),
};

/**
 * キー コードを返す
 * @param[in] lpName 名前
 * @return コード
 */
static unsigned int GetVKCode(LPCTSTR lpName)
{
	unsigned int i;
	for (i = 0; i < _countof(s_vkmapitem); i++)
	{
		if (lstrcmpi(s_vkmapitem[i].lpName, lpName) == 0)
		{
			return s_vkmapitem[i].code;
		}
	}
	return 0;
}

/**
 * キー マップを初期化
 * @param[in] pMap マップ
 */
static void InitVKMap(BYTE* pMap)
{
	unsigned int i;
	for (i = 0; i < 256; i++)
	{
		pMap[i] = i;
	}
}

/**
 * キー マップを読み取る
 * @param[in] pMap マップ
 * @param[in] lpPath パス
 * @retval TRUE 成功
 * @retval FALSE 失敗
 */
static BOOL ReadVKMap(BYTE* pMap, LPCTSTR lpPath)
{
	FILE *fp = _tfopen(lpPath, TEXT("r"));
	if (fp == NULL)
	{
		return FALSE;
	}
	while (TRUE /*CONSTCOND*/)
	{
		LPTSTR pStart;
		unsigned int nKey1;
		unsigned int nKey2;
		TCHAR szLine[256];
		LPTSTR p = _fgetts(szLine, _countof(szLine), fp);
		if (p == NULL)
		{
			break;
		}

		while ((*p > TEXT('\0')) && (*p <= TEXT(' '))) p++;
		pStart = p;
		while ((*p <= TEXT('\0')) || (*p > TEXT(' '))) p++;
		if (*p == TEXT('\0'))
		{
			continue;
		}
		*p++ = TEXT('\0');
		nKey1 = GetVKCode(pStart);
		if (nKey1 == 0)
		{
			continue;
		}

		while ((*p > TEXT('\0')) && (*p <= TEXT(' '))) p++;
		pStart = p;
		while ((*p <= TEXT('\0')) || (*p > TEXT(' '))) p++;
		*p = TEXT('\0');
		nKey2 = GetVKCode(pStart);
		if (nKey2 == 0)
		{
			continue;
		}
		pMap[nKey1] = nKey2;
	}
	fclose(fp);
	return TRUE;
}
