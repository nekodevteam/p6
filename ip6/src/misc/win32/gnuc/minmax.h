/**
 * @file	minmax.h
 * @brief	Defines min and max macros.
 */

#pragma once

#ifndef min
#define min(a, b)		((a) < (b)) ? (a) : (b)
#endif	// !min

#ifndef max
#define max(a, b)		((a) > (b)) ? (a) : (b)
#endif	// !max
