/*!
 * @file	guard.hpp
 * @brief	クリティカル セクション クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <windows.h>

/**
 * @brief クリティカル セクション クラス
 */
class Guard
{
public:
	/** コンストラクタ */
	Guard() { ::InitializeCriticalSection(&m_cs); }

	/** デストラクタ */
	~Guard() { ::DeleteCriticalSection(&m_cs); }

	/** クリティカル セクション開始 */
	void lock() { ::EnterCriticalSection(&m_cs); }

	/** クリティカル セクション終了 */
	void unlock() { ::LeaveCriticalSection(&m_cs); }

protected:
	CRITICAL_SECTION m_cs;		/**< クリティカル セクション情報 */
};
