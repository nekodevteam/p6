/**
 * @file	arithmetic.hpp
 * @brief	演算の宣言およびインターフェイスの定義をします
 */

#pragma once

/* VC6 や eVC3 などでは std::min / std::max が存在しないため */

/**
 * std::minと同じもの
 */
template <class T>
/*constexpr*/ const T& minimum(const T& v1, const T& v2)
{
	return (v1 < v2) ? v1 : v2;
}

/**
 * std::maxと同じもの
 */
template <class T>
/*constexpr*/ const T& maximum(const T& v1, const T& v2)
{
	return (v1 > v2) ? v1 : v2;
}

/**
 * std::clampと同じもの
 */
template <class T>
/*constexpr*/ const T& clamp(const T& v, const T& low, const T& high)
{
	return minimum<T>(maximum<T>(v, low), high);
}
