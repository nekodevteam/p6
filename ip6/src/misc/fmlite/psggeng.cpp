/**
 * @file	psggeng.cpp
 * @brief	Implementation of the PSG
 */

#include "../../os.h"
#include "fmlite.h"
#include "psggen.h"
#include "../arithmetic.hpp"

extern PSGGENCFG psggencfg;

/**
 * 取得
 * @param[in] psg ハンドル
 * @param[out] pcm バッファ
 * @param[in] count サンプル数
 * @return サンプル数
 */
void SOUNDCALL psggen_getpcm(PSGGEN psg, int32_t *pcm, UINT count)
{
	if ((psg->mixer & 0x3f) == 0)
	{
		count = minimum(count, psg->puchicount);
		psg->puchicount -= count;
	}
	if (count == 0)
	{
		return;
	}
	do
	{
		if (psg->envcnt)
		{
			psg->envcnt--;
			if (psg->envcnt == 0)
			{
				psg->envvolcnt--;
				if (psg->envvolcnt < 0)
				{
					if (psg->envmode & PSGENV_ONESHOT)
					{
						psg->envvol = (psg->envmode & PSGENV_LASTON) ? 15 : 0;
					}
					else
					{
						psg->envvolcnt = 15;
						if (!(psg->envmode & PSGENV_ONECYCLE))
						{
							psg->envmode ^= PSGENV_INC;
						}
						psg->envcnt = psg->envmax;
						psg->envvol = (psg->envvolcnt ^ psg->envmode) & 0x0f;
					}
				}
				else
				{
					psg->envcnt = psg->envmax;
					psg->envvol = (psg->envvolcnt ^ psg->envmode) & 0x0f;
				}
				psg->evol = psggencfg.volume[psg->envvol];
			}
		}
		uint_fast8_t mixer = psg->mixer;
		UINT noisetbl = 0;
		if (mixer & 0x38)
		{
			for (unsigned i = 0; i < (1 << PSGADDEDBIT); i++)
			{
				if (psg->noise.count > psg->noise.freq)
				{
					psg->noise.lfsr = (psg->noise.lfsr >> 1) ^ ((psg->noise.lfsr & 1) * 0x12000);
				}
				psg->noise.count -= psg->noise.freq;
				noisetbl |= (psg->noise.lfsr & 1) << i;
			}
		}
		PSGTONE* tone = psg->tone;
		PSGTONE* toneterm = tone + 3;
		do
		{
			const int32_t vol = *(tone->pvol);
			if (vol)
			{
				int32_t samp = 0;
				switch (mixer & 9)
				{
					case 0:							// no mix
						if (tone->puchi)
						{
							tone->puchi--;
							samp += vol << PSGADDEDBIT;
						}
						break;

					case 1:							// tone only
						{
							for (unsigned i = 0; i < (1 << PSGADDEDBIT); i++)
							{
								tone->count += tone->freq;
								samp += vol * ((tone->count >= 0) ? 1 : -1);
							}
						}
						break;

					case 8:							// noise only
						{
							UINT noise = noisetbl;
							for (unsigned i = 0; i < (1 << PSGADDEDBIT); i++)
							{
								samp += vol * ((noise & 1) ? 1 : -1);
								noise >>= 1;
							}
						}
						break;

					case 9:
						{
							UINT noise = noisetbl;
							for (unsigned i = 0; i < (1 << PSGADDEDBIT); i++)
							{
								tone->count += tone->freq;
								if ((tone->count >= 0) || (noise & 1))
								{
									samp += vol;
								}
								else
								{
									samp -= vol;
								}
								noise >>= 1;
							}
						}
						break;
				}
				if (!(tone->pan & 1))
				{
					pcm[0] += samp;
				}
				if (!(tone->pan & 2))
				{
					pcm[1] += samp;
				}
			}
			mixer >>= 1;
		} while (++tone < toneterm);
		pcm += 2;
	} while (--count);
}
