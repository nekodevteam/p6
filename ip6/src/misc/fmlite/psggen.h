/**
 * @file	psggen.h
 * @brief	Interface of the PSG
 */

#pragma once

enum
{
	PSGFREQPADBIT		= 12,
	PSGADDEDBIT			= 3
};

enum
{
	PSGENV_INC			= 15,
	PSGENV_ONESHOT		= 16,
	PSGENV_LASTON		= 32,
	PSGENV_ONECYCLE		= 64
};

/**
 * @brief PSG TONE
 */
struct PsgTone
{
	int32_t freq;			/*!< 周波数 */
	int32_t count;			/*!< カウンタ */
	int32_t *pvol;			/*!< ボリューム テーブル */
	uint16_t puchi;			/*!< プチノイズ*/
	uint8_t pan;			/*!< パン */
	uint8_t padding;		/*!< パディング */
};
typedef struct PsgTone PSGTONE;		/*!< トーン */

/**
 * @brief PSG ノイズ
 */
struct PsgNoise
{
	uint32_t freq;		/*!< frequency */
	uint32_t count;		/*!< counter */
	UINT lfsr;			/*!< linear feedback shift register */
};
typedef struct PsgNoise PSGNOISE;	/*!< ノイズ */

/**
 * @brief PSG レジスタ
 */
struct PsgReg
{
	uint8_t tune[3][2];		/*!< 0 */
	uint8_t noise;			/*!< 6 */
	uint8_t mixer;			/*!< 7 */
	uint8_t vol[3];			/*!< 8 */
	uint8_t envtime[2];		/*!< b */
	uint8_t env;			/*!< d */
	uint8_t io1;			/*!< e */
	uint8_t io2;			/*!< f */
};
typedef struct PsgReg PSGREG;		/*!< レジスタ */

/**
 * @brief PSG
 */
struct PsgGen
{
	PSGTONE tone[3];			/*!< トーン */
	PSGNOISE noise;				/*!< ノイズ */
	PSGREG reg;					/*!< レジスタ */
	uint16_t envcnt;			/*!< エンベローブ カウンタ */
	uint16_t envmax;			/*!< エンベローブ リミット */
	uint8_t mixer;				/*!< ミキサ */
	uint8_t envmode;			/*!< エンベローブ モード */
	uint8_t envvol;				/*!< エンベローブ ボリューム */
	int8_t envvolcnt;			/*!< エンベローブ ボリューム カウンタ */
	int32_t evol;				/*!< ボリューム値 */
	UINT puchicount;			/*!< プチノイズ */
};
typedef struct PsgGen _PSGGEN;		/*!< PSG */
typedef struct PsgGen *PSGGEN;		/*!< PSG */

/**
 * @brief コンフィグ
 */
struct PsgGenCfg
{
	int32_t volume[16];		/*!< ボリューム */
	int32_t voltbl[16];		/*!< テーブル */
	UINT rate;				/*!< レート */
	uint32_t base;			/*!< ベース */
	uint16_t puchidec;		/*!< プチノイズ */
};
typedef struct PsgGenCfg PSGGENCFG;		/*!< PSG コンフィグ */

#ifdef __cplusplus
extern "C"
{
#endif

void psggen_initialize(UINT rate);
void psggen_setvol(UINT vol);

void psggen_reset(PSGGEN psg);
void psggen_restore(PSGGEN psg);
void psggen_setreg(PSGGEN psg, UINT reg, REG8 val);
REG8 psggen_getreg(PSGGEN psg, UINT reg);
void psggen_setpan(PSGGEN psg, UINT ch, REG8 pan);

void SOUNDCALL psggen_getpcm(PSGGEN psg, int32_t *pcm, UINT count);

#ifdef __cplusplus
}
#endif
