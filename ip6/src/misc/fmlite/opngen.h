/**
 * @file	opngen.h
 * @brief	Interface of the OPN generator
 */

#pragma once

enum
{
	OPNCH_MAX		= 6,
	OPNA_CLOCK		= 3993600,

	OPN_CHMASK		= 0x80000000,
	OPN_STEREO		= 0x80000000,
	OPN_MONORAL		= 0x00000000
};

/**
 * @brief OPN スロット
 */
struct OpnSlot
{
	int32_t *detune1;				/*!< detune1 */
	int32_t totallevel;				/*!< total level */
	int32_t decaylevel;				/*!< decay level */
const int32_t *attack;				/*!< attack ratio */
const int32_t *decay1;				/*!< decay1 ratio */
const int32_t *decay2;				/*!< decay2 ratio */
const int32_t *release;				/*!< release ratio */
	int32_t freq_cnt;				/*!< frequency count */
	int32_t freq_inc;				/*!< frequency step */
	int32_t multiple;				/*!< multiple */
	uint8_t keyscalerate;			/*!< key scale */
	uint8_t env_mode;				/*!< envelope mode */
	uint8_t envratio;				/*!< envelope ratio */
	uint8_t ssgeg1;					/*!< SSG-EG */

	int32_t env_cnt;				/*!< envelope count */
	int32_t env_end;				/*!< envelope end count */
	int32_t env_inc;				/*!< envelope step */
	int32_t env_inc_attack;			/*!< envelope attack step */
	int32_t env_inc_decay1;			/*!< envelope decay1 step */
	int32_t env_inc_decay2;			/*!< envelope decay2 step */
	int32_t env_inc_release;		/*!< envelope release step */
};
typedef struct OpnSlot OPNSLOT;		/*!< OPN スロット */

/**
 * @brief OPN チャネル
 */
struct OpnCh
{
	OPNSLOT slot[4];			/*!< slot */
	uint8_t algorithm;			/*!< algorithm */
	uint8_t feedback;			/*!< self feedback */
	uint8_t playing;			/*!< playing */
	uint8_t outslot;			/*!< output slot */
	int32_t op1fb;				/*!< operator1 feedback */
	int32_t *connect1;			/*!< operator1 connect */
	int32_t *connect3;			/*!< operator3 connect */
	int32_t *connect2;			/*!< operator2 connect */
	int32_t *connect4;			/*!< operator4 connect */
	uint32_t keynote[4];		/*!< key note */

	uint8_t keyfunc[4];			/*!< key function */
	uint8_t kcode[4];			/*!< key code */
	uint8_t pan;				/*!< pan */
	uint8_t extop;				/*!< extendopelator-enable */
	uint8_t stereo;				/*!< stereo-enable */
	uint8_t padding;			/*!< padding */
};
typedef struct OpnCh OPNCH;		/*!< OPN チャネル */

/**
 * @brief OPN
 */
struct OpnGen
{
	UINT playchannels;			/*!< チャネル数 */
	UINT playing;				/*!< 再生フラグ */
	int32_t feedback2;			/*!< フィードバック */
	int32_t feedback3;			/*!< フィードバック */
	int32_t feedback4;			/*!< フィードバック */
	int32_t outdl;				/*!< 出力 L */
	int32_t outdc;				/*!< 出力 C */
	int32_t outdr;				/*!< 出力 R */
	int32_t calcremain;			/*!< 端数 */
	OPNCH opnch[OPNCH_MAX];		/*!< チャネル */
};
typedef struct OpnGen _OPNGEN;	/*!< OPN */
typedef struct OpnGen *OPNGEN;	/*!< OPN */

#ifdef __cplusplus
extern "C"
{
#endif

void opngen_initialize(UINT rate);
void opngen_setvol(UINT vol);
void opngen_setVR(REG8 channel, REG8 value);

void opngen_reset(OPNGEN opngen);
void opngen_setcfg(OPNGEN opngen, REG8 maxch, uint32_t flag);
void opngen_setextch(OPNGEN opngen, UINT chnum, REG8 data);
void opngen_setreg(OPNGEN opngen, REG8 chbase, UINT reg, REG8 value);
void opngen_keyon(OPNGEN opngen, UINT chnum, REG8 value);
void opngen_csm(OPNGEN opngen);

void SOUNDCALL opngen_getpcm(OPNGEN opngen, int32_t *buf, UINT count);
void SOUNDCALL opngen_getpcmvr(OPNGEN opngen, int32_t *buf, UINT count);

#ifdef __cplusplus
}
#endif
