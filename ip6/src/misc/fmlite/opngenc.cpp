/**
 * @file	opngenc.cpp
 * @brief	Implementation of the OPN generator
 */

#include "../../os.h"
#define _USE_MATH_DEFINES
#include "fmlite.h"
#include "opngen.h"
#include <math.h>
#include "opngencfg.h"

#define	OPM_ARRATE		 399128L	/*!< AR レート */
#define	OPM_DRRATE		5514396L	/*!< DR レート */

#define	EG_STEP			(96.0 / EVC_ENT)	/*!< dB step */
#define	SC(db)			(static_cast<int32_t>((db) * ((3.0 / EG_STEP) * (1 << ENV_BITS))) + EC_DECAY)		/*!< スケール */

	OPNCFG opncfg;					/*!< コンフィグ */

#if defined(OPNGENX86)
extern "C"
{
	char envshift[EVC_ENT];			/*!< シフタ */
	char sinshift[SIN_ENT];			/*!< シフタ */
}
#endif	/* defined(OPNGENX86) */

static int32_t detunetable[8][32];	/*!< デチューン */
static int32_t attacktable[94];		/*!< アタック */
static int32_t decaytable[94];		/*!< ディケイ */

/** ディケイ レベル */
static const int32_t decayleveltable[16] =
{
	SC( 0),SC( 1),SC( 2),SC( 3),SC( 4),SC( 5),SC( 6),SC( 7),
	SC( 8),SC( 9),SC(10),SC(11),SC(12),SC(13),SC(14),SC(31)
};

/** マルチプル */
static const uint8_t multipletable[] =
{
	1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30
};

/** NULL テーブル */
static const int32_t nulltable[] =
{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

/** KF */
static const uint8_t kftable[16] =
{
	0,0,0,0,0,0,0,1,2,3,3,3,3,3,3,3
};

/** DT */
static const uint8_t dttable[] =
{
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2,
	2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8, 8,
	1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5,
	5, 6, 6, 7, 8, 8, 9,10,11,12,13,14,16,16,16,16,
	2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7,
	8, 8, 9,10,11,12,13,14,16,17,19,20,22,22,22,22
};

/** 拡張スロット */
static const int extendslot[4] = {2, 3, 1, 0};

/** スロット */
static const int fmslot[4] = {0, 2, 1, 3};

/**
 * 初期化
 * @param[in] rate レート
 */
void opngen_initialize(UINT rate)
{
	UINT ratebit;
	if (rate > (OPNA_CLOCK / 144.0))
	{
		ratebit = 0;
	}
	else if (rate > (OPNA_CLOCK / 288.0))
	{
		ratebit = 1;
	}
	else
	{
		ratebit = 2;
	}
	opncfg.calc1024 = (int32_t)((FMDIV_ENT * (rate << ratebit) / (OPNA_CLOCK / 72.0)) + 0.5);

	for (int i = 0; i < EVC_ENT; i++)
	{
#ifdef OPNGENX86
		char sft = ENVTBL_BIT;
		while (sft < (ENVTBL_BIT + 8))
		{
			double pom = static_cast<double>(1 << sft) / pow(10.0, EG_STEP * (EVC_ENT - i) / 20.0);
			opncfg.envtable[i] = static_cast<int32_t>(pom);
			envshift[i] = sft - TL_BITS;
			if (opncfg.envtable[i] >= (1 << (ENVTBL_BIT - 1)))
			{
				break;
			}
			sft++;
		}
#else
		const double pom = static_cast<double>(1 << ENVTBL_BIT) / pow(10.0, EG_STEP * (EVC_ENT - i) / 20.0);
		opncfg.envtable[i] = static_cast<int32_t>(pom);
#endif
	}
	for (int i = 0; i < SIN_ENT; i++)
	{
#ifdef OPNGENX86
		char sft = SINTBL_BIT;
		while (sft < (SINTBL_BIT + 8))
		{
			const double pom = static_cast<double>((1 << sft) - 1) * sin(2 * M_PI * i / SIN_ENT);
			opncfg.sintable[i] = static_cast<int32_t>(pom);
			sinshift[i] = sft;
			if (opncfg.sintable[i] >= (1 << (SINTBL_BIT - 1)))
			{
				break;
			}
			if (opncfg.sintable[i] <= -1 * (1 << (SINTBL_BIT - 1)))
			{
				break;
			}
			sft++;
		}
#else
		const double pom = static_cast<double>((1 << SINTBL_BIT) - 1) * sin(2 * M_PI * i / SIN_ENT);
		opncfg.sintable[i] = static_cast<int32_t>(pom);
#endif
	}
	for (int i = 0; i < EVC_ENT; i++)
	{
		const double pom = pow((static_cast<double>(EVC_ENT - 1 - i) / EVC_ENT), 8) * EVC_ENT;
		opncfg.envcurve[i] = static_cast<int32_t>(pom);
		opncfg.envcurve[EVC_ENT + i] = i;
	}
	opncfg.envcurve[EVC_ENT * 2] = EVC_ENT;

	opncfg.ratebit = ratebit;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 32; j++)
		{
			int32_t detune = dttable[i*32 + j];
			const char sft = ratebit + (FREQ_BITS - 20);
			if (sft >= 0)
			{
				detune <<= sft;
			}
			else
			{
				detune >>= (0 - sft);
			}

			detunetable[i + 0][j] = detune;
			detunetable[i + 4][j] = -detune;
		}
	}
	for (int i = 0; i < 4; i++)
	{
		attacktable[i] = decaytable[i] = 0;
	}
	for (int i = 4; i < 64; i++)
	{
		double freq = (EVC_ENT << (ENV_BITS + ratebit)) * 72.0 / 64.0;
		if (i < 60)
		{
			freq *= 1.0 + (i & 3) * 0.25;
		}
		freq *= static_cast<double>(1 << ((i >> 2) - 1));
		attacktable[i] = static_cast<int32_t>(freq / OPM_ARRATE);
		decaytable[i] = static_cast<int32_t>(freq / OPM_DRRATE);
#if 0
		if (attacktable[i] >= EC_DECAY)
		{
			TRACEOUT(("attacktable %d %d %ld", i, attacktable[i], EC_DECAY));
		}
		if (decaytable[i] >= EC_DECAY)
		{
			TRACEOUT(("decaytable %d %d %ld", i, decaytable[i], EC_DECAY));
		}
#endif	// 0
	}
	attacktable[62] = EC_DECAY - 1;
	attacktable[63] = EC_DECAY - 1;
	for (int i = 64; i < 94; i++)
	{
		attacktable[i] = attacktable[63];
		decaytable[i] = decaytable[63];
	}
}

/**
 * ボリューム設定
 * @param[in] vol ボリューム
 */
void opngen_setvol(UINT vol)
{
	opncfg.fmvol = vol * 5 / 4;
#if defined(OPNGENX86)
	opncfg.fmvol <<= (32 - (OPM_OUTSB + 1 - FMVOL_SFTBIT + FMDIV_BITS + 6));
#endif
}

/**
 * VR 設定
 * @param[in] channel チャネル
 * @param[in] value 値
 */
void opngen_setVR(REG8 channel, REG8 value)
{
	if ((channel & 3) && (value))
	{
		opncfg.vr_en = TRUE;
		opncfg.vr_l = (channel & 1) ? value : 0;
		opncfg.vr_r = (channel & 2) ? value : 0;
	}
	else
	{
		opncfg.vr_en = FALSE;
	}
}


/* ---- */

/**
 * アルゴリズム更新
 * @param[in] opngen ハンドル
 * @param[in] ch チャネル
 */
static void set_algorithm(OPNGEN opngen, OPNCH *ch)
{
	int32_t *outd = &opngen->outdc;
	if (ch->stereo)
	{
		switch (ch->pan & 0xc0)
		{
			case 0x80:
				outd = &opngen->outdl;
				break;

			case 0x40:
				outd = &opngen->outdr;
				break;
		}
	}

	uint8_t outslot;
	switch (ch->algorithm)
	{
		case 0:
			ch->connect1 = &opngen->feedback2;
			ch->connect2 = &opngen->feedback3;
			ch->connect3 = &opngen->feedback4;
			outslot = 0x08;
			break;

		case 1:
			ch->connect1 = &opngen->feedback3;
			ch->connect2 = &opngen->feedback3;
			ch->connect3 = &opngen->feedback4;
			outslot = 0x08;
			break;

		case 2:
			ch->connect1 = &opngen->feedback4;
			ch->connect2 = &opngen->feedback3;
			ch->connect3 = &opngen->feedback4;
			outslot = 0x08;
			break;

		case 3:
			ch->connect1 = &opngen->feedback2;
			ch->connect2 = &opngen->feedback4;
			ch->connect3 = &opngen->feedback4;
			outslot = 0x08;
			break;

		case 4:
			ch->connect1 = &opngen->feedback2;
			ch->connect2 = outd;
			ch->connect3 = &opngen->feedback4;
			outslot = 0x0a;
			break;

		case 5:
			ch->connect1 = 0;
			ch->connect2 = outd;
			ch->connect3 = outd;
			outslot = 0x0e;
			break;

		case 6:
			ch->connect1 = &opngen->feedback2;
			ch->connect2 = outd;
			ch->connect3 = outd;
			outslot = 0x0e;
			break;

		case 7:
		default:
			ch->connect1 = outd;
			ch->connect2 = outd;
			ch->connect3 = outd;
			outslot = 0x0f;
			break;
	}
	ch->connect4 = outd;
	ch->outslot = outslot;
}

/**
 * DT1/MUL
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_dt1_mul(OPNSLOT* slot, REG8 value)
{
	slot->multiple = static_cast<int32_t>(multipletable[value & 0x0f]);
	slot->detune1 = detunetable[(value >> 4) & 7];
}

/**
 * TL
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_tl(OPNSLOT* slot, REG8 value)
{
#if (EVC_BITS >= 7)
	slot->totallevel = ((~value) & 0x007f) << (EVC_BITS - 7);
#else
	slot->totallevel = ((~value) & 0x007f) >> (7 - EVC_BITS);
#endif
}

/**
 * KS/AR
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_ks_ar(OPNSLOT* slot, REG8 value)
{
	slot->keyscalerate = ((~value) >> 6) & 3;
	value &= 0x1f;
	slot->attack = (value) ? (attacktable + (value << 1)) : nulltable;
	slot->env_inc_attack = slot->attack[slot->envratio];
	if (slot->env_mode == EM_ATTACK)
	{
		slot->env_inc = slot->env_inc_attack;
	}
}

/**
 * D1R
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_d1r(OPNSLOT* slot, REG8 value)
{
	value &= 0x1f;
	slot->decay1 = (value) ? (decaytable + (value << 1)) : nulltable;
	slot->env_inc_decay1 = slot->decay1[slot->envratio];
	if (slot->env_mode == EM_DECAY1)
	{
		slot->env_inc = slot->env_inc_decay1;
	}
}

/**
 * DT2/D2R
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_dt2_d2r(OPNSLOT* slot, REG8 value)
{
	value &= 0x1f;
	slot->decay2 = (value) ? (decaytable + (value << 1)) : nulltable;
	if (slot->ssgeg1)
	{
		slot->env_inc_decay2 = 0;
	}
	else
	{
		slot->env_inc_decay2 = slot->decay2[slot->envratio];
	}
	if (slot->env_mode == EM_DECAY2)
	{
		slot->env_inc = slot->env_inc_decay2;
	}
}

/**
 * D1R/RR
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_d1l_rr(OPNSLOT* slot, REG8 value)
{
	slot->decaylevel = decayleveltable[(value >> 4)];
	slot->release = decaytable + ((value & 0x0f) << 2) + 2;
	slot->env_inc_release = slot->release[slot->envratio];
	if (slot->env_mode == EM_RELEASE)
	{
		slot->env_inc = slot->env_inc_release;
		if (value == 0xff)
		{
			slot->env_mode = EM_OFF;
			slot->env_cnt = EC_OFF;
			slot->env_end = EC_OFF + 1;
			slot->env_inc = 0;
		}
	}
}

/**
 * SSG-EG
 * @param[in] slot スロット
 * @param[in] value 値
 */
static void set_ssgeg(OPNSLOT* slot, REG8 value)
{
	value &= 0xf;
	if ((value == 0xb) || (value == 0xd))
	{
		slot->ssgeg1 = 1;
		slot->env_inc_decay2 = 0;
	}
	else
	{
		slot->ssgeg1 = 0;
		slot->env_inc_decay2 = slot->decay2[slot->envratio];
	}
	if (slot->env_mode == EM_DECAY2)
	{
		slot->env_inc = slot->env_inc_decay2;
	}
}

/**
 * チャネル更新
 * @param[in] ch チャネル
 */
static void channelupdate(OPNCH* ch)
{

	OPNSLOT* slot = ch->slot;
	if (!(ch->extop))
	{
		const uint32_t fc = ch->keynote[0];
		const uint8_t kc = ch->kcode[0];
		for (unsigned i = 0; i < 4; i++, slot++)
		{
			slot->freq_inc = ((fc + slot->detune1[kc]) * slot->multiple) >> 1;
			const UINT evr = kc >> slot->keyscalerate;
			if (slot->envratio != evr)
			{
				slot->envratio = evr;
				slot->env_inc_attack = slot->attack[evr];
				slot->env_inc_decay1 = slot->decay1[evr];
				slot->env_inc_decay2 = slot->decay2[evr];
				slot->env_inc_release = slot->release[evr];
			}
		}
	}
	else
	{
		for (unsigned i = 0; i < 4; i++, slot++)
		{
			const int s = extendslot[i];
			slot->freq_inc = ((ch->keynote[s] + slot->detune1[ch->kcode[s]]) * slot->multiple) >> 1;
			const UINT evr = ch->kcode[s] >> slot->keyscalerate;
			if (slot->envratio != evr)
			{
				slot->envratio = evr;
				slot->env_inc_attack = slot->attack[evr];
				slot->env_inc_decay1 = slot->decay1[evr];
				slot->env_inc_decay2 = slot->decay2[evr];
				slot->env_inc_release = slot->release[evr];
			}
		}
	}
}


/* ---- */

/**
 * リセット
 * @param[in] opngen ハンドル
 */
void opngen_reset(OPNGEN opngen)
{
	memset(opngen, 0, sizeof(*opngen));
	opngen->playchannels = 3;

	OPNCH* ch = opngen->opnch;
	for (unsigned i = 0; i < OPNCH_MAX; i++)
	{
		ch->keynote[0] = 0;
		OPNSLOT* slot = ch->slot;
		for (unsigned j = 0; j < 4; j++)
		{
			slot->env_mode = EM_OFF;
			slot->env_cnt = EC_OFF;
			slot->env_end = EC_OFF + 1;
			slot->env_inc = 0;
			slot->detune1 = detunetable[0];
			slot->attack = nulltable;
			slot->decay1 = nulltable;
			slot->decay2 = nulltable;
			slot->release = decaytable;
			slot++;
		}
		ch++;
	}
	for (unsigned i = 0x30; i < 0xc0; i++)
	{
		opngen_setreg(opngen, 0, i, 0xff);
		opngen_setreg(opngen, 3, i, 0xff);
	}
}

/**
 * コンフィグ
 * @param[in] opngen ハンドル
 * @param[in] maxch 最大チャネル
 * @param[in] flag フラグ
 */
void opngen_setcfg(OPNGEN opngen, REG8 maxch, uint32_t flag)
{
	opngen->playchannels = maxch;
	OPNCH* ch = opngen->opnch;
	if ((flag & OPN_CHMASK) == OPN_STEREO)
	{
		for (unsigned i = 0; i < OPNCH_MAX; i++)
		{
			if (flag & (1 << i))
			{
				ch->stereo = TRUE;
				set_algorithm(opngen, ch);
			}
			ch++;
		}
	}
	else
	{
		for (unsigned i = 0; i < OPNCH_MAX; i++)
		{
			if (flag & (1 << i))
			{
				ch->stereo = FALSE;
				set_algorithm(opngen, ch);
			}
			ch++;
		}
	}
}

/**
 * 拡張チャネル設定
 * @param[in] opngen ハンドル
 * @param[in] chnum チャネル
 * @param[in] data データ
 */
void opngen_setextch(OPNGEN opngen, UINT chnum, REG8 data)
{
	OPNCH* ch = opngen->opnch;
	ch[chnum].extop = data;
}

/**
 * レジスタ
 * @param[in] opngen ハンドル
 * @param[in] chbase チャネル ベース
 * @param[in] reg レジスタ
 * @param[in] value 値
 */
void opngen_setreg(OPNGEN opngen, REG8 chbase, UINT reg, REG8 value)
{
	const UINT chpos = reg & 3;
	if (chpos == 3)
	{
		return;
	}
	OPNCH* ch = opngen->opnch + chbase + chpos;
	if (reg < 0xa0)
	{
		OPNSLOT* slot = ch->slot + fmslot[(reg >> 2) & 3];
		switch (reg & 0xf0)
		{
			case 0x30:					/* DT1 MUL */
				set_dt1_mul(slot, value);
				channelupdate(ch);
				break;

			case 0x40:					/* TL */
				set_tl(slot, value);
				break;

			case 0x50:					/* KS AR */
				set_ks_ar(slot, value);
				channelupdate(ch);
				break;

			case 0x60:					/* D1R */
				set_d1r(slot, value);
				break;

			case 0x70:					/* DT2 D2R */
				set_dt2_d2r(slot, value);
				channelupdate(ch);
				break;

			case 0x80:					/* D1L RR */
				set_d1l_rr(slot, value);
				break;

			case 0x90:
				set_ssgeg(slot, value);
				channelupdate(ch);
				break;
		}
	}
	else
	{
		switch (reg & 0xfc)
		{
			case 0xa0:
				{
					const REG8 blk = ch->keyfunc[0] >> 3;
					const UINT fn = ((ch->keyfunc[0] & 7) << 8) + value;
					ch->kcode[0] = (blk << 2) | kftable[fn >> 7];
					ch->keynote[0] = fn << (opncfg.ratebit + blk + FREQ_BITS - 21);
					channelupdate(ch);
				}
				break;

			case 0xa4:
				ch->keyfunc[0] = value & 0x3f;
				break;

			case 0xa8:
				{
					OPNCH* ch = opngen->opnch + chbase + 2;
					const REG8 blk = ch->keyfunc[chpos + 1] >> 3;
					const UINT fn = ((ch->keyfunc[chpos+1] & 7) << 8) + value;
					ch->kcode[chpos + 1] = (blk << 2) | kftable[fn >> 7];
					ch->keynote[chpos + 1] = fn << (opncfg.ratebit + blk + FREQ_BITS - 21);
					channelupdate(ch);
				}
				break;

			case 0xac:
				{
					OPNCH* ch = opngen->opnch + chbase + 2;
					ch->keyfunc[chpos + 1] = value & 0x3f;
				}
				break;

			case 0xb0:
				ch->algorithm = (uint8_t)(value & 7);
				value = (value >> 3) & 7;
				if (value)
				{
					ch->feedback = 8 - value;
				}
				else
				{
					ch->feedback = 0;
				}
				set_algorithm(opngen, ch);
				break;

			case 0xb4:
				ch->pan = (uint8_t)(value & 0xc0);
				set_algorithm(opngen, ch);
				break;
		}
	}
}

/**
 * キーオン
 * @param[in] opngen ハンドル
 * @param[in] chnum チャネル
 * @param[in] value 値
 */
void opngen_keyon(OPNGEN opngen, UINT chnum, REG8 value)
{
	opngen->playing++;
	OPNCH* ch = opngen->opnch + chnum;
	ch->playing |= value >> 4;
	OPNSLOT* slot = ch->slot;
	REG8 bit = 0x10;
	for (unsigned i = 0; i < 4; i++)
	{
		if (value & bit)							/* keyon */
		{
			if (slot->env_mode <= EM_RELEASE)
			{
				slot->freq_cnt = 0;
				if (i == OPNSLOT1)
				{
					ch->op1fb = 0;
				}
				slot->env_mode = EM_ATTACK;
				slot->env_inc = slot->env_inc_attack;
				slot->env_cnt = EC_ATTACK;
				slot->env_end = EC_DECAY;
			}
		}
		else										/* keyoff */
		{
			if (slot->env_mode > EM_RELEASE)
			{
				slot->env_mode = EM_RELEASE;
				if (!(slot->env_cnt & EC_DECAY))
				{
					slot->env_cnt = (opncfg.envcurve[slot->env_cnt >> ENV_BITS] << ENV_BITS) + EC_DECAY;
				}
				slot->env_end = EC_OFF;
				slot->env_inc = slot->env_inc_release;
			}
		}
		slot++;
		bit <<= 1;
	}
}

/**
 * CSM
 * @param[in] opngen ハンドル
 */
void opngen_csm(OPNGEN opngen)
{
	opngen_keyon(opngen, 2, 0x02);
	opngen_keyon(opngen, 2, 0xf2);
}
