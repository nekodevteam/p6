/**
 * @file	fmlite.h
 * @brief	Interface of fmlite
 */

#pragma once

#include <stdio.h>

#if defined(_MSC_VER) && (_MSC_VER <= 1500 /*Visual Studio 2008*/)
typedef signed char			int8_t;
typedef unsigned char		uint8_t;
typedef signed short		int16_t;
typedef unsigned short		uint16_t;
typedef signed int			int32_t;
typedef unsigned int		uint32_t;
typedef signed __int64		int64_t;
typedef unsigned __int64	uint64_t;
typedef signed char			int_fast8_t;
typedef unsigned char		uint_fast8_t;
typedef signed int			int_fast16_t;
typedef unsigned int		uint_fast16_t;
#else
#include <stdint.h>
#endif	// defined(_MSC_VER) && (_MSC_VER <= 1500)

#if defined(_MSC_VER) && (_MSC_VER < 1300)
#define for					if (0 /*NEVER*/) { /* no process */ } else for			/*!< for scope */
#endif	/* defined(_MSC_VER) && (_MSC_VER < 1300) */

#if defined(_USE_MATH_DEFINES) && !defined(_MATH_DEFINES_DEFINED)
#define _MATH_DEFINES_DEFINED
#define M_PI		3.14159265358979323846
#endif

typedef unsigned char REG8;		/*!< レジスタ */

#if defined(_MSC_VER) && !defined(_WIN64)
#define SOUNDCALL __fastcall	/*!< サウンド コール規約 */
#endif	// defined(_MSC_VER) && !defined(_WIN64)

#ifndef SOUNDCALL
#define SOUNDCALL				/*!< サウンド コール規約 */
#endif	// !SOUNDCALL
