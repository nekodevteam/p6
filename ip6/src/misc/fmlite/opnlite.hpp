/**
 * @file	opnlite.hpp
 * @brief	Interface of the OPN
 */

#pragma once

#include "fmlite.h"
#include "opngen.h"
#include "opntimer.hpp"
#include "psggen.h"

/**
 * @brief Opn
 */
class OpnLite
{
protected:
	_OPNGEN m_opngen;		/*!< opn */
	OpnTimer m_opntimer;	/*!< timer */
	_PSGGEN m_psggen;		/*!< psg */

public:
	/**
	 * コンストラクタ
	 */
	OpnLite()
	{
		memset(&m_opngen, 0, sizeof(m_opngen));
		memset(&m_psggen, 0, sizeof(m_psggen));
	}

	/**
	 * デストラクタ
	 */
	~OpnLite()
	{
	}

	/**
	 * 初期化
	 * @param[in] nClock クロック
	 * @param[in] nRate レート
	 * @retval true 成功
	 */
	bool Init(uint32_t nClock, uint32_t nRate, bool = false, LPCTSTR = NULL)
	{
		opngen_initialize(nRate);
		opngen_setvol(64);
		m_opntimer.SetClock(nClock);
		psggen_initialize(nRate);
		psggen_setvol(64);
		Reset();
		opngen_setcfg(&m_opngen, 3, OPN_MONORAL | 0x007);
		return true;
	}

	/**
	 * リセット
	 */
	void Reset()
	{
		::opngen_reset(&m_opngen);
		::psggen_reset(&m_psggen);
	}

	/**
	 * ミックス
	 * @param[out] pBuffer バッファ
	 * @param[in] nSamples サンプル数
	 */
	void Mix(int32_t* pBuffer, unsigned int nSamples)
	{
		opngen_getpcm(&m_opngen, pBuffer, nSamples);
		psggen_getpcm(&m_psggen, pBuffer, nSamples);
	}

	/**
	 * レジスタ設定
	 * @param[in] addr アドレス
	 * @param[in] data データ
	 */
	void SetReg(unsigned int addr, REG8 data)
	{
		if (addr < 0x10)
		{
			psggen_setreg(&m_psggen, addr, data);
		}
		else if (addr < 0x30)
		{
			m_opntimer.SetReg(addr, data);
			if (addr == 0x28)
			{
				const UINT nChannel= data & 3;
				if (nChannel < 3)
				{
					opngen_keyon(&m_opngen, nChannel, data);
				}
			}
		}
		else if (addr < 0xc0)
		{
			opngen_setreg(&m_opngen, 0, addr, data);
		}
	}

	/**
	 * レジスタ取得
	 * @param[in] addr アドレス
	 * @return 値
	 */
	REG8 GetReg(unsigned int addr)
	{
		if (addr < 0x10)
		{
			return psggen_getreg(&m_psggen, addr);
		}
		else
		{
			return 0xff;
		}
	}

	/**
	 * タイマー時間処理
	 * @param[in] us 経過時間
	 * @return イベント
	 */
	bool Count(int32_t us)
	{
		return m_opntimer.Count(us);
	}

	/**
	 * ステータスを取得
	 * @return ステータス
	 */
	unsigned int ReadStatus() const
	{
		return m_opntimer.ReadStatus();
	}

	/**
	 * ヴォリューム設定
	 * @param[in] vol ヴォリューム
	 */
	void SetVolumeFM(uint_fast8_t vol)
	{
	}

	/**
	 * ヴォリューム設定
	 * @param[in] vol ヴォリューム
	 */
	void SetVolumePSG(uint_fast8_t vol)
	{
	}
};
