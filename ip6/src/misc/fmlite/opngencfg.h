/**
 * @file	opngencfg.h
 * @brief	Interface of the OPN generator
 */

#pragma once

#if defined(OPNGENX86)

enum
{
	FMDIV_BITS		= 10,
	FMDIV_ENT		= (1 << FMDIV_BITS),
	FMVOL_SFTBIT	= 4
};

#define SIN_BITS		11		/*!< サイン ビット数 */
#define EVC_BITS		10		/*!< カーブ ビット数 */
#define ENV_BITS		16		/*!< エンベ ビット数 */
#define FREQ_BITS		21		/*!< 周波数ビット数 */
#define ENVTBL_BIT		14		/*!< カーブ テーブル ビット数 */
#define SINTBL_BIT		14		/*!< サイン テーブル ビット数 */

#elif defined(OPNGENARM)

enum
{
	FMDIV_BITS		= 8,
	FMDIV_ENT		= (1 << FMDIV_BITS),
	FMVOL_SFTBIT	= 4
};

#define SIN_BITS		8		/*!< サイン ビット数 */
#define EVC_BITS		7		/*!< カーブ ビット数 */
#define ENV_BITS		16		/*!< エンベ ビット数 */
#define FREQ_BITS		20		/*!< 周波数ビット数 */
#define ENVTBL_BIT		14		/*!< サイン テーブル ビット数 */
#define SINTBL_BIT		14		/*!< サイン テーブル ビット数 */

#else

enum
{
	FMDIV_BITS		= 10,
	FMDIV_ENT		= (1 << FMDIV_BITS),
	FMVOL_SFTBIT	= 4
};

#define SIN_BITS		10		/*!< サイン ビット数 */
#define EVC_BITS		10		/*!< カーブ ビット数 */
#define ENV_BITS		16		/*!< エンベ ビット数 */
#define FREQ_BITS		21		/*!< 周波数ビット数 */
#define ENVTBL_BIT		14		/*!< カーブ テーブル ビット数 */
#define SINTBL_BIT		15		/*!< サイン テーブル ビット数 */

#endif

#define TL_BITS			(FREQ_BITS + 2)					/*!< TL ビット */
#define OPM_OUTSB		(TL_BITS + 2 - 16)				/*!< OPM output 16bit */

#define SIN_ENT			(1L << SIN_BITS)				/*!< サイン エントリ数 */
#define EVC_ENT			(1L << EVC_BITS)				/*!< カーブ エントリ数 */

#define EC_ATTACK		0								/*!< ATTACK start */
#define EC_DECAY		(EVC_ENT << ENV_BITS)			/*!< DECAY start */
#define EC_OFF			((2 * EVC_ENT) << ENV_BITS)		/*!< OFF */

enum
{
	/* slot number */
	OPNSLOT1		= 0,
	OPNSLOT2		= 1,
	OPNSLOT3		= 2,
	OPNSLOT4		= 3,

	EM_ATTACK		= 4,
	EM_DECAY1		= 3,
	EM_DECAY2		= 2,
	EM_RELEASE		= 1,
	EM_OFF			= 0
};

/**
 * @brief OPN コンフィグ
 */
struct OpnCfg
{
	int32_t calc1024;		/*!< 1024 */
	int32_t fmvol;			/*!< ボリューム */
	UINT ratebit;			/*!< レート */
	UINT vr_en;				/*!< VR 有効 */
	int32_t vr_l;			/*!< VR L */
	int32_t vr_r;			/*!< VR R */

	int32_t sintable[SIN_ENT];				/*!< サイン */
	int32_t envtable[EVC_ENT];				/*!< エンベローブ */
	int32_t envcurve[(EVC_ENT * 2) + 1];	/*!< カーブ */
};
typedef struct OpnCfg OPNCFG;				/*!< OPN コンフィグ */

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

extern OPNCFG opncfg;

#ifdef __cplusplus
}
#endif	// __cplusplus
