/**
 * @file	opntimer.hpp
 * @brief	タイマ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "../arithmetic.hpp"

/**
 * @brief タイマ クラス
 */
class OpnTimer
{
protected:
	uint8_t m_cStatus;		/*!< ステータス */
	uint8_t m_cPrescale;	/*!< プリスケーラ */
	uint8_t m_cReg24[2];	/*!< 24 */
	uint8_t m_cReg27;		/*!< 27 */
	uint32_t m_nClock;		/*!< OPN クロック */
	int32_t m_nTimerA;		/*!< タイマA */
	int32_t m_nTimerACount;	/*!< タイマA カウンタ */
	int32_t m_nTimerB;		/*!< タイマB */
	int32_t m_nTimerBCount;	/*!< タイマB カウンタ */
	int32_t m_nTimerStep;	/*!< FM クロック */

	/**
	 * セット
	 * @param[in] cBit ビット
	 */
	inline void SetStatus(uint_fast8_t cBit)
	{
		m_cStatus |= cBit;
	}

	/**
	 * リセット
	 * @param[in] cBit ビット
	 */
	inline void ResetStatus(uint_fast8_t cBit)
	{
		m_cStatus &= ~cBit;
	}

	/**
	 * プリスケーラ再設定
	 */
	void RebuildTimeTable()
	{
		static const uint8_t table[3] = {6 * 12, 3 * 12, 2 * 12};
		const uint32_t nFmClock = m_nClock / table[m_cPrescale];
#if defined(_WIN32) && !defined(_WIN32_WCE)
		m_nTimerStep = MulDiv(1000000, 65536, nFmClock);
#else	// defined(_WIN32) && !defined(_WIN32_WCE)
		m_nTimerStep = static_cast<uint32_t>(static_cast<uint64_t>(1000000U) * 65536U / nFmClock);
#endif	// defined(_WIN32) && !defined(_WIN32_WCE)
	}

public:
	/**
	 * コンストラクタ
	 * @param[in] nClock クロック
	 */
	OpnTimer(uint32_t nClock = 3993600)
		: m_cStatus(0)
		, m_cPrescale(0)
		, m_cReg27(0)
		, m_nClock(nClock)
		, m_nTimerA(0)
		, m_nTimerACount(0)
		, m_nTimerB(0)
		, m_nTimerBCount(0)
		, m_nTimerStep(0)
	{
		m_cReg24[0] = 0;
		m_cReg24[1] = 0;
		Reset();
	}

	/**
	 * クロック変更
	 * @param[in] nClock クロック
	 */
	void SetClock(uint32_t nClock)
	{
		m_nClock = nClock;
		RebuildTimeTable();
	}

	/**
	 * レジスタ設定
	 * @param[in] nAddr アドレス
	 * @param[in] cData データ
	 */
	void SetReg(uint_fast16_t nAddr, uint_fast8_t cData)
	{
		switch (nAddr)
		{
			case 0x24: case 0x25:
				{
					m_cReg24[nAddr - 0x24] = cData;
					const uint32_t nValue = (m_cReg24[0] << 2) + (m_cReg24[1] & 3);
					m_nTimerA = (1024 - nValue) * m_nTimerStep;
				}
				break;

			case 0x26:
				m_nTimerB = (256 - cData) * m_nTimerStep;
				break;

			case 0x27:
				{
					const uint_fast8_t cTrigger = (~m_cReg27) & cData;
					m_cReg27 = cData;

					ResetStatus((cData >> 4) & 3);

					if (cTrigger & (1 << 0))
					{
						m_nTimerACount = (cData & (1 << 0)) ? m_nTimerA : 0;
					}
					if (cTrigger & (1 << 1))
					{
						m_nTimerBCount = (cData & (1 << 1)) ? m_nTimerB : 0;
					}
				}
				break;

			case 0x2d: case 0x2e: case 0x2f:
				{
					const uint_fast8_t cPrescale = nAddr - 0x2d;
					if (m_cPrescale != cPrescale)
					{
						m_cPrescale = cPrescale;
						RebuildTimeTable();
					}
				}
				break;
		}
	}

	/**
	 * リセット
	 */
	void Reset()
	{
		m_cStatus = 0;
		m_cPrescale = 0;
		RebuildTimeTable();

		m_nTimerACount = 0;
		m_nTimerBCount = 0;

		for (unsigned i = 0x24; i < 0x28; i++)
		{
			SetReg(i, 0);
		}
	}

	/**
	 * タイマー時間処理
	 * @param[in] us 経過時間
	 * @return イベント
	 */
	bool Count(int32_t us)
	{
		bool event = false;

		if (m_nTimerACount)
		{
			m_nTimerACount -= us << 16;
			if (m_nTimerACount <= 0)
			{
				event = true;
				while (m_nTimerACount <= 0)
				{
					m_nTimerACount += m_nTimerA;
				}
				if (m_cReg27 & 4)
				{
					SetStatus(1);
				}
			}
		}
		if (m_nTimerBCount)
		{
			m_nTimerBCount -= us << 12;
			if (m_nTimerBCount <= 0)
			{
				event = true;
				while (m_nTimerBCount <= 0)
				{
					m_nTimerBCount += m_nTimerB;
				}
				if (m_cReg27 & 8)
				{
					SetStatus(2);
				}
			}
		}
		return event;
	}

	/**
	 * 次にタイマが発生するまでの時間を求める
	 * @return 時間(us)
	 */
	int32_t GetNextEvent() const
	{
		const uint32_t ta = ((m_nTimerACount + 0xffff) >> 16) - 1;
		const uint32_t tb = ((m_nTimerBCount + 0x0fff) >> 12) - 1;
		return minimum(ta, tb) + 1;
	}

	/**
	 * ステータス
	 * @return ステータス
	 */
	uint_fast8_t ReadStatus() const
	{
		return m_cStatus;
	}
};
