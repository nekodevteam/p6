
#pragma once

struct tm
{
	int tm_sec;	// 秒 (0-59)
	int tm_min;	// 分 (0-59)
	int tm_hour;	// 時 (0-23)
	int tm_mday;	// 日 (1-31)
	int tm_mon;	// 月 - 1 (0-11)
	int tm_year;	// 年 - 1900
	int tm_wday;	// 曜日 (0-6)
	int tm_yday;	// 年通算日 (0-365)
	int tm_isdst;	// 季節時間フラグ (-1)
};

__inline time_t time(time_t *tloc)
{
	if (tloc)
	{
		*tloc = 0;
	}
	return 0;
}

__inline struct tm *localtime(const time_t *timeval)
{
	static struct tm s_tm = {0};
	return &s_tm;
}
