
#pragma once

#include <tchar.h>
#include <windows.h>

__inline LPTSTR _tgetcwd(LPTSTR buffer, int maxlen)
{
	LPTSTR p;
	GetModuleFileName(NULL, buffer, maxlen);
	p = _tcsrchr(buffer, TEXT('\\'));
	if (p)
	{
		*p = TEXT('\0');
	}
	return buffer;
}
