﻿/**
 * @file	shlwapi.h
 * @brief	パス関係クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <stdbool.h>
#include "win32.h"

#ifdef __cplusplus
extern "C"
{
#endif	// __cplusplus

LPTSTR PathAddBackslash(LPTSTR pszPath);
bool PathAddExtension(LPTSTR pszPath, LPCTSTR pszExt);
bool PathAppend(LPTSTR pszPath, LPCTSTR pszMore);
LPTSTR PathCombine(LPTSTR pszDest, LPCTSTR pszDir, LPCTSTR pszFile);
LPTSTR PathFindExtension(LPCTSTR pszPath);
LPTSTR PathFindFileName(LPCTSTR pszPath);
bool PathMatchSpec(LPCTSTR pszFile, LPCTSTR pszSpec, unsigned int dwFlags);
void PathRemoveExtension(LPTSTR pszPath);
bool PathRemoveFileSpec(LPTSTR pszPath);
bool PathRenameExtension(LPTSTR pszPath, LPCTSTR pszExt);

#ifdef __cplusplus
}
#endif	// __cplusplus
