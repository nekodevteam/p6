﻿/**
 * @file	shlwapi.cpp
 * @brief	パス関係クラスの動作の定義を行います
 */

#include "shlwapi.h"
#include <algorithm>
#include <memory.h>

/**
 * セパレータか?
 * @param[in] c キャラクタ
 * @retval true セパレータである
 * @retval false セパレータでない
 */
inline static bool IsSeparator(int c)
{
	return (c == '/');
}

/**
 * 比較用の変換
 * @param[in] c キャラクタ
 * @return キャラクタ
 */
inline static int ToLower(int c)
{
	return c;
}

/**
 * 長さを得る
 * @param[in] pszPath パス
 */
template<class T>
static int Length(T* pszPath)
{
	int nIndex = 0;
	if (pszPath)
	{
		while (pszPath[nIndex] != '\0')
		{
			nIndex++;
		}
	}
	return nIndex;
}

/**
 * 追加
 * @param[in,out] pszPath パス
 * @param[in] cchPath パスサイズ
 * @param[in] pszAdd 追加
 * @retval true すべてコピーした
 * @retval false オーバーフローした
 */
template<class T>
static bool Cat(T* pszPath, int cchPath, const T* pszAdd)
{
	bool r = true;
	int nSize = Length(pszAdd);
	if (nSize > 0)
	{
		const int nStart = Length(pszPath);
		const int nLimit = cchPath - nStart - 1;
		if (nSize > nLimit)
		{
			r = false;
			nSize = nLimit;
		}
		if (nSize > 0)
		{
			memcpy(pszPath + nStart, pszAdd, nSize * sizeof(T));
			pszPath[nStart + nSize] = '\0';
		}
	}
	return r;
}

/**
 * コピー
 * @param[in,out] pszPath パス
 * @param[in] cchPath パスサイズ
 * @param[in] pszAdd 追加
 */
template<class T>
static void Cpy(T* pszPath, int cchPath, const T* pszAdd)
{
	if (cchPath > 0)
	{
		int nSize = Length(pszAdd);
		nSize = (std::min)(nSize, cchPath - 1);
		if (nSize > 0)
		{
			memcpy(pszPath, pszAdd, nSize * sizeof(T));
		}
		pszPath[nSize] = '\0';
	}
}

/**
 * Adds a backslash to the end of a string to create the correct syntax for a path.
 * @param[in, out] pszPath A pointer to a buffer with a string that represents a path
 * @return A pointer
 */
LPTSTR PathAddBackslash(LPTSTR pszPath)
{
	int nIndex = Length(pszPath);
	if ((nIndex > 0) && (pszPath[nIndex - 1] != '/') && ((nIndex + 2) < MAX_PATH))
	{
		pszPath[nIndex] = '/';
		nIndex++;
		pszPath[nIndex] = '\0';
	}
	return pszPath + nIndex;
}

/**
 * Adds a file name extension to a path string.
 * @param[in,out] pszPath A pointer to a buffer with the null-terminated string to which the file name extension will be appended
 * @param[in] pszExt A pointer to a null-terminated string that contains the file name extension
 * @retval true If an extension was added
 * @retval false Otherwise
 */
bool PathAddExtension(LPTSTR pszPath, LPCTSTR pszExt)
{
	LPCTSTR p = PathFindExtension(pszPath);
	if ((p[0] == '\0') && (pszExt))
	{
		Cat(pszPath, MAX_PATH, pszExt);
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * Appends one path to the end of another
 * @param[in,out] pszPath A pointer to a null-terminated string to which the path specified in pszMore is appended
 * @param[in] pszMore A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to be appended
 * @retval true if successful
 * @retval false Otherwise
 */
bool PathAppend(LPTSTR pszPath, LPCTSTR pszMore)
{
	PathAddBackslash(pszPath);
	const bool r = Cat(pszPath, MAX_PATH, pszMore);

	const int nIndex = Length(pszPath);
	if ((nIndex > 0) && (pszPath[nIndex - 1] == '.'))
	{
		pszPath[nIndex - 1] = '\0';
	}
	return r;
}

/**
 * Concatenates two strings that represent properly formed paths into one path
 * @param[in,out] pszDest A pointer to a buffer that, when this function returns successfully, receives the combined path string
 * @param[in] pszDir A pointer to a null-terminated string of maximum length MAX_PATH that contains the first path
 * @param[in] pszFile A pointer to a null-terminated string of maximum length MAX_PATH that contains the second path
 * @return A pointer to a buffer
 */
LPTSTR PathCombine(LPTSTR pszDest, LPCTSTR pszDir, LPCTSTR pszFile)
{
	if (pszDest != pszDir)
	{
		Cpy(pszDest, MAX_PATH, pszDir);
	}
	if (pszFile)
	{
		PathAppend(pszDest, pszFile);
	}
	return pszDest;
}

/**
 * Searches a path for an extension
 * @param[in] pszPath A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to search, including the extension being searched for
 * @return The address of the "."
 */
LPTSTR PathFindExtension(LPCTSTR pszPath)
{
	if (pszPath == nullptr)
	{
		return nullptr;
	}

	LPCTSTR ret = nullptr;
	while (true /*CONSTCOND*/)
	{
		TCHAR c = *pszPath;
		if (c == '\0')
		{
			if (ret == nullptr)
			{
				ret = pszPath;
			}
			break;
		}
		else if (IsSeparator(c))
		{
			ret = nullptr;
		}
		else if (c == '.')
		{
			ret = pszPath;
		}
		pszPath++;
	}
	return const_cast<LPTSTR>(ret);
}

/**
 * Searches a path for a file name
 * @param[in] pszPath A pointer to a null-terminated string of maximum length MAX_PATH that contains the path to search
 * @return A pointer to the address
 */
LPTSTR PathFindFileName(LPCTSTR pszPath)
{
	if (pszPath == nullptr)
	{
		return nullptr;
	}

	LPCTSTR ret = pszPath;
	while (true /*CONSTCOND*/)
	{
		const TCHAR c = *pszPath++;
		if (c == '\0')
		{
			break;
		}
		else if (IsSeparator(c))
		{
			ret = pszPath;
		}
	}
	return const_cast<LPTSTR>(ret);
}

/**
 * Matches a file name from a path against one or more file name patterns
 * @param[in] pszFile A pointer to a null-terminated string that contains the path from which the file name to be matched is taken
 * @param[in] pszSpec A pointer to a null-terminated string that contains the file name pattern for which to search
 * @retval true A file name pattern specified in pszSpec matched the file name found in the string pointed to by pszFile
 * @retval false No file name pattern specified in pszSpec matched the file name found in the string pointed to by pszFile
 */
bool PathMatchSpec(LPCTSTR pszFile, LPCTSTR pszSpec, unsigned int dwFlags)
{
	while (true /*CONSTCOND*/)
	{
		const TCHAR s = *pszSpec++;
		if (s == '*')
		{
			while (true /*CONSTCOND*/)
			{
				if (PathMatchSpec(pszFile, pszSpec, dwFlags))
				{
					return true;
				}
				if (*pszFile == '\0')
				{
					return false;
				}
				pszFile++;
			}
		}
		else
		{
			const TCHAR c = *pszFile++;
			if (s == '?')
			{
				if (c == '\0')
				{
					return false;
				}
			}
			else
			{
				if (ToLower(s) != ToLower(c))
				{
					return false;
				}
				else if (c == '\0')
				{
					return true;
				}
			}
		}
	}
	return true;
}

/**
 * Removes the file name extension from a path, if one is present
 * @param[in,out] pszPath A pointer to a null-terminated string of length MAX_PATH from which to remove the extension
 */
void PathRemoveExtension(LPTSTR pszPath)
{
	LPTSTR ext = PathFindExtension(pszPath);
	if (ext)
	{
		*ext = '\0';
	}
}

/**
 * Removes the trailing file name and backslash from a path, if they are present
 * @param[in,out] pszPath A pointer to a null-terminated string of length MAX_PATH that contains the path from which to remove the file name
 * @retval true If something was removed
 * @retval false Otherwise
 */
bool PathRemoveFileSpec(LPTSTR pszPath)
{
	LPTSTR p = PathFindFileName(pszPath);
	if (p > pszPath)
	{
		--p;
	}
	const bool ret = (p[0] != '\0');
	p[0] = '\0';
	return ret;
}

/**
 * Replaces the extension of a file name with a new extension
 * @param[in,out] pszPath Pointer to a null-terminated string of length MAX_PATH in which to replace the extension
 * @param[in] pszExt Pointer to a character buffer that contains a '.' character followed by the new extension
 * @retval true If successful
 * @retval false 
 */
bool PathRenameExtension(LPTSTR pszPath, LPCTSTR pszExt)
{
	PathRemoveExtension(pszPath);
	return PathAppend(pszPath, pszExt);
}
