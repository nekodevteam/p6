/**
 * @file	win32.h
 * @brief	definitions for generic international windows functions
 */

#pragma once

// winnt.h
typedef char			TCHAR;
typedef char*			LPTSTR;
typedef const char*		LPCTSTR;
#define TEXT(x)			x

// winbase.h
#define lstrcat			strcat
#define lstrcmp			strcmp
#define lstrcpy			strcpy
#define lstrlen			strlen
#define wsprintf		sprintf

// windef.h
#define MAX_PATH		260

// tchar.h
#define _fgetts			fgets
#define _ftprintf		fprintf
#define _stscanf		scanf
#define _stprintf		sprintf
#define _tfopen			fopen
#define _tprintf		printf
#define _ttoi			atoi
#define _vstprintf		vsprintf

#ifndef _countof
#define _countof(a)			(sizeof((a)) / sizeof((a)[0]))
#endif	// !_countof
