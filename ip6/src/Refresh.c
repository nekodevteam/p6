/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                         Refresh.c                       **/
/**                                                         **/
/** by ISHIOKA Hiroshi 1998,1999                            **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/** and Adaptions for any X-terminal by Arnold Metselaar    **/
/*************************************************************/
// modified by Windy
/* get Type for storing X-colors */
#include <string.h>
#include <assert.h>

#include "P6.h"

#ifdef UNIX
#include <X11/Xlib.h>
#endif
#include "os.h"

#define  GLOBAL
#include "Refresh.h"

  int scale = 2;

  int param[16][3] = /* {R,G,B} */
    { {5,5,5},{4,3,0},{0,4,3},{3,4,0},{3,0,4},{4,0,3},{0,3,4},{3,3,3},
      {5,5,5},{4,0,0},{0,4,0},{4,4,0},{0,0,4},{4,0,4},{0,4,4},{4,4,4} };
  unsigned short trans[] = { 0x0000, 0x3fff, 0x7fff, 0xafff, 0xffff ,0x1400 };
//  unsigned char trans[] = { 0x00, 0x3f, 0x7f, 0xaf, 0xff ,0x14};
  int Pal11[ 4] = { 15, 8,10, 8 };
  int Pal12[ 8] = { 10,11,12, 9,15,14,13, 1 };
  int Pal13[ 8] = { 10,11,12, 9,15,14,13, 1 };
  int Pal14[ 4] = {  8,10, 8,15 };
  int Pal15[ 8] = {  8,13,11,10, 8,13,10,15 };
  int Pal53[32] = {  0, 4, 1, 5, 2, 6, 3, 7, 8,12, 9,13,10,14,11,15,
		    10,11,12, 9,15,14,13, 1,10,11,12, 9,15,14,13, 1 };

typedef struct  
{ 
  int bp_bitpix; 
  void (* SeqPix21Wide)  (ColTyp); 
  void (* SeqPix21Narrow)(ColTyp); 
  void (* SeqPix41Wide)  (ColTyp); 
  void (* SeqPix41Narrow)(ColTyp); 
} bp_struct; 

/* pointers to pixelsetters, used to adapt to different screen depths
   and window widths: */
typedef void (* t_SeqPix21) (ColTyp);
typedef void (* t_SeqPix41) (ColTyp);
static t_SeqPix21 SeqPix21;
static t_SeqPix41 SeqPix41;
static const bp_struct *funcs;

/* variables used to handle the buffer, should be register static but
   this is hard to do */
static byte *P;

/* initialisation of these variables */
static void SetScrVar(int y1,int x1)
{
  P=XBuf+(long)(Width*y1+x1)*scale*bitpix/8;
}


/* functions to set various numbers of pixels for various values of
   bitpix and lsbfirst. their names are built as follows:
   sp<md>_<bpp>[<bitfirst>]<width>
   <md>  =  22   to set 2 pixels in 2 colors or
            21   to set 2 pixels in 1 color
   <bpp> =  the number of bits per pixel
   <bitfirst> = m for msb first or
                l for lsb first
   <width>    = w for wide screen (set both pixels) or
                n for narrow screen (set only one)
*/

static void sp21_8w(ColTyp c)
{ P[0]=c.ct_byte[0]; P[1]=c.ct_byte[0]; P+=2; }
static void sp21_8n(ColTyp c)
{ P[0]=c.ct_byte[0]; P+=1; }
static void sp41_8w(ColTyp c)
{ P[0]=c.ct_byte[0]; P[1]=c.ct_byte[0]; P[2]=c.ct_byte[0]; P[3]=c.ct_byte[0]; P+=4; }
/* sp41_8n = sp21_8w */

static void sp21_16w(ColTyp c)
{ ((word *)P)[0] = (word)c.ct_xid; ((word *)P)[1] = (word)c.ct_xid; P+=4; }
static void sp21_16n(ColTyp c)
{ ((word *)P)[0] = (word)c.ct_xid; P+=2; }
static void sp41_16w(ColTyp c)
{ ((word *)P)[0] = (word)c.ct_xid; ((word *)P)[1] = (word)c.ct_xid; ((word *)P)[2] = (word)c.ct_xid; ((word *)P)[3] = (word)c.ct_xid; P+=8; }
/* sp41_16n = sp21_16w */

static void sp21_24w(ColTyp c)
{ P[0] = c.ct_byte[0]; P[1] = c.ct_byte[1]; P[2] = c.ct_byte[2];
  P[3] = c.ct_byte[0]; P[4] = c.ct_byte[1]; P[5] = c.ct_byte[2]; P+=6; }
static void sp21_24n(ColTyp c)
{ P[0] = c.ct_byte[0]; P[1] = c.ct_byte[1]; P[2] = c.ct_byte[2]; P+=3; }
static void sp41_24w(ColTyp c)
{ P[0] = c.ct_byte[0]; P[ 1] = c.ct_byte[1]; P[ 2] = c.ct_byte[2];
  P[3] = c.ct_byte[0]; P[ 4] = c.ct_byte[1]; P[ 5] = c.ct_byte[2];
  P[6] = c.ct_byte[0]; P[ 7] = c.ct_byte[1]; P[ 8] = c.ct_byte[2];
  P[9] = c.ct_byte[0]; P[10] = c.ct_byte[1]; P[11] = c.ct_byte[2]; P+=12; }
/* sp41_24n = sp21_24w */

static void sp21_32w(ColTyp c)
{ ((XID *)P)[0]=c.ct_xid; ((XID *)P)[1]=c.ct_xid; P+=8; }
static void sp21_32n(ColTyp c)
{ ((XID *)P)[0]=c.ct_xid; P+=4; }
static void sp41_32w(ColTyp c)
{ ((XID *)P)[0]=c.ct_xid; ((XID *)P)[1]=c.ct_xid; ((XID *)P)[2]=c.ct_xid; ((XID *)P)[3]=c.ct_xid; P+=16; }
/* sp41_32n = sp21_32w */

/** Pixel setting routines for any number of bits per pixel **/
static const bp_struct PixSetters[]= 
{
  { 8, sp21_8w,  sp21_8n,  sp41_8w,  sp21_8w},
  {16, sp21_16w, sp21_16n, sp41_16w, sp21_16w},
  {24, sp21_24w, sp21_24n, sp41_24w, sp21_24w},
  {32, sp21_32w, sp21_32n, sp41_32w, sp21_32w},
};

/* duplicates scanlines, used if interlacing is off  */
static void NoIntLac( int Y)
{
   byte *P;
   word linlen;
  
  linlen=Width*bitpix/8;
  P=XBuf+(long)scale*Y*linlen;
  memcpy(P+linlen,P,linlen);
}

void choosefuncs(int lsbfirst, int bitpix)
{
  unsigned int i;

  for (i=0 ; i<_countof(PixSetters) ; i++)
  {
    if (PixSetters[i].bp_bitpix == bitpix)
    {
      funcs=&(PixSetters[i]);
      break;
    }
  }
}

void setwidth(int wide)
{
  SeqPix21=wide ? funcs->SeqPix21Wide : funcs->SeqPix21Narrow;
  SeqPix41=wide ? funcs->SeqPix41Wide : funcs->SeqPix41Narrow;
}


/** Screen Mode Handlers [N60/N66][SCREEN MODE] **************/
void (*SCR[2+2][4])() =
{
  { RefreshScr10, RefreshScr10, RefreshScr10, RefreshScr10 },
  { RefreshScr51, RefreshScr51, RefreshScr53, RefreshScr54 },
  					/* ************ add 2002/2  Windy ******** */
  { RefreshScr61, RefreshScr62, RefreshScr62, RefreshScr63 },
  { RefreshScr61, RefreshScr62, RefreshScr62, RefreshScr63 },
};


#define NOINTLACM1(Y)	NoIntLac((M5HEIGHT-M1HEIGHT)/2+Y)
#define NOINTLACM5(Y)	NoIntLac(Y)
#define SETSCRVARM1(Y)	SetScrVar((M5HEIGHT-M1HEIGHT)/2+Y,(M5WIDTH-M1WIDTH)/2)
#define SETSCRVARM5(Y)	SetScrVar(Y,0)


/** RefreshScr ***********************************************/
/** Draw window functions for each basic/screen mode        **/
/*************************************************************/

/** RefreshScr10: N60-BASIC select function ******************/
void RefreshScr10()
{
  if ((*VRAM&0x80) == 0x00)
    RefreshScr11();
  else
    switch (*(VRAM)&0x1C) {
    case 0x00: case 0x10: /*  64x 64 color / 128x 64 */
      RefreshScr13a(); break;
    case 0x08: /* 128x 64 color */
      RefreshScr13b(); break;
    case 0x18: /* 128x 96 */
      RefreshScr13c(); break;
    case 0x04: /* 128x 96 color */
      RefreshScr13d(); break;
    case 0x14: /* 128x192 */
      RefreshScr13e(); break;
    default: /* 128x192 color / 256x192 */
      RefreshScr13(); break;
    }
}

/** RefreshScr11: N60-BASIC screen 1,2 ***********************/
void RefreshScr11()
{
   uint_fast8_t X,Y;
  
  const byte *G = CGROM;		/* CGROM */ 
  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* ascii/semi-graphic data */
  for(Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for(X=0; X<32; X++, T1++, T2++) {
      /* get CGROM address and color */
      const byte *S;
      ColTyp FC,BC;
      byte K;
      if (*T1&0x40) {	/* if semi-graphic */
		if (*T1&0x20) {		/* semi-graphic 6 */
		  S = G+((*T2&0x3f)<<4)+0x1000;
		  FC = BPal12[(((*T1) & 0x02) << 1) + ((*T2) >> 6)]; BC = BPal[8];
		} else {		/* semi-graphic 4 */
		  S = G+((*T2&0x0f)<<4)+0x2000;
		  FC = BPal12[(*T2&0x70)>>4]; BC = BPal[8];
		}
      } else {		/* if normal character */
		S = G+((*T2)<<4); 
		FC = BPal11[(*T1&0x03)]; BC = BPal11[(*T1&0x03)^0x01];
	      }
	      K=*(S+Y%12);
	      SeqPix21(K&0x80? FC:BC); SeqPix21(K&0x40? FC:BC);
	      SeqPix21(K&0x20? FC:BC); SeqPix21(K&0x10? FC:BC);
	      SeqPix21(K&0x08? FC:BC); SeqPix21(K&0x04? FC:BC);
	      SeqPix21(K&0x02? FC:BC); SeqPix21(K&0x01? FC:BC);
	    }
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
    if (Y%12!=11) { T1-=32; T2-=32; }
  }
}

/** RefreshScr13: N60-BASIC screen 3,4 ***********************/
void RefreshScr13()
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<32; X++,T1++,T2++) {
      if (*T1&0x10) { /* 256x192 (SCREEN 4) */
	if (scr4col) {
	  const ColTyp *Pal = BPal15 + ((*T1&0x02)<<1);
	  SeqPix41(Pal[(*T2&0xC0)>>6]);
	  SeqPix41(Pal[(*T2&0x30)>>4]);
	  SeqPix41(Pal[(*T2&0x0C)>>2]);
	  SeqPix41(Pal[(*T2&0x03)   ]);
	} else {
	  const ColTyp *Pal = BPal14 + (*T1&0x02);
	  SeqPix21(Pal[(*T2&0x80)>>7]);
	  SeqPix21(Pal[(*T2&0x40)>>6]);
	  SeqPix21(Pal[(*T2&0x20)>>5]);
	  SeqPix21(Pal[(*T2&0x10)>>4]);
	  SeqPix21(Pal[(*T2&0x08)>>3]);
	  SeqPix21(Pal[(*T2&0x04)>>2]);
	  SeqPix21(Pal[(*T2&0x02)>>1]);
	  SeqPix21(Pal[(*T2&0x01)   ]);
	}
      } else { /* 128x192 color (SCREEN 3) */
	const ColTyp *Pal = BPal13 + ((*T1&0x02)<<1);
	SeqPix41(Pal[(*T2&0xC0)>>6]);
	SeqPix41(Pal[(*T2&0x30)>>4]);
	SeqPix41(Pal[(*T2&0x0C)>>2]);
	SeqPix41(Pal[(*T2&0x03)   ]);
      }
    }
    if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr13a: N60-BASIC screen 3,4 **********************/
void RefreshScr13a() /*  64x 64 color / 128x 64 */
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<16; X++,T1++,T2++) {
      if (*T1&0x10) { /* 128x 64 */
	if (scr4col) {
	  const ColTyp *Pal = BPal15 + ((*T1&0x02)<<1);
	  ColTyp L;
	  SeqPix41(L=Pal[(*T2&0xC0)>>6]);
	  SeqPix41(L);
	  SeqPix41(L=Pal[(*T2&0x30)>>4]);
	  SeqPix41(L);
	  SeqPix41(L=Pal[(*T2&0x0C)>>2]);
	  SeqPix41(L);
	  SeqPix41(L=Pal[(*T2&0x03)   ]);
	  SeqPix41(L);
	} else { /*  64x 64 color */
	  const ColTyp *Pal = BPal14 + (*T1&0x02);
	  SeqPix41(Pal[(*T2&0x80)>>7]);
	  SeqPix41(Pal[(*T2&0x40)>>6]);
	  SeqPix41(Pal[(*T2&0x20)>>5]);
	  SeqPix41(Pal[(*T2&0x10)>>4]);
	  SeqPix41(Pal[(*T2&0x08)>>3]);
	  SeqPix41(Pal[(*T2&0x04)>>2]);
	  SeqPix41(Pal[(*T2&0x02)>>1]);
	  SeqPix41(Pal[(*T2&0x01)   ]);
	}
      } else { /*  64x 64 color */
	const ColTyp *Pal = BPal13 + ((*T1&0x02)<<1);
	ColTyp L;
	SeqPix41(L=Pal[(*T2&0xC0)>>6]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x30)>>4]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x0C)>>2]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x03)   ]);
	SeqPix41(L);
      }
    }
    if (Y%3 != 2) { T1-=16; T2-=16; }
    else if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr13b: N60-BASIC screen 3,4 **********************/
void RefreshScr13b() /* 128x 64 color */
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<32; X++,T1++,T2++) {
      const ColTyp *Pal = BPal13 + ((*T1&0x02)<<1);
      SeqPix41(Pal[(*T2&0xC0)>>6]);
      SeqPix41(Pal[(*T2&0x30)>>4]);
      SeqPix41(Pal[(*T2&0x0C)>>2]);
      SeqPix41(Pal[(*T2&0x03)   ]);
    }
    if (Y%3 != 2) { T1-=32; T2-=32; }
    else if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr13c: N60-BASIC screen 3,4 **********************/
void RefreshScr13c() /* 128x 96 */
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<16; X++,T1++,T2++) {
      if (scr4col) {
	const ColTyp *Pal = BPal15 + ((*T1&0x02)<<1);
	ColTyp L;
	SeqPix41(L=Pal[(*T2&0xC0)>>6]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x30)>>4]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x0C)>>2]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x03)   ]);
	SeqPix41(L);
      } else {
	const ColTyp *Pal = BPal14 + (*T1&0x02);
	SeqPix41(Pal[(*T2&0x80)>>7]);
	SeqPix41(Pal[(*T2&0x40)>>6]);
	SeqPix41(Pal[(*T2&0x20)>>5]);
	SeqPix41(Pal[(*T2&0x10)>>4]);
	SeqPix41(Pal[(*T2&0x08)>>3]);
	SeqPix41(Pal[(*T2&0x04)>>2]);
	SeqPix41(Pal[(*T2&0x02)>>1]);
	SeqPix41(Pal[(*T2&0x01)   ]);
      }
    }
    if (!(Y&1)) { T1-=16; T2-=16; }
    else if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr13d: N60-BASIC screen 3,4 **********************/
void RefreshScr13d() /* 128x 96 color */
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<32; X++,T1++,T2++) {
      const ColTyp *Pal = BPal13 + ((*T1&0x02)<<1);
      SeqPix41(Pal[(*T2&0xC0)>>6]);
      SeqPix41(Pal[(*T2&0x30)>>4]);
      SeqPix41(Pal[(*T2&0x0C)>>2]);
      SeqPix41(Pal[(*T2&0x03)   ]);
    }
    if (!(Y&1)) { T1-=32; T2-=32; }
    else if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr13e: N60-BASIC screen 3,4 **********************/
void RefreshScr13e() /* 128x192 */
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0200;	/* graphic data */
  for (Y=0; Y<M1HEIGHT; Y++) {
    SETSCRVARM1(Y);	/* Drawing area */
    for (X=0; X<16; X++,T1++,T2++) {
      if (scr4col) {
	const ColTyp *Pal = BPal15 + ((*T1&0x02)<<1);
	ColTyp L;
	SeqPix41(L=Pal[(*T2&0xC0)>>6]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x30)>>4]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x0C)>>2]);
	SeqPix41(L);
	SeqPix41(L=Pal[(*T2&0x03)   ]);
	SeqPix41(L);
      } else {
	const ColTyp *Pal = BPal14 + (*T1&0x02);
	SeqPix41(Pal[(*T2&0x80)>>7]);
	SeqPix41(Pal[(*T2&0x40)>>6]);
	SeqPix41(Pal[(*T2&0x20)>>5]);
	SeqPix41(Pal[(*T2&0x10)>>4]);
	SeqPix41(Pal[(*T2&0x08)>>3]);
	SeqPix41(Pal[(*T2&0x04)>>2]);
	SeqPix41(Pal[(*T2&0x02)>>1]);
	SeqPix41(Pal[(*T2&0x01)   ]);
      }
    }
    if (T1 == VRAM+0x200) T1=VRAM;
    if ((scale==2) && !IntLac) NOINTLACM1(Y);
  }
}

/** RefreshScr51: N60m/66-BASIC screen 1,2 *******************/
void RefreshScr51()
{
   uint_fast8_t X,Y;

  const byte *G = CGROM;		/* CGROM */ 
  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x0400;	/* ascii/semi-graphic data */
  const ColTyp *Pal = BPal + ((CSS & 2) << 2);
  for(Y=0; Y<M5HEIGHT; Y++) {
    SETSCRVARM5(Y);	/* Drawing area */
    for(X=0; X<40; X++, T1++, T2++) {
      /* get CGROM address and color */
      const byte *S = G+(*T2<<4)+(*T1&0x80?0x1000:0);	/* semi-graphics */
      const ColTyp FC = BPal[(*T1)&0x0F];
      const ColTyp BC = Pal[((*T1)&0x70)>>4];
      const byte K=*(S+Y%10);
      SeqPix21(K&0x80? FC:BC); SeqPix21(K&0x40? FC:BC);
      SeqPix21(K&0x20? FC:BC); SeqPix21(K&0x10? FC:BC);
      SeqPix21(K&0x08? FC:BC); SeqPix21(K&0x04? FC:BC);
      SeqPix21(K&0x02? FC:BC); SeqPix21(K&0x01? FC:BC);
    }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
    if (Y%10!=9) { T1-=40; T2-=40; }
  }
}

/** RefreshScr53: N60m/66-BASIC screen 3 *********************/
void RefreshScr53()
{
   uint_fast8_t X,Y;
  
  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x2000;	/* graphic data */
  const ColTyp *Pal = BPal53 + ((CSS & 7) << 2);
  for(Y=0; Y<M5HEIGHT; Y++) {
    SETSCRVARM5(Y);	/* Drawing area */
    for(X=0; X<40; X++) {
      SeqPix41(Pal[(((*T1) & 0xC0) >> 6) + (((*T2) & 0xC0) >> 4)]);
      SeqPix41(Pal[(((*T1) & 0x30) >> 4) + (((*T2) & 0x30) >> 2)]);
      SeqPix41(Pal[(((*T1) & 0x0C) >> 2) + (((*T2) & 0x0C) >> 0)]);
      SeqPix41(Pal[(((*T1) & 0x03) >> 0) + (((*T2) & 0x03) << 2)]);
      T1++; T2++;
    }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
  }
}

/** RefreshScr54: N60m/66-BASIC screen 4 *********************/
void RefreshScr54()
{
   uint_fast8_t X,Y;

  const byte *T1 = VRAM;		/* attribute data */
  const byte *T2 = VRAM+0x2000;	/* graphic data */
  const ColTyp *Pal = BPal53 + ((CSS & 7) << 2);
  for(Y=0; Y<M5HEIGHT; Y++) {
    SETSCRVARM5(Y);	/* Drawing area */
    for(X=0; X<40; X++) {
      SeqPix21(Pal[(((*T1) & 0x80) >> 7) + (((*T2) & 0x80) >> 6)]);
      SeqPix21(Pal[(((*T1) & 0x40) >> 6) + (((*T2) & 0x40) >> 5)]);
      SeqPix21(Pal[(((*T1) & 0x20) >> 5) + (((*T2) & 0x20) >> 4)]);
      SeqPix21(Pal[(((*T1) & 0x10) >> 4) + (((*T2) & 0x10) >> 3)]);
      SeqPix21(Pal[(((*T1) & 0x08) >> 3) + (((*T2) & 0x08) >> 2)]);
      SeqPix21(Pal[(((*T1) & 0x04) >> 2) + (((*T2) & 0x04) >> 1)]);
      SeqPix21(Pal[(((*T1) & 0x02) >> 1) + (((*T2) & 0x02) >> 0)]);
      SeqPix21(Pal[(((*T1) & 0x01) >> 0) + (((*T2) & 0x01) << 1)]);
      T1++;T2++;
    }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
  }
}

/*  -------------------------------------------------
$B$*$\$($,$-(B

$B!z(B SR$B$N(BSCREEN 1$B$O!"%b!<%I(B5$B$N%k!<%A%s$H$[$H$s$IF1$8$G$9$,!"0J2<$NE@$,0c$$$^$9!#(B
      1.$BJ8;z%G!<%?$HB0@-%G!<%?$NJB$SJ}$,0c$&(B
      2.WIDTH $B$G!"J8;z$NBg$-$5$d!"=D2#$NBg$-$5$,JQ$o$k(B
      3.$B%Q%l%C%H$G?'$,JQ$o$k!#(B

$B!z(B SCREEN1 $B$G%Q%l%C%H$rE,MQ$9$k;~$O!"0J2<$NCm0U$,I,MW$G$9!#(B
      1.VRAM$B$NCM$r!"%+%i!<%3!<%I(B-1$B$KJQ49(B (textpalet1)
      2.$B%Q%l%C%H$rE,MQ!#(B                (palet)
      3.$B%+%i!<%3!<%I(B-1$B$r!"(BVRAM$B$NCM$KJQ49(B (textpalet2)
    SCREEN 2,3 $B$O$=$N$^$^E,MQ$7$F$bBg>fIW$_$?$$$G$9!#(B

$B!!0JA0$N%Q%l%C%H%k!<%A%s$O!"(B1$B%I%C%H$:$D!!$$$A$$$AJQ49$7$F$$$^$7$?$,!"(B
$B!!$=$l$@$H!!$"$^$j$K$bCY$$$N$G!"$"$i$+$8$a!!JQ49$7$F$*$/$h$&$K$7$^$7$?!#(B


 $B!z(B 640x200 $B%I%C%H$KBP1~$9$k$?$a$K$O!"(B
      1.scale==2$B$G5/F0$9$k$H!"(B640x400$B%I%C%H$K$J$k$N$G!"(B
      2.$B2#(B640$B%I%C%H$K$7$?$$$H$3$m$G!"(Bsetwidth(0)$B$H$9$k!#(B
      3.$B=*$C$?$i!"$9$0$K(B setwidth(1) $B$9$k!#(B
      
   $B0JA0$O!"(Bscale==1 $B$N$H$-!":8H>J,$7$+I=<($5$l$J$$MM$K$7$F$$$^$7$?$,!"(B
   $B<+F0E*$K!"(Bscale==2$B$K!!JQ$o$k$h$&$K$7$^$7$?!#(B
                                                                                                    Windy
*/


/** RefreshScr61: N66-SR BASIC screen 1 *******************/
/* Modified by windy from RefreshScR51() */
/* support: WIDTH 40,20 40,25 80,20 80,25  and PALET */
void RefreshScr61()
{
   uint_fast8_t X,Y;

							// MODE 6 $B$N(BTEXT VRAM$B$N@hF,%"%I%l%9$O!"(BTEXTVRAM$B$H$9$k!#(B 2003/10/25

  const byte *G = CGROM;		/* CGROM */
  const byte *T1 = TEXTVRAM+1;		/* attribute data */
  const byte *T2 = TEXTVRAM;	        	/* ascii/semi-graphic data */
  const byte high= (rows==20)? 10:8;       /* charactor's high   SR BASIC 2002/2/23 */
  const unsigned int addr= (rows==20)? 0x2000: 0x1000; /* CGROM address  SR BASIC 2002/2/23 */
  const unsigned int semi_addr= (rows==20)? 0x3000: 0x4000; /* semi address 2003/7/8 */
  const ColTyp *Pal = BPal + ((CSS & 2) << 2);

  if( cols==80) setwidth(0);	// 640x200 pixels

//  assert( CGROM==CGROM6);

  for(Y=0; Y<M5HEIGHT; Y++) {
    SETSCRVARM5(Y);	/* Drawing area */
    for(X=0; X< cols; X++, T1+=2, T2+=2) {
//      const byte *S= G+(*T2<<4)+(*T1&0x80?addr+0x1000:addr); /*for CGROM6 SR semi graph 2002/2/23*/
        const byte *S= G+(*T2<<4)+(*T1&0x80?semi_addr:addr); /*for CGROM6 SR semi graph 2002/2/23*//* 2003/7/8 */
        const ColTyp FC = BPal[ (*T1)&0x0F];					   /* fast Palet (2002/9/27) */
        const ColTyp BC = Pal[ ((*T1)&0x70)>>4];
	
        const byte K=*(S+Y%high);			/* character high 2002/2/23 */
        SeqPix21(K&0x80? FC:BC); SeqPix21(K&0x40? FC:BC);
        SeqPix21(K&0x20? FC:BC); SeqPix21(K&0x10? FC:BC);
        SeqPix21(K&0x08? FC:BC); SeqPix21(K&0x04? FC:BC);
        SeqPix21(K&0x02? FC:BC); SeqPix21(K&0x01? FC:BC);
    }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
    if (Y% high!=high-1) { T1-=cols*2; T2-=cols*2; } /* character high  2002/2/23 */
  }
  setwidth(scale-1);			// 320x200 pixels
}

/* -------------------------------------
N66-SR BASIC$B$N%0%i%U%#%C%/(BVRAM$B$N3JG<J}K!(B

$B%0%i%U%#%C%/2hLL$r!":81&$KJ,$1$F3JG<$5$l$F$$$^$9!#(BSCREEN 2$B$N$P$"$$!"(B
$B:8$,!"(B(0,0)-(255,199) $B1&$,(B (256,0)-(319,199)$B$NNN0h$K$J$j$^$9!#(B
$B:8$N3+;O%"%I%l%9$O!"(B1A00h  $B1&$O!"(B0000h$B$G$9!#(B

$B%T%/%;%k%G!<%?$N3JG<J}K!$OFHFC$G$9!#%o!<%IC10L$G3JG<$5$l$F$$$^$9!#(B
$B6v?t$H4q?t%i%$%s$,!"8r8_$K8=$l$F$$$^$9!#(B
$B$3$N%3!<%I$G!"1&C<$^$G9T$C$F$+$i!"0lC6La$C$F$$$k$N$O!"$=$N$?$a$G$9!#(B
$B$"$H(BSCREEN 3 $B$G$b!"#1%o!<%I$NCf?H$,0c$&$@$1$G!"$[$H$s$IF1$846$8$G$9!#(B

bugs:

$B$3$N%3!<%I$G$O!":8H>J,$H1&H>J,$rF1$8JB$SJ}$H$7$F07$C$F$$$^$9$,!"(B
$B<B5!$G$O!"1&H>J,$NJB$SJ}$O0c$$$^$9$N$G!"87L)E*$K$O!"4V0c$C$F$$$k$h$&$G$9!#(B
                                                                             Windy
*/

/** RefreshScr62  N66-SR BASIC screen 2 *********************/
/* Modified by windy  from RefreshScr54() *//* support: PALET */
void RefreshScr62()
{
  uint_fast8_t X,Y;

  const byte *T1 = VRAM+ 0x1a00;
  const byte *T2 = VRAM;
  const ColTyp *Pal = BPal53 + ((CSS & 4) << 2);

  for(Y=0; Y<M6HEIGHT; Y++) {
    SETSCRVARM5(Y); /* Drawing area */
       for(X=0; X< 256/4; X++) {
         SeqPix21(Pal[ ((*T1)&0x0f) ]);
         SeqPix21(Pal[ ((*T1)&0xf0)>>4 ]);
         T1++;
         SeqPix21(Pal[ ((*T1)&0x0f) ]);
         SeqPix21(Pal[ ((*T1)&0xf0)>>4 ]);
         T1+=3;
        }

      for(X=0 ;X<64/4 ; X++) {
         SeqPix21(Pal[ ((*T2)&0x0f) ]);
         SeqPix21(Pal[ ((*T2)&0xf0)>>4 ]);
         T2++;
         SeqPix21(Pal[ ((*T2)&0x0f) ]);
         SeqPix21(Pal[ ((*T2)&0xf0)>>4 ]);
         T2+=3;
        }
      if( (Y & 1)==0)
             { T1-=(254);  T2-=(62);}
      else
             { T1-=2; T2-=2; }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
  }
}

/** RefreshScr63  N66-SR BASIC screen 3 *********************/
/* Modified by windy  from RefreshScr54() */
void RefreshScr63()
{
  uint_fast8_t X,Y;

  const byte *T1 = VRAM+ 0x1a00;
  const byte *T2 = VRAM;
  const ColTyp *Pal = BPal53 + ((CSS & 7) << 2);

  if(scale!=2) return;	// scale==1 $BBP:v(B!! Thanks Bernie 2003/8/3

  setwidth( 0);	// add windy  640x200 pixels  add 2002/7/14

  for(Y=0; Y<M6HEIGHT; Y++) {
    SETSCRVARM5(Y); 	/* Drawing area */

       for(X=0; X< 256/4; X++) {
         SeqPix21(Pal[(((*T1) & 0x80) >> 7) + (((*(T1+1)) & 0x80) >> 6)]);
         SeqPix21(Pal[(((*T1) & 0x40) >> 6) + (((*(T1+1)) & 0x40) >> 5)]);
         SeqPix21(Pal[(((*T1) & 0x20) >> 5) + (((*(T1+1)) & 0x20) >> 4)]);
         SeqPix21(Pal[(((*T1) & 0x10) >> 4) + (((*(T1+1)) & 0x10) >> 3)]);
         SeqPix21(Pal[(((*T1) & 0x08) >> 3) + (((*(T1+1)) & 0x08) >> 2)]);
         SeqPix21(Pal[(((*T1) & 0x04) >> 2) + (((*(T1+1)) & 0x04) >> 1)]);
         SeqPix21(Pal[(((*T1) & 0x02) >> 1) + (((*(T1+1)) & 0x02) >> 0)]);
         SeqPix21(Pal[(((*T1) & 0x01) >> 0) + (((*(T1+1)) & 0x01) << 1)]);
         T1+=4;
       }

      for(X=0 ;X<64/4 ; X++) {
          SeqPix21(Pal[(((*T2) & 0x80) >> 7) + (((*(T2+1)) & 0x80) >> 6)]);
          SeqPix21(Pal[(((*T2) & 0x40) >> 6) + (((*(T2+1)) & 0x40) >> 5)]);
          SeqPix21(Pal[(((*T2) & 0x20) >> 5) + (((*(T2+1)) & 0x20) >> 4)]);
          SeqPix21(Pal[(((*T2) & 0x10) >> 4) + (((*(T2+1)) & 0x10) >> 3)]);
          SeqPix21(Pal[(((*T2) & 0x08) >> 3) + (((*(T2+1)) & 0x08) >> 2)]);
          SeqPix21(Pal[(((*T2) & 0x04) >> 2) + (((*(T2+1)) & 0x04) >> 1)]);
          SeqPix21(Pal[(((*T2) & 0x02) >> 1) + (((*(T2+1)) & 0x02) >> 0)]);
          SeqPix21(Pal[(((*T2) & 0x01) >> 0) + (((*(T2+1)) & 0x01) << 1)]);
         T2+=4;
        }
      if( (Y & 1)==0)
             { T1-=(254);  T2-=(62);}
      else
             { T1-=2; T2-=2; }
    if ((scale==2) && !IntLac) NOINTLACM5(Y);
  }

  setwidth( scale-1);	// add windy  320x200 pixels  2002/7/14
}


// *************** fast palet ************************************
void do_palet(int dest,int src)
{
 static const byte textpalet2[16]={0,4,1,5,2,6,3,7,8,12,9,13,10,14,11,15}; /*  color code-> VRAM code*/
 	// *************** for RefreshScr 53/54/62/63 ***************************
    if((CSS & 4) ==0 )	// CSS3 =0
    	{
	    if(dest>=0 && dest<32 && src>=0 && src<32)
	    	{
    	     BPal53[dest]= BPal62[src];
    	    }
    	}
	else					// CSS3 =1
		{
	    if(dest>=0 && dest<32 && src>=0 && src<32)
	       {
			int dest1 = -1;
			int dest2 = -1;
	        switch( dest+1)
	       	{
			 case 16: dest1 =13; dest2 = 5; break;
			 case 15: dest1 =10; dest2 = 2; break;
			 case 14: dest1 =14; dest2 = 6; break;
			 case 13: dest1 = 1; dest2 = 9; break;
			}
			if (dest1 != -1)
			{
				BPal53[16+dest1-1]= BPal62[src];
			}
			if (dest2 != -1)
			{
				BPal53[16+dest2-1]= BPal62[src];
			}
		   }
    	}
    	
   // ************** for RefreshScr51/61 **************************
	if(dest>=0 && dest<16 && src>=0 && src<16)
        BPal[textpalet2[dest]]= BPal61[ textpalet2[src]];  
}




               /* get CGROM address and color */
              // S = G+(*T2<<4)+(*T1&0x80?0x1000:0);
              // S = G+(*T2<<4)+(*T1&0x80?0x3000:0x2000);   /* for CGROM6 SR BASIC 2002/2/23*/
              //      K=*(S+Y%10);
            /*    if (Y%10!=9) { T1-=40*2; T2-=40*2; } */
