/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Win32fscr.h                   **/
/** modified by Windy 2002-2003                             **/
/*************************************************************/
#ifndef _WIN32FSCR_H
#define _WIN32FSCR_H

/* ----------- full screeen  --------------- */
void savewindowstate( HWND hwnd);
void restorewindowstate(HWND hwnd);
void saveDisplayMode(void);		//  ディスプレイモードの保存
void storeDisplayMode(void);		//  ディスプレィモードを元に戻す。
int toggleFullScr(void);			//  フルスクリーン <---> ウインドウ 切り替え
int isFullScreen(void);

/* ----------- popup menu  --------------- */
void loadPopupMenu( HINSTANCE hInstance, LPCTSTR menuname);//POPUP メニューを読み込む
void openPopupMenu( HWND hwnd , int x, int y); // POPUP メニューを開く
HMENU getPopupMenu( void);		// getHpopupMenu: POPUP メニューのハンドルを返す

#endif
