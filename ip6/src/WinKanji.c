/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           WinKanji.c                    **/
/**                    make kanji rom for WIN32             **/
/** This code is Written by Windy 2003                      **/
/*************************************************************/
#ifdef WIN32
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <assert.h>
#include "Win32.h"
#include "cgrom.h"

extern HWND hwndMain;

#define WIDTH  (16*2)
#define HEIGHT (16*2)

#if !defined(_WIN32_WCE)
static unsigned char table_extkanji[]={		// Ext Kanji rom
#include "extkanji.h"
0x00
};
#endif	// !defined(_WIN32_WCE)
/* $BBgNL$K(B $B7Y9p$,=P$k$N$O!"(Bunsigned $B$,H4$1$F$$$?$+$i$@$=$&$G$9!#$9$$$^$;$s!&!&!#!J!2!K(BWindy*/



// ****************************************************************************
//          make_extkanjirom: $B56$N3HD%4A;z(BROM$B$r%a%b%j!<>e$K:n@.(B
// ****************************************************************************
/* In: mem  EXTKANJIROM
  Out: ret  1:successfull  0: failed  */
int make_extkanjirom(char *mem)
{
 int   ret=0;
#if !defined(_WIN32_WCE)
 HDC   hdc;
 HIBMP hb;
 HFONT hfont;
 HFONT hfontbak;
 char  *str;
 BYTE  *p;
 int   w;
 int   maxlen;
 int   i;
 int   idx;
 int   dy;
 char  *left_rom = mem;
 char  *right_rom= mem +0x10000;
 
 hb = createIBMP(hwndMain, WIDTH ,HEIGHT,1);	// make surface 1bpp 
 if( hb.lpBMP != NULL)
 	{
#if 1			// *********** $B$f$/$f$/$O!"6&DL%k!<%A%s$K0\9T(B ************
	 hb.lpBmpInfo->bmiColors[0].rgbBlue  =0;
	 hb.lpBmpInfo->bmiColors[0].rgbGreen =0;
	 hb.lpBmpInfo->bmiColors[0].rgbRed   =0;
	 hb.lpBmpInfo->bmiColors[0].rgbReserved =0;

	 hb.lpBmpInfo->bmiColors[1].rgbBlue  =255;
	 hb.lpBmpInfo->bmiColors[1].rgbGreen =255;
	 hb.lpBmpInfo->bmiColors[1].rgbRed   =255;
	 hb.lpBmpInfo->bmiColors[1].rgbReserved =0;
#endif

	 hdc = GetDC( hwndMain);
 
	 // hBitmapbak = SelectObject(hdcimage, hBitmap);
	 SetDIBColorTable(hb.hdcBmp, 0, 2, ((BITMAPINFO *)hb.lpBmpInfo)->bmiColors);

	 hfont = CreateFont(16,0, 
 						FW_DONTCARE, FW_DONTCARE, FW_REGULAR,
						FALSE, FALSE, FALSE, 
						SHIFTJIS_CHARSET,
						OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
						DEFAULT_QUALITY, 
						FF_MODERN | FIXED_PITCH,
						 TEXT(""));
	 hfontbak = (HFONT)SelectObject(hb.hdcBmp, hfont);

	 SetTextColor(hb.hdcBmp, RGB(255, 255, 255));
	 SetBkColor(hb.hdcBmp, RGB(0, 0, 0));

	 idx= 0x1210;			// start address   (jis 1 suijun)
	 str   = (char *)table_extkanji;
	 maxlen= lstrlenA( str);
	 w   = 2;
	 for(i=0; i< maxlen; i+=2)
		{
		 SelectObject(hb.hdcBmp, GetStockObject( BLACK_BRUSH));
		 Rectangle(   hb.hdcBmp, 0,0 ,16  ,16);
		 TextOutA(     hb.hdcBmp, 0,0 ,str+i ,w);		// Put one kanji

		// ------- Convert FONT image ---------
		 p= hb.lpBMP;
		 for(dy=0 ; dy< 16; dy++)
			{
			left_rom [ idx ]= *(p+0);
			right_rom[ idx ]= *(p+1);
			p+= WIDTH/8;
			idx++;
			assert(idx < 0x10000);
		   }
#if 0
	 BitBlt( hdc ,0,0,WIDTH,HEIGHT, hb.hdcBmp,0,0,SRCCOPY);
	 UpdateScreen();
#endif
		}

	 left_rom[ 0x2c9 ]= 0x41;		// bios checks this character
	right_rom[ 0x2c9 ]= 0x41;

	 SelectObject(hb.hdcBmp, hfontbak);
	 DeleteObject( hfont );
	 ReleaseDC(hwndMain,hdc);
	 releaseIBMP( hb );
	ret=1;
	}
 else
    {
	 printf("createIBMP failed\n");
	}
#endif	// !defined(_WIN32_WCE)
 return( ret);
}
// SelectObject(hb.hdcBmp, hBitmapbak);
//DeleteDC(hb.hdcBmp);



#if 0
// ****************************************************************************
//          save_extkanjirom: $B56$N3HD%4A;z(BROM$B$NJ]B8!!!!!J:#$OL$;HMQ!K(B
// ****************************************************************************
int save_extkanjirom(char *file_name)
{
 FILE *fp;
 char *left_rom = mem;
 char *right_rom= mem +0x10000;

 printf("\nwriting extend kanji rom....");
 fp=fopen( file_name,"wb"); 
 if( fp==NULL) {printf("\n %s open failed \n",file_name);return(0);}
 
 do
    {
	 if(fwrite( left_rom,sizeof( left_rom),1,fp)!=1)
	 	break;
	 if(fwrite(right_rom,sizeof(right_rom),1,fp)!=1)
		break;
	 fclose(fp);
	 printf("done.\n");
	 return(1);
	}
 while(0);
 fclose(fp);
 printf("FAILED.\n");
 return(0);
}
#endif




#endif
