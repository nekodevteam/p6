// ******************************************************
//   Build.h  for iP6 Plus build number$B!!!J%S%k%I%J%s%P!<!K(B
//            by Windy
// *******************************************************

#define PROGRAM_NAME "iP6 Plus 0.6.4 "
#define BUILD_NO     "Rel.4.7 +2"
#define BUILD_DATE   "(Build 2023/12/29)"

#define AUTHOR       "Modified by Windy / yuinejp"
#define HOMEPAGE_URL "https://gitlab.com/nekodevteam/p6"
#define HELP_FILE    "HELP-4.7.html"
