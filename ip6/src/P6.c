/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           P6.c                          **/
/**                                                         **/
/** modified by Windy 2002-2004                             **/
/** This code is based on ISHIOKA Hiroshi 1998-2000         **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
/* Modified by windy */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "P6.h"
#include "fdc.h"
#include "disk.h"
#include "d88.h"
#include "Refresh.h"
#include "cgrom.h"
#include "Option.h"
#include "Sound.h"
#ifdef SOUND
#include "fm.h"
#endif	// SOUND
#include "error.h"

#include "os.h"
#include <shlwapi.h>

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define PRINTOK     if(Verbose) _tprintf(MsgOK)
#define PRINTFAILED if(Verbose) _tprintf(MsgFAILED)


static const TCHAR MsgOK[]      = TEXT("OK\n");
static const TCHAR MsgFAILED[]  = TEXT("FAILED\n");

extern int CPUclock;
extern int SaveCPU;

int UseCPUThread=0;       // 1: CPU $B$rJL%9%l%C%I$GF0$+$9!#(B  0: $BC10l%9%l%C%I$GF0$+$9!#(B

/* ****************** N66SR BASIC  ******************** add 2002/2 */
int port60[16];					//I/O[60..67] READ  MEMORY MAPPING
								//I/O[68-6f]  WRITE MEMORY MAPPING
int port93;						//I/O[93]     8255 MODE SET / BIT SET & RESET
int port94;						//I/O[94]	  shadow of I/O[90]
int portBC;						//I/O[BC]     INTERRUPT ADDRESS of VRTC

int portC1= 0x00;				//I/O[C1]     CRT CONTROLLER MODE
int portC8= 0x00;				//I/O[C8]     CRT CONTROLLER TYPE
int portCA;						//I/O[CA]     X GEOMETORY low  HARDWARE SCROLL
int portCB;						//I/O[CB]     X GEOMETORY high HARDWARE SCROLL
int portCC;						//I/O[CC]     Y GEOMETORY      HARDWARE SCROLL

int portCE;						//I/O[CE]     LINE SETTING  BITMAP (low) */
int portCF;						//I/O[CF]     LINE SETTING  BITMAP (High) */


int portFA;						//I/O[FA]     INTERRUPT CONTROLLER
int portFB;						//I/O[FB]     INTERRUPT ADDRESS CONTROLLER

int sr_mode;					// SR MODE TRUE: sr_mode  FALSE: not sr_mode
int sr_mode_bak;

int bitmap=0;					// TRUE: BITMAP MODE   FALSE: TEXT MODE
int cols=40;					// WIDTH COLUME 
int rows=20;					// WIDTH LINES  

int lines=200;					// graphics LINES



byte port_c_8255=0;				// 8255 port c



byte *SYSTEMROM1 = NULL;        // SYSTEMROM1
byte *SYSTEMROM2 = NULL;		// SYSTEMROM2
byte *CGROM6 = NULL;            // CGROM for N66SR-BASIC   add 2002/2/20
/* CGROM6 = CGROM1+ CGROM5  */


byte *EXTKANJIROM= NULL;		// EXT KANJIROM
int  extkanjirom_adr=0;			// EXT KANJIROM addr
int  extkanjirom=0;				// EXT KANJIROM 1:enable  0:disable
int  new_extkanjirom=0;

byte *EXTRAM     = NULL;		// EXT RAM 64kb  for SR
int  extram     =0;				// EXT RAM      1:enable  0:disable
int  new_extram =0;


/* palet datas $BA4$F$N?'$NG[Ns$r0l1~J]M-$7$F$$$k$@$1(B */
int palet[ 16]      ={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}; // SR PALET  add 2002/4/21 (windy)


/* ****************** FLOPPY DISK  ******************** */
int portDC;						//I/O[DC]     FDC status
int portD1=0;					//I/O[D1]     MINI DISK  (CMD/DATA OUTPUT
int portD2=0x00;				//I/O[D2]     MINI DISK  (CONTROL LINE INPUT)

int disk_type;					// TRUE: PD765A   FALSE: mini disk
int disk_num;					// DRIVE NUMBERS  0:disk non  1-2:disk on line
int new_disk_num;				//                     (next boot time)
int UseDiskLamp;				// 1: use disk lamp  0: no use
    /* ************************************************* */



int P6Version=4;				// CURRENT MACHINE TYPE
int newP6Version;				//     (next boot time)

int  PatchLevel = 1;
int CGSW93 = FALSE;
int portF0 = 0x11;				//I/O [F0]	MEMORY MAPPING [N60/N66]
int portF1 = 0xDD;				//I/O [F1]	MEMORY MAPPING [N60/N66]
int portF3 = 0;					//I/O [F3]  WAIT CONTROLL
int portF6;						//I/O [F6]  TIMER COUNTUP 
int portF7 = 0x06;				//I/O [F7]  TIMER INT ADDRESS 
/* $B=i4|CM$O(B0x06$B$H$9$k(B (PC-6001$BBP1~(B) */



byte Verbose = 1;
byte *BASICROM = NULL;			// BASICROM 
byte *VOICEROM = NULL;			// VOICEROM
byte *KANJIROM = NULL;			// KANJIROM
byte *CurKANJIROM = NULL;		// CURRENT KANJIROM


/*
byte *ROM2 = NULL;
*/
byte *SYSROM2 = NULL;			// SYSROM2   (old style)
/* 99.06.02.*/

byte *CGROM = NULL;				// CURRENT CGROM
byte *CGROM1 = NULL;			// N60 CGROM
byte *CGROM5 = NULL;			// N66 CGROM
byte *EXTROM = NULL;			// CURRENT EXTEND ROM
byte *EXTROM1 = NULL;			// EXTEND ROM 1
byte *EXTROM2 = NULL;			// EXTEND ROM 2
TCHAR Ext1Name[PATH_MAX] = TEXT("");    /* Extension ROM 1 file  (4000h-5fffh)*/
TCHAR Ext2Name[PATH_MAX] = TEXT("");    /* Extension ROM 2 file  (6000h-7fffh)*/

byte *RdMem[8];					// READ  MEMORY MAPPING ADDRESS
byte *WrMem[8];					// WRITE MEMORY MAPPING ADDRESS
byte *VRAM;						// VRAM ADDRESS
byte *TEXTVRAM;					// TEXT VRAM ADDRESS (MODE 6 $B$N$_(B) screen 2,2,1$BBP:v(B  add 2003/10/25

static unsigned int VRAMHead[2][4] = {
  { 0xc000, 0xe000, 0x8000, 0xa000 },
  { 0x8000, 0xc000, 0x0000, 0x4000 },
};



byte *EmptyRAM;
byte *RAM;
byte EnWrite[4];				// MEMORY MAPPING WRITE ENABLE [N60/N66]
byte CRTMode1,CRTMode2,CRTMode3;

byte PSGReg=0;					// PSG REGISTER LATCH
//byte PSG[16];
byte PSGTMP[ 0xc0];				// PSG & FM REGISTER add 2002/10/15
byte JoyState[2];
byte CSS;						// CSS

byte UPeriod     = 2;           // Interrupts/scr. update 1:60fps  2:30fps
static byte EndOfFrame=1;              // 1 when end of frame

byte p6key = 0;
byte stick0 = 0;
byte keyGFlag = 0;
byte kanaMode = 0;
byte katakana = 0;
byte kbFlagGraph = 0;
byte kbFlagCtrl = 0;
byte TimerSW = 0;
byte TimerSW_F3 = 1;		/* $B=i4|CM$O(B1$B$H$9$k!J(BPC-6001$BBP1~!K(B */

int IntSW_F3 = 1;
int Code16Count = 0;

TCHAR PrnName[PATH_MAX] = TEXT("");    /* Printer redirect. file */
FILE *PrnStream  = NULL;

	/* ************** TAPE $B$r(BLOAD /SAVE $B$GJ,$1$k!!(B2003/10/27  ****************** */
byte CasMode = CAS_NONE;
TCHAR CasName[2][PATH_MAX] = {TEXT(""),TEXT("")};  /* Tape image file      0:load  1:save  */
FILE *CasStream[2]  = {NULL,NULL};

TCHAR DskName[2][PATH_MAX] = {TEXT(""),TEXT("")};    /* Disk image file      */
FILE *DskStream[2]  = {NULL,NULL};

TCHAR CasPath[2][PATH_MAX] = {TEXT(""),TEXT("")};    /* Tape image path */
TCHAR DskPath[2][PATH_MAX] = {TEXT(""),TEXT("")};    /* Disk image path */
TCHAR RomPath[PATH_MAX] = TEXT("rom/");/* Rom  image path */

TCHAR ImgPath[PATH_MAX] = TEXT("");    /* snapshot image path */
TCHAR MemPath[PATH_MAX] = TEXT("");    /* memory   image path */

int FastTape;	// use Fast Tape     1: high  0:normal


int   CPUclock;					// CPU clock
int   CPUclock_bak;				// CPU clock backup
int   drawwait;					// draw wait
int   drawwait_bak;				// draw wait backup

int busreq;						// busreq  1:ON  0:OFF
int busreq_bak;
int srline;						// sr line   enable when busreq=1
int srline_bak;					// sr line   enable when busreq=1

int   scr4col;
int   Console=0;				// use console mode    1: use

int   UseSaveTapeMenu=0;		// save tape menu      1: on  0: off
int   UseStatusBar=0;           // status bar          1: on  0: off


// *********** N66SR BAIC  *************************
// *********** Date Read Interrupt *****************
#define DATE_NONE	0			// Date Interrupt: None
#define DATE_WRITE	1			// Date Interrupt: Writting...
#define DATE_READ	2			// Date Interrupt: Reading...
byte 	DateMode= DATE_NONE;	// Date mode
byte 	DateBuff[ 5];			// Date buffer
int		DateIdx;				// Index of Date buffer


// ****************************************************************************
//          fmtdate: $B8=:_F|;~$r(B PC-6601SR $B$N%?%$%^!<MQ$NJ8;zNs$K%;%C%H$7$FJV5Q(B
// ****************************************************************************
char *fmtdate(void)
{
	char tmpbuff[100+1] ;
	static char outbuff[5+1];
	int i;
	int wday;
 
	time_t  t;
	struct tm *tms;
	time(&t);
	tms =localtime(&t);
        
	// fixed wday (PC-6601SR's wday is 0:Mon 1:Tue ... 6:Sun), add windy 2002/8/16
	wday = (tms->tm_wday >0)? tms->tm_wday-1 : 6;	
	outbuff[0]=((tms ->tm_mon)+1)<<4 | (wday);

	sprintf(tmpbuff,"%02d%02d%02d%02d", tms->tm_mday ,tms->tm_hour ,tms->tm_min ,tms->tm_sec);
	for (i=0; i<4; i++)
     {
      outbuff[1+i]= ((tmpbuff[i*2]-'0')<<4) + (tmpbuff[i*2+1]-'0');
     }
	return ( outbuff);
}


// *********** TV Reserve Data Read Interrupt ***********
#define TVR_NONE		 0	// TV Reserve Interrupt: none
#define TVR_WRITE		 1	// TV Reserve Interrupt: writting...
#define TVR_READ		 2	// TV Reserve Interrupt: reading...
byte TvrMode= TVR_NONE;
byte TvrBuff[ 2];			// data buffer (dummy array    max length: Unknown)
int    TvrIdx;			// Index of data buffer


/****************************************************************/
/*** INTERNAL FUNCTION  PROTOTYPE                             ***/
/****************************************************************/


/* Load a ROM file into given pointer */
byte *LoadROM(byte **mem, LPCTSTR name, size_t size);
int LoadEXTROM(byte *mem, LPCTSTR name, size_t size);

void Printer(byte V);             /* Send a character to a printer   */

static byte_fast gvram_read(register word_fast A);
static int chk_gvram(register word_fast A,int flag);
static void gvram_write(register word_fast A,register byte_fast V);
void subcpuOUT(byte Value);
byte subcpuIN(void);

void init_tapebuffer(void);
void write_tapebuffer( byte Value);
int read_tapebuffer(byte *Value);
void InitMemmap(void);

int  make_internalroms(void);
void make_semigraph(void);
int load_roms(void);

// ****************************************************************************
//          peek_memory: RAM$B$+$i%G!<%?$rFI$_=P$9!#!J>o$K(BRAM$B$+$iFI$_=P$9!#!K(B
//	In:  A: address
//  Out: data
// ****************************************************************************
byte_fast peek_memory(register word_fast A)
{
	return( *(RAM+A));
}

/****************************************************************/
/*** This function is called when a read from RAM occurs.     ***/
/****************************************************************/
byte_fast M_RDMEM(register word_fast A)
{
			/* Graphics Vram Read (SR basic) add 2002/2 */
	if(sr_mode && chk_gvram(A,0))
    	{
    	 return(gvram_read(A));
     	}
	else
			/* normal memory read ..*/
    	{
      	return(RdMem[A>>13][A&0x1FFF]);
     	}
}

/****************************************************************/
/*** This function is called when a write to RAM occurs. It   ***/
/*** checks for write protection and slot selectors.          ***/
/****************************************************************/
void M_WRMEM(register word_fast A,register byte_fast V)
{
			/* Graphics Vram Write (SR basic) add 2002/2 */
   if(sr_mode && chk_gvram(A ,8)) 
		{
        gvram_write(A,V);
   		}
   else {			/* normal memory write ..*/
      	if(EnWrite[A>>14]) 
      		WrMem[A>>13][A&0x1FFF]=V;
#ifdef DEBUG
      else printf("M_WRMEM:%4X\n",A);
#endif
   		}
}


/****************************************************************/
/*** Vram Window check  ($B%P%s%/@ZBX$($NAk$+$I$&$+$rH=JL!#(B)      */
/*                                                              */
/*   In: A: Address   flag: 0 or 8                              */
/*   Out: True:  Vram Window       False: normal access         */
/*                                      add 2002/2            ***/
/****************************************************************/
static int chk_gvram(register word_fast A,int flag)
{
	int ret;
	ret=0;

/* printf("port%X[%X] \n",(A>>13)+flag,port60[(A>>13)+flag]); */
	if(  port60[ (A>>13)+flag ]==0x00 && (bitmap ))	// VRAM $B$N@hF,$+$D!"(BCRT$B$,(B BITMAP mode 
     	      ret=1;         /* TRUE: vram window */
return(ret);
}


/****************************************************************/
/*** Vram Read           SR BASIC      add 2002/2             ***/
/***                                                            */
/*** $B%P%s%/@ZBX$($G$J$/!"%"%/%;%9$9$k$Y$-(BVRAM$B$r7W;;$7$F$$$^$9!#(B */
/*** $BB>$K$b!"<B:]$K%P%s%/@ZBX$($9$kJ}K!$b$"$k$+$bCN$l$^$;$s!#(B   */
/****************************************************************/
static byte_fast gvram_read(register word_fast A)
{
	 byte* adr;
	 byte  ret;
	 int x,y,z,w,off;

//if( A >319) {  PRINTDEBUG(TEXT("gvram_read:out of range:  x is %d **************\n"),A);}
	 x = A & 0x1fff;
	 y = portCF*16+portCE;		      /* y$B:BI8(B */
	 if( y >=204) y-=204;		      /* Y$B:BI8(B 204 $B0J>e$@$H(B 204 $B0z$/(B add 2003/10/22 */
	 w = (x <256) ? 256: 64;          /* width:0..255 $B$J$i(B256 / 256..319$B$J$i(B 64$B$K$9$k(B*/
	 off=(x <256) ? 0x1a00: 0x0000;   /* offset: Vram offset address */
	 x = (x <256) ? x: x-256;	      /* x:256..319 $B$J$i(B 256$B$r0z$/!!(B */
	 z = ((y & 1 )==1) ? 2: 0;        /* z:Y$B:BI8$,4q?t$J$i!"(B2$B$rB-$9(B  */
	 adr = (VRAM+ (off+ (y>>1)*w + (x&0xffc)+z));

/* printf("read) x=%d y=%d w=%d off=%04X add=%d\n",x,y,w,off,(off+(y>>1)*w+x+z));*/
	 switch(x & 3) {		/* Word Access $B$r$9$k(B */
	    case 0: ret=  *(adr);      break;
	    case 1: ret=  *(adr)>>4;   break;
	    case 2: ret=  *(adr+1);    break;
	    case 3: ret=  *(adr+1)>>4; break;
	   }
 	return(ret );
}


/****************************************************************/
/*** Vram Write            SR BASIC       add 2002/2  Windy   ***/
/***                                                            */
/*** $B%P%s%/@ZBX$($G$J$/!"%"%/%;%9$9$k$Y$-(BVRAM$B$r7W;;$7$F$$$^$9!#(B */
/*** $BB>$K$b!"<B:]$K%P%s%/@ZBX$($9$kJ}K!$b$"$k$+$bCN$l$^$;$s!#(B   */
/****************************************************************/
static void gvram_write(register word_fast A,register byte_fast V)
{
 byte* adr;
 int x,y,z,w,off;

 //if( A >319) { PRINTDEBUG(TEXT("gvram_write:out of range:  x is %d **************\n"),A);}
	x = A & 0x1fff;
	y = portCF*16+portCE;           /* y$B:BI8(B */
	if( y >=204) y-=204;			/* Y$B:BI8(B 204 $B0J>e$@$H(B 204 $B0z$/(B add 2003/10/22 */
	w = (x <256) ? 256: 64;         /* width:0..255 $B$J$i(B256 / 256..319$B$J$i(B 64$B$K$9$k(B*/
	off=(x <256) ? 0x1a00: 0x0000;  /* offset: Vram offset address */
	x = (x <256) ? x: x-256;	    /* x:256..319 $B$J$i(B 256$B$r0z$/!!(B */
	z = ((y & 1 )==1) ? 2: 0;       /* z:Y$B:BI8$,4q?t$J$i!"(B2$B$rB-$9(B  */
	V&= 0x0f;

	adr = VRAM+(off+ (y>>1)*(w) + (x&0xffc)+z);
/* printf("write) x=%d y=%d w=%d off=%04X ,\n",x,y,w,off,(off+(y>>1)*w+x+z)); */
	 switch(x & 3) {
	    case 0: *(adr)=(*(adr)  &0xf0)  |V;    break;
	    case 1: *(adr)=(*(adr)  &0x0f)  |V<<4; break;
	    case 2: *(adr+1)=(*(adr+1)&0xf0)|V;    break;
	    case 3: *(adr+1)=(*(adr+1)&0x0f)|V<<4; break;
	   }
}


/*
   $BA0$+$iIT;W5D$@$C$?$N$G$9$,!"(B
   SR$B$N(BY$B:BI8$O!"(B0$B!A(B203$B$^$G$J$N$K!"2?8N$+!"(BportCE/CF $B$G!"(B9$B%S%C%H!J(B0$B!A(B511$B!K$b;XDj$,(B
   $B=PMh$k$h$&$K$J$C$F$$$^$9!#IT;W5D$G$9$M$'!A!#(B

   $B$G$b!"<B:]$K$O!"(B204$B0J>e;XDj$7$?$i!"4,J*$N$h$&$K!">e$KLa$j$^$9!#(B
   $B$D$^$j!"(B204$B0J>e$@$H!";XDjCM$h$j(B 204$B0z$$$?:BI8$,!"K\Ev$N(BY$B:BI8$K$J$j$^$9!#(B

   $B$b$7$+$9$m$H!"(BY$BJ}8~(B512$B%I%C%H$^$GI=<(2DG=$@$C$?L>;D$J$N$+$b!)(B
   $B$=$&$$$($P!"(BX$BJ}8~$N(BVRAM$B$O!"(B256$B%I%C%H!\(B64$B%I%C%H$H$$$&Iw$K!"J,$+$l$F$$$?$N$G!"(B
   $B$b$7$+$9$k$H!"$b$7$+$9$k$+$b!)(B(^^;

   $B$A$J$_$K!"(BPort CF$B$O(B  $B=PNO$7$F$"$C$F$b!"0UL#$,L5$$$h$&$G$9!#(B
   $B$I$&$d$i!"L$;HMQ$_$?$$$G$9$M!#(B($B4@(B)
   								                        2003/10/22  Windy
*/


/****************************************************************/
/*** Write number into given IO port.                         ***/
/****************************************************************/
void DoOut(register byte_fast Port,register byte Value)
{
  if (Verbose&0x02) printf("--- DoOut : %02X, %02X ---\n", Port, Value);

		// ------------- PALET ----------------
  switch(Port) {
   case 0x40:
   case 0x41:
   case 0x42:
   case 0x43: 
		  if( P6Version ==2 || P6Version ==4)
		    {
			int reg,val;
			reg= 15-(Port-0x40);
			val= 15-Value;
			
			if( palet[reg] !=val)
				{
				PRINTDEBUG(TEXT("[P6.c][DoOut] palet port=%x   Value=%x \n"),Port,Value);
				}
			palet[ reg]= val;	// $B0l1~!"J];}$7$F$*$/(B
			do_palet( reg,val);	// fast palet  2002/7/29 
			}
			break;	

		// --------- SR  READ MEMORY MAPPING -------------- add 2002/2
   case 0x60:
   case 0x61:
   case 0x62:
   case 0x63:
   case 0x64:
   case 0x65:
   case 0x66:
   case 0x67:
		  if( P6Version ==2 || P6Version ==4)
		    {
             int start_adr;
             start_adr= Value & 0xe;

             port60[Port-0x60]= Value;
             switch( Value & 0xf0) {
	    	 case 0xf0: RdMem[(Port& 0xf)]=SYSTEMROM1+(start_adr)*0x1000;break;
	    	 case 0xe0: RdMem[(Port& 0xf)]=SYSTEMROM2+(start_adr)*0x1000;break;
	    	 case 0xd0: RdMem[(Port& 0xf)]=    CGROM6+(start_adr)*0x1000;break;
	    	 case 0xc0: RdMem[(Port& 0xf)]=   EXTROM2; /*+(start_adr)*0x1000; */break;
	    	 case 0xb0: RdMem[(Port& 0xf)]=   EXTROM1; /*+(start_adr)*0x1000; */break;
	    	 case 0x00: RdMem[(Port& 0xf)]=       RAM+(start_adr)*0x1000;break;
	    	 case 0x20: if(EXTRAM)
			 			  RdMem[ Port & 0xf]=  EXTRAM+((start_adr)*0x1000);
			 			break;
             }
            }
            return;

	/* EXTROM1$B$H!"(BEXTROM2 $B$N(Bstart_adr $B$O!"0UL#$,L5$$$h$&$J$N$GB-$5$J$$$3$H$K$7$^$7$?!#(B
    $B!!(B 2003/10/27   Thanks Chocobon!
    */

		// --------- SR WRITE  MEMORY MAPPING -------------- add 2002/2
   case 0x68:
   case 0x69:
   case 0x6a:
   case 0x6b:
   case 0x6c:
   case 0x6d:
   case 0x6e:
   case 0x6f:
   /* printf("write map [%X]=%X  EnWrite=%X\n",Port,Value,((Port&0xe)-8)/2); */
		  if( P6Version ==2 || P6Version ==4)
		    {
            port60[Port-0x60]= Value;
			if((Value & 0xf0)==0x00)
				{
				 WrMem[ (Port& 0xf)-8]= RAM+((Value & 0xe)*0x1000);
				 EnWrite[ ((Port & 0xe)-8)/2 ]= 1;
				}

			if( EXTRAM )
	          if((Value & 0xf0)==0x20)
				{
				 WrMem[ (Port& 0xf)-8]= EXTRAM+((Value & 0xe)*0x1000);
				}
	       }
           return;
           
		// --------- 8251 (RS-232C) --------------
   case 0x80: return;
   case 0x81: return;                   /* 8251 status          */

		// --------- 8255 PORT A   (SUB CPU) --------------
   case 0x90:                           /* subCPU               */
			subcpuOUT(Value);
			return;

		// --------- 8255 PORT B   (SUB CPU) --------------
   case 0x91: Printer((byte)~Value); return;  /* printer data         */

		// --------- 8255 PORT C   (SUB CPU) --------------
   case 0x92: return;                   /* printer,CRT,CG,sub   */

		// --------- MODE SET / BIT SET&RESET (PORT C)   --------------
   case 0x93:
    		port93 = Value;
    		if( Value &1)
    				port_c_8255 |=   1<<((Value>>1)&0x07);
			else
    				port_c_8255 &= ~(1<<((Value>>1)&0x07));

			switch(Value) {
		    	 /*
		    	case 0x02: EndOfFrame=0; break;
		    	case 0x03: EndOfFrame=1; break;
		    	*/
	        	case 0x04: CGSW93 = TRUE; RdMem[3]=CGROM; break;
	        	case 0x05: CGSW93 = FALSE; if(!sr_mode){ DoOut(0xF0,(byte)portF0);}
	        	case 0x08: port_c_8255 |= 0x88; break;	// $BAw?.5v2D(B
	         	case 0x09: port_c_8255 &= 0xf7; break;	// $BAw?.=`Hw40N;(B
	         	case 0x0c: port_c_8255 |= 0x28; break;	// $B<u?.5v2D?=@A(B
	         	case 0x0d: port_c_8255 &= 0xf7; break;	// $B<u?.=`Hw40N;(B
				}
			port_c_8255 |= 0xa8;
		    return;

		// --------- SOUND REGISTER ADDRESS LATCH   --------------
   case 0xA0:
    		if( P6Version==2 || P6Version==4)
				PSGReg=Value;			/* YM-2203 reg addr */
			else
				PSGReg=Value&0x0F;		/* 8910 reg addr */
			return; 

		// --------- SOUND WRITE DATA               --------------
   case 0xA1: 
			PSGTMP[PSGReg]=Value;        /* 8910 /YM-2203 data            */
#ifdef SOUND
			ym2203_setreg(PSGReg,Value);
#endif	// SOUND
	        return;

   case 0xA3: return;                   /* 8910 inactive ?      */

		// --------- SYSTEM LATCH              --------------
   case 0xB0:
            if( !sr_mode)    /*  not VRAM address in the SR MODE  add 2002/2*/
                {
                 VRAM=RAM+VRAMHead[CRTMode1][(Value&0x06)>>1];
                }
            TimerSW=(Value&0x01)?0:1;
			PRINTDEBUG(TEXT("[P6.c][DoOut] port B0  Value=%02X  PC=%04X  TimerSW=%d  \n"),Value ,getPC(),TimerSW );
            return;

		// --------- INTERRUPT ADDRESS  (B8 - BF) ----------
   case 0xBC:
            portBC= Value;  		// VRTC Interrupt address 2002/4/21
            PRINTDEBUG(TEXT("[P6.c][DoOut] Port 0xBC  Value=%2X \n"),Value); 
            return; 

		// --------- Color Set Select        --------------
   case 0xC0:                           /* CSS                  */
            CSS = Value;
            return;
		// --------- CRT CONTROLLER MODE     --------------
   case 0xC1:
			if( Value != portC1)
               {
                PRINTDEBUG(TEXT("[P6.c][DoOut] Port 0xc1  cols=%4d ,scrnmode=%d  ,  g_w=%d  \n")
              ,(Value&2)?40:80  ,(Value&4)?1:2 ,(Value&8)?320:640 ); 
               }
            portC1= Value;

            if( sr_mode_bak   != sr_mode)  ClearScr();// 2002/9/3  $B$/$jH4$-BP:v(B
            if((CRTMode1)&&(Value&0x02)) ClearScr();
            
            CRTMode1=(Value&0x02) ? 0 : 1;
            CRTMode2=(Value&0x04) ? 0 : 1;
            CRTMode3=(Value&0x08) ? 0 : 1;
			
			if( sr_mode)	// lines    (sr mode only)
	            lines=(Value&0x01) ? 200 : 204;
			
            if( !sr_mode)	// use CGROM 
                CGROM = ((CRTMode1 == 0) ? CGROM1 : CGROM5);
            else
                CGROM = CGROM6;    // N66SR BASIC use CGROM6  add 2002/2/20

			if( sr_mode && CRTMode3 && CRTMode2 && bitmap) // N66SR BASIC & screen 3
				{
				if( scale==1)  resizewindow( 2,2);	// scale 1 -> resize scale 2
				}

		// ********* select  width 80  / width 40    2002/7/14 ***********
        // ********* 0xc8 $B$+$i0\F0$7$F$-$^$7$?!#(B
		  if( sr_mode)	
		    {
		     if(CRTMode1==1      && CRTMode2==0 && !bitmap) /* width 80 */
		 	 	{
				 cols=80;
				 if( scale==1) resizewindow(2,2); // scale 1 -> resize scale 2 2003/4/27
				}
 		     else if(CRTMode1==0 && CRTMode2==0 && !bitmap) /* Width 40 */
			 	{
				cols=40;
				}
			 }

            sr_mode_bak  = sr_mode;	// add windy 2002/9/3 $B$/$jH4$-BP:v(B
            return;

		// --------- ROM SELECT     --------------
   case 0xC2:
			/* ---------------------------------
				B1   B0
				|     ----0: VOICEROM       1:KANJIROM
				----------0: KANJIROM LEFT  1:KANJIROM RIGHT
			--------------------------------*/  
            if(sr_mode) { return;}     /* not SR basic   add 2002/2 */
            							// KANJIROM ON
            if ((Value&0x02)==0x00) 
				CurKANJIROM=KANJIROM;
            else 
				CurKANJIROM=KANJIROM+0x4000;
			
            if ((Value&0x01)==0x00) 	// VOICEROM ON
             	{
				// --- 2000.10.13.
             	if(P6Version==3)
	               {
	               if(RdMem[0]!=BASICROM       && RdMem[0]!=RAM ) 
	               		RdMem[0]=VOICEROM;
	               if(RdMem[1]!=BASICROM+0x2000&& RdMem[1]!=RAM+0x2000)
	               		RdMem[1]=VOICEROM+0x2000;

    	           }

			     if( P6Version==4) /* add 2002/2/25 for TV yoyaku PC-6601SR*/
	               {  			/* mapping SYSTEMROM2 on mode=5 */
	               if(RdMem[0]!=BASICROM)        RdMem[0]=SYSTEMROM2;
	               if(RdMem[1]!=BASICROM+0x2000) RdMem[1]=SYSTEMROM2+0x2000;
		          }
			     // --- 2000.10.13.
				    {
				     if(RdMem[2]!=BASICROM+0x4000) RdMem[2]=VOICEROM;
			         if(RdMem[3]!=BASICROM+0x6000) RdMem[3]=VOICEROM+0x2000;
			        }
                }
           else 
		        {
		         DoOut(0xF0,(byte)portF0);	// curKANJIROM $B$rE,MQ(B
                };
               /* 99.06.02. */
           /*
             else {RdMem[0]=CurKANJIROM;RdMem[1]=CurKANJIROM+0x2000;};
           */

           return;

		// --------- I/O C2H IN/OUT     --------------
		// FFH: OUT   00H: IN
   case 0xC3: return;                   /* C2H in/out switch    */

		// --------- CRT CONTROLLER TYPE --------------
   case 0xc8:
			//printf("c8==%02X\n",Value);
		   if( Value != portC8)
		   	{
		     PRINTDEBUG(TEXT("[P6.c][DoOut] Port 0xc8  vram=%4X ,bitmap=%d    , rows=%d "),(Value & 0x10)?0x8000:0x0,  (Value &8)? 0:1, (Value&4)?20:25);
		     PRINTDEBUG(TEXT(",bus_req=%d, sr_mode=%d\n"),(Value & 0x2)?0:1,((Value&1)==1)?0:1);
		   	}
		   busreq_bak = busreq;
		   
		   portC8  = Value;        /* crt contoller type  SR BASIC add 2002/2 */
		   bitmap  = (Value & 8)? 0:1;
		   rows    = (Value & 4)? 20:25;
		   busreq  = (Value & 2)? 0:1;
		   sr_mode = ((Value & 1)==1) ? 0 : 1;                 /* sr_mode */

#if 1
		   if( busreq_bak != busreq)	// busreq change ?
		   	{
	 		SetValidLine_sr( drawwait);	// re-calc drawwait ($B:F7W;;(B)
			SetClock(CPUclock*1000000);
			}
#endif

		   if( bitmap && sr_mode)
          	{                 /* VRAM address (bitmap) */
	    	 VRAM = (Value & 0x10) ? RAM+0x8000:RAM+0x0000; /*vram address*/
	        }

    /* 1)sr_mode $B$K$J$C$?$i(B MODE 5$B$+$i(B6$B$K0\9T$9$k$H$-!"(BCGROM$B$,A0$N$^$^$J$N$K(B
        RefreshScr61 $B$r8F$P$l$k$H$$$1$J$$$N$G(B CGROM$B$r=i4|2=$7$F$*$/(B    2003/11/9
       2)sr_mode $B$K$J$C$?$i(B MODE 5$B$+$i(B6$B$KLa$C$F!"(B6$B$+$i(B5$B$K$J$k$H$-$b$"$k$N$G(B
         portF0$B$r=i4|2=$7$F$*$/!#(B
       */
           if(sr_mode)
            {
             CGROM=CGROM6; 
             portF0=0x11;
            }
		 return;
		
		// --------- TEXT VRAM ADDRESS -------------- add 2002/2
  case 0xC9:
	/*  VRAM $B$+$i(B TEXTVRAM $B$KJQ99!#(BVRAM$B@hF,%"%I%l%9$r!"%0%i%U%#%C%/$H!"%F%-%9%H$GJ,$1$?!#(B
                 screen 2,2,1$BBP:v(B  add 2003/10/25 */
	      if( sr_mode && !bitmap ) 
	          {		
	           TEXTVRAM=RAM+(Value & 0xf)*0x1000;
	           //printf("VRAM= %4X  \n",  (Value & 0xf)*0x1000);
	          }
	       return;
	       
		// --------- HARDWARE SCROLL  -------------- add 2002/2
  case 0xCA: if( P6Version ==2 || P6Version ==4)
	  			 portCA=Value;	// Graphics scroll X low
  			 return;	
  case 0xCB: if( P6Version ==2 || P6Version ==4)
				 portCB=Value;  // Graphics scroll X high
  			 return;	
  case 0xCC: if( P6Version ==2 || P6Version ==4)
				 portCC=Value;	// Graphics scroll Y
  			 return;

	// --------- BITMAP ACCESS LINE  -------------- add 2002/2
  case 0xCE: if( P6Version ==2 || P6Version ==4)
				portCE=Value; /* Graphics Y zahyou SR-BASIC add 2002/2 */
  			 return;
  case 0xCF: if( P6Version ==2 || P6Version ==4) 
  					{
				    //portCF=Value; 	// port CF $BL$;HMQ$K$7$^$7$?!#(B2003/10/22
					portCF=0;
                    }
			 return;

	// *********   DISK DRIVE  ***************  add 2002/2
	/* ---------------------------------
	mini floppy disk unit:
		D0h: data input
		D1h: data output
		D2h: control input
		D3h: control output

	FDC:
		D0h: FDC buffer
		D1h: FDC buffer
		D2h: FDC buffer
		D3h: FDC buffer
          --------------------------------*/
    	
  case 0xD0:
  case 0xD2: if(disk_type) {fdc_push_buffer( Port,Value);}/* FDC fd buffer */
                       return;
  case 0xD1:
  case 0xD3: if(disk_type)
	             {fdc_push_buffer( Port,Value);}
	       else
	             {disk_out( Port, Value);} /* disk command,data 2002/3/13*/
	       return;

  case 0xDA: fdc_outDA( Value);		/* FDC read/write sectors */
	       return;
  case 0xDD: fdc_outDD( Value);		/* FDC command */
           return;


	/* ------------------------------------------
		N60m/N66 BASIC  $B$N%a%b%j!<%^%C%T%s%0(B
       ------------------------------------------ */

  case 0xF0:                         		/* read block set       */
	  if( sr_mode || P6Version==0) return;	/* sr_mode or PC-6001 do nothing! 9/20*/
      										// thanks Mr.Bernie 2003/7/25
      portF0 = Value;
      switch(Value&0x0f)
       {
	    case 0x00: RdMem[0]=				RdMem[1]=EmptyRAM; break;
	    case 0x01: RdMem[0]=BASICROM;   	RdMem[1]=BASICROM+0x2000; break;
	    case 0x02: RdMem[0]=CurKANJIROM;	RdMem[1]=CurKANJIROM+0x2000; break;
	    case 0x03: RdMem[0]=				RdMem[1]=EXTROM2; break;
	    case 0x04: RdMem[0]=				RdMem[1]=EXTROM1; break;
	    case 0x05: RdMem[0]=CurKANJIROM;	RdMem[1]=BASICROM+0x2000; break;

	    /*
	   case 0x06: RdMem[0]=BASICROM;RdMem[1]=CurKANJIROM+0x2000; break;
	   */
	   case 0x06:
	         RdMem[0]=BASICROM;
	         RdMem[1]=(SYSROM2==EmptyRAM ? CurKANJIROM+0x2000 : SYSROM2);
	         break;
	         /* 99.06.02. */

	   case 0x07: RdMem[0]=EXTROM1;			RdMem[1]=EXTROM2; 			break;
	   case 0x08: RdMem[0]=EXTROM2;			RdMem[1]=EXTROM1; 			break;
	   case 0x09: RdMem[0]=EXTROM2;			RdMem[1]=BASICROM+0x2000; 	break;
	   case 0x0a: RdMem[0]=BASICROM;		RdMem[1]=EXTROM2; 			break;
	   case 0x0b: RdMem[0]=EXTROM1;			RdMem[1]=CurKANJIROM+0x2000;break;
	   case 0x0c: RdMem[0]=CurKANJIROM;		RdMem[1]=EXTROM1;			break;
	   case 0x0d: RdMem[0]=RAM;				RdMem[1]=RAM+0x2000; 		break;
	 /*case 0x0e: RdMem[0]=					RdMem[1]=EmptyRAM; break; */
	   case 0x0e: if( EXTRAM) 
	   				{RdMem[0]=EXTRAM;		RdMem[1]=EXTRAM+0x2000;break;} // 2003/5/4
	   case 0x0f: RdMem[0]=					RdMem[1]=EmptyRAM; break;
      };
          
      switch(Value&0xf0) 
      {
	   case 0x00: RdMem[2]=					RdMem[3]=EmptyRAM; break;
	   case 0x10: RdMem[2]=BASICROM+0x4000;	RdMem[3]=BASICROM+0x6000; break;
	   case 0x20: RdMem[2]=VOICEROM;		RdMem[3]=VOICEROM+0x2000; break;
	   case 0x30: RdMem[2]=					RdMem[3]=EXTROM2; break;
	   case 0x40: RdMem[2]=					RdMem[3]=EXTROM1; break;
	   case 0x50: RdMem[2]=VOICEROM;		RdMem[3]=BASICROM+0x6000; break;
	   case 0x60: RdMem[2]=BASICROM+0x4000;	RdMem[3]=VOICEROM+0x2000; break;
	   case 0x70: RdMem[2]=EXTROM1;			RdMem[3]=EXTROM2; break;
	   case 0x80: RdMem[2]=EXTROM2;			RdMem[3]=EXTROM1; break;
	   case 0x90: RdMem[2]=EXTROM2;			RdMem[3]=BASICROM+0x6000; break;
	   case 0xa0: RdMem[2]=BASICROM+0x4000;	RdMem[3]=EXTROM2; break;
	   case 0xb0: RdMem[2]=EXTROM1;			RdMem[3]=VOICEROM+0x2000; break;
	   case 0xc0: RdMem[2]=VOICEROM;		RdMem[3]=EXTROM1; break;
	   case 0xd0: RdMem[2]=RAM+0x4000;		RdMem[3]=RAM+0x6000; break;
	/* case 0xe0: RdMem[2]=					RdMem[3]=EmptyRAM; break; */
	   case 0xe0: if( EXTRAM) 
	   				{RdMem[2]=EXTRAM+0x4000;RdMem[3]=EXTRAM+0x6000; break;} //2003/5/4
	   case 0xf0: RdMem[2]=					RdMem[3]=EmptyRAM; break;
       };
      if (CGSW93) RdMem[3] = CGROM;
      return;
  case 0xF1:                           /* read block set       */
	  if( sr_mode || P6Version==0) return;	/* sr_mode or PC-6001 do nothing! 9/20*/
										// (thanks Mr. Bernie 3/7/25)
      portF1 = Value;
     switch(Value&0x0f) {
	   case 0x00: RdMem[4]=					RdMem[5]=EmptyRAM; break;
	   case 0x01: RdMem[4]=BASICROM;		RdMem[5]=BASICROM+0x2000; break;
	   case 0x02: RdMem[4]=CurKANJIROM;		RdMem[5]=CurKANJIROM+0x2000; break;
	   case 0x03: RdMem[4]=					RdMem[5]=EXTROM2; break;
	   case 0x04: RdMem[4]=					RdMem[5]=EXTROM1; break;
	   case 0x05: RdMem[4]=CurKANJIROM;		RdMem[5]=BASICROM+0x2000; break;
	   case 0x06: RdMem[4]=BASICROM;		RdMem[5]=CurKANJIROM+0x2000; break;
	   case 0x07: RdMem[4]=EXTROM1;			RdMem[5]=EXTROM2; break;
	   case 0x08: RdMem[4]=EXTROM2;			RdMem[5]=EXTROM1; break;
	   case 0x09: RdMem[4]=EXTROM2;			RdMem[5]=BASICROM+0x2000; break;
	   case 0x0a: RdMem[4]=BASICROM;		RdMem[5]=EXTROM2; break;
	   case 0x0b: RdMem[4]=EXTROM1;			RdMem[5]=CurKANJIROM+0x2000; break;
	   case 0x0c: RdMem[4]=CurKANJIROM;		RdMem[5]=EXTROM1; break;
	   case 0x0d: RdMem[4]=RAM+0x8000;		RdMem[5]=RAM+0xa000; break;
	 /*case 0x0e: RdMem[4]=					RdMem[5]=EmptyRAM; break; */
	   case 0x0e: if( EXTRAM) 
	   				{RdMem[4]=EXTRAM+0x8000;RdMem[5]=EXTRAM+0xa000; break;}// 2003/5/4
	   case 0x0f: RdMem[4]=					RdMem[5]=EmptyRAM; break;
          };
      switch(Value&0xf0) {
	   case 0x00: RdMem[6]=					RdMem[7]=EmptyRAM; break;
	   case 0x10: RdMem[6]=BASICROM+0x4000;	RdMem[7]=BASICROM+0x6000; break;
	   case 0x20: RdMem[6]=CurKANJIROM;		RdMem[7]=CurKANJIROM+0x2000; break;
	   case 0x30: RdMem[6]=					RdMem[7]=EXTROM2; break;
	   case 0x40: RdMem[6]=					RdMem[7]=EXTROM1; break;
	   case 0x50: RdMem[6]=CurKANJIROM;		RdMem[7]=BASICROM+0x6000; break;
	   case 0x60: RdMem[6]=BASICROM+0x4000;	RdMem[7]=CurKANJIROM+0x2000; break;
	   case 0x70: RdMem[6]=EXTROM1;			RdMem[7]=EXTROM2; break;
	   case 0x80: RdMem[6]=EXTROM2;			RdMem[7]=EXTROM1; break;
	   case 0x90: RdMem[6]=EXTROM2;			RdMem[7]=BASICROM+0x6000; break;
	   case 0xa0: RdMem[6]=BASICROM+0x4000;	RdMem[7]=EXTROM2; break;
	   case 0xb0: RdMem[6]=EXTROM1;			RdMem[7]=CurKANJIROM+0x2000; break;
	   case 0xc0: RdMem[6]=CurKANJIROM;		RdMem[7]=EXTROM1; break;
	   case 0xd0: RdMem[6]=RAM+0xc000;		RdMem[7]=RAM+0xe000; break;
	 /*case 0xe0: RdMem[6]=					RdMem[7]=EmptyRAM; break; */
	   case 0xe0: if( EXTRAM) 
	   				{RdMem[6]=EXTRAM+0xc000;RdMem[7]=EXTRAM+0xe000; break;}// 2003/5/4
	   case 0xf0: RdMem[6]=					RdMem[7]=EmptyRAM; break;
          };
          return;
   case 0xF2:                           /* write ram block set  */
	  if( sr_mode || P6Version==0) return;	/* sr_mode or PC-6001 do nothing! 9/20*/
         if(Value&0x40) 
		 	{EnWrite[3]=1;WrMem[6]=RAM+0xc000;WrMem[7]=RAM+0xe000;}
         else
		 	 EnWrite[3]=0;
         if(Value&0x010)
		 	{EnWrite[2]=1;WrMem[4]=RAM+0x8000;WrMem[5]=RAM+0xa000;}
         else
		 	 EnWrite[2]=0;
         if(Value&0x04)
		 	{EnWrite[1]=1;WrMem[2]=RAM+0x4000;WrMem[3]=RAM+0x6000;}
         else
		 	 EnWrite[1]=0;
         if(Value&0x01)
		 	{EnWrite[0]=1;WrMem[0]=RAM;WrMem[1]=RAM+0x2000;}
         else
		 	 EnWrite[0]=0;

		 if( EXTRAM)
		 {
         if(Value&0x80){EnWrite[3]=2;WrMem[6]=EXTRAM+0xc000;WrMem[7]=EXTRAM+0xe000;}
         if(Value&0x20){EnWrite[2]=2;WrMem[4]=EXTRAM+0x8000;WrMem[5]=EXTRAM+0xa000;}
         if(Value&0x08){EnWrite[1]=2;WrMem[2]=EXTRAM+0x4000;WrMem[3]=EXTRAM+0x6000;}
         if(Value&0x02){EnWrite[0]=2;WrMem[0]=EXTRAM+0x0000;WrMem[1]=EXTRAM+0x2000;}
		 }
		return;

  case 0xF3:                           /* wait,int control     */
	     portF3 = Value;
	     TimerSW_F3=(Value&0x04)?0:1;
         IntSW_F3=(Value&0x01)?0:1;
		 PRINTDEBUG(TEXT("[P6.c][DoOut] port F3  Value=%02X  PC=%04X  TimerSW_F3=%d \n"), Value ,getPC(), TimerSW_F3);
         return;
  case 0xF4: return;                   /* int1 addr set        */
  case 0xF5: return;                   /* int2 addr set        */
  case 0xF6:                           /* timer countup value  */
           portF6 = Value;
           SetTimerIntClock(Value);
           return;
  case 0xF7:                           /* timer int addr set    */
           portF7 = Value;
           return;
  case 0xF8: return;                   /* CG access control    */

		/* ---------------------------------  (SR)
		FAh: interrupt control 
		    7    6      5     4     3     2       1      0
		    EXT  PRINT  232C  VRTC VOICE  TIMER  STICK  SUB CPU
		    
		    1:DI   0:EI
		 $B$*$=$i$/!"(BB8H-BFH $B$G;XDj$9$k%"%I%l%9$H!!4X78$,$"$j$=$&$G$9!#(B
		      --------------------------------*/
  case 0xFA:
  		  if( P6Version ==2 || P6Version ==4)
		    {
	       	portFA= Value;   	// Interrupt DI/EI controll 2002/4/21
	       	PRINTDEBUG(TEXT("[P6.c][DoOut] Port 0xFA  Value=%02X\n"),portFA);
	       	}
	       return; 

		/* ---------------------------------   (SR)
		FBh: interrupt vector address control
		    7    6      5     4     3     2       1      0
		    EXT  PRINT  232C  VRTC VOICE  TIMER  STICK  SUB CPU
		    
		    1:Enable  0: Disable
		 $B$*$=$i$/!"(BB8H-BFH $B$G;XDj$9$k%"%I%l%9$H!!4X78$,$"$k$H;W$o$l$k!#(B
	      --------------------------------*/
  case 0xFB:
  		  if( P6Version ==2 || P6Version ==4)
		    {
	       	portFB= Value; 		// Interrupt address controll 2002/4/21
	       	PRINTDEBUG(TEXT("[P6.c][DoOut] Port 0xFB  Value=%02X\n"),portFB);
	       	}
	       return;   

	/* ---------------------------------   (SR)
		$B3HD%4A;z(BROM$B!!%"%I%l%9%i%C%A(B

        $B$3$N%]!<%H$O!"3HD%4A;z(BROM$B$r$5$7$?$H$-$N$_!"M-8z$K$J$j$^$9!#(B
		FCh $B$K>e0L#8%S%C%H$r=PNO$9$k$H!"(BB$B%l%8%9%?$r$I$&$K$+$7$FFI$_=P$7$F!"(B
        $B2<0L%"%I%l%9$H$9$kMM$G$9!#(B
	   --------------------------------*/
  case 0xFC:
			 extkanjirom_adr= Value*256 + getB();

			//printf("%X \n",extkanjirom_adr);
			//if( !(getB() & 0xf)) {printf("%04X \n", Value*256 + getB() );}
			break;					// address latch
  case 0xFF:break;					// 00h: enable / ffh:disable
  default: //PRINTDEBUG3("[P6.c][DoOut] Unused  port=%02X  value=%02x \n",Port,Value);
			break;
  }
}

/****************************************************************/
/*** Read number from given IO port.                          ***/
/****************************************************************/
byte DoIn(register byte_fast Port)
{
  /*static byte Last90Data = 0;*/
  byte Value;
  switch(Port) {
	/* ------------------------------
		    memory mapping (SR)		2002/2 Windy
	   ------------------------------ */
    case 0x60:case 0x61:case 0x62:case 0x63:case 0x64:case 0x65:case 0x66:case 0x67:
    case 0x68:case 0x69:case 0x6a:case 0x6b:case 0x6c:case 0x6d:case 0x6e:case 0x6f:
    		  if( P6Version ==2 || P6Version ==4)
		    	{
                 Value=port60[ Port-0x60 ];
                }
               else
                {
				 Value= NORAM;
				}
               break;

	/* ------------------------------
		    8251 
	   ------------------------------ */
    case 0x80: Value=NORAM;break;		 /* 8251 data */
    case 0x81: Value=0x85;break;         /* 8251 ACIA status     */

	/* ------------------------------
		    sub CPU
	   ------------------------------ */
    case 0x90:                           /* subCPU input */
			  Value= subcpuIN();
			  break;
			  
    case 0x92: //Value=NORAM;break; 
    		   Value=port_c_8255; break;	/* 8255 port c (sub cpu)*/

    case 0x93: Value=NORAM;break;
    		   //Value=port93;		/* 8255 port c bit set/reset (sub cpu)*/
    /* FFh$B$rJV5Q$7$J$$$H!"(Bcsave$B$G$-$J$$!#>\:Y$O!"(BCSAVE $B$N(BREADY CHECK$B;2>H$N$3$H(B */

	/* ------------------------------
		    8910 / YM-2203 
	   ------------------------------ */
    case 0xA2:                           /* 8910 data */
			  if(PSGReg!=14) 
			  		Value=(PSGReg>13? 0xff:PSGTMP[PSGReg]);
		      else
               {
				JoyState[0]= ~JoystickGetState(0);	/* add JOYSTICK 2003/8/31 */
			    JoyState[1]= ~JoystickGetState(1);
			  	if(PSGTMP[15]==0xC0) 
			  		Value=JoyState[0];
				else
				    Value=JoyState[1];
                }
		      break;
	// JoyStat[]$B$K!"(BSTICK$B$N7k2L$NH?E>$7$?%G!<%?$r=q$-9~$s$G$*$/!#(B

	/* ---------------------------------
	A3h: YM-2203 status
		7     6 5 4 3 2    1    0
		SBC               OVB   OVA
		|                  |    |--TIMER A  1:overflow
		|                  --------TIMER B  1:overflow
		-------------- 1:busy  0:ready
          --------------------------------*/
    case 0xA3: if(sr_mode) 
				{
//				 Value=2+1; 	/* YM-2203 status */
#ifdef SOUND
				 Value= ym2203_ReadStatus();	// read from fmgen  2003/10/13
#else	// SOUND
				 Value = 0;
#endif	// SOUND
				}
	    	 break;

	/* ---------------------------------
	B2h: floppy interrupt / machine type 
	    7 6 5 4 3 2   1   0
	                 CHK  INT
	                  |    |------ floppy intterupt    1:kaijyo  0:warikomicyuu
	                  ------------ machine type        1:66SR    0:mk2SR
          --------------------------------*/
    case 0xB2:
	          Value=((P6Version==4)? 2:0)| 1; break; 

    case 0xC0: Value=NORAM;break;        /* pr busy,rs carrier   */
    case 0xC2: Value=NORAM;break;        /* ROM switch           */


	// *************************** DISK ****************  by Windy
    case 0xD1:
    case 0xD3:if( disk_type)
 	             Value= fdc_pop_buffer( Port);  	/* FDC buffer */
 	          else
	             Value= disk_inp(Port);	/* support for mk2 DISK add 2002/4/4 */
	          break;

    case 0xD2:
    case 0xD0: if( disk_type)
	             {Value= fdc_pop_buffer( Port);}
	           else
	             {Value=disk_inp(Port);}     /* disk data  add 2002/3/13 */
	           break;
    case 0xD4: Value=0;    break;	// test

    case 0xDC: Value=fdc_inpDC();break;    /* FDC status */
    case 0xDD: Value=fdc_inpDD();break;    /* FDC data */
 	// *********************************************************

    case 0xE0: Value=0x40;break;         /* uPD7752              */
    case 0xF0: if(sr_mode || P6Version==0)// sr_mode and PC-6001
					Value=NORAM;
			   else
					Value=portF0; 
			   break; 
    case 0xF1: if(sr_mode || P6Version==0)// sr_mode and PC-6001
					Value=NORAM;
			   else
					Value=portF1; 
			   break;
    case 0xF3: Value=portF3;break;
    case 0xF6: Value=portF6;break;
    case 0xF7: Value=portF7;break;

	/* ---------------------------------   (SR)
		$B3HD%4A;z(BROM
	   --------------------------------*/
	case 0xFD:Value= NORAM;
			    if( EXTKANJIROM)
			       Value= EXTKANJIROM[ extkanjirom_adr]; 	// read left font
			  break;
  	case 0xFE:Value= NORAM;
			    if( EXTKANJIROM)
			       Value= EXTKANJIROM[ extkanjirom_adr+0x10000];// right font
			  break;
//    default: printf("Unused  port=%02X  value=%02x \n",Port,Value);
    default:   Value=NORAM;break;        /* If no port, ret FFh  */
  }
  if (Verbose&0x02) printf("--- DoIn  : %02X, %02X ---\n", Port, Value);
  return(Value);
}


/****************************************************************/
/*** ROM files                                                ***/
/****************************************************************/
byte **ROMList[6+3] = { &BASICROM, &CGROM1, &CGROM5, &KANJIROM, &VOICEROM,
			 &SYSROM2, &SYSTEMROM1, &SYSTEMROM2 ,&CGROM6};
	/* add 2002/2    added SYSTEMROM1 , SYSTEMROM2  by Windy*/

static LPCTSTR ROMName[5][6+3] = {
    {TEXT("BASICROM.60"),TEXT("CGROM60.60"),TEXT("")           ,TEXT("")           ,TEXT("")           ,TEXT("")},
    {TEXT("BASICROM.62"),TEXT("CGROM60.62"),TEXT("CGROM60m.62"),TEXT("KANJIROM.62"),TEXT("VOICEROM.62"),TEXT("")},
    {TEXT("           "),TEXT("          "),TEXT("           "),TEXT("           "),TEXT("           "),
     TEXT("           "),TEXT("SYSTEMROM1.64"),TEXT("SYSTEMROM2.64"),TEXT("CGROM68.64")},
    {TEXT("BASICROM.66"),TEXT("CGROM60.66"),TEXT("CGROM66.66") ,TEXT("KANJIROM.66"),TEXT("VOICEROM.66"),TEXT("")},
    {TEXT("           "),TEXT("          "),TEXT("           "),TEXT("           "),TEXT("           "),
     TEXT("           "),TEXT("SYSTEMROM1.68"),TEXT("SYSTEMROM2.68"),TEXT("CGROM68.68")},
  };
    /* 99.06.02. */
    /* add 2002/2       SYSTEMROM1 , SYSTEMROM2  :-) */

  int sizeList[5][6+3] = {
    { 0x4000, 0x2400,      0,      0,      0,      0 ,0,0},
    { 0x8000, 0x2400, 0x2000, 0x8000, 0x4000,      0 ,0,0},
    { 0x0000, 0     , 0     , 0x0000, 0x0000, 0x0000,0x10000,0x10000 ,0x5000},
    { 0x8000, 0x2400, 0x2000, 0x8000, 0x4000,      0 ,0,0},
    { 0x0000, 0     , 0     , 0x0000, 0x0000, 0x0000,0x10000,0x10000 ,0x5000},
  };
    /* 99.06.02. */
    /* add 2002/2  by Windy */

    /* 2002/5/25   SR $B$J$i(B BASICROM ,KANJIROM,SYSROM2, VOICEROM$B$rFI$^$J$$(B */
    /* 2003/8/8    SR $B$N(BCGROM$B$O!"(B16kb$BC1BNBP1~$KJQ99(B */

/****************************************************************/
/*** existROM(): exist ROM file ?                             ***/
/****************************************************************/
int existROM(int version)
{
	int ret;
	printf("\n Checking ROM files ...");

	ret= existROMsub(version, TEXT(""));		// search current directory
	if( ret==0)
	     ret = existROMsub(version, RomPath);	// search rom path

	 if( Verbose) 
	    if( ret) 
	    	 printf("OK\n");
		else
	         printf("FAILED\n");
	 return(ret);
}


/****************************************************************/
/*** existROMsub(): exist ROM file ?                             ***/
/****************************************************************/
int existROMsub(int version, LPCTSTR dir)
{
	TCHAR path[MAX_PATH];
	FILE *fp;
	int i;
	int ret=1;
 
	 for(i=0; i<9;i++)
	 	{
		 if( sizeList[version][i])
		   {
		   PathCombine(path,dir, ROMName[version][i]);
		   fp= _tfopen( path ,TEXT("rb")); 
		   if(fp!=NULL) 
	       		fclose(fp);
	       else
    	 		ret=0;
		   }
		}

	return(ret);
}

/****************************************************************/
/*** Allocate memory, load ROM images, initialize mapper, VDP ***/
/*** CPU and start the emulation. This function returns 0 in  ***/
/*** the case of failure.                                     ***/
/****************************************************************/
int StartP6(void)
{
//	word A;
	int *T,J;
	byte *P;



	/* patches in BASICROM */
	word BROMPatches[5][32] = {
	    { 0x10EA,0x18,0 },		/* 60:to limit 16K	*/
	    { 0 },			/* 62:do nothing	*/
	    { 0 },			/* 64:do nothing        */
	    { 0 },			/* 66:do nothing        */
	    { 0 }			/* 68:do nothing        */
	};

/*    { 0x601C,0x18,0x601D,0x03,0 }, */	/* 66:skip disk check  */
/* { 0x9025,0xcd,0x9026,0x30,0x9027,0x10,0 },*/  /* 64:screen can change 2002/2 */
/* { 0x9025,0xcd,0x9026,0x30,0x9027,0x10,0 },*/  /* 68:screen can change 2002/2 */
/*
patch $B$O!"$+$J$j8:$j$^$7$?!#(B
66 $B$G$O!"%G%#%9%/$r;H$($k$h$&$K$J$C$?$N$r<u$1$F!"(B
64$B$H(B68$B$O!"?bD>F14|$N3d$j9~$_$,(B $B2><BAu$5$l$?$?$a$G$9!#(B
                                                                         Windy
*/

	/*** PSG initial register states: ***/
	static byte PSGInit[16]  = { 0,0,0,0,0,0,0,0xFD,0,0,0,0,0,0,0xFF,0 };

	/*** STARTUP CODE starts here: ***/

	T=(int *)"\01\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
#ifdef LSB_FIRST
	if(*T!=1)
	  {
	    printf("********** This machine is high-endian. **********\n");
	    printf("Take #define LSB_FIRST out and compile iP6 again.\n");
	    return(0);
	  }
#else
	  if(*T==1)
	  {
	    printf("********* This machine is low-endian. **********\n");
	    printf("Insert #define LSB_FIRST and compile iP6 again.\n");
	    return(0);
	  }
#endif
	CasStream[0]=CasStream[1]=PrnStream=NULL;
	DskStream[0]=DskStream[1]=NULL;		/* add 2002/3/14  by Windy*/

	OpenFile1(FILE_PRNT);
	OpenFile1(FILE_LOAD_TAPE);
	OpenFile1(FILE_SAVE_TAPE);
	OpenFile1(FILE_DISK);		/* add 2002/3/14  by Windy*/


	sr_mode  = (P6Version==4 || P6Version==2)? 1: 0;/*  add 2002/3/9  SR_MODE ? */
	disk_type = (P6Version>=3 )? 1: 0;	  /*  add 2002/3/30 disk type ? */

	/* ******* FDC $B$r=i4|2=(B ************************ */
	if(disk_type)  fdc_init();		/* add 2002/3/20   disk initialize */

	/* ******* $B6u%a%b%j!<(B(8KB) $B$r:n@.(B ************************ */
	if(Verbose) printf("Allocating 8kB for empty space...");
	EmptyRAM=(byte *)malloc(0x2000);

	/* ******* $B%a%$%s(BRAM(64KB) $B$r:n@.(B ************************ */
	/* allocate 64KB RAM */
	if(Verbose) printf("OK\nAllocating 64kB for RAM space...");
	RAM=(byte*)malloc(0x10000);  if(!RAM) return(0);
	memset(RAM ,0,0x10000);	// clear RAM   add 2002/11/4

  /* ******* $B3HD%(BRAM(64KB) $B$rA^F~$9$k(B ************************ add 2003/4/6 */
  	if( extram)
		{
		 if(Verbose) printf("OK\nAllocating 64kB for EXT-RAM space...");
		 EXTRAM=(byte*)malloc(0x10000);  if(!EXTRAM)  return(0);
		 memset(EXTRAM ,0,0x10000);
		}

	if( !load_roms()) return(0);           /* load roms */
	if( !make_internalroms()) return(0);   /* make internal roms */

   /* 2003/8/8  CGROM6$B$rJ,3d$7$F$+$i!"%;%_%0%i%G!<%?$r:n@.$9$k$3$H!#(B*/
	make_semigraph();		/* make semigraph */

	// CGROM = CGROM1;



	/* ***************  patch BASICROM *****************/
    if((PatchLevel)&&(BROMPatches[P6Version][0])) {
     if(Verbose)  printf( (sr_mode)? "  Patching SYSTEMROM1:  ": "  Patching BASICROM: ");
     for(J=0;BROMPatches[P6Version][J];) 
       {
       if(Verbose) printf("%04X..",BROMPatches[P6Version][J]);
       if( sr_mode) {                   /* for SR-BASIC add 2002/2 by Windy*/
       		P=SYSTEMROM1+BROMPatches[P6Version][J++];
      }else {
          P=BASICROM+BROMPatches[P6Version][J++];
	  }
      P[0]=(byte)BROMPatches[P6Version][J++];
    };
    if(Verbose) printf(".OK\n");
  };

  /* ***************  INIT MEMORY MAPPER *****************/
	InitMemmap();

	if(Verbose) printf("OK\nInitializing PSG, and CPU...");
	memcpy(PSGTMP,PSGInit,sizeof(PSGInit));
	JoyState[0]=JoyState[1]=0xFF;
	PSGReg=0;
	EndOfFrame=1;


	SetValidLine_sr( drawwait);	// re-calc drawwait ($B:F7W;;(B)
	SetClock(CPUclock*1000000);

	ResetZ80();
                    // Z80 CPU $B$N<B9T$O!"(BStartP6() $B$N30$K0\F0(B  2003/11/7
//#ifndef WIN32
//  if(Verbose) printf("OK\nRUNNING ROM CODE...\n");
//  A=Z80();
//  if(Verbose) printf("EXITED at PC = %04Xh.\n",A);
//#endif
  return(1);
}



/****************************************************************/
/*** InitMemmap(): initialize  memory map                     ***/
/****************************************************************/
void InitMemmap(void)
{
	int J;
	if(Verbose) printf("Initializing memory mappers...");

	if( P6Version ==2 || P6Version==4 ){       /* SR-BASIC     add 2002/2 by Windy*/
		for(J=0;J<4;J++) {RdMem[J]=SYSTEMROM1+0x2000*J+0x8000;WrMem[J]=RAM+0x2000*J;};
    	}
    else{
  	    for(J=0;J<4;J++) {RdMem[J]=BASICROM+0x2000*J;WrMem[J]=RAM+0x2000*J;};
    	}

	RdMem[2] = EXTROM1; RdMem[3] = EXTROM2;

	for(J=4;J<8;J++) {RdMem[J]=RAM+0x2000*J;WrMem[J]=RAM+0x2000*J;};
	EnWrite[0]=EnWrite[1]=0; EnWrite[2]=EnWrite[3]=1;
	VRAM=RAM;
	TEXTVRAM=RAM;         // TEXT VRAM  for SR-BASIC 
  
	if( sr_mode)      /* CGROM 2003/11/9 */
  		CGROM=CGROM6;
    else
        CGROM=CGROM1;

	/* PC-6001$B$@$H!"(BportF0 $B$O!"$=$b$=$bB8:_$7$J$$!#(B   2003/7/20*/
  	//if( P6Version ==0) portF0= 0x71;
}



/****************************************************************/
/*** Free memory allocated with StartP6().                   ***/
/****************************************************************/
void TrashP6(void)
{
 int i;
	// fixed double free() EmptyRAM ,add windy 2002/7/7
  if(BASICROM        && BASICROM!=EmptyRAM)  {free(BASICROM); BASICROM=NULL;}
  if(CGROM1          && CGROM1  !=EmptyRAM)  {free(CGROM1);   CGROM1=NULL;}
  if(CGROM5          && CGROM5  !=EmptyRAM)  {free(CGROM5);   CGROM5=NULL;}
  if(KANJIROM        && KANJIROM!=EmptyRAM)  {free(KANJIROM); KANJIROM=NULL;}
  if(VOICEROM        && VOICEROM!=EmptyRAM)  {free(VOICEROM); VOICEROM=NULL;}
  if(RAM)                                    {free(RAM); RAM=NULL;}
  if(PrnStream&&(PrnStream!=stdout))         {fclose(PrnStream); PrnStream=NULL;}
  for(i=0;i<2;i++)
  	{
  	if(CasStream[i])                         {fclose(CasStream[i]); CasStream[i]=NULL;}
  	if(DskStream[i])                         {fclose(DskStream[i]); DskStream[i]=NULL;}
    }


   /* add 2002/3/9  for N66SR  */
  if(SYSTEMROM1      && SYSTEMROM1!=EmptyRAM)  {free(SYSTEMROM1); SYSTEMROM1=NULL;}
  if(SYSTEMROM2      && SYSTEMROM2!=EmptyRAM)  {free(SYSTEMROM2); SYSTEMROM2=NULL;}
  if(CGROM6          && CGROM6    !=EmptyRAM)  {free(CGROM6);     CGROM6=NULL;}

  if(EXTKANJIROM     && EXTKANJIROM!=EmptyRAM) {free(EXTKANJIROM);EXTKANJIROM=NULL;}
  if(EXTRAM          && EXTRAM  !=EmptyRAM)    {free(EXTRAM);     EXTRAM=NULL;}
  if(EXTROM          && EXTROM  !=EmptyRAM)    {free(EXTROM); EXTROM=EXTROM1=EXTROM2=NULL;}

  if(EmptyRAM) { free(EmptyRAM); EmptyRAM=NULL;}
}


/****************************************************************/
/*** load_roms(): load roms                                   ***/
/****************************************************************/
int load_roms(void)
{
 int K;
  /* ******* $B3F<oFbB!(BROM$B$r(B $BFI$_9~$`(B ************************ */
  if(Verbose) printf("OK\nLoading ROMs:\n");

  for(K=0;K<6+3;K++) {  /* 99.06.02. */ /* add 2002/2  for SR BASIC */
    if(sizeList[P6Version][K]) {
      if(!LoadROM(ROMList[K],ROMName[P6Version][K],sizeList[P6Version][K]))
		{ PRINTFAILED;return(0); }

    } else {
      if( !*ROMList[K]) *ROMList[K] = EmptyRAM;		// NULL?  add Windy 2002/7/7
      /* 99.06.02. */
  
    };
  }

  /* **************** Load ExtROMs (4000h-5fffh) (6000h-7fffh)  8KB*2  ********************/
  if ((*Ext1Name) || (*Ext2Name)) {
    if((EXTROM=(byte *)malloc(0x4000))!=NULL) {
      EXTROM1 = EXTROM; EXTROM2 = EXTROM + 0x2000;
      if (*Ext1Name)
		if(!LoadEXTROM(EXTROM1,Ext1Name,0x4000))
	      { EXTROM1 = EXTROM2 = EmptyRAM; }		// extrom Empty   2003/10/16
//		  { PRINTFAILED;return(0); }
      if (*Ext2Name)
		if(!LoadEXTROM(EXTROM2,Ext2Name,0x2000))
	      { EXTROM1 = EXTROM2 = EmptyRAM; }
//		  { PRINTFAILED;return(0); }
      }
  } else {
    EXTROM1 = EXTROM2 = EmptyRAM;
  };

  /* ************** Load ExtKANJI ROM ******************/
   if( extkanjirom ) {
    if((EXTKANJIROM=(byte *)malloc(0x20000))!=NULL)
	 if(!LoadEXTROM(EXTKANJIROM,TEXT("EXTKANJI.ROM"),0x20000))
	    {
		 if(Verbose) printf("  Creating EXTKANJI.ROM...");
		 if( make_extkanjirom( (char *)EXTKANJIROM)) /* make ExtKANJI ROM (win32/Unix)*/
		 	{ printf("Ok\n\n");}
		 else
		    { PRINTFAILED; free(EXTKANJIROM); EXTKANJIROM=NULL;} 
		}
   }
 return(1);
}

/****************************************************************/
/*** make_internalrom(): make internal rom                    ***/
/***                   for N66-SR BASIC  by Windy   2002/5/25 ***/
/****************************************************************/
int make_internalroms(void)
{
  // $BFbIt(BROM $B$N:n@.!#(B $B8=>u$N%W%m%0%i%`$G$O!"FbItE*$KI,MW$J!"(B
  // BASICROM.68 ,KANJIROM.68 ,VOICEROM.68 ,SYSROM2.68 ,CGROM1 ,CGROM5 $B$r:n@.$7$J$$$H$$$1$^$;$s!#(B
  if(sr_mode)
    {
    int K;
    struct {
      char *name;	// ROM name
      byte **out;	// output
      byte **in;	// input
      int start;	// start address of input
      int length;	// length of output
      }  internal_rom[]={{"BASICROM",&BASICROM , &SYSTEMROM1, 0x0000,0x8000},
                         {"KANJIROM",&KANJIROM , &SYSTEMROM2 ,0x8000,0x8000},
					  	 {"VOICEROM",&VOICEROM , &SYSTEMROM2 ,0x4000,0x4000},
					  	 {"SYSROM2" ,&SYSROM2  , &SYSTEMROM2 ,0x2000,0x2000},
					  	 {"CGROM1"  ,&CGROM1   , &CGROM6     ,0x0000,0x2400},
					  	 {"CGROM5"  ,&CGROM5   , &CGROM6     ,0x2000,0x2000},};
							/* add 2003/8/8 CGROM6 $BJ,3d(B */
     if(Verbose) printf("Creating  internal ROMs:\n");
     for(K=0 ; K< 6; K++)
        {
         if(Verbose) printf("  Creating %s ....",internal_rom[K].name);
         if( (*internal_rom[K].out = (byte *)malloc( internal_rom[K].length))==NULL) {PRINTFAILED; return(0);}
         memcpy( *internal_rom[K].out, *internal_rom[K].in+ internal_rom[K].start  ,internal_rom[K].length);
         if(Verbose) printf("OK\n");

        }
    }
 return(1);
}

static int startWith(LPCTSTR lpString1, LPCTSTR lpString2)
{
	return (memcmp(lpString1, lpString2, lstrlen(lpString2) * sizeof(*lpString2)) == 0);
}

/****************************************************************/
/*** Load a ROM data from the file name into mem.             ***/
/*** Return memory address of a ROM or 0 if failed.           ***/
/****************************************************************/
/* In: mem : $BFI$_9~$_%P%C%U%!(B  name: $B%U%!%$%kL>!!(Bsize: $B3NJ]$9$k%a%b%j%5%$%:(B */
byte *LoadROM(byte **mem, LPCTSTR name, size_t size)
{
	TCHAR path[MAX_PATH];
	TCHAR errmsg[256];
	FILE *F;
  
	if(Verbose) _tprintf(TEXT("  Opening %s..."), name);
	F=_tfopen( name, TEXT("rb"));
	if (!F)
  		{					// Read ROM directory
     	PathCombine( path,RomPath,name);
	 	F=_tfopen( path, TEXT("rb"));
		}

	if (F) {
	    if(Verbose) printf("OK\n");
	    if((*mem=(byte *)malloc(size))!=NULL) {
	      if(Verbose) printf("    loading...");
	      /* resize for CGROM60 */
	      if (!lstrcmp(name,TEXT("CGROM60.60"))) {
			size=0x1000;
	      } else if (startWith(name,TEXT("CGROM60."))) {
			size=0x2000;
		  }
		  /* resize for CGROM68 */	/* 2003/8/8 */
	      else if (startWith(name,TEXT("CGROM68."))) {
			size=0x4000;
	      }
      if(fread(*mem,1,size,F)!=size) { free(*mem); *mem=NULL; };
	  };
    fclose(F);
  	};

	if(Verbose) _tprintf(*mem? MsgOK:MsgFAILED);
	if( !*mem || !F) {
		_stprintf(errmsg ,TEXT("Can't read ROM file [%s] "), name);
		textout(50,100, errmsg);
		return(*mem);		// when error is exit  ,add windy 2002/7/7
		}

	return(*mem);
}
/*
$B",ESCf$G!"%5%$%:$rJQ99$7$F$$$^$9!#2?8N$+$H$$$&$H!"(B
ROM$B%5%$%:$h$j$bBg$-$J%a%b%j!<$r3NJ]$5$;$k$?$a$K9T$C$F$$$k$h$&$G$9!#(B
$B$D$^$j!"(BROM$B%5%$%:$bEO$9$h$&$K$9$l$P!"%5%$%:JQ99$7$F$J$/$bNI$/$J$j$^$9!#(B

											2004/1/11	Windy
*/



// ****************************************************************************
//          make_semigraph: $B%;%_%0%i%G!<%?$N:n@.%k!<%A%s(B
// ****************************************************************************
void make_semigraph(void)
{
	/* make semi-graphic 6 for 6001 */
	// --- 2000.10.13.
	//if(!lstrcmp(name,TEXT("CGROM60.60"))) {
#if 0
	if((!lstrcmp(name,TEXT("CGROM60.60"))) || ( !lstrcmp(name,TEXT("CGROM60.64"))) ||
     (!lstrcmp(name,TEXT("CGROM60.68")))) {
  	// --- 2000.10.13.
    byte *P = *mem+0x1000;
#endif
    if( CGROM1 !=EmptyRAM) {	/* add !=EmptyRAM  2003/09/15 windy*/
    byte *P = CGROM1+0x1000;
    unsigned int i, j, m1, m2;
    for(i=0; i<64; i++) {
      for(j=0; j<16; j++) {
		switch (j/4) {
          case 0: m1=0x20; m2=0x10; break;
          case 1: m1=0x08; m2=0x04; break;
          case 2: m1=0x02; m2=0x01; break;
          default: m1=m2=0;
		};
		*P++=(i&m1 ? 0xF0: 0) | (i&m2 ? 0x0F: 0);
       };
     };
   };

  /* make semi-graphic 4 for N60-BASIC */
#if 0
	if (startWith(name,TEXT("CGROM60."))) {
	    byte *P = *mem+0x2000;
#endif

	if( CGROM1 !=EmptyRAM) {		/* add !=EmptyRAM  2003/09/15 windy*/
	    byte *P = CGROM1+0x2000;
	    unsigned int i, j, m1, m2;
	    for(i=0; i<16; i++) {
	      for(j=0; j<16; j++) {
			switch (j/6) {
	          case 0: m1=0x08; m2=0x04; break;
	          case 1: m1=0x02; m2=0x01; break;
	          default: m1=m2=0;
			};
			*P++=(i&m1 ? 0xF0: 0) | (i&m2 ? 0x0F: 0);
	      };
	    };
	  };


#if 1
  /* make semi-graphic  for N66SR-BASIC */ /* 2003/8/8 */
	if( CGROM6 !=EmptyRAM) {		/* add !=EmptyRAM  2003/09/15 windy*/
		byte *P = CGROM6+0x4000;	/* CGROM6 $B$N8e$m$K:n@.(B */
  		unsigned int i, j, m1, m2;
    	for(i=0; i<256; i++) {
    	  for(j=0; j<16; j++) {
			switch (j/2) {
    	      case 0: m1=0x80; m2=0x40; break;
    	      case 1: m1=0x20; m2=0x10; break;
     	      case 2: m1=0x08; m2=0x04; break;
    	      case 3: m1=0x02; m2=0x01; break;
     	     default: m1=m2=0;
			};
			*P++=(i&m1 ? 0xF0: 0) | (i&m2 ? 0x0F: 0);
    	  };
  	  };
  	};
#endif
}

/****************************************************************/
/*** Load an extension ROM data from the file name into mem.  ***/
/*** Return memory address of a ROM or 0 if failed.           ***/
/****************************************************************/
int LoadEXTROM(byte *mem, LPCTSTR name, size_t size)
{
	TCHAR path[MAX_PATH];
	FILE *F;
	size_t s=0;

	if(Verbose) _tprintf(TEXT("  Opening %s as EXTENSION ROM..."), name);
	F=_tfopen( name, TEXT("rb"));
	if (!F)
	    {					// Read ROM directory
	     PathCombine( path,RomPath,name);
		 F=_tfopen( path, TEXT("rb"));
		}

	if (F) 
    	{
	    if(Verbose) printf("OK\n");
    	if(Verbose) printf("    loading...");
    	s = fread(mem,1,size,F);
    	if(Verbose) printf("(%05x bytes) ", (unsigned)s);
    	fclose(F);
  		};
	if(Verbose) _tprintf((F!=NULL) ? MsgOK : MsgFAILED);
  	return(F!=NULL);
}

/****************************************************************/
/*** Close(if needed) and Open tape/disk/printer file.        ***/
/****************************************************************/
void OpenFile1(unsigned int num)
{
	TCHAR fullpath[ MAX_PATH];		// Path+Name
	switch(num) {
	   case FILE_LOAD_TAPE:
	  	 PathCombine( fullpath,CasPath[0], CasName[0]);	// make fullpath
	   	if(CasStream[0]) {fclose(CasStream[0]); CasStream[0]=NULL;}
    	if(*CasName[0])
    	if((CasStream[0]=_tfopen(fullpath,TEXT("rb"))) != NULL)
			{
		 	if(Verbose) _tprintf(TEXT("Using %s as a load tape\n"),fullpath);
			}
      	else
			_tprintf(TEXT("Can't open %s as a load tape\n"),CasName[0]);
    	break;

	   case FILE_SAVE_TAPE:
  	 	PathCombine( fullpath,CasPath[1], CasName[1]);	// make fullpath
    	if(CasStream[1]) {fclose(CasStream[1]); CasStream[1]=NULL;}
    	if(*CasName[1])
      		if((CasStream[1]=_tfopen(fullpath,TEXT("r+b"))) != NULL)
			{
		 	if(Verbose) _tprintf(TEXT("Using %s as a save tape\n"),fullpath);
			}
      	else
        	{
         	if((CasStream[1]=_tfopen(fullpath,TEXT("wb"))) != NULL)
           	{
		    }
         else
           {
            _tprintf(TEXT("Can't open %s as a save tape\n"),CasName[1]);
            *CasName[1]=0;
           }
         }
		break;

  case FILE_DISK:
  		setDiskProtect( 0); // set writeable
  	 	PathCombine( fullpath,DskPath[0], DskName[0]);	// make fullpath
     	if(DskStream[0]) {fclose( DskStream[0]); DskStream[0]=NULL;}
     	/* disk file add 2002/3/14*/
     	if(*DskName[0])		/* disk file exist */
        	{
       		if((DskStream[0]=_tfopen( fullpath,TEXT("r+b")))!= NULL)
				{
		  		if(Verbose) _tprintf(TEXT("Using %s as a disk\n"),fullpath);
				read_d88head();
				}
      		else
        		{
        		if((DskStream[0]=_tfopen( fullpath,TEXT("rb")))!= NULL) // write protect
          			{
		  			if(Verbose) _tprintf(TEXT("Using %s as a disk (write protected)\n"),fullpath);
		  			read_d88head();
		  			setDiskProtect( 1); // write protected
		  			}
				else
                	{
		    		_tprintf(TEXT("Can't open %s as a disk. \n"),DskName[0]);
#if 0 // AUTO_FORMAT
                    if( createNewD88( fullpath , (P6Version==2||P6Version==4)? 1:0))
                    	{
			       		if((DskStream[0]=_tfopen( fullpath,TEXT("r+b")))!= NULL)
                        	{
		  					if(Verbose) _tprintf(TEXT("Using %s as a disk (blank disk)\n"),fullpath);
		  					read_d88head();
                            }
                    	else
                       		{
					    	*DskName[0]=0;
                        	}
                    	}
                    else
#endif // AUTO_FORMAT
                       	{
					    *DskName[0]=0;
                        }

                    }
	    		}
			}
    	break;

  case FILE_PRNT:
    	if(PrnStream&&(PrnStream!=stdout)) fclose(PrnStream);
    	if(!*PrnName) PrnStream=stdout;
    	else
    	    {
			if(Verbose) _tprintf(TEXT("Redirecting printer output to %s..."),PrnName);
			//if(!(PrnStream=(FILE*)_tfopen(PrnName,TEXT("wb")))) PrnStream=stdout;
			PrnStream=(FILE*)_tfopen(PrnName,TEXT("wb"));
			if(!PrnStream) PrnStream=stdout;

			if(Verbose) _tprintf((PrnStream==stdout)? MsgFAILED:MsgOK);
			}
	    break;
  	}
 setMenuTitle(num);
}

/****************************************************************/
/*** Send a character to the printer.                         ***/
/****************************************************************/
void Printer(byte V) { fputc(V,PrnStream); }

/****************************************************************/
/*** Refresh screen, check keyboard and sprites. Call this    ***/
/*** function on each interrupt.                              ***/
/****************************************************************/
word Interrupt(void)
{
	/* interrupt priority (PC-6601SR) */
	/* 1.Timer     , 2.subCPU    , 3.Voice     , 4.VRTC      */
	/* 5.RS-232C   , 6.Joy Stick , 7.EXT INT   , 8.Printer   */

			// IntSW_F3 $B$H(B TimerSW_F3 $B$r>r7o$KDI2C(B 2003/9/16 Windy
	if (TimerIntFlag == INTFLAG_REQ  && IntSW_F3 && TimerSW_F3)
	  	return(INTADDR_TIMER); /* timer interrupt */
	else if (CasMode && (p6key == 0xFA) && (keyGFlag == 1))
    	return(INTADDR_CMTSTOP); /* Press STOP while CMT Load or Save */
	else if ((CasMode==CAS_LOADING) && (CmtIntFlag == INTFLAG_REQ)) 
	    {
	    /* CMT Loading */
	    CmtIntFlag = INTFLAG_NONE;
	    if(!feof(CasStream[0])) 
	       { 				/* if not EOF then Interrupt to Load 1 byte */
	        CasMode=CAS_LOADBYTE;
	        return(INTADDR_CMTREAD);
	       } else { 		/* if EOF then Error */
            printf("tape file reached EOF\n");
            CasMode=CAS_NONE;
            return(INTADDR_CMTSTOP); /* Break */
       	   }
		}
    else if ((StrigIntFlag == INTFLAG_REQ) && IntSW_F3) /* if command 6 */
	    return(INTADDR_STRIG);
	else if ((KeyIntFlag == INTFLAG_REQ) && IntSW_F3) /* if any key pressed */
	    if (keyGFlag == 0) 
        	return(INTADDR_KEY1); /* normal key */
	    else 
        	return(INTADDR_KEY2); /* special key (graphic key, etc.) */

  // ***************** PC-6601SR ************************* 2002/4/8 ( by Windy)
	else if( DateIntFlag == INTFLAG_REQ && DateMode ==DATE_READ) //date interrupt
    	return( INTADDR_DATE);
	else if( TvrIntFlag   == INTFLAG_REQ && TvrMode ==TVR_READ) 
    	return( INTADDR_TVR);
	else if( VrtcIntFlag == INTFLAG_REQ ) 	// VRTC interrupt 2002/4/21
		return( INTADDR_VRTC);
  // ***************************************************************
	else /* none */
  		return(INT_NONE);
}


/****************************************************************/
/*** SUB CPU OUTPUT                                           ***/
/****************************************************************/
void subcpuOUT(byte Value)
{
	// ------------ TAPE --------------------
      if (CasMode==CAS_SAVEBYTE) /* CMT SAVE */
		{ 
		 fputc(Value,CasStream[1]);
		 CasMode=CAS_NONE; 
		 return; 
		}
      if ((Value==0x1a)&&(CasMode==CAS_LOADING)) /* CMT LOAD STOP */
		{ 
		 CasMode=CAS_NONE;
		 return;
		}
		/* CMT LOAD OPEN(0x1E,0x19(1200baud)/0x1D,0x19(600baud)) */
      if ((Value==0x19)&&(CasStream[0]))
		{
		 CasMode=CAS_LOADING;
		 return; 
		}
      if ((Value==0x38)&&(CasStream[1])) /* CMT SAVE DATA */
		{
		 CasMode=CAS_SAVEBYTE; 
		 //printf("CMT SAVE BYTE\n");
		 return;
	 	}

	// ------------ KEYIN  --------------------
      if (Value==0x04) 
      	{ kanaMode = kanaMode ? 0 : 1; return; }
      if (Value==0x05) 
      	{ katakana = katakana ? 0 : 1; return; }
      if (Value==0x06) /* strig,stick */
		{ StrigIntFlag = INTFLAG_REQ; return; }	// STRIG Interrupt REQUEST

	// ------------ TIMER (PC-6601SR)  --------------------
      if( DateMode ==DATE_WRITE && P6Version ==4)// DATE Write    2002/4/8
		{								// SUB CPU$B$K!"(B5 byte$BEO$9(B
         if( DateIdx <5)
			{ DateBuff[ DateIdx++]= Value;}
		 else 
			{ DateMode = DATE_NONE;}
		}

      if (Value==0x32 && P6Version ==4 )
		{ 
		    DateIntFlag = INTFLAG_REQ;   // DATE READ Interrupt REQUEST
		    DateMode  = DATE_READ;
		    DateIdx      = 0;
		    memcpy( DateBuff, fmtdate(),5);  // get date&time  at now
	        return;
		}
      if (Value==0x33 && P6Version ==4)
		{
		   DateMode = DATE_WRITE;      // DATE WRITE
		   DateIdx      = 0;
		   return;
		}
      if (Value==0x31 &&  P6Version ==4)
		{		     // TV Reserve-DATA READ  Interrupt REQUEST
		   TvrIntFlag = INTFLAG_REQ;
		   TvrMode   = TVR_READ;
		   TvrIdx       = 0;
		   return;
		}
}

/****************************************************************/
/*** SUB CPU INPUT                                            ***/
/****************************************************************/
byte subcpuIN(void)
{
byte Value=0;
     if ((CmtIntFlag == INTFLAG_EXEC) && (CasMode==CAS_LOADBYTE)) {
		/* -------- CMT Load 1 Byte ------ */
		CmtIntFlag = INTFLAG_NONE;
		CasMode=CAS_LOADING;
    	Value=fgetc(CasStream[0]);

      } else if (StrigIntFlag == INTFLAG_EXEC) {
		/* ------- stick,strig ------- */
		StrigIntFlag = INTFLAG_NONE;
		Value = stick0;
		/*
		if (ExecStringInt) {
			Value=stick0;
			ExecStringInt = 0;
			Code16Count = 0;
		} else {
			Code16Count++;
			if (10 == Code16Count) {
				Value = 0x16;
				Code16Count = 0;
			} else {
				Value = stick0;
			}
		}
		*/
      } else if (KeyIntFlag == INTFLAG_EXEC) {
		/* keyboard */
		KeyIntFlag = INTFLAG_NONE;
		if ((p6key == 0xFE) && (keyGFlag == 1)) kanaMode = kanaMode ? 0 : 1;
		if ((p6key == 0xFC) && (keyGFlag == 1)) katakana = katakana ? 0 : 1;
		Value = p6key;
		/*
		if (0 != p6key) Last90Data = Value;
		p6key = 0;
		*/

		// ------------ TIMER  (PC-6601SR)   -------------- 2002/4/8
      } else if ( DateIntFlag==INTFLAG_EXEC && DateMode == DATE_READ) {	
		if( DateIdx <5)						// DATE Interrupt
		   Value= DateBuff[ DateIdx++];		// SUB CPU$B$+$i!"%G!<%?$rEO$9(B
		else
		 {
          Value=0xff;
		  DateMode = DATE_NONE;
		  DateIntFlag=INTFLAG_NONE;
		 }
	  } else if( TvrIntFlag ==INTFLAG_EXEC && TvrMode == TVR_READ) {
		if( TvrIdx <1)					// TV RESERVE-DATA Interrupt
		   Value = TvrBuff[ TvrIdx++];		// dummy data$B$rEO$9!#(B
		else
		 {
		    Value=0xff;
		    TvrMode   = TVR_NONE;
		    TvrIntFlag = INTFLAG_NONE;
		} 
     } else {
        /*if (0 == p6key) Value = Last90Data; else Value = p6key;*/
		Value = p6key;
        }
 return(Value);
}

/* **************************************************** */
/* test code */
#if 0
struct _tapebuffer {			// 2002/11/5
	int read_idx;
	int write_idx;
	int length;
	byte buffer[65536];
} tapebuffer;

void init_tapebuffer(void)
{
 tapebuffer.read_idx =0;
 tapebuffer.write_idx=0;
 tapebuffer.length   =sizeof( tapebuffer.buffer)-1;
}

void write_tapebuffer( byte Value)
{
 tapebuffer.buffer[ tapebuffer.write_idx ] = Value;
 if( ++tapebuffer.write_idx > tapebuffer.length)
	{
	 tapebuffer.write_idx=0;
	}
}

int read_tapebuffer(byte *Value)
{
 int ret;
 ret=0;
 if( tapebuffer.read_idx != tapebuffer.write_idx)
	{
	 ret=1;
	 *Value = tapebuffer.buffer[ tapebuffer.read_idx ];
	 if( ++tapebuffer.read_idx > tapebuffer.length)
		{
		 tapebuffer.read_idx=0;
		}
	}
 return( ret);
}
#endif
/* **************************************************** */




void Patch(reg *R)
{
  printf("**********iP6: patch at PC:%4X**********\n",R->PC.W);
}


/* $B2hLL$N99?7=hM}(B */
/* Z80.c$B$+$i8F$P$l$k(B */
void UpdateScreen(void)
{
	static unsigned int count = 0;
	static unsigned int frames = 0;
	static char validate = 0;
	const int tape = (CasMode && FastTape);
	int draw = !EndOfFrame;

	if (++count >= 60)
	{
		count = 0;
		if (CasStream[0])
		{
			fpos_t pos;   /* = 0 */
			fgetpos(CasStream[0], &pos);    // $B%+%;%C%H$NFI$_9~$_0LCV<hF@(B
			SetTitle(pos);                  // $B%?%$%H%k99?7(B
		}
		if (UseStatusBar)
		{
			putStatusBar();                 // $B%9%F!<%?%9%P!<99?7(B
		}

		if (tape)
		{
			draw = 1;
		}
	}

	if (!tape)
	{
		++frames;
		if (UPeriod != 0)
		{
			if (frames >= UPeriod)
			{
				draw = 1;
			}
		}
		else
		{
			if ((!validate) || (frames >= MAX_AUTOPERIOD))
			{
				draw = 1;
			}
		}
	}

	if (draw)      // $B2hLLIA2h=hM}(B
  	{
		frames = 0;
		validate = 1;

    	/* Refreshing screen: */
     	/*printf("ref:%d,%d,%d \n",CRTMode1,CRTMode2,CRTMode3); */

		if( sr_mode) {			/* SR-BASIC   (add 2002/2) by Windy*/
			SCR[CRTMode1+2][CRTMode2 ? (CRTMode3 ? 3 : 2) : 0]();
     	}else {
			SCR[CRTMode1][CRTMode2 ? (CRTMode3 ? 3 : 2) : 0]();
     	}
     	if(EndOfFrame) PutImage();
  	}

	/* $B%?%$%_%s%0(B */
	if (!tape)
	{
		if (WaitFlag)
		{
			const int r = unixIdle();
			if (r > 0)
			{
				validate = 0;
			}
		}
	}
}


/* CMT$B3d$j9~$_$N5v2D$N30It%$%s%?%U%'!<%9(B */
#if 0
void enableCmtInt(void)
{ if (CasMode) CmtIntFlag = INTFLAG_REQ; }
#endif



/** InitVariable ********************************************/
/* Initialize  Variable                                      */
/*     before calling InitMachine32                          */
/*************************************************************/
void InitVariable(void)
{
	int i;

	UPeriod =2;
	CasMode =0;			// Casset stop	2003/8/24
	UseSound=1;
	UseSaveTapeMenu=0;
	
	SaveCPU=1;
	scale=2;
	new_scale=scale;
 
	IntLac=1;
	P6Version=4;
	newP6Version= P6Version;
 
	extram =0;
	new_extram =extram;
	extkanjirom =0;
	new_extkanjirom = extkanjirom;
 
	FastTape=0;

	disk_num=0;
	new_disk_num = disk_num;
	UseDiskLamp=1;
 
	// depth=32;
	//CasName[0]=0;
	//DskName[0]=0;
	//PrnName[0]=0;

	CPUclock=4;
	drawwait=192;
	srline  = 80;
	srline_bak = srline;
	TimerSWFlag = 1;

 
 
 /* ********************************************************************** */
 
	port60[0]= 0xf8; 				//I/O[60..67] READ  MEMORY MAPPING
	for(i=1;i<15;i++) port60[i]=0;	//I/O[68-6f]  WRITE MEMORY MAPPING
	//port93;						//I/O[93]     8255 MODE SET / BIT SET & RESET
	//port94;						//I/O[94]	  shadow of I/O[90]
	//portBC;						//I/O[BC]     INTERRUPT ADDRESS of VRTC

	portC1= 0x00;					//I/O[C1]     CRT CONTROLLER MODE
	portC8= 0x00;					//I/O[C8]     CRT CONTROLLER TYPE
	//portCA;						//I/O[CA]     X GEOMETORY low  HARDWARE SCROLL
	//portCB;						//I/O[CB]     X GEOMETORY high HARDWARE SCROLL
	//portCC;						//I/O[CC]     Y GEOMETORY      HARDWARE SCROLL
	//portCE;						//I/O[CE]     LINE SETTING  BITMAP (low) */
	//portCF;						//I/O[CF]     LINE SETTING  BITMAP (High) */
	//portFA;						//I/O[FA]     INTERRUPT CONTROLLER
	//portFB;						//I/O[FB]     INTERRUPT ADDRESS CONTROLLER


	port_c_8255=0;					// 8255 port c

    /* ****************** FLOPPY DISK  ******************** */
	// portDC;						//I/O[DC]     FDC status
	portD1=0;						//I/O[D1]     MINI DISK  (CMD/DATA OUTPUT
	portD2=0x00;					//I/O[D2]     MINI DISK  (CONTROL LINE INPUT)

    /* ************************************************* */

				
	portF0 = 0x11;				//I/O [F0]	MEMORY MAPPING [N60/N66]
	portF3 = 0;					//I/O [F3]  WAIT CONTROLL
	portF6 = 3;					//I/O [F6]  TIMER COUNTUP 
	portF7 = 0x06;				//I/O [F7]  TIMER INT ADDRESS 

	PatchLevel = 1;
	CGSW93 = FALSE;

	p6key = 0;
	stick0 = 0;
	keyGFlag = 0;
	kanaMode = 0;
	katakana = 0;
	kbFlagGraph = 0;
	kbFlagCtrl = 0;
	TimerSW = 0;
	TimerSW_F3 = 1;		/* $B=i4|CM$O(B1$B$H$9$k!J(BPC-6001$BBP1~!K(B */

	IntSW_F3 = 1;
	Code16Count = 0;

	CRTMode1=0;			/* $B=i4|CM$O#0$H$9$k(B   2003/10/20 */
	CRTMode2=0;
	CRTMode3=0;

}

/* ************** strncpy  (add string with terminated)  ***************** */
LPTSTR my_strncpy(LPTSTR dest, LPCTSTR src, int max)
{
	int i;
	for(i=0; i< max-1;i++)
	    {
	    *(dest+i) = *(src+i);
	    *(dest+i+1)=0;
	    if( *(src+i)==0) break;
		}
	return( dest);
}

/* ************** isScroll ***************** */
int isScroll(void)
{
	 int x,y;
	  x = portCB *256 + portCA;
	  y = portCC;
	  return(( x !=0 || y !=0) && bitmap && sr_mode);
}

// ****************************************************************************
//          sw_nowait_mode: NO WAIT$B%b!<%I(B/ NORMAL $B%b!<%I$N@Z$jBX$((B
//  In: mode TRUE: nowait mode     FALSE: normal mode
// ****************************************************************************
void sw_nowait_mode(int mode)
{
	if( mode)		// no wait and high speed
		{
		CPUclock_bak = CPUclock;
		drawwait_bak = drawwait;
		CPUclock     = 20;
		drawwait     = 10;
		}
	else				// normal speed
		{
		CPUclock     = CPUclock_bak;
		drawwait     = drawwait_bak;
		}
}


// ****************************************************************************
//          sw_srline: 
//  In: mode TRUE: tape moving      FALSE: normal mode
// ****************************************************************************
void sw_srline(int mode)
{
	if( mode)
    	{
         srline     = 0;
        }
    else
    	{
         srline     = srline_bak;
        }

     SetValidLine_sr( drawwait);	// re-calc drawwait ($B:F7W;;(B)
}

// ****************************************************************************
//          ResetPC:
//  In:  reboot   1:force reboot
//  Out: non zero : successfull
//       zero     : failed
// ****************************************************************************
int ResetPC(int reboot)
{
 if( !existROM( newP6Version))  // exist rom ?
	 { return(0);}

	if(P6Version != newP6Version || reboot)
		{			// --- reboot ---
        CPURunning =0;      // stop CPU
		TrashP6();
#ifdef SOUND
		StopSound();			// add 2002/10/15
#endif
		InitVariable();
		ConfigRead();
		ClearScr();
		StartP6();
        CPURunning =1;          // run CPU
#ifdef SOUND
		ResumeSound();			// add 2002/10/15
#endif
		}
	else
		{			// --- soft reset ---
		if( CPURunning)		// check CPU running ... 2003/4/26
			{
	        CPURunning =0;      // stop CPU
			InitVariable();		// initialize variable 2003/8/24
#ifdef WIN32
			ConfigRead();
#endif
			InitMemmap();		// initialize memory map
			ResetZ80();
	        CPURunning =1;      // run CPU
			ClearScr();
			}
		}
 ClearWindow();
 return(1);
}



// 2002/2         first version --  N66SR-BASIC can  run ...
// 2002/2/20  Implimented CGROM for SR
// 2002/2/23  Implimented Width 40,25 for SR
// 2002/3/16  Implimented fdd (Inteligent)
// 2002/3/21  Implimented fdd (PD765)
// 2002/3/24  Fixed screen 2,2,1 bug for SR
// 2002/3/30  Support option -64 and -68 for PC-6001mk2SR and PC-6601SR
// 2002/4/8   Implimented TV reserver ,Date$ and TIMe$  (PC-6601SR)
// 2002/4/21  Implimented PALET , VRTC Interrupt ,menu's color OK! (PC-6601SR)
// 2002/4/29  Implimented WIDTH 80, and 640x200 dots
// 2002/5/3   Implimented SCREEN 3  (BUGGY)
// 2002/7/7   Support 16kb CGROM and 8kb CGROM, and against double free()
// 2002/7/14  Fixed  WIDTH 80 and SCREEN 3 !   (640x400 dots large window)
// 2002/8/16  Fixed wday on TV reserver , wday is 0:Mon .... 6:Sun
// 2002/9/29  Improved performance of draw routine (mode 6)
// 2002/10/17 Fixed wait timer (WIN32)
// 2002/10/24 Implimented sound (WIN32)
// 2003/11/23 Release 4.0
// 2003/12    Implimented fm sound (WIN32)
// 2003/12/31 Implimented tape faster
