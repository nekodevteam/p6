/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Win32.c                       **/
/** modified by Windy 2002-2004                             **/
/** This code is based on ISHIOKA Hiroshi 1998-2000         **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/

#ifdef WIN32


#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <assert.h>

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
//#include <signal.h>
#include <direct.h>
#include <shlwapi.h>

#ifdef __GNUC__
#include <minmax.h>
#endif	// __GNUC__

#include "P6.h"
#include "Refresh.h"
#include "Sound.h"

//#define  GLOBAL
#include "WinMenu.h"
#include "Option.h"
#include "Build.h"
#include "Debug.h"
#include "Win32.h"

#include "message.h"
#include "buffer.h"
#include "misc/win32/vkmap.h"

#if defined(_MSC_VER)
#pragma comment(lib, "shlwapi.lib")
#if !defined(_WIN32_WCE)
#pragma comment(lib, "comctl32.lib")
#pragma comment(lib, "winmm.lib")
#endif	// !defined(_WIN32_WCE)
#endif	/* defined(_MSC_VER) */

#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

// ****************************************************************************
//           internal function
// ****************************************************************************
static int makeWindow(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPTSTR lpszArgs, int nWinMode);

static int savesnapshot(HIBMP hb, LPCTSTR path);
static int setMenuCheck(int type ,int sw);
void setMenuOnOff(int type ,int sw);

int isScroll(void);
static void InitColor(void);


/* ----------- key repeat  --------------- */
static void setKeyrepeat(int value); 	//  setKeyrepeat: $B%-!<%j%T!<%H$NB.EY$r@_Dj$9$k(B
static void storeKeyrepeat(void);		//  storeKeyrepeat: $B%-!<%j%T!<%H$NB.EY$r85$KLa$9(B




// ****************************************************************************
//           Variable
// ****************************************************************************
LPCTSTR Title=TEXT(PROGRAM_NAME) TEXT(BUILD_NO);		// title name
word  ExitedAddr;						// exit code from Z80()


int   UseJoystick;						// use joystick 1: use  add 2003/8/31
int   SaveCPU;
int   CPUThreadRun;         			// CPU Thread  1: run

byte *Keys;

static BYTE s_vkmap[256];
static DWORD s_dwLastFrameTick;
static DWORD s_dwLastStatusTick;
static unsigned int s_nFrames;
static unsigned int s_nDrawFrames;

// --------- bitmap -----------
static const TCHAR szWinName[]= TEXT("MyWin");
HIBMP hb1,hb2;		/* bitmap surface */
HIBMP hb_monitor;	/* monitor's surface */
HIBMP hb_statusbar; /* status bar's surface */
static ColTyp s_PalTbl[16];

HWND hwndMain;
HINSTANCE hInst;


int paddingw = PADDINGW;
int paddingh = PADDINGH;

static char s_enteringUI;		/*!< UI$BCf(B */

/**
 * UI $B$KF~$C$?(B
 */
static void OnEnteringUI()
{
	if (!s_enteringUI)
	{
		s_enteringUI = TRUE;
#ifdef SOUND
		if (!UseCPUThread) StopSound();
#endif	// SOUND
	}
}

/**
 * UI $B$+$i=P$?(B
 */
static void OnLeavingUI()
{
	if (s_enteringUI)
	{
		s_enteringUI = FALSE;
#ifdef SOUND
		if (!UseCPUThread) ResumeSound();
#endif	// SOUND
	}
}

// ****************************************************************************
//          configureFunc: $B%3%s%U%#%0%@%$%"%m%0$N(B CALLBACK $B4X?t(B
// ****************************************************************************
#if !defined(_WIN32_WCE)
static int isSpace(LPCTSTR str, int max)
{
	int i;
	const int len = lstrlen(str);
	if (max < len) max = len;

	for(i=0; i< max; i++)
	   	{
	   	if( *(str+i)!=' ') return 0;
	    }
	return 1;
}

BOOL CALLBACK configureFunc(HWND hdwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
int i;
int  alert;

static TCHAR Ext1Name_bak[PATH_MAX] = TEXT(""); /* Extension ROM 1 backup */
static int  CPUclock_bak;

static const TCHAR nextboottime[]=TEXT("Please REBOOT 'iP6 Plus' to enable the change of options.");

    HWND hEditboxWnd1,hEditboxWnd2,hEditboxWnd3;
//    HWND udWnd1,udWnd2 ,udWnd3;
//    HWND hSoundRatebox;

    char  curdir[ MAX_PATH];
    TCHAR fullpath[MAX_PATH];
    TCHAR name[ MAX_PATH];
    OPENFILENAME op = {0};

  // ---------------- open file structer ------------
    fullpath[0]=0;
    op.lStructSize        = sizeof( OPENFILENAME);
    op.hwndOwner          = hdwnd;
    op.lpstrFilter        = TEXT("All files {*.*}\0*.*\0\0");
    op.lpstrCustomFilter  = NULL;
    op.lpstrFile          = fullpath;
    op.lpstrFileTitle     = name;
    op.nMaxFile           = _countof(fullpath);
    op.nMaxFileTitle      = _countof(name);
    op.Flags              = OFN_FILEMUSTEXIST | OFN_NONETWORKBUTTON | OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY;	// Overwrite check 2003/4/29
    op.lpstrInitialDir= NULL;


    switch( message)
    {   			

	// ********************* INITIALIZE DIALOG ***********************
    case WM_INITDIALOG:

		{// --------- set Configure title ------
      	LPCTSTR tmp;
    	TCHAR str[ 1024];
		switch( P6Version)
			{
			case 0: tmp = TEXT("PC-6001");     break;
			case 1: tmp = TEXT("PC-6001mk2");  break;
			case 2: tmp = TEXT("PC-6001mk2SR");break;
			case 3: tmp = TEXT("PC-6601");     break;
			case 4: tmp = TEXT("PC-6601SR");   break;
			default: tmp = TEXT("unknown");    break;
			}
		wsprintf(str,TEXT("Configure - %s  %s   [ %s ]"),TEXT(PROGRAM_NAME) ,TEXT(BUILD_NO), tmp);
		SetWindowText( hdwnd, str );
    	}
	  
		// ************* Init RADIO BUTTONS and CHECKBOXES *************************
    /* AUTORADIOBUTTON $B$N>l9g!"%A%'%C%/$7$?$$E[$K(B BST_CHECKED $BAw$k$@$1$G$$$$$_$?$$(B*/
                                             // ---- machine type -----
    	SendDlgItemMessage( hdwnd , ID_RDPC60+newP6Version*2,BM_SETCHECK,BST_CHECKED,0);
                                             // ---- fd numbers -------
    	SendDlgItemMessage( hdwnd , ID_RDFD0+new_disk_num, BM_SETCHECK, BST_CHECKED,0);
                                             // ----- screen scale --------
    	SendDlgItemMessage( hdwnd , ID_SCALE1+scale-1,BM_SETCHECK,BST_CHECKED,0);
                                             // ---- fast tape -------
		SendDlgItemMessage( hdwnd , ID_FASTTAPE, BM_SETCHECK , FastTape,0);
                                             // ------ screen update ----
    	SendDlgItemMessage( hdwnd , ID_60FPS+UPeriod-1, BM_SETCHECK,BST_CHECKED,0);
                                             // ------ screen 4 color ----
    	SendDlgItemMessage( hdwnd , ID_SCR4MONO+scr4col,BM_SETCHECK,BST_CHECKED,0);
                                             // -- scan line -- 2003/10/24
		SendDlgItemMessage( hdwnd , ID_SCANLINE, BM_SETCHECK , IntLac,0);
		IntLac_bak = IntLac;

		SendDlgItemMessage( hdwnd , ID_EXTKANJION  , BM_SETCHECK , new_extkanjirom,0);
    	SendDlgItemMessage( hdwnd , ID_EXTRAMON    , BM_SETCHECK , new_extram,0);
    	SendDlgItemMessage( hdwnd , ID_SOUNDON     , BM_SETCHECK , UseSound,0);
		SendDlgItemMessage( hdwnd , ID_SAVETAPEMENU, BM_SETCHECK , UseSaveTapeMenu,0);
		SendDlgItemMessage( hdwnd , ID_STATUSBAR   , BM_SETCHECK , UseStatusBar,0);
		SendDlgItemMessage( hdwnd , ID_DISKLAMP    , BM_SETCHECK , UseDiskLamp ,0);
 	
			// ************ set spin edit **************************
 		hEditboxWnd1 =  GetDlgItem( hdwnd, ID_CPUCLOCK);	// ---- cpu clock ------
 	  /*udWnd1 = */ CreateUpDownControl( WS_CHILD | WS_BORDER | WS_VISIBLE | 
 	    UDS_SETBUDDYINT | UDS_ALIGNRIGHT, 
 	    5,100,20,10, hdwnd, ID_CPUCLOCK_UPDOWN ,hInst, hEditboxWnd1, 20,1,CPUclock);
 	
 		hEditboxWnd2 =  GetDlgItem( hdwnd, ID_DRAWWAIT);	// ---- drawwait ------
 	  /*udWnd2 = */ CreateUpDownControl( WS_CHILD | WS_BORDER | WS_VISIBLE | 
 	    UDS_SETBUDDYINT | UDS_ALIGNRIGHT, 
 	    5,100,20,10, hdwnd, ID_CPUCLOCK_UPDOWN ,hInst, hEditboxWnd2, 192,0,drawwait);

 		hEditboxWnd3 =  GetDlgItem( hdwnd, ID_SRLINE);	// ---- sr wait ------
 	  /*udWnd3 = */ CreateUpDownControl( WS_CHILD | WS_BORDER | WS_VISIBLE | 
 	    UDS_SETBUDDYINT | UDS_ALIGNRIGHT, 
 	    5,100,20,10, hdwnd, ID_SRLINE_UPDOWN ,hInst, hEditboxWnd3, 80,0,srline);

/*
    hSoundRatebox = CreateWindow("Sound Rate", NULL ,WS_CHILD| WS_VISIBLE | CBS_SORT | CBS_SIMPLE,
     150,10,300,20, hwndMain , (HMENU)1,(HINSTANCE)GetWindowLong(hwndMain , GWL_HINSTANCE), NULL);

     LoadString((HINSTANCE)GetWindowLong(hwndMain , GWL_HINSTANCE) ,	ID_NEXTBOOTTIME ,nextboottime ,1024);
*/

										// ---- Extend Rom Name ---  2003/10/16
		SetDlgItemText( hdwnd, ID_EXTROMNAME ,Ext1Name);
		my_strncpy( Ext1Name_bak , Ext1Name, PATH_MAX);
		CPUclock_bak = CPUclock;
		return TRUE;

   case WM_COMMAND:
    	switch( LOWORD(wParam))
    	{
		// ******************** OK BUTTON DOWN *************************
	 	case IDOK:
			alert=0;
			for(i=ID_RDPC60; i<=ID_RDPC68 ; i+=2)		// machine type
			  if(SendDlgItemMessage(hdwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
				{
				 newP6Version= (i- ID_RDPC60)/2;
				 if( newP6Version != P6Version)    /* 2003/5/24 */
				 	alert=1;
				 break;
				}
                                            		// -- sound --
	        {
	        const int newUseSound = (int)SendDlgItemMessage( hdwnd , ID_SOUNDON, BM_GETCHECK , 0,0);
	        if( newUseSound != UseSound)
					alert=1;
			}

			for(i=ID_RDFD0; i<=ID_RDFD2 ; i++)			// -- fd numbers --
			  if(SendDlgItemMessage(hdwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
				{
				new_disk_num = (i- ID_RDFD0);
				if( new_disk_num != disk_num)
					alert=1;
				break;
				}

			for(i=ID_SCALE1; i<=ID_SCALE2 ; i++)		// -- screen scale --
			  if(SendDlgItemMessage(hdwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
				{
				new_scale = (i- ID_SCALE1)+1;
				 if( new_scale != scale)
				   {
	                if( !isFullScreen())
	                	{
						scale= new_scale;
						resizewindow( scale, scale); 
						setMenuCheck( IDM_SCREENSIZE , scale-1);	// menu check
	                    }
				   }
				break;
				}
                                            // -- tape fast --
	        if( !CasMode)                       // not moving tape
	     	    FastTape = (int)SendDlgItemMessage( hdwnd , ID_FASTTAPE, BM_GETCHECK , 0,0);

            
			for(i=ID_60FPS; i<=ID_30FPS ; i++)			// -- screen update --
			  if(SendDlgItemMessage(hdwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
				{
				UPeriod = (i- ID_60FPS)+1;
				break;
				}
			for(i=ID_SCR4MONO; i<=ID_SCR4COL ; i++)		// -- screen 4 color --
			  if(SendDlgItemMessage(hdwnd,i,BM_GETCHECK,0,0)==BST_CHECKED)
				{
				scr4col = (i- ID_SCR4MONO);
				break;
				}
            		                                // -- scan line --
			IntLac = (int)SendDlgItemMessage( hdwnd , ID_SCANLINE, BM_GETCHECK , 0,0);
			setMenuCheck( IDM_SCANLINE, IntLac);
													// ----  status bar --
			UseStatusBar    = (int)SendDlgItemMessage( hdwnd ,ID_STATUSBAR, BM_GETCHECK , 0,0);
			setMenuCheck( IDM_STATUSBAR, UseStatusBar);
													// ----  use disk lamp ---
			UseDiskLamp     = (int)SendDlgItemMessage( hdwnd , ID_DISKLAMP, BM_GETCHECK , 0,0);
			setMenuCheck( IDM_DISK_LAMP, UseDiskLamp);
            		                                // -- extend kanjirom --
		    new_extkanjirom = (int)SendDlgItemMessage( hdwnd , ID_EXTKANJION, BM_GETCHECK,0,0);
	        if( new_extkanjirom != extkanjirom)
	            alert=1;
                                                    // -- extend RAM --
		    new_extram = (int)SendDlgItemMessage( hdwnd , ID_EXTRAMON, BM_GETCHECK , 0,0);
	        if( new_extram != extram)
	            alert=1;

			if( IntLac_bak != IntLac) ClearScr();	// $B%9%-%c%s%i%$%s@Z$jBX$((B  $B2hLL>C$9!!(B2003/10/24

			drawwait= GetDlgItemInt( hdwnd, ID_DRAWWAIT, NULL,0); // -- draw wait
			srline  = GetDlgItemInt( hdwnd, ID_SRLINE  , NULL,0); // -- sr   wait
			SetValidLine_sr( drawwait);

			CPUclock= GetDlgItemInt( hdwnd, ID_CPUCLOCK, NULL,0); // -- cpu clock
			if( CPUclock_bak != CPUclock)	// $B0c$C$F$$$?$i!"E,MQ(B
				{
				if( CPUclock<=0) CPUclock=4;
		    	SetClock(CPUclock*1000000);
				}

												// ---- Ext1Name ---  2003/10/16
	        if( !isSpace( Ext1Name , _countof( Ext1Name)) )       // space only?
	    		GetDlgItemText( hdwnd, ID_EXTROMNAME ,Ext1Name ,_countof(Ext1Name));

			if( lstrcmp( Ext1Name_bak , Ext1Name)!=0)	// check changes
				alert=1;

			if(alert==1)
			      messagebox(_N(nextboottime),TEXT("Information"));

			ConfigWrite();		// write to config file

			EndDialog(hdwnd, alert); // close dialog     (return alert variable)
            ClearWindow();		// clear window	2004/1/9
			return(1);

		// ******************** CANCEL BUTTON DOWN *************************
		 case IDCANCEL: 
			EndDialog(hdwnd,0); // close dialog
			return(1);

		// ******************** DEFAULT BUTTON DOWN *************************
		 case ID_DEFCLOCK:		// Default clocks
			SetDlgItemInt( hdwnd, ID_DRAWWAIT, 192,0); // -- draw wait
			SetDlgItemInt( hdwnd, ID_SRLINE  ,  80,0); // -- sr   wait
			SetDlgItemInt( hdwnd, ID_CPUCLOCK,   4,0); // -- cpu clock
		 	break;

		// ******************** OPEN BUTTON DOWN *************************
		 case ID_OPENEXTROM:	// Open extrom
			op.lpstrFilter  = TEXT("rom files {*.rom}\0*.rom\0")
						  TEXT("All files {*.*}  \0*.*\0\0");
			op.lpstrInitialDir= RomPath;
			_getcwd( curdir, _countof(curdir));	// backup curdir
			if(GetOpenFileName( &op))
				{
				_chdir( curdir);					// resotre curdir
                my_strncpy( Ext1Name , fullpath , PATH_MAX);
				SetDlgItemText( hdwnd, ID_EXTROMNAME ,Ext1Name);	// ---- Ext1Name ---  2003/10/16
				}
     	break;

		// ******************** CLEAR BUTTON DOWN *************************
		 case ID_CLEAREXTROM:	// Clear  extrom name editbox
			Ext1Name[0]=0;						// ---- Ext1Name ---  	2003/10/16
			Ext2Name[0]=0;
			SetDlgItemText( hdwnd, ID_EXTROMNAME ,Ext1Name);
	        break;
		}
	    break;
   }
 return(0);
}
#endif	// !defined(_WIN32_WCE)

// ****************************************************************************
//          aboutFunc: ABOUT $B%@%$%"%m%0$N(B CALLBACK $B4X?t(B
// ****************************************************************************
BOOL CALLBACK aboutFunc(HWND hdwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
			return TRUE;

		case WM_COMMAND:
			switch( LOWORD(wParam))
			{
			 case IDOK: 
			 		EndDialog(hdwnd,0); // close dialog
				    return(1);

/*			 case IDM_URL: 
 					ShellExecute(hdwnd,"open",HOMEPAGE_URL,NULL,NULL,SW_SHOWNORMAL);
		 			break; */
			}
		break;
	}
 
 return(0);
}

// ****************************************************************************
//          wm_command: $B%a%K%e!<A*Br$N=hM}J,$1(B $B!J(B WM_COMMAND$B%a%C%;!<%8MQ!K(B
// ****************************************************************************
void wm_command( HWND hwnd, UINT message, WPARAM wParam , LPARAM lParam)
{
#if !defined(_WIN32_WCE)
	char  curdir[ MAX_PATH];
	TCHAR fullpath[MAX_PATH];
	TCHAR name[ MAX_PATH];
	OPENFILENAME op = {0};

	switch( LOWORD( wParam))
	{
		case IDM_CONFIG:
		case IDM_RESET:
#ifdef DEBUG
		case IDM_WRITE_RAM:
#endif	/* DEBUG */
		case IDM_SNAPSHOT:
		case IDM_EXIT: 
		case IDM_OPEN_LOAD_TAPE:
		case IDM_REWIND_LOAD_TAPE:
		case IDM_OPEN_SAVE_TAPE:
		case IDM_REWIND_SAVE_TAPE:
		case IDM_OPEN_DISK:
		case IDM_VER:
			OnEnteringUI();
			break;
	}

	  // ---------------- open file structer ------------
	fullpath[0]=0;
	op.lStructSize        = sizeof( OPENFILENAME);
	op.hwndOwner          = hwnd;
	op.lpstrFilter        = TEXT("All files {*.*}\0*.*\0\0");
	op.lpstrCustomFilter  = NULL;
	op.lpstrFile          = fullpath;
	op.lpstrFileTitle     = name;
	op.nMaxFile           = _countof(fullpath);
	op.nMaxFileTitle      = _countof(name);
	op.Flags              = OFN_NONETWORKBUTTON | OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY;	// Overwrite check 2003/4/29
	op.lpstrInitialDir= NULL;

	switch( LOWORD( wParam))
	{
	case IDM_CONFIG:
			{
			if( DialogBox(hInst, TEXT("Configure") ,hwnd,(DLGPROC) configureFunc)==1 )
				{
				/* $B",(B1 $B$,JV5Q$5$l$?$i!":F5/F0$,I,MW$G$9!#(B*/
				if(MessageBox(hwnd,_N(TEXT("Reboot to enable new setting?")),TEXT(""),MB_YESNO)==IDYES) 
					{
					ConfigWrite();
				    if( !ResetPC(1))
				        messagebox(_N(TEXT("RESET failed!  check ROM files!")),TEXT(""));
			        setMenuOnOff( IDM_OPEN_DISK, disk_num);
					setMenuCheck( IDM_NO_TIMER ,!TimerSWFlag);
					}
				}
			break;
			}
	case IDM_RESET: 
			if(MessageBox(hwnd,_N(TEXT("RESET CPU Ok?")),TEXT(""),MB_YESNO)==IDYES) 
				{
				 if( !ResetPC(0))
				    messagebox(_N(TEXT("RESET failed!  check ROM files!")),TEXT(""));
		        setMenuOnOff( IDM_OPEN_DISK , disk_num);
				setMenuCheck( IDM_NO_TIMER  ,!TimerSWFlag);
				}
			break;

#ifdef DEBUG
	case IDM_WRITE_RAM:
			op.lpstrFilter  = TEXT("RAM image files {*.mem}\0*.mem\0")
							  TEXT("All files {*.*}  \0*.*\0\0");
			op.lpstrDefExt  = TEXT("*.mem");
			op.lpstrInitialDir= MemPath;			// initial directory
			_getcwd( curdir, _countof(curdir));		// backup curdir

			if(GetSaveFileName( &op))
				{
				ramdump( fullpath);
				_chdir( curdir);						// restore curdir
				}
			fullpath[ op.nFileOffset]=0;			// fullpath -> directory
			my_strncpy( MemPath , fullpath, _countof(fullpath));	// directory only
			ConfigWrite();
			break;
#endif	/* DEBUG */

	case IDM_NOWAIT:
			WaitFlag = !WaitFlag;
			setMenuCheck( IDM_NOWAIT, !WaitFlag);
			sw_nowait_mode( !WaitFlag);		// switch nowait mode / normal mode
			break;
	case IDM_SNAPSHOT:
			op.lpstrFilter  = TEXT("Bitmap image files {*.bmp}\0*.bmp\0")
							  TEXT("All files {*.*}  \0*.*\0\0");
			op.lpstrInitialDir= ImgPath;			// initial directory
			op.lpstrDefExt  = TEXT("*.bmp");
			_getcwd( curdir, _countof(curdir));	// backup curdir

			if(GetSaveFileName( &op))
				{
				 _chdir( curdir);	// restore curdir
				 if( isScroll())	// save snapshot (fullpath)
					 savesnapshot(hb2 ,fullpath);	// scroll mode
				 else
					 savesnapshot(hb1 ,fullpath);	// normal mode
				}
			fullpath[ op.nFileOffset]=0;			// fullpath -> directory
			my_strncpy( ImgPath , fullpath, _countof(fullpath));	// directory only
			ConfigWrite();
			break;
	case IDM_EXIT: 
			if(MessageBox(hwnd, _N(TEXT("Exit Ok?")),TEXT(""),MB_YESNO)==IDYES) 
				SendMessage( hwnd , WM_CLOSE, 0,0);
			break;

					/* *********************** TAPE (LOAD) ************************* */
	case IDM_OPEN_LOAD_TAPE: 
			op.Flags |= OFN_FILEMUSTEXIST;
			op.lpstrFilter  = TEXT("cas files {*.cas *.p6 *p6t}\0*.cas;*.p6;*.p6t\0")
							  TEXT("All files {*.*}  \0*.*\0\0");
			op.lpstrInitialDir= CasPath[0];
			_getcwd( curdir, _countof(curdir));	// backup curdir
			if(GetOpenFileName( &op))
				{
				_chdir( curdir);					// resotre curdir
				my_strncpy( CasName[0] , name    , _countof(CasName[0]));
				fullpath[ op.nFileOffset]=0;
				my_strncpy( CasPath[0] , fullpath, _countof(CasPath[0]));
				ConfigWrite();
				OpenFile1( FILE_LOAD_TAPE);
				}
			break;
	case IDM_REWIND_LOAD_TAPE: 
			if( *CasName[0]!=0)
				if(MessageBox(hwnd, TEXT("Rewind LOAD TAPE ok?"),TEXT(""),MB_YESNO)==IDYES) 
					OpenFile1( FILE_LOAD_TAPE);
			break;
	case IDM_EJECT_LOAD_TAPE: 
			*CasName[0]=0;
			ConfigWrite();
			OpenFile1( FILE_LOAD_TAPE);
			break;

					/* *********************** TAPE (SAVE)************************* */
	case IDM_OPEN_SAVE_TAPE:
			op.Flags |=    OFN_CREATEPROMPT |OFN_OVERWRITEPROMPT; /* $B:n$k$+?R$M$k!#>e=q$-$9$k$+?R$M$k(B 2003/10/27 */
			op.lpstrFilter  = TEXT("cas files {*.cas *.p6 *p6t}\0*.cas;*.p6;*.p6t\0")
							  TEXT("All files {*.*}  \0*.*\0\0");
			op.lpstrInitialDir= CasPath[1];
			_getcwd( curdir, _countof(curdir));	// backup curdir
			if(GetSaveFileName( &op))
				{
				_chdir( curdir);					// resotre curdir
				my_strncpy( CasName[1] , name    , _countof(CasName[1]));
				fullpath[ op.nFileOffset]=0;
				my_strncpy( CasPath[1] , fullpath, _countof(CasName[1]));
				ConfigWrite();
				OpenFile1( FILE_SAVE_TAPE);
				}
			break;
	case IDM_REWIND_SAVE_TAPE: 
			if( *CasName[1]!=0)
				if(MessageBox(hwnd, TEXT("Rewinding SAVE TAPE, Ready?"),TEXT(""),MB_YESNO)==IDYES) 
					OpenFile1( FILE_SAVE_TAPE);
			break;
	case IDM_EJECT_SAVE_TAPE: 
			*CasName[1]=0;
			ConfigWrite();
			OpenFile1( FILE_SAVE_TAPE);
			break;
    case IDM_USE_SAVE_TAPE:
            UseSaveTapeMenu = !UseSaveTapeMenu;
      		setMenuCheck( IDM_USE_SAVE_TAPE    , UseSaveTapeMenu);
       	    setMenuOnOff( IDM_OPEN_SAVE_TAPE   , UseSaveTapeMenu);
        	setMenuOnOff( IDM_REWIND_SAVE_TAPE , UseSaveTapeMenu);
        	setMenuOnOff( IDM_EJECT_SAVE_TAPE  , UseSaveTapeMenu);
            ConfigWrite();
            break;

	case IDM_OPEN_DISK: 
			if( disk_num)
				{
				op.lpstrFilter  = TEXT("d88 files {*.d88}\0*.d88\0")
							     TEXT("All files {*.*}\0*.*\0\0");
			   	op.lpstrInitialDir= DskPath[0];			// initial directory

#if 0    // AUTO_FORMAT
				op.Flags |=    OFN_CREATEPROMPT |OFN_OVERWRITEPROMPT; /* $B:n$k$+?R$M$k!#(B  $B>e=q$-$9$k$+?R$M$k(B 2003/10/27 */
#endif   // AUTO_FORMAT
			   	op.Flags |=    OFN_FILEMUSTEXIST; /* $BB8:_$9$k%U%!%$%k$N$_(B */

			   	_getcwd( curdir, _countof(curdir));		// backup curdir
				if(GetOpenFileName( &op))
			   		{
			    	_chdir( curdir);						// restore curdir
					my_strncpy( DskName[0] , name    , _countof(DskName[0]));	// filename only
					my_strncpy( DskPath[0] , fullpath, op.nFileOffset+1); // directory only
					OpenFile1( FILE_DISK);
                    ConfigWrite();
					}
			   }
			break;
	case IDM_EJECT_DISK: 
			DskName[0][0]=0;
			ConfigWrite();
			OpenFile1( FILE_DISK);
			break;
    case IDM_MONITOR:
#ifdef DEBUG
#if 1
			if( !Console) {
				Console = 1;
				AllocConsole();		// alloc console
				_tfreopen(TEXT("CON"), TEXT("w"), stdout);
			}
			Trace=!Trace;
#else
			monitor_mode = !monitor_mode;
			if(monitor_mode)
                {
				resizewindow( 1,2);
                }
			else
                {
				resizewindow( new_scale, new_scale);
                }
            Trace=!Trace;
#endif
#endif
			break;
	case IDM_NO_TIMER:
			TimerSWFlag = !TimerSWFlag;
			setMenuCheck( IDM_NO_TIMER ,!TimerSWFlag);
			break;
	case IDM_VER:  
			DialogBox(hInst,TEXT("about"),hwnd,(DLGPROC) aboutFunc);
			break;
	case IDM_URL: 
 			ShellExecute(hwnd,TEXT("open"),TEXT(HOMEPAGE_URL),NULL,NULL,SW_SHOWNORMAL);
	 		break;
	case IDM_HELP: 
 			ShellExecute(hwnd,TEXT("open"),TEXT(HELP_FILE),NULL,NULL,SW_SHOWNORMAL);
	 		break;
    case IDM_FULLSCR:
    		toggleFullScr();
			setMenuCheck( IDM_FULLSCR ,isFullScreen() );
    		break;

#ifdef VIEW_MENU
	case IDM_SCANLINE:                         // scan line
			IntLac = !IntLac;
			setMenuCheck( IDM_SCANLINE, IntLac);
            ConfigWrite();
            ClearScr();
			break;
	case IDM_STATUSBAR:                         // status bar
			UseStatusBar = !UseStatusBar;
            if( !UseStatusBar) ClearStatusBar();
			setMenuCheck( IDM_STATUSBAR, UseStatusBar);
            ConfigWrite();
			break;
    case IDM_DISK_LAMP:
			UseDiskLamp = !UseDiskLamp;
			setMenuCheck( IDM_DISK_LAMP, UseDiskLamp);
		    ConfigWrite();
			break;
	case IDM_SCREENSIZE:
		    if( !isFullScreen())
            	{
				scale=(scale==2) ? 1:2;
				resizewindow( scale,scale);		// resize
				ConfigWrite();
                }
			break;
#endif

	}

	OnLeavingUI();
#endif	// !defined(_WIN32_WCE)
}


// ****************************************************************************
//          setMenuTitle: $B%a%K%e!<%"%$%F%`$NI=<(JQ99(B
//                        $B%^%&%s%H$7$F$$$k%U%!%$%kL>$J$I(B
// ****************************************************************************
/* set filename to open menu */
/* input: type = FILE_TAPE , FILE_DISK */
int setMenuTitle(int type)
{
	int   ret = 0;
#if !defined(_WIN32_WCE)
	HMENU hmenu = NULL;
	UINT  itemId;
	TCHAR buff[PATH_MAX+20];
	MENUITEMINFO menuinfo={0};
	int   i;

     switch(type)
 	    {
	    case FILE_LOAD_TAPE:
     				 itemId = IDM_OPEN_LOAD_TAPE;			// menu item id
	 				 if(*CasName[0])
		 				 wsprintf(buff,TEXT("&Open (%s) ... [Load]"),CasName[0]); // new title
					 else
		 				 lstrcpy(buff,TEXT("&Open ... [Load]"));
	 				 break;
	    case FILE_SAVE_TAPE:
     				 itemId = IDM_OPEN_SAVE_TAPE;			// menu item id
	 				 if(*CasName[1])
		 				 wsprintf(buff,TEXT("&Open (%s) ... [Save]"),CasName[1]); // new title
					 else
		 				 lstrcpy(buff,TEXT("&Open ... [Save]"));
	 				 break;
	    case FILE_DISK: itemId = IDM_OPEN_DISK;
	 				 if(*DskName[0])
		 				 wsprintf(buff,TEXT("&Open (%s)"),DskName[0]);
					 else
		 				 lstrcpy(buff,TEXT("&Open"));
	 				 break;
	    default: return 0;			// FILE_PRNT
	    }

	for(i=0; i<2;i++)
	 	{
		 menuinfo.cbSize = sizeof( MENUITEMINFO);
		 menuinfo.fMask = MIIM_TYPE;
		 menuinfo.dwTypeData = NULL;

		 switch( i) {
		   	case 0:	hmenu = GetMenu(hwndMain); break;
		    case 1: hmenu = getPopupMenu(); break;
		   }

		 ret= GetMenuItemInfo( hmenu , itemId ,0 ,&menuinfo);

		 menuinfo.dwTypeData = buff;
		 menuinfo.cch        = lstrlen(buff);
		 ret= SetMenuItemInfo( hmenu , itemId ,0 ,&menuinfo);
	     }
#endif	// !defined(_WIN32_WCE)
	 return(ret);
}


// ****************************************************************************
//          setMenuCheck: $B%a%K%e!<$K%A%'%C%/$rF~$l$?$j!"30$7$?$j(B
// ****************************************************************************
/* input:  menu item number
   output: 1: checked  0: unchecked*/
static int setMenuCheck(int type ,int sw)
{
	 int   status = 0;
#if !defined(_WIN32_WCE)
	 HMENU hmenu = NULL;
	 UINT  itemId;
	 MENUITEMINFO menuinfo={0};
	 int   i;
//	 int   ret;

	 switch( type)
	 	{
		case IDM_NO_TIMER:      itemId= IDM_NO_TIMER;break;
		case IDM_NOWAIT:        itemId= IDM_NOWAIT;  break;
        case IDM_USE_SAVE_TAPE: itemId= IDM_USE_SAVE_TAPE;  break;
        case IDM_FULLSCR:    itemId= IDM_FULLSCR; break;

#ifdef VIEW_MENU
        case IDM_SCANLINE:   itemId= IDM_SCANLINE; break;
        case IDM_STATUSBAR:  itemId= IDM_STATUSBAR; break;
        case IDM_DISK_LAMP:  itemId= IDM_DISK_LAMP; break; 
		case IDM_SCREENSIZE: itemId= IDM_SCREENSIZE;break;
#endif
		default: return(0);
		}
	
    
    for(i=0; i< 2; i++)
    	{
	 	menuinfo.cbSize = sizeof( MENUITEMINFO);
	 	menuinfo.fMask = MIIM_STATE;
        switch( i) {
        	case 0:	hmenu = GetMenu(hwndMain); break;
            case 1: hmenu = getPopupMenu(); break;
            }
        // if( hmenu==0) break;
		 /*ret= */ GetMenuItemInfo( hmenu , itemId ,0 ,&menuinfo);

		 if( sw )
	 		{
	 	 	menuinfo.fState =  MFS_CHECKED;
	 	 	status =1;
	 		}
	 	else
	 		{
		 	menuinfo.fState =  MFS_UNCHECKED;
	 	 	status =0;
	 		}
 		/*ret = */ SetMenuItemInfo( hmenu , itemId ,0 ,&menuinfo);
    	}
#endif	// !defined(_WIN32_WCE)
 	return( status );
}

// ****************************************************************************
//          setMenuOnOff: $B%a%K%e!<$rM-8z$K$7$?$j!"L58z$K$7$?$j(B
// ****************************************************************************
/* input:  menu item number
   output: 1: checked  0: unchecked*/
void setMenuOnOff(int type ,int sw)
{
#if !defined(_WIN32_WCE)
	 HMENU hmenu = NULL;
	 UINT  itemId;
	 MENUITEMINFO menuinfo={0};
	 int   i;
//	 int   ret;
	 int   status;

	 itemId = type;
     for(i=0; i< 2; i++)
    	{
	 	menuinfo.cbSize = sizeof( MENUITEMINFO);
	 	menuinfo.fMask = MIIM_STATE;
        switch( i) {
        	case 0:	hmenu = GetMenu(hwndMain); break;
            case 1: hmenu = getPopupMenu(); break;
            }

		if( sw )
	 		{
	 	 	menuinfo.fState =  MFS_ENABLED;
	 	 	status =1;
	 		}
	 	else
	 		{
		 	menuinfo.fState =  MFS_DISABLED;
	 	 	status =0;
	 		}
 		/*ret = */ SetMenuItemInfo( hmenu , itemId ,0 ,&menuinfo);
        }
#endif	// !defined(_WIN32_WCE)
}

// ****************************************************************************
//          WindowFunc: Windows$B$N%a%C%;!<%8=hM}$N$?$a$N(B CALLBACK $B4X?t(B
// ****************************************************************************
LRESULT CALLBACK WindowFunc( HWND hwnd, UINT message, WPARAM wParam , LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT paintstruct;
	// static BITMAP bitmap;
	// static HDC    hdcBmp;
	hwndMain = hwnd;

	 switch(message) {
	   case WM_DESTROY: 

			//UnprepareLayeredWindow(); // transparent
	        CPUThreadRun=0;         // CPU Thread aborting ... 
	        Sleep(200);
	
		   	TrashP6();
			TrashMachine();			// add 2002/10/15
			if(Verbose) printf("EXITED at PC=%04Xh.\n",ExitedAddr);
#if !defined(_WIN32_WCE)
		   	if( Console) {
				FreeConsole();		// free console
		   	}
#endif	// !defined(_WIN32_WCE)
			releaseIBMP( hb1);
			releaseIBMP( hb2);
			releaseIBMP( hb_monitor);
			releaseIBMP( hb_statusbar);
		   	PostQuitMessage(0);
			break;
	
	   case WM_RBUTTONDOWN:		// RIGHT CLICK
			if( isFullScreen())	// if fullscreen mode ,open popupmenu 
		   		openPopupMenu( hwnd , LOWORD(lParam) , HIWORD(lParam));
	   		break;
	
	   case WM_CREATE:
	   		loadPopupMenu( ((LPCREATESTRUCT)(lParam))->hInstance, TEXT("SUBMENU"));

#if !defined(_WIN32_WCE)
			if( Console) {
				FreeConsole();
				AllocConsole();		// alloc console
				_tfreopen(TEXT("CON"), TEXT("w"), stdout);
			}
#endif	// !defined(_WIN32_WCE)

			hb1= createIBMP(hwnd, Width ,Height,bitpix);
			hb2= createIBMP(hwnd, Width ,Height,bitpix);
			hb_monitor   = createIBMP(hwnd, M6WIDTH*2 ,M6HEIGHT*2 ,bitpix);
			hb_statusbar = createIBMP(hwnd, M6WIDTH*2 ,BORDERH    ,bitpix);
			XBuf = hb1.lpBMP;
			//if( hb1==0)break;
	
			if( !StartP6()) {
				CPURunning =0;
				break;
			}
			
			setMenuCheck( IDM_USE_SAVE_TAPE , UseSaveTapeMenu);
	
#ifdef VIEW_MENU
			setMenuCheck( IDM_SCANLINE   , IntLac);
			setMenuCheck( IDM_STATUSBAR  , UseStatusBar);
			setMenuCheck( IDM_SCREENSIZE , scale-1);
			setMenuCheck( IDM_DISK_LAMP  , UseDiskLamp);
#endif
        
	        setMenuOnOff( IDM_OPEN_SAVE_TAPE   , UseSaveTapeMenu);
	        setMenuOnOff( IDM_REWIND_SAVE_TAPE , UseSaveTapeMenu);
	        setMenuOnOff( IDM_EJECT_SAVE_TAPE  , UseSaveTapeMenu);
	
	        setMenuOnOff( IDM_OPEN_DISK        , disk_num);
			InvalidateRect( hwnd, NULL, TRUE);
	
			//PrepareLayeredWindow(hwnd);	// transparent
			//MySetLayeredWindowAttributes(hwnd,0,  210,LWA_ALPHA);
	        UseJoystick= JoysticOpen();
			break;
	
	   case WM_PAINT: 
	   		{
			int w,h,top;
			w= hb1.Width;
			h= (bitmap ? lines :200) * scale; // text mode =200  graphics mode=204
			top = (204-lines)*scale; 			// 200 line= 4     204 line= 0
	
			hdc= BeginPaint(hwnd, &paintstruct);
			if( isScroll())	// scroll mode
				BitBlt( hdc, 0+paddingw,top+paddingh, w , h, hb2.hdcBmp,0,0, SRCCOPY);
			else			// normal mode
				BitBlt( hdc, 0+paddingw,top+paddingh, w , h, hb1.hdcBmp,0,0, SRCCOPY);
	
			PutDiskAccessLamp(255);	// $B%G%#%9%/%i%s%W$NOH$rI=<((B

			EndPaint(hwnd,&paintstruct);
			}
			break;
	
	   case WM_SYSKEYDOWN:			// key in  buffering 
	   case WM_KEYDOWN:
			if (wParam < 0x100) wParam = s_vkmap[wParam];
			write_keybuffer((DWORD)wParam, (DWORD)lParam,0);
			break;
	
	   case WM_SYSKEYUP:
	   case WM_KEYUP:
			if (wParam < 0x100) wParam = s_vkmap[wParam];
			if(wParam== VK_SPACE || wParam== VK_LEFT || wParam == VK_RIGHT
			|| wParam== VK_DOWN  || wParam== VK_UP   || wParam == VK_PAUSE
	              || wParam== VK_SHIFT || wParam== VK_CONTROL || wParam == VK_MENU)
			write_keybuffer((DWORD)wParam, (DWORD)lParam,0);
		break;
	   case WM_COMMAND:
			wm_command(hwnd, message, wParam , lParam);	// menu selection
			break;
	   case WM_SETFOCUS: 		// set keyrepeat slowly  when getting focus  2003/10/18
	   		setKeyrepeat(10);
	   		break;
	   case WM_KILLFOCUS: 		// set keyrepeat fastly when  killing focus  2003/10/18
	   		storeKeyrepeat();
	   		break;

		case WM_ENTERMENULOOP:
			OnEnteringUI();
			break;

		case WM_EXITMENULOOP:
			OnLeavingUI();
			break;

#ifndef _WIN32_WCE
		case WM_ENTERSIZEMOVE:
			OnEnteringUI();
			break;

		case WM_EXITSIZEMOVE:
			OnLeavingUI();
			break;
#endif	// !_WIN32_WCE

	   default: return DefWindowProc(hwnd, message,wParam,lParam);
	}
 return 0;
}

/**
 * $B%9%j!<%W$9$k(B
 * @param[in] until $B;XDj;~4V$^$G%9%j!<%W$9$k(B
 * @return $BBT$C$?;~4V(B(ms)
 */
static unsigned int SleepUntil(DWORD until)
{
	unsigned int ret = 0;
	while ((signed int)(until - GetTickCount()) > 0)
	{
		if (!UseCPUThread)
		{
			MSG msg;
			if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					break;
				}
				GetMessage(&msg, NULL, 0, 0);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				continue;
			}
		}
		Sleep(1);
		ret++;
	}
	return ret;
}

/**
 * 1$B%U%l!<%`BT$D(B
 * @return $BBT$C$?;~4V(B(ms)
 * @retval -1 $B=hM}Mn$A(B
 */
int unixIdle(void)
{
	const DWORD now = GetTickCount();
	const int past = now - s_dwLastFrameTick;
	if (past >= (1000 * MAX_AUTOPERIOD / 60))
	{
		s_dwLastFrameTick = now;
		return -1;
	}
	else
	{
		static unsigned char s_fraction;
		const DWORD wait = (s_fraction == 0) ? 16 : 17;		/* =16.67 */
		if (++s_fraction >= 3) s_fraction = 0;
		s_dwLastFrameTick += wait;
		return SleepUntil(s_dwLastFrameTick);
	}
}


/** Keyboard bindings ****************************************/
#include "WinKeydef.h"

/** TrashMachine *********************************************/
/** Deallocate all resources taken by InitMachine().        **/
/*************************************************************/
void TrashMachine(void)
{
	if(Verbose) printf("Shutting down...\n");
	storeKeyrepeat();			// store key repeat

	storeDisplayMode(); 		// store display mode

#ifdef SOUND
	TrashSound();
#endif	// SOUND
}

// ****************************************************************************
//          InitMachine32: MS-Windows $B$N%&%$%s%I%&=i4|2=$J$I(B
// ****************************************************************************
static int InitMachine32(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPTSTR lpszArgs, int nWinMode)
{
	bitpix= DEPTH;
	Width=M6WIDTH*scale;
	Height=M6HEIGHT*scale;

	 InitColor();

	 makeWindow(hThisInst, hPrevInst, lpszArgs, nWinMode);	/* make a window */

	  /* ********** init p6 *************** */
	 SetValidLine_sr( drawwait);
	 SetClock(CPUclock*1000000);
	 //SetTimerIntClock(3);
#ifdef LSB_FIRST
	 lsbfirst= 1;
#else
	 lsbfirst= 0;
#endif
	 choosefuncs(lsbfirst,bitpix);
	
#ifdef SOUND
	  if(UseSound) InitSound();
#endif	// SOUND

	 setwidth( scale-1);
	
	
	
	 setKeyrepeat(12);		// set key repeat  2003/10/18
	 
	 saveDisplayMode();		// save current display mode  for fullscreen  2003/9/28
	
	s_dwLastFrameTick = GetTickCount();

	{
		TCHAR szPath[MAX_PATH];
		GetModuleFileName(NULL, szPath, _countof(szPath));
		PathRemoveFileSpec(szPath);
		lstrcat(szPath, TEXT("\\vkmap.txt"));

		InitVKMap(s_vkmap);
		ReadVKMap(s_vkmap, szPath);
	}

	 return(1);
}


// ****************************************************************************
//          makeWindow: MS-Windows $B$N(B Window$B:n@.=hM}(B
// ****************************************************************************
static int makeWindow(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPTSTR lpszArgs, int nWinMode)
{
	WNDCLASS wcl;
	ZeroMemory(&wcl, sizeof(wcl));

	wcl.hInstance  = hThisInst;      /* $B$3$N%$%s%9%?%s%9$N%O%s%I%k(B*/
	wcl.lpszClassName = szWinName;   /* $B$3$N%&%$%s%I%&%/%i%9$NL>A0(B */
	wcl.lpfnWndProc   = WindowFunc;
	wcl.style= 0;
	wcl.hIcon = LoadIcon(hThisInst, MAKEINTRESOURCE(IC_PC66SR) /*IDI_APPLICATION*/);
	wcl.hCursor= LoadCursor(NULL, IDC_ARROW);

#if !defined(_WIN32_WCE)
	wcl.lpszMenuName= TEXT("PCMENU");		/* menu */
#endif	// !defined(_WIN32_WCE)

	wcl.hbrBackground = (HBRUSH) GetStockObject( BLACK_BRUSH); /* back ground */

	if( !RegisterClass( &wcl)) 	/* register window class */
		return 0;

#if defined(_WIN32_WCE)
	hwndMain = CreateWindow(szWinName, Title, WS_VISIBLE,
		0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN),
		NULL, NULL, hThisInst, NULL);
#else	// defined(_WIN32_WCE)
	hwndMain = CreateWindow(
	    szWinName,
	    Title,
	    WS_OVERLAPPED |WS_MINIMIZEBOX |WS_SYSMENU |WS_CAPTION,
	    CW_USEDEFAULT,				/* X */
	    CW_USEDEFAULT,				/* Y */
	    Width +BORDERW+PADDINGW*2,	/* WIDTH */
	    Height+BORDERH+PADDINGH*2,	/* HEIGHT */
	    HWND_DESKTOP,				/* non parent window*/
	    NULL,	
	    hThisInst, 		/* program window instance */
	    NULL
   	);
#endif	// defined(_WIN32_WCE)

	ShowWindow(hwndMain, nWinMode);
	UpdateWindow( hwndMain);

#if !defined(DEBUG)
	setMenuOnOff(IDM_WRITE_RAM, 0);
#endif	/* !defined(DEBUG) */

	return(1);
}


// ****************************************************************************
//          Keyboard: $B%-!<%\!<%I$NAv::=hM}(B
//  $B!J(BZ80 CPU core $B$+$i!"Dj4|E*$K8F$S=P$5$l$F$$$k!#!K(B
// ****************************************************************************
void Keyboard(void)
{
	DWORD wParam, lParam;
#if defined(_WIN32_WCE)
	static BOOL s_bShift = FALSE;
#endif	// defined(_WIN32_WCE)

	if( read_keybuffer( &wParam, &lParam ,NULL) )
		{
  		int J= wParam & 0xff;
    	/* for stick,strig */
    	{
      	byte tmp;
      	switch(J) {
			case VK_SPACE  : tmp = STICK0_SPACE; break;
			case VK_LEFT   : tmp = STICK0_LEFT;  break;
			case VK_RIGHT  : tmp = STICK0_RIGHT; break;
			case VK_DOWN   : tmp = STICK0_DOWN;  break;
			case VK_UP     : tmp = STICK0_UP;    break;
			case VK_PAUSE  : tmp = STICK0_STOP;  break;
			case VK_SHIFT  : tmp = STICK0_SHIFT; break;
			default: tmp = 0;
	      }
		if(!(HIWORD(lParam) & 0x8000))	// keydown?
      		stick0 |= tmp;
      	else
      		stick0 &=~tmp;
    	}
    	/* end of for stick,strig */

	
	if(!(HIWORD(lParam) & 0x8000))	// keydown?
		{
    	switch(J)
			{
			case VK_F4:
				if(kbFlagGraph) CPURunning=0;
				break;
#if !defined(_WIN32_WCE)
			case VK_END:
				toggleFullScr();  /* switch fullscreen or window */
		  		setMenuCheck( IDM_FULLSCR ,isFullScreen() );
          		break;
#endif	// !defined(_WIN32_WCE)

        	case VK_F10:
#if defined(_WIN32_WCE)
				CPURunning=0;
#else	// defined(_WIN32_WCE)
        		SendMessage(hwndMain, WM_COMMAND, IDM_CONFIG, 0);
#endif	// defined(_WIN32_WCE)
		  		break;
			case VK_F11:
#if defined(_WIN32_WCE)
				s_bShift = !s_bShift;
#elif defined(DEBUG)
				if( Console) Trace=!Trace;break;
#endif	// defined(_WIN32_WCE), defined(DEBUG)
				break;
	        case VK_F12: CPURunning=0;break;

	        case VK_CONTROL: kbFlagCtrl=1;break;	// ctrl
	        case VK_MENU:    kbFlagGraph=1;break;	// graph

//	        case VK_INSERT:   J=XK_F13;break; /* Ins -> F13 */
	      }

		if((P6Version==0)&&(J==VK_F8)) J=0; /* MODE key when 60 */

#if defined(_WIN32_WCE)
	if (s_bShift)
	{
		static const char number[10] = {'P', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O'};
		unsigned int i;
		for (i = 0; i < _countof(number); i++)
		{
			if (J == number[i])
			{
				J = '0' + i;
				break;
			}
		}
	}
#endif	// defined(_WIN32_WCE)
    	if (kbFlagGraph)
			Keys = Keys7[J];
      	else if (kanaMode)
			if (katakana)
	  			if (stick0 & STICK0_SHIFT) 
                	Keys = Keys6[J];
	  			else 
                	Keys = Keys5[J];
			else if (stick0 & STICK0_SHIFT) 
                	Keys = Keys4[J];
	  			else 
                	Keys = Keys3[J];
      		else if (stick0 & STICK0_SHIFT) 
            		Keys = Keys2[J];
				else 
            		Keys = Keys1[J];
      
	  	keyGFlag = Keys[0]; p6key = Keys[1];

		/* control key + alphabet key */
		if ((kbFlagCtrl == 1) && (J >= 0x41) && (J <= 0x5a))
			{keyGFlag = 0; p6key = J - 0x41 + 1;}
		/*if (p6key != 0x00) IFlag = 1;*/
		if (Keys[1] != 0x00) 
        	KeyIntFlag = INTFLAG_REQ;
      	} 
	else 
		{
		if (J==VK_MENU) kbFlagGraph=0;		// keyup ?
		if (J==VK_CONTROL) kbFlagCtrl=0;
     	}
	}
}




/* Below are the routines related to allocating and deallocating
   colors */

// ****************************************************************************
//          InitColor: $B%+%i!<%G!<%?$N=i4|2=(B
//    $B!!!!!J%(%_%e%l!<%?2hLL$KD>@\=q$-9~$`%G!<%?!K(B
// ****************************************************************************
/* set up coltable, alind8? etc. */
static void InitColor(void)
{
  unsigned int i;
  unsigned int R,G,B,H;

// param Pal11  Pal12  Pal13  Pal14  Pal15  Pal53$B$K$D$$$F$O!"(BRefresh.c $B$K0\F0$7$^$7$?!#(B

  for(i=0;i<6;i++)
	 trans[i] /= 256;

  for(H=0;H<2;H++)
    for(B=0;B<2;B++)
      for(G=0;G<2;G++)
        for(R=0;R<2;R++)
        {
          i=R|(G<<1)|(B<<2)|(H<<3);
          s_PalTbl[i].ct_byte[3]= 0;			// not use
          s_PalTbl[i].ct_byte[2]= (byte)trans[param[i][0]];	// red
          s_PalTbl[i].ct_byte[1]= (byte)trans[param[i][1]];	// green
          s_PalTbl[i].ct_byte[0]= (byte)trans[param[i][2]];	// blue
        }
  for(i=0;i<16;i++)
  {
    if (bitpix <= 8)		// ======== 256 color mode ============
    {
      BPal[i].ct_byte[0]=i;	// 256 $B%b!<%I$N>l9g!"%Q%l%C%H$N%$%s%G%C%/%9$rBeF~(B
    }
    else if (bitpix == 16)
    {
      BPal[i].ct_byte[0]=((s_PalTbl[i].ct_byte[1] & 0x1c) << 3) | (s_PalTbl[i].ct_byte[0] >> 3);
      BPal[i].ct_byte[1]=((s_PalTbl[i].ct_byte[2] & 0xf8) << 0) | (s_PalTbl[i].ct_byte[1] >> 5);
    }
    else
    {				// ======== full color mode ============
      BPal[i]=s_PalTbl[i];	// full color $B$N>l9g$O!"(BRGB$B$NCM$rBeF~(B
    }
  }

  /* setting color list for each screen mode */
  for(i=0;i<4;i++) BPal11[i] = BPal[Pal11[i]];
  for(i=0;i<8;i++) BPal12[i] = BPal[Pal12[i]];
  for(i=0;i<8;i++) BPal13[i] = BPal[Pal13[i]];
  for(i=0;i<4;i++) BPal14[i] = BPal[Pal14[i]];
  for(i=0;i<8;i++) BPal15[i] = BPal[Pal15[i]];
  for(i=0;i<32;i++) BPal53[i] = BPal[Pal53[i]];
  
	// ******************* fast palet      add 2002/9/27 *********
  for(i=0;i<32;i++) BPal62[i] = BPal53[i];  // for RefreshScr62/63
  for(i=0;i<16;i++) BPal61[i] = BPal[i];    // for RefreshScr61

//  for(i=0;i<32;i++) BPalet[i] = BPal53[i];    // for Palet Color 2003/2/4
}

// ****************************************************************************
//          ClearScr: $B%(%_%e%l!<%?2hLL$rA4%/%j%"(B
// ****************************************************************************
void ClearScr()
{
 memset(XBuf,0,Width*Height*bitpix/8);
}

// ****************************************************************************
//          ClearWindow: $B%&%$%s%I%&A4BN$r%/%j%"(B
// ****************************************************************************
void ClearWindow(void)
{
#if 1
    HDC hdc;
    HBRUSH hbrush, hOldbrush;
    int r=0,g=0,b=0;

    hdc = GetDC( hwndMain);         // <-- $B2?8N$+!"H4$1$F$$$^$7$?!#2?8N!)(B
    hbrush = CreateSolidBrush( RGB(r,g,b));
    hOldbrush = (HBRUSH) SelectObject(hdc,hbrush);
	Rectangle( hdc, 0,0,Width+paddingw ,Height+paddingh); 

    SelectObject( hdc,hOldbrush);
    DeleteObject( hbrush);
    ReleaseDC(hwndMain,hdc);
    InvalidateRect(hwndMain, NULL,1);
#endif
}

// ****************************************************************************
//          ClearStatusBar: $B%9%F!<%?%9%P!<$r%/%j%"(B
// ****************************************************************************
void ClearStatusBar(void)
{
 memset(hb_statusbar.lpBMP ,0 ,hb_statusbar.Width* hb_statusbar.Height*bitpix/8);
}







// ****************************************************************************
//          SetTitle: $B%&%$%s%I%&$N%?%$%H%k%P!<$NJQ99(B
// ****************************************************************************
void SetTitle(fpos_t pos)
{
  TCHAR TitleBuf[256];
  wsprintf(TitleBuf, TEXT("%s  [%ldcnt.]"), Title, (long)pos);
  SetWindowText(hwndMain , TitleBuf);		// add windy 2002/10/27
}

// ****************************************************************************
//          conv_argv: Windows$B$N(B lpszArgs $B$+$i!"(Bargc, argv $B$KJQ49$9$k(B
// ****************************************************************************
/**  bug fixed: set the first option to argv[1]     2003/5/11 */
static void conv_argv( LPTSTR lpszArgs , int *argc ,LPTSTR argv[])
{
	static TCHAR aaa[100][31];
	LPTSTR p;

	argv[0]=0; /* this command filename (not implimented) */
	*argc=1;	 /* start argc==1 */
	p =_tcstok( lpszArgs , TEXT(" "));
	while(p)		//  sepalate cmdline to argv
	     {
		  argv[*argc]= aaa[*argc];
		  my_strncpy( argv[*argc], p,30);
		  p= _tcstok(NULL,TEXT(" "));
		  if( ++(*argc) > 99) break;
		 }
}


// ****************************************************************************
//          textout: $B%(%_%e%l!<%?$N2hLL$KJ8;z$r=PNO(B
// ****************************************************************************
int textout(int x,int y,LPCTSTR str)
{
	int ret;
	ret=0;
	if( hwndMain)
		{
#if !defined(_WIN32_WCE)
		TextOut(hb1.hdcBmp ,x,y,str,lstrlen(str));    // write to bitmap
#else	// !defined(_WIN32_WCE)
		ExtTextOut(hb1.hdcBmp, x, y, ETO_OPAQUE, NULL, str, lstrlen(str), NULL);
#endif	// !defined(_WIN32_WCE)
		ret=1;
    	}
	return(ret);
}

// ****************************************************************************
//          messagebox: $B%a%C%;!<%8%\%C%/%9$rI=<((B
// ****************************************************************************
int messagebox(LPCTSTR str, LPCTSTR title)
{
	int ret;
	 ret=0;
	 if( hwndMain)
	{
		OnEnteringUI();
	    ret=MessageBox( hwndMain, str,title, MB_OK);
		OnLeavingUI();
	}
	 return(ret);
}


// ****************************************************************************
//          outputdebugstring: $B%G%P%C%,$K%a%C%;!<%8$rAw$k(B
// ****************************************************************************
void outputdebugstring(LPCTSTR buff)
{
	OutputDebugString( buff);
}

// ****************************************************************************
//          putlasterror: $B:G8e$N%(%i!<$rI=<((B
// ****************************************************************************
void putlasterror(void)
{
 LPTSTR lpMsgBuf;

 OnEnteringUI();

 //$B%(%i!<%3!<%I$r=q<02=(B
 FormatMessage((FORMAT_MESSAGE_ALLOCATE_BUFFER |
 FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS),
 NULL,GetLastError(),MAKELANGID(LANG_NEUTRAL,
 SUBLANG_DEFAULT),(LPTSTR)&lpMsgBuf,0,NULL);
 MessageBox(NULL, lpMsgBuf, TEXT("Error"), MB_OK | MB_ICONINFORMATION);

 OnLeavingUI();

 LocalFree(lpMsgBuf);
}

// ****************************************************************************
//        CPUThreadProc: CPU $B%9%l%C%I$N%k!<%W(B 
//        $B%9%l%C%I;HMQ$7$J$$$H$-$O!"%a%C%;!<%8$NMh$F$J$$4V$rK%$C$F!"0l2hLL$@$1(BCPU$B$rF0$+$9(B
// ****************************************************************************
static DWORD WINAPI CPUThreadProc(LPVOID lpParameter)
    {
    do {
        // -------- CPURunning $B$,#0$@$H!"(BCPU$B$O<B9T$7$J$$(B -----------
        if(CPURunning)
			{
		    	ExitedAddr= Z80();
				s_nFrames++;

				if( !CPURunning ) 
                	{ 
					OnEnteringUI();
					if(MessageBox(hwndMain,_N(TEXT("Exit Ok?")),TEXT(""),MB_YESNO)==IDYES)
						SendMessage( hwndMain , WM_CLOSE, 0,0);
		     		else
						CPURunning=1;
					OnLeavingUI();
		    		}
			}
		} 
    while(  CPUThreadRun);

    return(0);
}

static DWORD dwThreadId;

// ****************************************************************************
//          WinMain: MS-Windows $B$N%a%$%s4X?t(B
// ****************************************************************************

#if defined(__GNUC__)
#define _tWinMain	WinMain
#endif	// defined(__GUNC__)

int WINAPI _tWinMain( HINSTANCE hThisInst, HINSTANCE hPrevInst, LPTSTR lpszArgs, int nWinMode)
{
    HANDLE hCPUThread = NULL;
    MSG  message;
    LPTSTR argv[100];
    int argc;
    hInst = hThisInst;	// save hThisInst 

    InitVariable();			// <--- init variable
    init_keybuffer();			// <--- init key buffer
    ConfigInit();				// <--- config init
    ConfigRead();				// <--- config read 

#ifdef _WIN32_WCE
	_tgetcwd(CasPath[0], _countof(CasPath[0]));
	_tgetcwd(CasPath[1], _countof(CasPath[1]));
	_tgetcwd(DskPath[0], _countof(DskPath[0]));
	_tgetcwd(DskPath[1], _countof(DskPath[1]));
	_tgetcwd(RomPath, _countof(RomPath));
	lstrcat(RomPath, TEXT("\\rom"));
#endif	// _WIN32_WCE

    conv_argv( lpszArgs, &argc , argv);     // convert lpszArgs --> argc, argv
    if(!chkOption( argc, argv)) return(0);	// <--- option check
    InitMachine32(hThisInst, hPrevInst, lpszArgs ,nWinMode);

    if( UseCPUThread)  // *****************  CPU $B$OJL%9%l%C%I$G<B9T(B **************
        {
         CPUThreadRun=1;
         hCPUThread = CreateThread( NULL,0,(LPTHREAD_START_ROUTINE) CPUThreadProc ,NULL,0,&dwThreadId);
         CloseHandle( hCPUThread);
         while(GetMessage( &message,NULL,0,0))
            {
          	 TranslateMessage(&message);	/* $B%-!<%\!<%I%a%C%;!<%8$rJQ49(B*/
        	 DispatchMessage( &message);	/* Windows $B$K@)8f$rJV$9(B */
        	}
        }
    else
        {
        while(1)		// ***************** $B%a%C%;!<%8$,Mh$F$J$$4V$rK%$C$F!"(BZ80$B$r<B9T(B *******
        	{
        	BOOL bDone = FALSE;
        	while(PeekMessage( &message , NULL,0,0,PM_NOREMOVE	))
        		{
        		if(!GetMessage( &message,NULL,0,0)) {
        			bDone = TRUE;
        			break;
        		}
          		TranslateMessage(&message);	/* $B%-!<%\!<%I%a%C%;!<%8$rJQ49(B*/
        		DispatchMessage( &message);	/* Windows $B$K@)8f$rJV$9(B */
        		}
        	if (bDone)
        	{
        		break;
        	}
            CPUThreadProc(NULL);
            }
        }
    return (int)message.wParam;
}





// ****************************************************************************
//          PutImage: $B%(%_%e%l!<%?2hLL$r!"%9%/%j!<%s$K(B bitblt
// ****************************************************************************
/*
    normal:
	   screen <-- hb1
	
	scroll:
	   screen <-- hb2 <-- hb1

*/
void PutImage(void)
{
#if 1
  HDC hdc;
  int x,y;
  int w,h;		// blt size:           width height
  int top;		// display start line
  static int lasttop = 0;
  static int lastbottom = 204;
  
  hdc= GetDC(hwndMain);

  w= hb1.Width;
  h= (bitmap ? lines :200) * scale; // text mode =200  graphics mode=204
  top = (204-lines)*scale; 			// 200 line= 4     204 line= 0
  
	if (lasttop < top)
	{
		RECT rc;
		rc.left = paddingw;
		rc.top = paddingh + lasttop;
		rc.right = paddingw + w;
		rc.bottom = paddingh + top;
		FillRect(hdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
	}
	lasttop = top;

	{
		const int bottom = top + h;
		if (lastbottom > bottom)
		{
			RECT rc;
			rc.left = paddingw;
			rc.top = paddingh + bottom;
			rc.right = paddingw + w;
			rc.bottom = paddingh + lastbottom;
			FillRect(hdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
		}
		lastbottom = bottom;
	}

//	  BitBlt( hdc, 0,top+paddingh+h+5, hb_statusbar.Width , hb_statusbar.Height, hb_statusbar.hdcBmp,0,0, SRCCOPY);  


  if( isScroll())	// scroll mode 
  	{
	  int dx,dy;
	  x = portCB *256 + portCA;
	  y = portCC;

	  x *= scale;
	  y *= scale;

	  dx = hb1.Width-x+1;
	  dy = hb1.Height-y+1;
	  BitBlt( hb2.hdcBmp, dx ,dy, x,  y,hb1.hdcBmp, 0, 0,SRCCOPY);
	  BitBlt( hb2.hdcBmp, 0  ,dy,dx,  y,hb1.hdcBmp, x, 0,SRCCOPY);
	  BitBlt( hb2.hdcBmp, dx ,0 , x, dy,hb1.hdcBmp, 0, y,SRCCOPY);
	  BitBlt( hb2.hdcBmp, 0  ,0, dx, dy,hb1.hdcBmp, x, y,SRCCOPY);

	  BitBlt( hdc, 0+paddingw,top+paddingh, w , h, hb2.hdcBmp,0,0, SRCCOPY);  // to screen
	}
  else		// normal mode
  	{
    BitBlt( hdc, 0+paddingw,top+paddingh, w , h, hb1.hdcBmp,0,0, SRCCOPY);  // to screen
    }
 ReleaseDC(hwndMain,hdc);
#endif
	s_nDrawFrames++;
}


// ****************************************************************************
//          resizewindow: $BAk$NBg$-$5$H!"%(%_%e%l!<%?2hLL$NBg$-$5$rJQ99$9$k(B
// ****************************************************************************
/* In: bitmap_scale: Emulator's scale
       win_scale:    window's   scale
  Out: 

  Date: 2003/4/27
*/
int resizewindow( int bitmap_scale , int win_scale)
{
	setMenuCheck( IDM_SCREENSIZE , bitmap_scale-1);	// menu check /uncheck


	Width = M6WIDTH * win_scale;		// change size
	Height= M6HEIGHT* win_scale;

	SetWindowPos( hwndMain,NULL,0,0,Width+BORDERW+PADDINGW*2,Height+BORDERH+PADDINGH*2,SWP_NOMOVE);

	Width = M6WIDTH * bitmap_scale;		// change size
	Height= M6HEIGHT* bitmap_scale;

	releaseIBMP( hb1);				// release & create bitmap
	releaseIBMP( hb2);
	hb1= createIBMP(hwndMain, Width ,Height,bitpix);
	hb2= createIBMP(hwndMain, Width ,Height,bitpix);
	XBuf = hb1.lpBMP;
 
	setwidth( bitmap_scale-1);			// setting new scale
	ClearWindow();						// clear window
	new_scale= scale;
	scale= bitmap_scale;
	return 1;
}

// ****************************************************************************
//          BITMAP CONTROL
// ****************************************************************************

/*
typedef struct tagBITMAPFILEHEADER { 	// ------- bmpFileHeader -----------
    WORD    bfType; 
    DWORD   bfSize; 
    WORD    bfReserved1; 
    WORD    bfReserved2; 
    DWORD   bfOffBits; 
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER{ 	// ------- hb.lpBmpInfoh -----------
    DWORD  biSize; 			// size of structor
    LONG   biWidth; 		// Width
    LONG   biHeight; 		// Height
    WORD   biPlanes; 		// 0
    WORD   biBitCount;		// 1/ 4 / 8 / 16 / 24 /32 bpp
    DWORD  biCompression; 	// BI_RGB: non compression
    DWORD  biSizeImage; 	// size of image
    LONG   biXPelsPerMeter; // 0
    LONG   biYPelsPerMeter; // 0
    DWORD  biClrUsed; 		// number of color table
    DWORD  biClrImportant;  // number of important color
} BITMAPINFOHEADER; 

typedef struct tagBITMAPINFO { // hb.lpBmpInfo
    BITMAPINFOHEADER bmiHeader; 
    RGBQUAD          bmiColors[1]; 
} BITMAPINFO;

typedef struct tagRGBQUAD {
    BYTE    rgbBlue; 
    BYTE    rgbGreen; 
    BYTE    rgbRed; 
    BYTE    rgbReserved; 
} RGBQUAD;

$BO@M}%Q%l%C%H9=B$BN(B
typedef struct tagLOGPALETTE { // lgpl 
    WORD         palVersion; 	// 0x0300: palet version
    WORD         palNumEntries; // number of color table
    PALETTEENTRY palPalEntry[1]; 
} LOGPALETTE; 

typedef struct tagPALETTEENTRY { // pe 
    BYTE peRed; 
    BYTE peGreen; 
    BYTE peBlue; 
    BYTE peFlags; 
} PALETTEENTRY;


256$B%+%i!<$rIA2h$9$k$K$O!"(B

//$B!!O@M}%Q%l%C%H$r:n@.$9$k(B:
//  hpalette = CreatePalette( &logpalette);
//$B!!%G%P%$%9%3%s%F%-%9%H$K!"O@M}%Q%l%C%H$r@_Dj(B:
//  HPALETTE = SelectPalette( hdc, hpalette, TRUE / FALSE);
//$B!!O@M}%Q%l%C%H$r!!%7%9%F%`%Q%l%C%H$K@_Dj(B
//  num = RealizePalette(hdc);

*/

// ****************************************************************************
//          createIBMP: $B?7$7$$%S%C%H%^%C%W$N%5!<%U%'%9$r:n@.$7$F!"JV5Q$9$k(B
// ****************************************************************************
/* In: HWND           handle of window
       width, height  new bitmap size
	   bitpix         color depth   1bpp 8bpp 24bpp 32bpp
  Out: HIBMP          struct of new bitmap  */
HIBMP createIBMP(HWND hwnd, int width , int height, int bitpix)
{
 HDC   hdc;
 HIBMP hb;
 
	// ------------------------------------------------------------
	hb.lpBmpInfo = (BITMAPINFO *)malloc( offsetof( BITMAPINFO, bmiColors[256]));
	ZeroMemory(hb.lpBmpInfo, offsetof( BITMAPINFO, bmiColors[256]));

	hb.lpBmpInfo->bmiHeader.biSize   = sizeof( hb.lpBmpInfo->bmiHeader);
	hb.lpBmpInfo->bmiHeader.biWidth  = width;
	hb.lpBmpInfo->bmiHeader.biHeight = height*(-1);	// top down bitmap
	hb.lpBmpInfo->bmiHeader.biPlanes = 1;
	hb.lpBmpInfo->bmiHeader.biBitCount= bitpix; /*depth; */
	hb.lpBmpInfo->bmiHeader.biCompression= BI_RGB;
	hb.lpBmpInfo->bmiHeader.biClrUsed= (bitpix<=8)? 1<<bitpix: 0;
	if (bitpix <= 8)
	{
		const unsigned int palettes = min(hb.lpBmpInfo->bmiHeader.biClrUsed, _countof(s_PalTbl));
		memcpy(hb.lpBmpInfo->bmiColors, s_PalTbl, palettes * sizeof(RGBQUAD));
	}
	else if (bitpix == 16)
	{
		hb.lpBmpInfo->bmiHeader.biCompression= BI_BITFIELDS;
		*(DWORD *)(&hb.lpBmpInfo->bmiColors[0]) = 0xf800;
		*(DWORD *)(&hb.lpBmpInfo->bmiColors[1]) = 0x07e0;
		*(DWORD *)(&hb.lpBmpInfo->bmiColors[2]) = 0x001f;
	}

	hb.Width = width;
	hb.Height= height;

	// ---- Create dib section bitmap ---
	// XBuf   pixel buffer  ( auto allocation memory)
	hb.hBitmap1 = CreateDIBSection( NULL,(BITMAPINFO*)hb.lpBmpInfo, DIB_RGB_COLORS,(void**)&hb.lpBMP,NULL,0);
	if( hb.hBitmap1 !=NULL)
	   {
		// ---- mapping to compatible DC ---
		hdc= GetDC( hwnd);					 // get main window DC
		hb.hdcBmp = CreateCompatibleDC(hdc);    // create compatible DC
		SelectObject(hb.hdcBmp, hb.hBitmap1);	 // select bitmap to compatible DC

		// ---- get bitmap information from bitmap handle (hBitmap1 --> bitmap1)
		//GetObject(   hBitmap1, sizeof(BITMAP), &bitmap1);
		ReleaseDC( hwnd,hdc);
	   }
	return(hb);
	// ------------------------------------------------------------
}

// ****************************************************************************
//          releaseIBMP: createIBMP $B$G:n@.$7$?%S%C%H%^%C%W$N%5!<%U%'%$%9$r2rJ|(B
// ****************************************************************************
void releaseIBMP(HIBMP hb)
{
	DeleteDC(hb.hdcBmp);
	DeleteObject(hb.hBitmap1);
	free( hb.lpBmpInfo );
}


// ****************************************************************************
//          savesnapshot: $B%9%J%C%W%7%g%C%H$r=PNO(B
// ****************************************************************************
/*
bitmap $B%U%!%$%k$N9=B$(B

	BITMAPFILEHEADER
	BITMAPINFOHEADER
   ( RGBQUAD x $B?'?t(B )   <-- 256 $B?'%+%i!<$N>l9g(B
	dib section  (bitmap images)
	
	bug fix: save 200 lines when 200 line mode   Date : 2003/4/25
*/
static int savesnapshot(HIBMP hb, LPCTSTR path)
{
 BITMAPFILEHEADER bmpFileHeader;		// BMP file header
 int w,h;
 int y;
 FILE *fp;
 BYTE *p;
 int  ret;		// 1: success 0:failed
 int  depth;
 int  bakHeight;
 int  colors;
 int  cbBitmapInfo;

 bakHeight = hb.lpBmpInfo->bmiHeader.biHeight;	// backup Height
 w = hb.lpBmpInfo->bmiHeader.biWidth;
 h = hb.lpBmpInfo->bmiHeader.biHeight = lines*scale;
 
 ret  = 1;
 depth= hb.lpBmpInfo->bmiHeader.biBitCount;
 colors= (hb.lpBmpInfo->bmiHeader.biCompression== BI_BITFIELDS) ? 3 : hb.lpBmpInfo->bmiHeader.biClrUsed;
 cbBitmapInfo = sizeof(BITMAPINFOHEADER) + (sizeof(RGBQUAD) * colors);

 bmpFileHeader.bfType = 'M'*256+'B';
 bmpFileHeader.bfSize = (DWORD)(sizeof( BITMAPFILEHEADER)+cbBitmapInfo+w * h*depth/8);
 bmpFileHeader.bfOffBits = (DWORD)(sizeof( BITMAPFILEHEADER)+cbBitmapInfo);

 fp=_tfopen(path,TEXT("wb")); 
 if(fp!=NULL)
	{
	 fwrite( &bmpFileHeader, sizeof(bmpFileHeader) ,1,fp);
	 fwrite( hb.lpBmpInfo , cbBitmapInfo ,1,fp);

	 for(y=h-1; y>=0 ; y--)		// write image from bottom to up
		{
		 p = hb.lpBMP+y*(w*depth/8);
		 if(fwrite( (char*)p  , w*depth/8 ,1,fp)!=1) { ret = 0;break;}
		}
	}
 else
 	 ret=0;

 hb.lpBmpInfo->bmiHeader.biHeight = bakHeight;		// restore Height
 fclose(fp);
 return(ret);
}



// ****************************************************************************
//          SLEEP: $BL2$k(B
// ****************************************************************************
void SLEEP(int s)
{
	s_dwLastFrameTick += s;
	s_dwLastStatusTick += s;
	SleepUntil(s + GetTickCount());
}

#if !defined(_WIN32_WCE)
static int keyrepeat=-1;
static int new_keyrepeat;
#endif	// !defined(_WIN32_WCE)

// ****************************************************************************
//          setKeyrepeat: $B%-!<%j%T!<%H$NB.EY$r@_Dj$9$k(B
// ****************************************************************************
void setKeyrepeat(int value)
{
#if !defined(_WIN32_WCE)
 	if( keyrepeat ==-1)		/* $B5/F0;~$N%-!<%j%T!<%H$NCM$r<hF@$9$k(B */
		SystemParametersInfo(SPI_GETKEYBOARDSPEED, 0    , &keyrepeat, 0);	// get key repeat
	SystemParametersInfo(SPI_SETKEYBOARDSPEED, value, 0, 0);	// set key repeat
    new_keyrepeat = value;
#endif	// !defined(_WIN32_WCE)
}

// ****************************************************************************
//          storeKeyrepeat: $B%-!<%j%T!<%H$NB.EY$r85$KLa$9(B
// ****************************************************************************
void storeKeyrepeat(void)
{
#if !defined(_WIN32_WCE)
	if( keyrepeat !=-1)
    	{
/*		if( keyrepeat == new_keyrepeat)
			keyrepeat=30; */
     	SystemParametersInfo(SPI_SETKEYBOARDSPEED, keyrepeat, 0, 0);	// restore key repeat 
        keyrepeat = -1;
        }
#endif	// !defined(_WIN32_WCE)
}





// ********************************************************************************
// TO DO: $B4D6-Hs0MB8%3!<%I$K$9$k$3$H!*(B

// ****************************************************************************
//          PutDiskAccessLamp: $B%G%#%9%/$N%"%/%;%9%i%s%W!!E@Et(B/$B>CEt(B
// ****************************************************************************
void PutDiskAccessLamp(int sw)
{
#if !defined(_WIN32_WCE)
    if( UseDiskLamp)
        {
        HDC hdc;
        int r=0,g=0,b=0;
        int sx,sy,ex,ey;
    
        sx= Width-paddingw  -40;
        sy= Height+paddingh+10;
		ex= sx+30;
		ey= sy+10;
		
        switch(sw)
           {
            case 0:  r=  0; g=0;   b=0; break;
            case 1:  r=255; g=0;   b=0; break;
			case 255:r=255; g=255; b=255; break;
           }

        hdc = GetDC( hwndMain);

		if( sw <2)
			{
				HBRUSH hBrush =CreateSolidBrush(RGB(r, g, b));
				hBrush = (HBRUSH)SelectObject(hdc, hBrush);
				Rectangle( hdc, sx   ,sy  , ex , ey); 
				hBrush = (HBRUSH)SelectObject(hdc, hBrush);
				DeleteObject(hBrush);
			}
		else
			{
			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(r, g, b));
			hPen = (HPEN)SelectObject(hdc, hPen);
			MoveToEx(  hdc, sx-1 ,sy-1 ,NULL);
			LineTo(    hdc, ex+0 ,sy-1);
			LineTo(    hdc, ex+0 ,ey+0);
			LineTo(    hdc, sx-1 ,ey+0);
			LineTo(    hdc, sx-1 ,sy-1);
			hPen = (HPEN)SelectObject(hdc, hPen);
			DeleteObject(hPen);
			}

        ReleaseDC( hwndMain , hdc);
        }
#endif	// !defined(_WIN32_WCE)
}


// ****************************************************************************
//          putStatusBarSub: $B%9%F!<%?%9%P!<$NI=<((B (sub)
// ****************************************************************************
void putStatusBarSub(void)
{
    HDC hdc;
  	int h;		// blt size:           height
  	int top;		// display start line
  
  	h= (bitmap ? lines :200) * scale; // text mode =200  graphics mode=204
  	top = (204-lines)*scale; 			// 200 line= 4     204 line= 0
	hdc= GetDC( hwndMain);
    BitBlt( hdc, 0,top+paddingh+h+5, hb_statusbar.Width , hb_statusbar.Height, hb_statusbar.hdcBmp,0,0, SRCCOPY);  

    ReleaseDC( hwndMain, hdc);
}

// ****************************************************************************
//          putStatusBar: $B%9%F!<%?%9%P!<$NI=<((B
// ****************************************************************************
void putStatusBar(void)
{
	TCHAR tmp[256];
	TCHAR str[256];
	HFONT hfont;
	HFONT hfontbak;
	HDC  hdc;
	int  xx,yy;
	fpos_t pos[2]={-1,-1};
  
	const DWORD now = GetTickCount();
	const DWORD past = now - s_dwLastStatusTick;
	if (past < 1000)
	{
		return;
	}
	s_dwLastStatusTick = now;
  
	xx= 4;
	yy= 4;
  
	hdc  = hb_statusbar.hdcBmp;
#if !defined(_WIN32_WCE)
	hfont = CreateFont(12,0, 
    				FW_DONTCARE, FW_DONTCARE, FW_REGULAR,
					FALSE, FALSE, FALSE, 
					SHIFTJIS_CHARSET,
					OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY, 
					FF_MODERN | FIXED_PITCH,
					 TEXT(""));
#else	// !defined(_WIN32_WCE)
	{
		LOGFONT lf;
		ZeroMemory(&lf, sizeof(lf));
		lf.lfHeight = 12;
		lf.lfWeight = FW_THIN;
		lf.lfCharSet = SHIFTJIS_CHARSET;
		lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
		lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
		lf.lfQuality = DEFAULT_QUALITY;
		lf.lfPitchAndFamily = FF_MODERN | FIXED_PITCH;
		hfont = CreateFontIndirect(&lf);
	}
#endif	// !defined(_WIN32_WCE)
	hfontbak = (HFONT)SelectObject(hdc, hfont);

	SetTextColor( hdc ,RGB(110,110,110));     // character color
	SetBkColor(   hdc, RGB(0,0,0));

	wsprintf( str, TEXT("Load Tape: %-15s  "), CasName[0] );
	if( CasStream[0])
         fgetpos(CasStream[0], &pos[0]);
	if( pos[0] !=-1) 
         wsprintf( tmp, TEXT("[%05ld]"), (long)pos[0] );
	else
         tmp[0]=0; 
	lstrcat( str,tmp);
#if !defined(_WIN32_WCE)
	TextOut(hdc ,xx,yy   ,str,lstrlen(str));    // write to bitmap
#else	// !defined(_WIN32_WCE)
	ExtTextOut(hdc, xx, yy, ETO_OPAQUE, NULL, str, lstrlen(str), NULL);
#endif	// !defined(_WIN32_WCE)

	wsprintf( str, TEXT("Save Tape: %-15s  "), CasName[1]);
	if( CasStream[1])
         fgetpos(CasStream[1], &pos[1]);
	if( pos[1] !=-1)
         wsprintf( tmp, TEXT("[%05ld]"), (long)pos[1] );
	else
         tmp[0]=0; 
	lstrcat( str,tmp);
#if !defined(_WIN32_WCE)
	TextOut(hdc ,xx,yy+14,str,lstrlen(str));    // write to bitmap
#else	// !defined(_WIN32_WCE)
	ExtTextOut(hdc, xx, yy + 14, ETO_OPAQUE, NULL, str, lstrlen(str), NULL);
#endif	// !defined(_WIN32_WCE)

	{
		const unsigned int nSpeed = s_nFrames * 1000 * 1000 / (past * 60);
		const unsigned int nDrawFrames = s_nDrawFrames * 10 * 1000 / past;
		s_nFrames = 0;
		s_nDrawFrames = 0;

		wsprintf(str, TEXT("%3d.%d%% %3d.%dfps  "), nSpeed / 10, nSpeed % 10, nDrawFrames / 10, nDrawFrames % 10);
		ExtTextOut(hdc, xx + 240, yy, ETO_OPAQUE, NULL, str, lstrlen(str), NULL);
		s_nDrawFrames = 0;
	}

	SelectObject( hdc, hfontbak);
	DeleteObject( hfont );

	putStatusBarSub();

}


#endif // WIN32
