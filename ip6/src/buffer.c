// ******************************************************
// 			  buffer.h  
// by Windy 2002-2003
// *******************************************************
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "buffer.h"

// ****************************************************************************
//           key buffer (ring buffer)
// ****************************************************************************
struct _buffer2 {
  int   read_idx;
  int   write_idx;
  int   length;
  DWORD wParam[1024];
  DWORD lParam[1024];
  char  chr   [1024];
} keybuffer;



void init_keybuffer(void)
{
  keybuffer.read_idx=0;
  keybuffer.write_idx=0;
  keybuffer.length=1024-1;
}

void write_keybuffer(DWORD wParam , DWORD lParam , char chr)
{
  keybuffer.wParam[ keybuffer.write_idx]= wParam;
  keybuffer.lParam[ keybuffer.write_idx]= lParam;
  keybuffer.chr   [ keybuffer.write_idx]= chr;
  if( ++keybuffer.write_idx >keybuffer.length)
	{
	 keybuffer.write_idx=0;
	}
}

int read_keybuffer(DWORD *wParam , DWORD *lParam , char *chr)
{
 int ret;
 ret=0;
  if( keybuffer.read_idx != keybuffer.write_idx)
  	{
	 if(wParam) *wParam = keybuffer.wParam[ keybuffer.read_idx];
	 if(lParam) *lParam = keybuffer.lParam[ keybuffer.read_idx];
	 if(chr   ) *chr    = keybuffer.chr   [ keybuffer.read_idx];
	 if( ++keybuffer.read_idx >keybuffer.length)
		{
		 keybuffer.read_idx=0;
		}
	 ret=1;
	}
 return(ret);
}

