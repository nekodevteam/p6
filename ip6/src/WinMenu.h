/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           WinMenu.h                     **/
/** Written by Windy 2002-2003                              **/
/*************************************************************/

#define IDM_CONFIG		100
#define IDM_RESET	 	102
#define IDM_WRITE_RAM 	103
#define IDM_SNAPSHOT 	104
#define IDM_STATUSBAR   105
#define IDM_NOWAIT   	106
#define IDM_EXIT 		110

#define IDM_OPEN_LOAD_TAPE		150
#define IDM_EJECT_LOAD_TAPE		151
#define IDM_REWIND_LOAD_TAPE	152

#define IDM_OPEN_SAVE_TAPE		155
#define IDM_EJECT_SAVE_TAPE		156
#define IDM_REWIND_SAVE_TAPE	157

#define IDM_USE_SAVE_TAPE       159

#define IDM_OPEN_DISK	160
#define IDM_EJECT_DISK	161

#define IDM_SCANLINE    170
#define IDM_DISK_LAMP   171
#define IDM_SCREENSIZE  172
#define IDM_FULLSCR     183


#define IDM_MONITOR 	180
#define IDM_NO_TIMER 	181

#define IDM_HELP 		190
#define IDM_VER     	191
#define IDM_URL         192

#define ID_TEXT1 	2000

#define ID_RDPC60	60
#define ID_RDPC62	62
#define ID_RDPC64 	64
#define ID_RDPC66	66
#define ID_RDPC68 	68

#define ID_RDFD0 	80
#define ID_RDFD1 	81
#define ID_RDFD2 	82

#define ID_SOUNDOFF 90
#define ID_SOUNDON 	91

#define ID_SCALE1 	101
#define ID_SCALE2 	102

#define ID_FULLSCR  105

#define ID_SCANLINE  110

#define ID_SAVETAPEMENU 111
#define ID_STATUSBAR    112
#define ID_DISKLAMP     114



#define ID_NORMALTAPE 	200
#define ID_FASTTAPE 	201

#define ID_60FPS 	300
#define ID_30FPS 	301

#define ID_CPUCLOCK     400
#define ID_CPUCLOCK_UPDOWN 401

#define ID_DRAWWAIT     500
#define ID_DRAWWAIT_UPDOWN 501

#define ID_R11025       600
#define ID_R22050		601
#define ID_R44100		602

#define ID_SCR4MONO		700
#define ID_SCR4COL		701

#define ID_EXTKANJIOFF  800
#define ID_EXTKANJION   801

#define ID_EXTRAMOFF    900
#define ID_EXTRAMON     901

#define ID_SRLINE        1000
#define ID_SRLINE_UPDOWN 1001

#define ID_DEFCLOCK      1100


#define ID_EXTROMNAME    1200
#define ID_OPENEXTROM	 1201
#define ID_CLEAREXTROM	 1202

#define IC_PC66SR        1

#define ID_OTHER         -1
