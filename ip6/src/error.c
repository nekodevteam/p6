/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           error.c                       **/
/** Modified by Windy 2002-2003                             **/
/*************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "P6.h"
#include "error.h"
#include "os.h"



int debug_level=0;

// ****************************************************************************
//          debug_printf: $B%G%P%C%0%W%j%s%H(B
// ****************************************************************************
int debug_printf( LPCTSTR fmt ,...)
{
	int  ret;
	static TCHAR buff[5000];
	va_list ap;

	if( debug_level==0) return(0);

	va_start( ap ,fmt);
	ret = _vstprintf( buff  , fmt , ap);
	va_end( ap);

   	_ftprintf(stdout,TEXT("%s"),buff);
	outputdebugstring( buff);

	return(ret);
}


