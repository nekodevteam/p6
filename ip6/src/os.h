/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           os.h                          **/
/**                                                         **/
/** by Windy                                                **/
/*************************************************************/
#ifdef WIN32
#include "Win32.h"
#endif

#ifdef UNIX
#include "Unix.h"
#endif
