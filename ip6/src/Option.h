/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Option.h                      **/
/** modified by Windy 2002-2004                             **/
/** This code is based on ISHIOKA Hiroshi 1998-2000         **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/
int ConfigWrite(void);
int ConfigRead(void);
void ConfigInit(void);

int chkOption( int argc, LPTSTR argv[]);
