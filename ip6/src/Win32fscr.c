/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Win32fscr.c                   **/
/**                                                         **/
/** This code is Written by Windy 2003                      **/
/*************************************************************/
/*
Win32環境のフルスクリーンと、窓モードの切り替えです。
Date:  2003/9/29
*/
#ifdef WIN32
#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>
//#include <time.h>

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
//#include <signal.h>
#include <direct.h>
#include "Refresh.h"
#include "WinMenu.h"
#include "Win32.h"
#include "Win32fscr.h"

extern byte Verbose;
extern int paddingw,paddingh;
extern void setMenuOnOff(int type ,int sw);
extern HWND hwndMain;


static DEVMODE pre_devmode;
static int  isFullScr=0;
static HMENU hPopupMenu=0;

static void savemenu(void);
static int hidemenu(void);
static int restoremenu(void);

// ****************************************************************************
//          isFullScreen: ディスプレイモードの取得
// ****************************************************************************
int isFullScreen(void)
{
	return( isFullScr);
}


// ****************************************************************************
//          saveDisplayMode: ディスプレイモードの保存
// ****************************************************************************
void saveDisplayMode(void)
{
	HDC	hdc;
	
    hdc = GetDC(0);

	pre_devmode.dmSize = sizeof(DEVMODE);
	pre_devmode.dmPelsWidth  = GetDeviceCaps(hdc, HORZRES);
	pre_devmode.dmPelsHeight = GetDeviceCaps(hdc, VERTRES);
	pre_devmode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;

#if !defined(_WIN32_WCE)
	// Windows NT/2000用
#if (WINVER < 0x0600)
	if ((GetVersion() & 0x80000000) == 0) 
#endif	/* (WINVER < 0x0600) */
    	{				
		pre_devmode.dmFields |= DM_DISPLAYFREQUENCY;
		pre_devmode.dmDisplayFrequency = GetDeviceCaps(hdc, VREFRESH);
		}
#endif	// !defined(_WIN32_WCE)

	ReleaseDC(0,hdc);
}

// ****************************************************************************
//          storeDisplayMode: ディスプレィモードを元に戻す。
// ****************************************************************************
void storeDisplayMode(void)
{

	 if( isFullScr) toggleFullScr();
}


// ****************************************************************************
//          togglefullScr: フルスクリーン <---> ウインドウ
// ****************************************************************************
int toggleFullScr(void)
{
	int ret=0;
#if !defined(_WIN32_WCE)
	DEVMODE	devmode;

	if( !isFullScr )
    	{			// change to full screen 
		setMenuOnOff( IDM_SCREENSIZE , 0);
		if(scale ==1) resizewindow(2,2);		/* scale 1なら２にする */

		devmode = pre_devmode;
		devmode.dmPelsWidth  = 320*scale;
		devmode.dmPelsHeight = 240*scale;
		devmode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;

		if(Verbose) printf("Changing to FULL SCREEN ...");

        savewindowstate( hwndMain);

		if (SetWindowPos(hwndMain,NULL,0,0, 0 ,0 ,SWP_NOMOVE | SWP_NOSIZE |SWP_SHOWWINDOW))
		  if (SetWindowLong(hwndMain,GWL_STYLE,WS_VISIBLE))
			if (ChangeDisplaySettings(&devmode, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL)
				if (SetWindowPos(hwndMain,HWND_TOPMOST,0,0, devmode.dmPelsWidth ,devmode.dmPelsHeight ,SWP_SHOWWINDOW))
                	{
               		if(Verbose) printf("OK\n");

                     savemenu();	// save menu
					 hidemenu();	// hide menu
                     paddingw = 0;	// SET padding zero
                //     paddingh = 0; // do nothing
                     isFullScr=1;
					 ret=1;
                    }
        }
	else
    	{			// change to window
		if(Verbose) printf("Changing to WINDOW ...");
		devmode = pre_devmode;
		if (SetWindowPos(hwndMain,NULL,0,0, 0 ,0 ,SWP_NOMOVE | SWP_NOSIZE |SWP_SHOWWINDOW))
	      if( ChangeDisplaySettings( &devmode , CDS_FULLSCREEN ) == DISP_CHANGE_SUCCESSFUL )
            {
       		if(Verbose) printf("OK\n");
	        restorewindowstate( hwndMain);
			restoremenu();
			setMenuOnOff( IDM_SCREENSIZE , 1);
	         paddingw = PADDINGW;		// RESTORE padding 
    	     paddingh = PADDINGH;
        	 isFullScr=0;
         	 ret=1;
            }
        }

	if( Verbose && !ret) printf("FAILED \n");
	ClearWindow();						// clear window
#endif	// !defined(_WIN32_WCE)

	return(ret);
}



static RECT  prev_rect;
static DWORD prev_wndstyle;
static DWORD prev_wndstyleEx;


// ****************************************************************************
//          savewindowstate: ウインドウの状態を保存
// ****************************************************************************
void savewindowstate( HWND hwnd)
{
	GetWindowRect( hwnd , &prev_rect);
	prev_wndstyle   = GetWindowLong( hwnd , GWL_STYLE);
	prev_wndstyleEx = GetWindowLong( hwnd , GWL_EXSTYLE);
}

// ****************************************************************************
//          restorewindowstate: ウインドウの状態を復帰
// ****************************************************************************
void restorewindowstate(HWND hwnd)
{
	SetWindowLong( hwnd ,GWL_STYLE   , prev_wndstyle);
	SetWindowLong( hwnd ,GWL_EXSTYLE , prev_wndstyleEx);
	SetWindowPos(  hwnd ,HWND_NOTOPMOST,prev_rect.left ,
    									prev_rect.top  ,
                                        prev_rect.right - prev_rect.left,
                                        prev_rect.bottom- prev_rect.top,
                                        SWP_SHOWWINDOW);
}

#if !defined(_WIN32_WCE)

HMENU   prev_hmenu=0;

// ****************************************************************************
//          savemenu: メニューの情報を保存
// ****************************************************************************
static void savemenu(void)
{
	prev_hmenu = GetMenu(hwndMain);
}


// ****************************************************************************
//          hidemenu: メニューを隠す
// ****************************************************************************
static int hidemenu(void)
{
	int ret;
    if( prev_hmenu) 
	  	ret= SetMenu( hwndMain, NULL);
	return(ret);
}

// ****************************************************************************
//          hidemenu: メニューを元に戻す
// ****************************************************************************
static int restoremenu(void)
{
	int ret=0;
    if( prev_hmenu)
		ret = SetMenu( hwndMain , prev_hmenu);
    prev_hmenu =0;
	return( ret);
}
#endif	// !defined(_WIN32_WCE)

// ****************************************************************************
//          loadPopupMenu: POPUP メニューを読み込む
// ****************************************************************************
void loadPopupMenu( HINSTANCE hInstance, LPCTSTR menuname)
{
	HMENU htmpMenu;
	htmpMenu = LoadMenu( hInstance, menuname);
	hPopupMenu = GetSubMenu(htmpMenu,0);
}

// ****************************************************************************
//          openPopupMenu: POPUP メニューを開く
// ****************************************************************************
void openPopupMenu( HWND hwnd , int x, int y)
{
	if( hPopupMenu )
    	{
		POINT po;
		po.x = x; po.y = y;
		ClientToScreen( hwnd , &po);
		TrackPopupMenu( hPopupMenu , TPM_LEFTALIGN | TPM_BOTTOMALIGN, po.x , po.y ,0 ,hwnd ,NULL);
        }
}


// ****************************************************************************
//          getHpopupMenu: POPUP メニューのハンドルを返す
// ****************************************************************************
HMENU getPopupMenu( void)
{
	return( hPopupMenu);
}

#endif
