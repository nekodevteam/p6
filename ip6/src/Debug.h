/** Z80: portable Z80 emulator *******************************/
/**                                                         **/
/**                         Debug.h                         **/
/**                                                         **/
/** This file contains the built-in debugging routine for   **/
/** the Z80 emulator which is called on each Z80 step when  **/
/** Trap!=0.                                                **/
/**                                                         **/
/** Copyright (C) Marat Fayzullin 1995-1998                 **/
/**     You are not allowed to distribute this software     **/
/**     commercially. Please, notify me, if you make any    **/
/**     changes to this file.                               **/
/*************************************************************/
#include <stdio.h>

void locate(int x,int y);
char *d_fgets( char *buff,int len ,FILE *stream);
void ramdump(LPCTSTR path);


int monitor(reg *R);
