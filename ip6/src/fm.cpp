/**
 * @file	fm.cpp
 * @brief	FM $B2;8;$NF0:n$NDj5A$r9T$$$^$9(B
 */

#include "P6.h"
#include "fm.h"
#ifdef SOUND

#include "fm.hpp"

static Fm s_fm;		/*!< OPN $B%$%s%9%?%s%9(B */

/**
 * $B=i4|2=(B
 * @retval 1 $B@.8y(B
 * @retval 0 $B<:GT(B
 */
int ym2203_init(void)
{
	return s_fm.Initialize();
}

/**
 * $B%l%8%9%?=q$-9~$_(B
 * @param[in] r $B%l%8%9%?(B
 * @param[in] v $BCM(B
 */
void ym2203_setreg(unsigned int r, unsigned int v)
{
	s_fm.WriteRegister(r, v);
}

/**
 * $B%l%8%9%?<hF@(B
 * @param[in] r $B%l%8%9%?(B
 * @return $BCM(B
 */
unsigned int ym2203_getreg(unsigned int r)
{
	return s_fm.ReadRegister(r);
}

/**
 * $B%9%H%j!<%`$r<hF@$9$k(B
 * @param[in] pBuffer $B%P%C%U%!(B
 * @param[in] cbBuffer $B%5%$%:(B
 */
void ym2203_makewave(short *pBuffer, unsigned int cbBuffer)
{
	const unsigned int nSamples = cbBuffer / (sizeof(*pBuffer) * 2);
	s_fm.Get(pBuffer, nSamples * sizeof(*pBuffer) * CHANNEL);
}

/**
 * $B%9%F!<%?%9(B
 * @return $B%9%F!<%?%9(B
 */
unsigned int ym2203_ReadStatus(void)
{
	return s_fm.ReadStatus();
}

/**
 * HSync $B$4$H$K8F$P$l$k(B
 */
void ym2203_HSync(void)
{
	s_fm.IncLine();
}

/**
 * $B0l2hLL$N(B HSync $B2s?t$r%;%C%H(B
 * @param[in] nLines $B%i%$%s?t(B
 */
void ym2203_HMax(unsigned int nLines)
{
	s_fm.SetLines(nLines);
}

/**
 * $B%_%e!<%H$9$k(B
 * @param[in] mute $B%_%e!<%H(B
 * @return $B0JA0$N%_%e!<%H>uBV(B
 */
int ym2203_mute(int mute)
{
	return s_fm.Mute(mute != 0);
}

#endif	// SOUND
