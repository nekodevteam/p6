/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           fdc.h                         **/
/**                                                         **/
/*                     Internal disk unit                    */
/*************************************************************/
/* Modified  by windy */

void fdc_init(void);
void fdc_push_buffer( int port ,byte dat);
byte fdc_pop_buffer( int port);
byte fdc_inpDC(void);
void fdc_outDD(byte Value);
byte fdc_inpDD(void);
void fdc_outDA(byte Value);

