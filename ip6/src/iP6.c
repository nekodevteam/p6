/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           iP6.c                         **/
/**                                                         **/
/** by ISHIOKA Hiroshi 1998,1999                            **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <signal.h>
#include "P6.h"
#include "Option.h"

#ifdef UNIX
#include "Xconf.h"
#endif

extern LPCTSTR Title;
extern int   UseSHM;
extern int   UseSound;
extern int   scale;
extern int   IntLac;

#ifdef UNIX
int main(int argc,char *argv[])
{
  int N,J;
  TrapBadOps=0;
  Verbose=1;P6Version=1;PatchLevel=1;

#ifdef MITSHM
  UseSHM=1;
#endif
#ifdef SOUND
  UseSound=0;
#endif
  InitVariable();		// init variable
  chkOption(argc,argv);
  Xt_init(&argc,&argv);

	{
	  if(!InitMachine()) return(1);
	  if( StartP6() )
        {
        word A;
        if(Verbose) printf("OK\nRUNNING ROM CODE...\n");
        A=Z80();
        if(Verbose) printf("EXITED at PC = %04Xh.\n",A);
        }

      TrashP6();
	  TrashMachine();
	}
  return(0);
}

#endif
