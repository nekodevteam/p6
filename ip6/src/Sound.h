/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           Sound.h                       **/
/** modified by Windy 2002                                  **/
/** This code is based on ISHIOKA Hiroshi 1998-2000         **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/


// ****************************************************************************
//             	      DEFINE
// ****************************************************************************
/*
** Output rate (Hz)
*/

#ifndef SOUND_RATE
#define SOUND_RATE 22050
#endif // SOUND_RATE

#define FREQ 2052654284
#define CHANNEL         1   // sound channel  1:mono  2:streao
#define SAMPLING_BITS  16
/*
** Buffer size. SOUND_BUFSIZE should be equal to (or at least less than)
** 2 ^ SOUND_BUFSIZE_BITS
*/
#define SOUND_BUFSIZE_BITS 10
#define SOUND_BUFSIZE   (2 << SOUND_BUFSIZE_BITS)

/*
** Number of audio buffers to use. Must be at least 2, but bigger value
** results in better behaviour on loaded systems (but output gets more
** delayed)
*/
#define SOUND_NUM_OF_BUFS 2  // 2

// ****************************************************************************
//             	      PUBLIC
// ****************************************************************************
#ifdef SOUND
int InitSound(void);
void TrashSound(void);
void StopSound(void);
void ResumeSound(void);
#endif

extern int UseSound;

