/**
 * @file	fm.h
 * @brief	FM 音源の宣言およびインターフェイスの定義をします
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif	// __cplusplus

int ym2203_init(void);
void ym2203_setreg(unsigned int r, unsigned int v);
void ym2203_makewave(short *pBuffer, unsigned int cbBuffer);
unsigned int ym2203_getreg(unsigned int r);
unsigned int ym2203_ReadStatus(void);
void ym2203_HSync(void);
void ym2203_HMax(unsigned int nLines);
int ym2203_mute(int mute);

#ifdef __cplusplus
}
#endif	// __cplusplus
