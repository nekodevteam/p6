/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                      Refresh.h                          **/
/**                                                         **/
/** by ISHIOKA Hiroshi 1998,1999                            **/
/** This code is based on fMSX written by Marat Fayzullin   **/
/*************************************************************/

// --------------- $B@k8@$9$k>l9g$O!"(B#define GLOBAL $B$9$k(B -------------
#ifndef _REFRESH_H
#define _REFRESH_H

#ifdef  GLOBAL
#undef  EXTRN
#define EXTRN
#else
#undef  EXTRN
#define EXTRN extern
#endif


/* size of ximage */
#define M1WIDTH   256		// mode 1
#define M1HEIGHT  192
#define M5WIDTH   320		// mode 5
#define M5HEIGHT  200

#define M6WIDTH   320		// mode 6      add 2003/3/16
#define M6HEIGHT  204


#define PADDINGW   15		// PADDING   ($B%S%C%H%^%C%W$H!"%&%$%s%I%&$N7d4V!K(B
#define PADDINGH   40
//#define WIDTH     M5WIDTH*2
//#define HEIGHT    M5HEIGHT*2
//#define SCRWIDTH  WIDTH +PADDINGW	// SCREEN WIDTH
//#define SCRHEIGHT HEIGHT+PADDINGH

#define BORDERW    8        // BORDER    ($B%&%$%s%I%&OH$^$G$N7d4V(B)
#define BORDERH   46


#if defined(_WIN32_WCE)
#define DEPTH	  16 		// color depth
#else	// defined(_WIN32_WCE)
#define DEPTH	  32 		// color depth
#endif	// defined(_WIN32_WCE)

extern int param[16][3];
extern unsigned short trans[];
extern int Pal11[ 4];
extern int Pal12[ 8];
extern int Pal13[ 8];
extern int Pal14[ 4];
extern int Pal15[ 8];
extern int Pal53[32];



#ifndef XID
#define XID  unsigned int
#endif
#ifndef Bool
#define Bool int
#endif

/** Some typedef's **/
typedef union {
    XID ct_xid;
    byte ct_byte[4]; 
} ColTyp; /* sizeof ColTyp should be 4 */

extern void (*SCR[2+2][4])() ;	/* Screen Mode Handlers */
EXTRN  int IntLac; 
EXTRN  int IntLac_bak;

extern int scale;
EXTRN  int new_scale;

/* functions and variables in Unix.c used by Refresh.c */
EXTRN  byte *XBuf;
EXTRN  int bitpix;
EXTRN  ColTyp BPal[16],BPal53[32],
  BPal11[4],BPal12[8],BPal13[8],BPal14[4],BPal15[8], BPal62[32],BPal61[16];

/* functions and variables in Refresh.c used by Unix.c */
EXTRN  Bool lsbfirst;
EXTRN  int  Width,Height;

void choosefuncs(int lsbfirst, int bitpix);
void setwidth(int wide);


void RefreshScr10(void);
void RefreshScr11(void);
void RefreshScr12(void);
void RefreshScr13(void);
void RefreshScr13a(void);
void RefreshScr13b(void);
void RefreshScr13c(void);
void RefreshScr13d(void);
void RefreshScr13e(void);
void RefreshScr51(void);
void RefreshScr52(void);
void RefreshScr53(void);
void RefreshScr54(void);

void RefreshScr61(void);        /* add  2002/2 */
void RefreshScr62(void);		/* add  2002/2 */
void RefreshScr63(void);		/* add  2002/5/2 */

void do_palet(int dest,int src);


#endif
