// ******************************************************
// 			  buffer.h  
//            by Windy
// *******************************************************
#ifdef WIN32
#include <windows.h>
#else
#define DWORD int
#endif

void init_keybuffer(void);
void write_keybuffer(DWORD wParam , DWORD lParam , char chr);
int read_keybuffer(DWORD *wParam , DWORD *lParam , char *chr);
