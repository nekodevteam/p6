/** iP6: PC-6000/6600 series emualtor ************************/
/**                                                         **/
/**                           d88.h                         **/
/**                                                         **/
/*                     Inteligent  disk unit                 */
/*************************************************************/
/* This file is written by windy */



int is1DD(void);
int isSYS(void);
int read_d88head(void);
byte* read_d88sector(int kai,int drive ,int track ,int sector);
int write_d88sector(int kai, int drive ,int track ,int sector, byte *buff);

void setDiskProtect(int Value);
int  getDiskProtect(void);
