LPCTSTR mgettext(LPCTSTR key);

#if !defined(_WIN32_WCE)
#define _N(a) mgettext(a) 
#else	// !defined(_WIN32_WCE)
#define _N(a) a
#endif	// !defined(_WIN32_WCE)
