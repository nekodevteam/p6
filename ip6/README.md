# iP6
PC-6000/PC-6600 エミュレータ iP6 Plus

## ビルド
- Visual C++ 6.0
  - ip6.dsw を開いてビルドしてください
- Visual Studio 2017
  - ip6.sln を開いてビルドしてください
- Ubuntu
  - build-essential に加えて libxt0dev libxaw7-dev が必要です
  - Makefile で OS = LINUX を選択してください

## unix 系 の FAQ
- よく使う起動コマンド ライン
  | オプション | 内容         |
  |------------| ------------ |
  | -60        | PC-6001      |
  | -62        | PC-6001mk2   |
  | -64        | PC-6001mk2SR |
  | -66        | PC-6601      |
  | -68        | PC-6601SR    |
  | -wait      | ウェイト有効 |
  | -sound     | サウンド有効 |
- Error: Can't open display:
  - ssh から起動する場合は、環境変数 DISPLAY を設定してください
    ~~~
    $ export DISPLAY=:0.0
    ~~~
- サウンド関係
  - oss が必要です。osspd あたりを導入してください
    ~~~
    $ sudo apt install osspd
    ~~~
  - Raspberry Pi では、標準で DSP が有効になってないようなので modprobe してください
    ~~~
    $ sudo modprobe snd_pcm_oss
    ~~~

## 主な修正点
- 動作タイミングを修正
- 8/16bppに対応
- オート フレームを追加
- サウンドがモノラルにならない不具合を修正
- サウンド バッファを改善

## オリジナル コード
- isio さん作成 [iP6 P6 emulator for Unix/X](http://www.retropc.net/isio/ip6/) ip6-0.6.4.tar.gz
- Windy さん作成 [PC-6000/PC-6600 エミュレータ iP6 Plus](https://www.kisweb.ne.jp/personal/windy/pc6001/ip6plus/ip6.html) P6-4.7.zip
