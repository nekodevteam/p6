# P6
PC-6001 関係のレポジトリです

## ip6
- iP6 レポジトリです
- 下記のソースコードから派生しています
  - isio さん作成 [iP6 P6 emulator for Unix/X](http://www.retropc.net/isio/ip6/) ip6-0.6.4.tar.gz
  - Windy さん作成 [PC-6000/PC-6600 エミュレータ iP6 Plus](https://www.kisweb.ne.jp/personal/windy/pc6001/ip6plus/ip6.html) P6-4.7.zip

----
Twitter: [@yuinejp](https://twitter.com/yuinejp)
